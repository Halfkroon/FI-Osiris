from ID_items import *
from ID_quests import *
from ID_factions import *
from ID_troops import *
##############################################################
# These constants are used in various files.
# If you need to define a value that will be used in those files,
# just define it here rather than copying it across each file, so
# that it will be easy to change it if you need to.
##############################################################

########################################################
##  ITEM SLOTS             #############################
########################################################

slot_item_is_checked               = 0
slot_item_food_bonus               = 1
slot_item_book_reading_progress    = 2
slot_item_book_read                = 3
slot_item_intelligence_requirement = 4

slot_item_amount_available         = 7

slot_item_urban_demand             = 11 #consumer demand for a good in town, measured in abstract units. The more essential the item (ie, like grain) the higher the price
slot_item_rural_demand             = 12 #consumer demand in villages, measured in abstract units
slot_item_desert_demand            = 13 #consumer demand in villages, measured in abstract units

slot_item_production_slot          = 14 
slot_item_production_string        = 15 

slot_item_is_shotgun			   = 17

slot_item_tied_to_good_price       = 20 #ie, weapons and metal armor to tools, padded to cloth, leather to leatherwork, etc

slot_item_num_positions            = 22
slot_item_positions_begin          = 23 #reserve around 5 slots after this

slot_item_banner_buff_script	  = 40 #Borridian
slot_item_banner_buff_desc		  = 41 #Borridian
slot_item_banner_buff_1_id		  = 42 #Borridian
slot_item_banner_buff_1_val		  = 43 #Borridian
slot_item_banner_buff_2_id		  = 44 #Borridian
slot_item_banner_buff_2_val		  = 45 #Borridian
slot_item_banner_buff_3_id		  = 46 #Borridian
slot_item_banner_buff_3_val		  = 47 #Borridian

slot_item_multiplayer_faction_price_multipliers_begin = 800 #reserve around 10 slots after this

slot_item_primary_raw_material    		= 50
slot_item_is_raw_material_only_for      = 51
slot_item_input_number                  = 52 #ie, how many items of inputs consumed per run
slot_item_base_price                    = 53 #taken from module_items
#slot_item_production_site			    = 54 #a string replaced with function - Armagan
slot_item_output_per_run                = 55 #number of items produced per run
slot_item_overhead_per_run              = 56 #labor and overhead per run
slot_item_secondary_raw_material        = 57 #in this case, the amount used is only one
slot_item_enterprise_building_cost      = 58 #enterprise building cost

slot_item_extra_text					= 100 #Borridian

slot_item_multiplayer_class_restricted      = 1998 #Borridian - Set to only permit this item to be picked up from the ground by classes that can purchase the item normally (Mainly used for magic ammo)
slot_item_multiplayer_item_class   = 1999 #temporary, can be moved to higher values
slot_item_multiplayer_availability_linked_list_begin = 2000 #temporary, can be moved to higher values


########################################################
##  AGENT SLOTS            #############################
########################################################

slot_agent_target_entry_point     = 0
slot_agent_target_x_pos           = 1
slot_agent_target_y_pos           = 2
slot_agent_is_alive_before_retreat= 3
slot_agent_is_in_scripted_mode    = 4
slot_agent_is_not_reinforcement   = 5
slot_agent_tournament_point       = 6
slot_agent_arena_team_set         = 7
slot_agent_spawn_entry_point      = 8
slot_agent_target_prop_instance   = 9
slot_agent_map_overlay_id         = 10
slot_agent_target_entry_point     = 11
slot_agent_initial_ally_power     = 12
slot_agent_initial_enemy_power    = 13
slot_agent_enemy_threat           = 14
slot_agent_is_running_away        = 15
slot_agent_courage_score          = 16
slot_agent_is_respawn_as_bot      = 17
slot_agent_cur_animation          = 18
slot_agent_next_action_time       = 19
slot_agent_state                  = 20
slot_agent_in_duel_with           = 21
slot_agent_duel_start_time        = 22

slot_agent_ability_fired		  = 23

slot_agent_walker_occupation      = 25

slot_agent_last_damage_dealer_weapon = 27
slot_agent_marked_for_remove	  	 = 28

################################################
slot_agent_hp_bar_foreground	  		= 30   #
slot_agent_hp_bar_background	  		= 31   #
slot_agent_max_hp				  		= 32   #
slot_agent_cur_false_hp			  		= 33   # Borridian - HP bar stuff
slot_agent_hp_overflow_mult		  		= 34   #
slot_agent_leadership_overlay			= 35   #
slot_agent_is_receiving_relative_buff	= 36   #
slot_agent_is_giving_relative_buff		= 37   #
bot_hp_bar_max_distance					= 40   #
max_hp_that_can_be_stored		  		= 2048 #
################################################

slot_agent_playing_sound          = 40
slot_agent_particle_effect_turns  = 41
slot_agent_attached_prop		  = 42

slot_agent_poison_damage		  = 43
slot_agent_poisoner				  = 44
slot_agent_poison_length		  = 45

slot_agent_is_resting			  = 46
slot_agent_standard_hp			  = 47
slot_agent_cur_max_hp			  = 48
slot_agent_cur_kills			  = 49

slot_agent_controller			  = 50
slot_agent_bodyguard_1			  = 51
slot_agent_bodyguard_2			  = 52
slot_agent_bodyguard_3			  = 53
slot_agent_bodyguard_4			  = 54
slot_agent_bodyguard_5			  = 55
slot_agent_summon_has_dr		  = 60
slot_agent_is_wielding_lightsaber = 61

slot_agent_is_dev				  = 65
slot_agent_dev_invulnerability	  = 66
slot_agent_dev_is_neutral  		  = 67
slot_agent_dev_is_invisible		  = 68
slot_agent_kill_time              = 69

slot_agent_stagger_counter        = 70
slot_agent_has_stagger_resistance = 71

slot_agent_waypoint_mode_set	  = 99
slot_agent_cur_waypoint_target	  = 100
slot_agent_waypoint_options_begin = 101

slot_agent_last_cheer_time		  	= 200
slot_agent_last_hit				  	= 202
slot_agent_last_hit_weapon			= 203

slot_agent_cur_ranged_point			= 500

slot_agent_combat_voice_cooldown  = 260	#Stores earliest time period agent can make another 'yell' sound
agent_combat_voice_cooldown_length	= 6 #Cooldown period (in seconds) applied to the cooldown slot after each combat 'yell' (slot_agent_combat_voice_cooldown = cur_mission_time + agent_combat_voice_cooldown_length)
horse_neigh_cooldown_length			= 3 #Cooldown period (in seconds) applied to the cooldown slot for horses
agent_callout_chance			  = 20 #% Chance of emitting sound per second (if eligible)
mount_idle_sound_chance			  = 10 #% Chance of emitting sound per second (if eligible)

max_sound_distance				  = 100 #Max distance of hearing generic agent sounds (in metres)

agent_bot_callout_cooldown				= 10	#Seconds
agent_standard_minimum_callout_cooldown	= 3		#Seconds
agent_action_group_window_fight	  		= 10	#Seconds
agent_action_group_window_charge  		= 10	#Seconds

slot_agent_leadership_effects_begin						= 300
slot_agent_leadership_effect_prop_use_speed				= slot_agent_leadership_effects_begin	#Value = % modifier from base; (Net % = 100 + value)
slot_agent_leadership_effect_move_speed					= 301	#Value = % modifier from base; (Net % = 100 + value)
slot_agent_leadership_effect_melee_damage				= 302	#Value = % modifier from base; (Net % = 100 + value)
slot_agent_leadership_effect_ranged_damage				= 303	#Value = % modifier from base; (Net % = 100 + value)
slot_agent_leadership_effect_accuracy					= 304	#Value = % modifier from base; (Net % = 100 + value)
slot_agent_leadership_effect_reload_speed				= 305	#Value = % modifier from base; (Net % = 100 + value)
slot_agent_leadership_effect_melee_damage_resistance	= 306	#Value = % of extra damage to subtract from hit (calculated after base defender damage resistance)
slot_agent_leadership_effect_ranged_damage_resistance	= 307	#Value = % of extra damage to subtract from hit (calculated after base defender damage resistance)
slot_agent_leadership_effect_shield_damage_resistance	= 308	#Value = % of extra damage to shield to subtract from hit
slot_agent_leadership_effect_mount_speed				= 309	#Value = % modifier from base; (Net % = 100 + value)
slot_agent_leadership_effect_mount_damage_resistance	= 310	#Value = % of extra damage to subtract from hit (calculated after base defender damage resistance)
slot_agent_leadership_effect_health_regen				= 311	#Value = HP restored per cycle (1 second)
slot_agent_leadership_effect_max_health					= 312	#Value = % extra maximum health from base (after any base defender health modifier); (Net % = 100 + value)
slot_agent_leadership_effect_max_mount_health			= 313	#Value = % extra maximum health from base; (Net % = 100 + value)
slot_agent_leadership_effect_enemy_shield_damage		= 314	#Value = % extra damage inflicted to shields (Net % = 100 + value)
slot_agent_leadership_effect_damage_to_boss				= 315	#Value = % extra damage inflicted to bosses (Net % = 100 + value)
slot_agent_leadership_effect_damage_to_trolls			= 316	#Value = % extra damage inflicted to trolls (Net % = 100 + value)
slot_agent_leadership_effect_damage_to_demons			= 317	#Value = % extra damage inflicted to demons (Net % = 100 + value)
slot_agent_leadership_effect_damage_over_time			= 318	#Value = % of enemy's total health inflicted as damage per cycle (1 second)
slot_agent_leadership_effect_gold_on_hit				= 319	#Value = % extra gold received on hits (Net % = 100 + value)
slot_agent_leadership_effect_gold_on_kill				= 320	#Value = % extra gold received on kills (Net % = 100 + value)
slot_agent_leadership_effect_health_on_kill				= 321	#Value = hp restored per kill
slot_agent_leadership_effect_damage_negation_chance		= 322	#Value = % chance to avoid all damage from an attack
slot_agent_leadership_effect_instant_kill_chance		= 323	#Value = % chance to instantly kill an enemy on a hit
slot_agent_leadership_effect_ward						= 324	#Value = 1 if active, 0 if not
slot_agent_leadership_effect_mortal_coil				= 325	#Value = 1 if active, 0 if not
slot_agent_engineer_turret_1                            = 326   #Value = turret prop instance id
slot_agent_engineer_turret_2                            = 327   #Value = turret prop instance id
slot_agent_leadership_statuses_begin					= slot_agent_engineer_turret_2 + 1
slot_agent_affliction_effect_slow						= 328	#Value = duration
slot_agent_affliction_effect_damage_over_time			= 329	#Value = duration
slot_agent_affliction_effect_fear						= 330	#Value = duration
slot_agent_affliction_effect_reanimate					= 331	#Value = duration
slot_agent_leadership_status_windwalker					= 332	#Value = duration
slot_agent_leadership_status_windrider					= 333	#Value = duration
slot_agent_leadership_status_cheat_death				= 334	#Value = duration
slot_agent_leadership_status_barrier					= 335	#Value = duration
slot_agent_leadership_status_fear						= 336	#Value = Duration (secs) bots will rout
slot_agent_leadership_status_no_stagger					= 337	#Value = Duration (secs) that the player cannot be staggered
slot_agent_leadership_status_bodyguards					= 338	#Value = Number of living bodyguards
slot_agent_leadership_status_clones						= 339	#Value = Number of living clones
slot_agent_leadership_status_decoy						= 340	#Value = Duration (secs) the decoy will live for
slot_agent_leadership_status_blink						= 341	#Value = 1 if blink location set
slot_agent_leadership_status_reflect					= 342	#Value = duration
slot_agent_leadership_status_combat_stance				= 343	#Value = duration
slot_agent_leadership_status_iron_horse					= 344	#Value = duration
slot_agent_leadership_status_battle_frenzy				= 345   #Value = duration
slot_agent_leadership_status_reinforce					= 346	#Value = duration
slot_agent_leadership_status_slow						= 347	#Value = duration
slot_agent_leadership_status_raise_thrall				= 348	#Value = duration
slot_agent_leadership_status_monster_slayer				= 349	#Value = duration
slot_agent_leadership_status_inspire					= 350	#Value = duration
slot_agent_leadership_status_explosive_ammo				= 351	#Value = duration
slot_agent_leadership_status_arcane_turret				= 352	#Value = duration
slot_agent_leadership_status_invisibility				= 353	#Value = duration
slot_agent_leadership_status_siphon						= 354	#Value = ID of the siphon
slot_agent_leadership_status_siphon_reverse				= 355	#Value = ID of the siphoner
slot_agent_leadership_status_soulbound					= 356	#Value = duration
slot_agent_leadership_status_soulbound_confusion		= 357	#Value = duration
slot_agent_leadership_status_pulse_of_undeath			= 358	#Value = duration
slot_agent_leadership_statuses_end						= slot_agent_leadership_status_pulse_of_undeath + 1
slot_agent_leadership_effects_end						= slot_agent_leadership_statuses_end

leadership_effect_secondary_duration_slot_offset		= 100	#For effects that don't normally have an associated duration, if an active temporary effect provides one, store that duration in (100 + effect_slot) slot

slot_agent_hero_ability_cooldown  						= 360 #Borridian
slot_agent_override_cooldown_set						= 361 #Borridian

slot_agent_blink_marker									= 1000 #Borridian
slot_agent_owned_prop			  						= 1001 #Borridian
slot_agent_owned_prop_2                                 = 1002 #Borridian

slot_agent_conduit_summon_interval						= 1010 #Borridian

#Other effects:
#Group Heal
#Slow Enemies
#Convert Enemies
#Mirror Image - Clone nearby allies

########################################################
##  FACTION SLOTS          #############################
########################################################
slot_faction_ai_state                   = 4
slot_faction_ai_object                  = 5
slot_faction_ai_rationale               = 6 #Currently unused, can be linked to strings generated from decision checklists


slot_faction_marshall                   = 8
slot_faction_ai_offensive_max_followers = 9

slot_faction_culture                    = 10
slot_faction_leader                     = 11

slot_faction_temp_slot                  = 12

##slot_faction_vassal_of            = 11
slot_faction_banner                     = 15

slot_faction_number_of_parties    = 20
slot_faction_state                = 21

slot_faction_adjective            = 22


slot_faction_player_alarm         		= 30
slot_faction_last_mercenary_offer_time 	= 31
slot_faction_recognized_player    		= 32

#overriding troop info for factions in quick start mode.
slot_faction_quick_battle_tier_1_infantry      = 41
slot_faction_quick_battle_tier_2_infantry      = 42
slot_faction_quick_battle_tier_1_archer        = 43
slot_faction_quick_battle_tier_2_archer        = 44
slot_faction_quick_battle_tier_1_cavalry       = 45
slot_faction_quick_battle_tier_2_cavalry       = 46

slot_faction_tier_1_troop         = 41
slot_faction_tier_2_troop         = 42
slot_faction_tier_3_troop         = 43
slot_faction_tier_4_troop         = 44
slot_faction_tier_5_troop         = 45

slot_faction_deserter_troop       = 48
slot_faction_guard_troop          = 49
slot_faction_messenger_troop      = 50
slot_faction_prison_guard_troop   = 51
slot_faction_castle_guard_troop   = 52

slot_faction_town_walker_male_troop      = 53
slot_faction_town_walker_female_troop    = 54
slot_faction_village_walker_male_troop   = 55
slot_faction_village_walker_female_troop = 56
slot_faction_town_spy_male_troop         = 57
slot_faction_town_spy_female_troop       = 58

slot_faction_has_rebellion_chance = 60

slot_faction_instability          = 61 #last time measured


#UNIMPLEMENTED FEATURE ISSUES
slot_faction_war_damage_inflicted_when_marshal_appointed = 62 #Probably deprecate
slot_faction_war_damage_suffered_when_marshal_appointed  = 63 #Probably deprecate

slot_faction_political_issue 							 = 64 #Center or marshal appointment
slot_faction_political_issue_time 						 = 65 #Now is used


#Rebellion changes
#slot_faction_rebellion_target                     = 65
#slot_faction_inactive_leader_location         = 66
#slot_faction_support_base                     = 67
#Rebellion changes



#slot_faction_deserter_party_template       = 62

slot_faction_reinforcements_a        = 77
slot_faction_reinforcements_b        = 78
slot_faction_reinforcements_c        = 79

slot_faction_num_armies              = 80
slot_faction_num_castles             = 81
slot_faction_num_towns               = 82

slot_faction_last_attacked_center    = 85
slot_faction_last_attacked_hours     = 86
slot_faction_last_safe_hours         = 87

slot_faction_num_routed_agents       = 90

#useful for competitive consumption
slot_faction_biggest_feast_score      = 91
slot_faction_biggest_feast_time       = 92
slot_faction_biggest_feast_host       = 93


#Faction AI states
slot_faction_last_feast_concluded       = 94 #Set when a feast starts -- this needs to be deprecated
slot_faction_last_feast_start_time      = 94 #this is a bit confusing


slot_faction_ai_last_offensive_time 	= 95 #Set when an offensive concludes
slot_faction_last_offensive_concluded 	= 95 #Set when an offensive concludes

slot_faction_ai_last_rest_time      	= 96 #the last time that the faction has had default or feast AI -- this determines lords' dissatisfaction with the campaign. Set during faction_ai script
slot_faction_ai_current_state_started   = 97 #

slot_faction_ai_last_decisive_event     = 98 #capture a fortress or declaration of war

slot_faction_morale_of_player_troops    = 99

#diplomacy
slot_faction_truce_days_with_factions_begin 			= 120
slot_faction_provocation_days_with_factions_begin 		= 130
slot_faction_war_damage_inflicted_on_factions_begin 	= 140
slot_faction_sum_advice_about_factions_begin 			= 150

#revolts -- notes for self
#type 1 -- minor revolt, aimed at negotiating change without changing the ruler
#type 2 -- alternate ruler revolt (ie, pretender, chinese dynastic revolt -- keep the same polity but switch the ruler)
	#subtype -- pretender (keeps the same dynasty)
	#"mandate of heaven" -- same basic rules, but a different dynasty
	#alternate/religious
	#alternate/political
#type 3 -- separatist revolt
	# reGonalist/dynastic (based around an alternate ruling house
	# regionalist/republican
	# messianic (ie, Canudos)
	
slot_faction_subfac_names_begin = 150

##Invasion FuIn invader slots##
slot_faction_invader_1 = 200
slot_faction_invader_2 = 201
slot_faction_invader_3 = 202
slot_faction_invader_4 = 203
slot_faction_invader_5 = 204
slot_faction_invader_6 = 205
slot_faction_invader_7 = 206
slot_faction_invader_8 = 207
slot_faction_invader_9 = 208
slot_faction_invader_10 = 209
slot_faction_invader_11 = 210
slot_faction_invader_end = 211

slot_faction_boss_1 = 221
slot_faction_boss_2 = 222
slot_faction_boss_3 = 223
slot_faction_boss_4 = 224
slot_faction_boss_5 = 225
slot_faction_boss_6 = 226
slot_faction_boss_7 = 227
slot_faction_boss_8 = 228
slot_faction_boss_9 = 229
slot_faction_boss_10 = 230
slot_faction_boss_11 = 231
slot_faction_boss_end = 232

slot_player_profile_gender = 255

slot_faction_type			= 260 #Borridian - 0 = defender/1 = invader
slot_faction_disabled		= 261 #Borridian - 1 = disabled for polls

slot_faction_desc_begin			= 265
slot_faction_desc_end			= 266
slot_faction_strengths_begin	= 267
slot_faction_strengths_end		= 268
slot_faction_weaknesses_begin	= 269
slot_faction_weaknesses_end		= 270
slot_faction_difficulty_str		= 271
slot_faction_shield_button		= 272
slot_faction_image				= 273

slot_invasion_chest_open = 10
slot_invasion_chest_timer = 11 
##FuIn end		

########################################################
##  PARTY SLOTS            #############################
########################################################
slot_party_type                = 0  #spt_caravan, spt_town, spt_castle

slot_party_retreat_flag        = 2
slot_party_ignore_player_until = 3
slot_party_ai_state            = 4
slot_party_ai_object           = 5
slot_party_ai_rationale        = 6 #Currently unused, but can be used to save a string explaining the lord's thinking

#slot_town_belongs_to_kingdom   = 6
slot_town_lord                 = 7
slot_party_ai_substate         = 8
slot_town_claimed_by_player    = 9

slot_cattle_driven_by_player = slot_town_lord #hack

slot_town_center        = 10
slot_town_castle        = 11
slot_town_prison        = 12
slot_town_tavern        = 13
slot_town_store         = 14
slot_town_arena         = 16
slot_town_alley         = 17
slot_town_walls         = 18
slot_center_culture     = 19

slot_town_tavernkeeper  = 20
slot_town_weaponsmith   = 21
slot_town_armorer       = 22
slot_town_merchant      = 23
slot_town_horse_merchant= 24
slot_town_elder         = 25
slot_center_player_relation = 26

slot_center_siege_with_belfry = 27
slot_center_last_taken_by_troop = 28

# party will follow this party if set:
slot_party_commander_party = 30 #default -1   #Deprecate
slot_party_following_player    = 31
slot_party_follow_player_until_time = 32
slot_party_dont_follow_player_until_time = 33

slot_village_raided_by        = 34
slot_village_state            = 35 #svs_normal, svs_being_raided, svs_looted, svs_recovering, svs_deserted
slot_village_raid_progress    = 36
slot_village_recover_progress = 37
slot_village_smoke_added      = 38

slot_village_infested_by_bandits   = 39

slot_center_last_visited_by_lord   = 41

slot_center_last_player_alarm_hour = 42

slot_village_player_can_not_steal_cattle = 46

slot_center_accumulated_rents      = 47 #collected automatically by NPC lords
slot_center_accumulated_tariffs    = 48 #collected automatically by NPC lords
slot_town_wealth        = 49 #total amount of accumulated wealth in the center, pays for the garrison
slot_town_prosperity    = 50 #affects the amount of wealth generated
slot_town_player_odds   = 51


slot_party_last_toll_paid_hours = 52
slot_party_food_store           = 53 #used for sieges
slot_center_is_besieged_by      = 54 #used for sieges
slot_center_last_spotted_enemy  = 55

slot_party_cached_strength        = 56
slot_party_nearby_friend_strength = 57
slot_party_nearby_enemy_strength  = 58
slot_party_follower_strength      = 59

slot_town_reinforcement_party_template = 60
slot_center_original_faction           = 61
slot_center_ex_faction                 = 62

slot_party_follow_me                   = 63
slot_center_siege_begin_hours          = 64 #used for sieges
slot_center_siege_hardness             = 65

slot_center_sortie_strength            = 66
slot_center_sortie_enemy_strength      = 67

slot_party_last_in_combat              = 68 #used for AI
slot_party_last_in_home_center         = 69 #used for AI
slot_party_leader_last_courted         = 70 #used for AI
slot_party_last_in_any_center          = 71 #used for AI



slot_castle_exterior    = slot_town_center

#slot_town_rebellion_contact   = 76
#trs_not_yet_approached  = 0
#trs_approached_before   = 1
#trs_approached_recently = 2

argument_none         = 0
argument_claim        = 1 #deprecate for legal
argument_legal        = 1

argument_ruler        = 2 #deprecate for commons
argument_commons      = 2

argument_benefit      = 3 #deprecate for reward
argument_reward       = 3 

argument_victory      = 4
argument_lords        = 5
argument_rivalries    = 6 #new - needs to be added

slot_town_village_product = 76

slot_town_rebellion_readiness = 77
#(readiness can be a negative number if the rebellion has been defeated)

slot_town_arena_melee_mission_tpl = 78
slot_town_arena_torny_mission_tpl = 79
slot_town_arena_melee_1_num_teams = 80
slot_town_arena_melee_1_team_size = 81
slot_town_arena_melee_2_num_teams = 82
slot_town_arena_melee_2_team_size = 83
slot_town_arena_melee_3_num_teams = 84
slot_town_arena_melee_3_team_size = 85
slot_town_arena_melee_cur_tier    = 86
##slot_town_arena_template	  = 87

slot_center_npc_volunteer_troop_type   = 90
slot_center_npc_volunteer_troop_amount = 91
slot_center_mercenary_troop_type  = 90
slot_center_mercenary_troop_amount= 91
slot_center_volunteer_troop_type  = 92
slot_center_volunteer_troop_amount= 93

#slot_center_companion_candidate   = 94
slot_center_ransom_broker         = 95
slot_center_tavern_traveler       = 96
slot_center_traveler_info_faction = 97
slot_center_tavern_bookseller     = 98
slot_center_tavern_minstrel       = 99

num_party_loot_slots    = 5
slot_party_next_looted_item_slot  = 109
slot_party_looted_item_1          = 110
slot_party_looted_item_2          = 111
slot_party_looted_item_3          = 112
slot_party_looted_item_4          = 113
slot_party_looted_item_5          = 114
slot_party_looted_item_1_modifier = 115
slot_party_looted_item_2_modifier = 116
slot_party_looted_item_3_modifier = 117
slot_party_looted_item_4_modifier = 118
slot_party_looted_item_5_modifier = 119

slot_village_bound_center         = 120
slot_village_market_town          = 121
slot_village_farmer_party         = 122
slot_party_home_center            = 123 #Only use with caravans and villagers

slot_center_current_improvement   = 124
slot_center_improvement_end_hour  = 125

slot_party_last_traded_center     = 126 



slot_center_has_manor            = 130 #village
slot_center_has_fish_pond        = 131 #village
slot_center_has_watch_tower      = 132 #village
slot_center_has_school           = 133 #village
slot_center_has_messenger_post   = 134 #town, castle, village
slot_center_has_prisoner_tower   = 135 #town, castle

village_improvements_begin = slot_center_has_manor
village_improvements_end          = 135

walled_center_improvements_begin = slot_center_has_messenger_post
walled_center_improvements_end               = 136

slot_center_player_enterprise     				  = 137 #noted with the item produced
slot_center_player_enterprise_production_order    = 138
slot_center_player_enterprise_consumption_order   = 139 #not used
slot_center_player_enterprise_days_until_complete = 139 #Used instead

slot_center_player_enterprise_balance             = 140 #not used
slot_center_player_enterprise_input_price         = 141 #not used
slot_center_player_enterprise_output_price        = 142 #not used



slot_center_has_bandits                        = 155
slot_town_has_tournament                       = 156
slot_town_tournament_max_teams                 = 157
slot_town_tournament_max_team_size             = 158

slot_center_faction_when_oath_renounced        = 159

slot_center_walker_0_troop                   = 160
slot_center_walker_1_troop                   = 161
slot_center_walker_2_troop                   = 162
slot_center_walker_3_troop                   = 163
slot_center_walker_4_troop                   = 164
slot_center_walker_5_troop                   = 165
slot_center_walker_6_troop                   = 166
slot_center_walker_7_troop                   = 167
slot_center_walker_8_troop                   = 168
slot_center_walker_9_troop                   = 169

slot_center_walker_0_dna                     = 170
slot_center_walker_1_dna                     = 171
slot_center_walker_2_dna                     = 172
slot_center_walker_3_dna                     = 173
slot_center_walker_4_dna                     = 174
slot_center_walker_5_dna                     = 175
slot_center_walker_6_dna                     = 176
slot_center_walker_7_dna                     = 177
slot_center_walker_8_dna                     = 178
slot_center_walker_9_dna                     = 179

slot_center_walker_0_type                    = 180
slot_center_walker_1_type                    = 181
slot_center_walker_2_type                    = 182
slot_center_walker_3_type                    = 183
slot_center_walker_4_type                    = 184
slot_center_walker_5_type                    = 185
slot_center_walker_6_type                    = 186
slot_center_walker_7_type                    = 187
slot_center_walker_8_type                    = 188
slot_center_walker_9_type                    = 189

slot_town_trade_route_1           = 190
slot_town_trade_route_2           = 191
slot_town_trade_route_3           = 192
slot_town_trade_route_4           = 193
slot_town_trade_route_5           = 194
slot_town_trade_route_6           = 195
slot_town_trade_route_7           = 196
slot_town_trade_route_8           = 197
slot_town_trade_route_9           = 198
slot_town_trade_route_10          = 199
slot_town_trade_route_11          = 200
slot_town_trade_route_12          = 201
slot_town_trade_route_13          = 202
slot_town_trade_route_14          = 203
slot_town_trade_route_15          = 204
slot_town_trade_routes_begin = slot_town_trade_route_1
slot_town_trade_routes_end = slot_town_trade_route_15 + 1


num_trade_goods = itm_siege_supply - itm_spice
slot_town_trade_good_productions_begin       = 500 #a harmless number, until it can be deprecated

#These affect production but in some cases also demand, so it is perhaps easier to itemize them than to have separate 

slot_village_number_of_cattle   = 205
slot_center_head_cattle         = 205 #dried meat, cheese, hides, butter
slot_center_head_sheep			= 206 #sausages, wool
slot_center_head_horses		 	= 207 #horses can be a trade item used in tracking but which are never offered for sale

slot_center_acres_pasture       = 208 #pasture area for grazing of cattles and sheeps, if this value is high then number of cattles and sheeps increase faster
slot_production_sources_begin = 209
slot_center_acres_grain			= 209 #grain
slot_center_acres_olives        = 210 #olives
slot_center_acres_vineyard		= 211 #fruit
slot_center_acres_flax          = 212 #flax
slot_center_acres_dates			= 213 #dates

slot_center_fishing_fleet		= 214 #smoked fish
slot_center_salt_pans		    = 215 #salt

slot_center_apiaries       		= 216 #honey
slot_center_silk_farms			= 217 #silk
slot_center_kirmiz_farms		= 218 #dyes

slot_center_iron_deposits       = 219 #iron
slot_center_fur_traps			= 220 #furs

slot_center_mills				= 221 #bread
slot_center_breweries			= 222 #ale
slot_center_wine_presses		= 223 #wine
slot_center_olive_presses		= 224 #oil

slot_center_linen_looms			= 225 #linen
slot_center_silk_looms          = 226 #velvet
slot_center_wool_looms          = 227 #wool cloth

slot_center_pottery_kilns		= 228 #pottery
slot_center_smithies			= 229 #tools
slot_center_tanneries			= 230 #leatherwork
slot_center_shipyards			= 231 #naval stores - uses timber, pitch, and linen

slot_center_household_gardens   = 232 #cabbages
slot_production_sources_end = 233

#all spice comes overland to Tulga
#all dyes come by sea to Jelkala

#chicken and pork are perishable and non-tradeable, and based on grain production
#timber and pitch if we ever have a shipbuilding industry
#limestone and timber for mortar, if we allow building

slot_town_last_nearby_fire_time                         = 240

#slot_town_trade_good_prices_begin            = slot_town_trade_good_productions_begin + num_trade_goods + 1
slot_party_following_orders_of_troop        = 244
slot_party_orders_type				        = 245
slot_party_orders_object				    = 246
slot_party_orders_time				    	= 247

slot_party_temp_slot_1			            = 248 #right now used only within a single script, merchant_road_info_to_s42, to denote closed roads. Now also used in comparative scripts
slot_party_under_player_suggestion			= 249 #move this up a bit
slot_town_trade_good_prices_begin 			= 250

slot_center_last_reconnoitered_by_faction_time 				= 350
#slot_center_last_reconnoitered_by_faction_cached_strength 	= 360
#slot_center_last_reconnoitered_by_faction_friend_strength 	= 370




#slot_party_type values
##spt_caravan            = 1
spt_castle             = 2
spt_town               = 3
spt_village            = 4
##spt_forager            = 5
##spt_war_party          = 6
##spt_patrol             = 7
##spt_messenger          = 8
##spt_raider             = 9
##spt_scout              = 10
spt_kingdom_caravan    = 11
##spt_prisoner_train     = 12
spt_kingdom_hero_party = 13
##spt_merchant_caravan   = 14
spt_village_farmer     = 15
spt_ship               = 16
spt_cattle_herd        = 17
spt_bandit_lair       = 18
#spt_deserter           = 20

kingdom_party_types_begin = spt_kingdom_caravan
kingdom_party_types_end = spt_kingdom_hero_party + 1

#slot_faction_state values
sfs_active                     = 0
sfs_defeated                   = 1
sfs_inactive                   = 2
sfs_inactive_rebellion         = 3
sfs_beginning_rebellion        = 4


#slot_faction_ai_state values
sfai_default                   		 = 0 #also defending
sfai_gathering_army            		 = 1
sfai_attacking_center          		 = 2
sfai_raiding_village           		 = 3
sfai_attacking_enemy_army      		 = 4
sfai_attacking_enemies_around_center = 5
sfai_feast             		 		 = 6 #can be feast, wedding, or major tournament
#Social events are a generic aristocratic gathering. Tournaments take place if they are in a town, and hunts take place if they are at a castle.
#Weddings will take place at social events between betrothed couples if they have been engaged for at least a month, if the lady's guardian is the town lord, and if both bride and groom are present


#Rebellion system changes begin
sfai_nascent_rebellion          = 7
#Rebellion system changes end

#slot_party_ai_state values
spai_undefined                  = -1
spai_besieging_center           = 1
spai_patrolling_around_center   = 4
spai_raiding_around_center      = 5
##spai_raiding_village            = 6
spai_holding_center             = 7
##spai_helping_town_against_siege = 9
spai_engaging_army              = 10
spai_accompanying_army          = 11
spai_screening_army             = 12
spai_trading_with_town          = 13
spai_retreating_to_center       = 14
##spai_trading_within_kingdom     = 15
spai_visiting_village           = 16 #same thing, I think. Recruiting differs from holding because NPC parties don't actually enter villages

#slot_village_state values
svs_normal                      = 0
svs_being_raided                = 1
svs_looted                      = 2
svs_recovering                  = 3
svs_deserted                    = 4
svs_under_siege                 = 5

#$g_player_icon_state values
pis_normal                      = 0
pis_camping                     = 1
pis_ship                        = 2


########################################################
##  SCENE SLOTS            #############################
########################################################
slot_scene_visited              = 0
slot_scene_belfry_props_begin   = 10

slot_scene_skybox				= 15
slot_scene_disabled				= 20

slot_scene_desc_begin			= 25
slot_scene_desc_end				= 26
slot_scene_recommended_players	= 27
slot_scene_difficulty_str		= 28
slot_scene_image				= 29

slot_scene_spawn_point_names_begin  = 50

########################################################
##  TROOP SLOTS            #############################
########################################################
#slot_troop_role         = 0  # 10=Kingdom Lord

slot_troop_occupation          = 2  # 0 = free, 1 = merchant
#slot_troop_duty               = 3  # Kingdom duty, 0 = free
#slot_troop_homage_type         = 45
#homage_mercenary =             = 1 #Player is on a temporary contract
#homage_official =              = 2 #Player has a royal appointment
#homage_feudal   =              = 3 #


slot_troop_state               = 3  
slot_troop_last_talk_time      = 4
slot_troop_met                 = 5 #i also use this for the courtship state -- may become cumbersome
slot_troop_courtship_state     = 5 #2 professed admiration, 3 agreed to seek a marriage, 4 ended relationship

slot_troop_party_template      = 6
#slot_troop_kingdom_rank        = 7

slot_troop_renown              = 7

##slot_troop_is_prisoner         = 8  # important for heroes only
slot_troop_prisoner_of_party   = 8  # important for heroes only
#slot_troop_is_player_companion = 9  # important for heroes only:::USE  slot_troop_occupation = slto_player_companion

slot_troop_present_at_event    = 9

slot_troop_leaded_party         = 10 # important for kingdom heroes only
slot_troop_wealth               = 11 # important for kingdom heroes only
slot_troop_cur_center           = 12 # important for royal family members only (non-kingdom heroes)

slot_troop_banner_scene_prop    = 13 # important for kingdom heroes and player only

slot_troop_original_faction     = 14 # for pretenders
#slot_troop_loyalty              = 15 #deprecated - this is now derived from other figures
slot_troop_player_order_state   = 16 #Deprecated
slot_troop_player_order_object  = 17 #Deprecated

#troop_player order state are all deprecated in favor of party_order_state. This has two reasons -- 1) to reset AI if the party is eliminated, and 2) to allow the player at a later date to give orders to leaderless parties, if we want that


#Post 0907 changes begin
slot_troop_age                 =  18
slot_troop_age_appearance      =  19

#Post 0907 changes end

slot_troop_does_not_give_quest = 20
slot_troop_player_debt         = 21
slot_troop_player_relation     = 22
#slot_troop_player_favor        = 23
slot_troop_last_quest          = 24
slot_troop_last_quest_betrayed = 25
slot_troop_last_persuasion_time= 26
slot_troop_last_comment_time   = 27
slot_troop_spawned_before      = 28

#Post 0907 changes begin
slot_troop_last_comment_slot   = 29
#Post 0907 changes end

slot_troop_spouse              = 30
slot_troop_father              = 31
slot_troop_mother              = 32
slot_troop_guardian            = 33 #Usually siblings are identified by a common parent.This is used for brothers if the father is not an active npc. At some point we might introduce geneologies
slot_troop_betrothed           = 34 #Obviously superseded once slot_troop_spouse is filled
#other relations are derived from one's parents 
#slot_troop_daughter            = 33
#slot_troop_son                 = 34
#slot_troop_sibling             = 35
slot_troop_love_interest_1     = 35 #each unmarried lord has three love interests
slot_troop_love_interest_2     = 36
slot_troop_love_interest_3     = 37
slot_troop_love_interests_end  = 38
#ways to court -- discuss a book, commission/compose a poem, present a gift, recount your exploits, fulfil a specific quest, appear at a tournament
#preferences for women - (conventional - father's friends)
slot_lady_no_messages          				= 37
slot_lady_last_suitor          				= 38
slot_lord_granted_courtship_permission      = 38

slot_troop_betrothal_time                   = 39 #used in scheduling the wedding

slot_troop_trainer_met                       = 30
slot_troop_trainer_waiting_for_result        = 31
slot_troop_trainer_training_fight_won        = 32
slot_troop_trainer_num_opponents_to_beat     = 33
slot_troop_trainer_training_system_explained = 34
slot_troop_trainer_opponent_troop            = 35
slot_troop_trainer_training_difficulty       = 36
slot_troop_trainer_training_fight_won        = 37


slot_lady_used_tournament					= 40

slot_troop_cheer_vocals_begin	  = 290 #Borridian
slot_troop_cheer_vocals_end		  = 291 #Borridian
slot_troop_taunt_vocals_begin	  = 292 #Borridian
slot_troop_taunt_vocals_end		  = 293 #Borridian

slot_troop_is_boss				  = 295 #Borridian
slot_troop_boss_ability_script	  = 296 #Borridian
slot_troop_power_index			  = 297 #Borridian

slot_troop_is_hero				  = 300 #Borridian
slot_troop_hero_num_loadouts	  = 301 #Borridian
slot_troop_hero_stance_anim		  = 302 #Borridian
slot_troop_hero_cost			  = 303 #Borridian
slot_troop_hero_item0			  = 304 #Borridian
slot_troop_hero_item1			  = 305 #Borridian
slot_troop_hero_item2			  = 306 #Borridian
slot_troop_hero_item3			  = 307 #Borridian
slot_troop_hero_head			  = 308 #Borridian
slot_troop_hero_body			  = 309 #Borridian
slot_troop_hero_foot			  = 310 #Borridian
slot_troop_hero_gloves			  = 311 #Borridian
slot_troop_hero_horse			  = 312 #Borridian

slot_troop_hero_leadership_script = 314 #Borridian
slot_troop_hero_ability_script	  = 315 #Borridian
slot_troop_hero_ability_icon	  = 316 #Borridian
slot_troop_hero_max_count		  = 317 #Borridian
slot_troop_disabled               = 318 #Borridian

slot_troop_hero_buff_1_id		  = 320 #Borridian
slot_troop_hero_buff_1_val		  = 321 #Borridian
slot_troop_hero_buff_2_id		  = 322 #Borridian
slot_troop_hero_buff_2_val		  = 323 #Borridian
slot_troop_hero_buff_3_id		  = 324 #Borridian
slot_troop_hero_buff_3_val		  = 325 #Borridian
slot_troop_hero_buff_4_id		  = 326 #Borridian
slot_troop_hero_buff_4_val		  = 327 #Borridian
slot_troop_hero_buff_5_id		  = 328 #Borridian
slot_troop_hero_buff_5_val		  = 329 #Borridian
slot_troop_hero_buff_6_id		  = 330 #Borridian
slot_troop_hero_buff_6_val		  = 331 #Borridian

slot_troop_max_health			  = 41 #Borridian

slot_troop_is_invisible_rider	  = 42 #Borridian

slot_troop_num_ai_troops		  = 43 #Borridian

slot_troop_button_overlay_id	  = 44 #Borridian

slot_troop_disable_toggle_overlay = 400 #Borridian

slot_troop_current_rumor       = 45
slot_troop_temp_slot           = 46
slot_troop_promised_fief       = 47

slot_troop_set_decision_seed       = 48 #Does not change
slot_troop_temp_decision_seed      = 49 #Resets at recalculate_ai
slot_troop_recruitment_random      = 50 #used in a number of different places in the intrigue procedures to overcome intermediate hurdles, although not for the final calculation, might be replaced at some point by the global decision seed
#Decision seeds can be used so that some randomness can be added to NPC decisions, without allowing the player to spam the NPC with suggestions
#The temp decision seed is reset 24 to 48 hours after the NPC last spoke to the player, while the set seed only changes in special occasions
#The single seed is used with varying modula to give high/low outcomes on different issues, without using a separate slot for each issue

slot_troop_intrigue_impatience = 51
#recruitment changes end

#slot_troop_honorable          = 50
#slot_troop_merciful          = 51
slot_lord_reputation_type     		  = 52
slot_lord_recruitment_argument        = 53 #the last argument proposed by the player to the lord
slot_lord_recruitment_candidate       = 54 #the last candidate proposed by the player to the lord

slot_troop_change_to_faction          = 55

#slot_troop_readiness_to_join_army     = 57 #possibly deprecate
#slot_troop_readiness_to_follow_orders = 58 #possibly deprecate

# NPC-related constants

#NPC companion changes begin
slot_troop_first_encountered          = 59
slot_troop_home                       = 60

slot_troop_morality_state       = 61
tms_no_problem         = 0
tms_acknowledged       = 1
tms_dismissed          = 2

slot_troop_morality_type = 62
tmt_aristocratic = 1
tmt_egalitarian = 2
tmt_humanitarian = 3
tmt_honest = 4
tmt_pious = 5

slot_troop_morality_value = 63

slot_troop_2ary_morality_type  = 64
slot_troop_2ary_morality_state = 65
slot_troop_2ary_morality_value = 66

slot_troop_town_with_contacts  = 67
slot_troop_town_contact_type   = 68 #1 are nobles, 2 are commons

slot_troop_morality_penalties =  69 ### accumulated grievances from morality conflicts


slot_troop_personalityclash_object     = 71
#(0 - they have no problem, 1 - they have a problem)
slot_troop_personalityclash_state    = 72 #1 = pclash_penalty_to_self, 2 = pclash_penalty_to_other, 3 = pclash_penalty_to_other,
pclash_penalty_to_self  = 1
pclash_penalty_to_other = 2
pclash_penalty_to_both  = 3
#(a string)
slot_troop_personalityclash2_object   = 73
slot_troop_personalityclash2_state    = 74

slot_troop_personalitymatch_object   =  75
slot_troop_personalitymatch_state   =  76

slot_troop_personalityclash_penalties = 77 ### accumulated grievances from personality clash
slot_troop_personalityclash_penalties = 77 ### accumulated grievances from personality clash

slot_troop_home_speech_delivered = 78 #only for companions
slot_troop_discussed_rebellion   = 78 #only for pretenders

#courtship slots
slot_lady_courtship_heroic_recited 	    = 74
slot_lady_courtship_allegoric_recited 	= 75
slot_lady_courtship_comic_recited 		= 76
slot_lady_courtship_mystic_recited 		= 77
slot_lady_courtship_tragic_recited 		= 78



#NPC history slots
slot_troop_met_previously        = 80
slot_troop_turned_down_twice     = 81
slot_troop_playerparty_history   = 82

pp_history_scattered         = 1
pp_history_dismissed         = 2
pp_history_quit              = 3
pp_history_indeterminate     = 4

slot_troop_playerparty_history_string   = 83
slot_troop_return_renown        = 84

slot_troop_custom_banner_bg_color_1      = 85
slot_troop_custom_banner_bg_color_2      = 86
slot_troop_custom_banner_charge_color_1  = 87
slot_troop_custom_banner_charge_color_2  = 88
slot_troop_custom_banner_charge_color_3  = 89
slot_troop_custom_banner_charge_color_4  = 90
slot_troop_custom_banner_bg_type         = 91
slot_troop_custom_banner_charge_type_1   = 92
slot_troop_custom_banner_charge_type_2   = 93
slot_troop_custom_banner_charge_type_3   = 94
slot_troop_custom_banner_charge_type_4   = 95
slot_troop_custom_banner_flag_type       = 96
slot_troop_custom_banner_num_charges     = 97
slot_troop_custom_banner_positioning     = 98
slot_troop_custom_banner_map_flag_type   = 99

#conversation strings -- must be in this order!
slot_troop_intro 						= 101
slot_troop_intro_response_1 			= 102
slot_troop_intro_response_2 			= 103
slot_troop_backstory_a 					= 104
slot_troop_backstory_b 					= 105
slot_troop_backstory_c 					= 106
slot_troop_backstory_delayed 			= 107
slot_troop_backstory_response_1 		= 108
slot_troop_backstory_response_2 		= 109
slot_troop_signup   					= 110
slot_troop_signup_2 					= 111
slot_troop_signup_response_1 			= 112
slot_troop_signup_response_2 			= 113
slot_troop_mentions_payment 			= 114 #Not actually used
slot_troop_payment_response 			= 115 #Not actually used
slot_troop_morality_speech   			= 116
slot_troop_2ary_morality_speech 		= 117
slot_troop_personalityclash_speech 		= 118
slot_troop_personalityclash_speech_b 	= 119
slot_troop_personalityclash2_speech 	= 120
slot_troop_personalityclash2_speech_b 	= 121
slot_troop_personalitymatch_speech 		= 122
slot_troop_personalitymatch_speech_b 	= 123
slot_troop_retirement_speech 			= 124
slot_troop_rehire_speech 				= 125
slot_troop_home_intro           		= 126
slot_troop_home_description    			= 127
slot_troop_home_description_2 			= 128
slot_troop_home_recap         			= 129
slot_troop_honorific   					= 130
slot_troop_kingsupport_string_1			= 131
slot_troop_kingsupport_string_2			= 132
slot_troop_kingsupport_string_2a		= 133
slot_troop_kingsupport_string_2b		= 134
slot_troop_kingsupport_string_3			= 135
slot_troop_kingsupport_objection_string	= 136
slot_troop_intel_gathering_string	    = 137
slot_troop_fief_acceptance_string	    = 138
slot_troop_woman_to_woman_string	    = 139
slot_troop_turn_against_string	        = 140

slot_troop_strings_end 					= 141

slot_troop_payment_request 				= 141

#141, support base removed, slot now available

slot_troop_kingsupport_state			= 142
slot_troop_kingsupport_argument			= 143
slot_troop_kingsupport_opponent			= 144
slot_troop_kingsupport_objection_state  = 145 #0, default, 1, needs to voice, 2, has voiced

slot_troop_days_on_mission		        = 150
slot_troop_current_mission			    = 151
slot_troop_mission_object               = 152
npc_mission_kingsupport					= 1
npc_mission_gather_intel                = 2
npc_mission_peace_request               = 3
npc_mission_pledge_vassal               = 4
npc_mission_seek_recognition            = 5
npc_mission_test_waters                 = 6
npc_mission_non_aggression              = 7
npc_mission_rejoin_when_possible        = 8

#Number of routed agents after battle ends.
slot_troop_player_routed_agents                 = 146
slot_troop_ally_routed_agents                   = 147
slot_troop_enemy_routed_agents                  = 148

#Special quest slots
slot_troop_mission_participation        = 149
mp_unaware                              = 0 
mp_stay_out                             = 1 
mp_prison_break_fight                   = 2 
mp_prison_break_stand_back              = 3 
mp_prison_break_escaped                 = 4 
mp_prison_break_caught                  = 5 

#Below are some constants to expand the political system a bit. The idea is to make quarrels less random, but instead make them serve a rational purpose -- as a disincentive to lords to seek 

slot_troop_controversy                     = 150 #Determines whether or not a troop is likely to receive fief or marshalship
slot_troop_recent_offense_type 	           = 151 #failure to join army, failure to support colleague
slot_troop_recent_offense_object           = 152 #to whom it happened
slot_troop_recent_offense_time             = 153
slot_troop_stance_on_faction_issue         = 154 #when it happened

tro_failed_to_join_army                    = 1
tro_failed_to_support_colleague            = 2

#CONTROVERSY
#This is used to create a more "rational choice" model of faction politics, in which lords pick fights with other lords for gain, rather than simply because of clashing personalities
#It is intended to be a limiting factor for players and lords in their ability to intrigue against each other. It represents the embroilment of a lord in internal factional disputes. In contemporary media English, a lord with high "controversy" would be described as "embattled."
#The main effect of high controversy is that it disqualifies a lord from receiving a fief or an appointment
#It is a key political concept because it provides incentive for much of the political activity. For example, Lord Red Senior is worried that his rival, Lord Blue Senior, is going to get a fied which Lord Red wants. So, Lord Red turns to his protege, Lord Orange Junior, to attack Lord Blue in public. The fief goes to Lord Red instead of Lord Blue, and Lord Red helps Lord Orange at a later date.


slot_troop_will_join_prison_break      = 161


troop_slots_reserved_for_relations_start        = 165 #this is based on id_troops, and might change

slot_troop_relations_begin				= 0 #this creates an array for relations between troops
											#Right now, lords start at 165 and run to around 290, including pretenders
											
											
											
########################################################
##  PLAYER SLOTS           #############################
########################################################

slot_player_spawned_this_round                 = 0
slot_player_last_rounds_used_item_earnings     = 1
slot_player_selected_item_indices_begin        = 2
slot_player_selected_item_indices_end          = 11
slot_player_cur_selected_item_indices_begin    = slot_player_selected_item_indices_end
slot_player_cur_selected_item_indices_end      = slot_player_selected_item_indices_end + 9
slot_player_join_time                          = 21
slot_player_button_index                       = 22 #used for presentations
slot_player_can_answer_poll                    = 23
slot_player_first_spawn                        = 24
slot_player_spawned_at_siege_round             = 25
slot_player_poll_disabled_until_time           = 26
slot_player_total_equipment_value              = 27
slot_player_last_team_select_time              = 28
slot_player_death_pos_x                        = 29
slot_player_death_pos_y                        = 30
slot_player_death_pos_z                        = 31
slot_player_damage_given_to_target_1           = 32 #used only in destroy mod
slot_player_damage_given_to_target_2           = 33 #used only in destroy mod
slot_player_last_bot_count                     = 34
slot_player_bot_type_1_wanted                  = 35
slot_player_bot_type_2_wanted                  = 36
slot_player_bot_type_3_wanted                  = 37
slot_player_bot_type_4_wanted                  = 38
slot_player_bot_type_5_wanted                  = 39######
slot_player_bot_type_6_wanted                  = 40		#
slot_player_bot_type_7_wanted                  = 41		#	Borridian - new defender bot slots
slot_player_bot_type_8_wanted                  = 42		#
slot_player_bot_type_9_wanted                  = 43		#
slot_player_bot_type_10_wanted                 = 44######
slot_player_spawn_count                        = 45
slot_player_chosen_spawn_point                 = 46
##FuIn
slot_player_suspending_spawn      = 156
slot_player_respawn_time          = 157
slot_FuIn_respawn                 = 158		#Holds number of player lives
slot_player_lives_initialised     = 159
##FuIn
slot_player_last_message_received = 160
slot_player_is_muted			  = 161
slot_current_map_name_to_update   = 162

slot_player_newly_joined		  = 163

slot_player_died_this_wave        = 164

slot_player_special_spawn		  = 165		#Borridian - for wardrobe code

slot_player_is_admin			  = 167		#Borridian - chat system variables
slot_player_is_dev				  = 168
slot_player_is_patron			  = 169

slot_player_cur_equipped_item0	  = 170		#Borridian - for Wardrobe cost calculations
slot_player_cur_equipped_item1	  = 171
slot_player_cur_equipped_item2	  = 172
slot_player_cur_equipped_item3	  = 173
slot_player_cur_equipped_head	  = 174
slot_player_cur_equipped_body	  = 175
slot_player_cur_equipped_foot	  = 176
slot_player_cur_equipped_gloves	  = 177
slot_player_cur_equipped_horse	  = 178

slot_player_in_ghost_mode		  = 180

slot_player_do_not_save_stats	  = 181

slot_player_last_refresh_time     = 182

player_chat_default_colour		  = 0xC8C8C8
player_chat_admin_colour		  = 0xFFDD8A
player_chat_dev_colour			  = 0x00B4FF
player_chat_patron_colour		  = 0x9B59B6	#Purple
admin_chat_colour				  = 0x22B14C	#Green
player_persistent_slots_count	  = 6

########################################################
##  TEAM SLOTS             #############################
########################################################

slot_team_flag_situation                       = 0




#Rebellion changes end
# character backgrounds
cb_noble = 1
cb_merchant = 2
cb_guard = 3
cb_forester = 4
cb_nomad = 5
cb_thief = 6
cb_priest = 7

cb2_page = 0
cb2_apprentice = 1
cb2_urchin  = 2
cb2_steppe_child = 3
cb2_merchants_helper = 4

cb3_poacher = 3
cb3_craftsman = 4
cb3_peddler = 5
cb3_troubadour = 7
cb3_squire = 8
cb3_lady_in_waiting = 9
cb3_student = 10

cb4_revenge = 1
cb4_loss    = 2
cb4_wanderlust =  3
cb4_disown  = 5
cb4_greed  = 6

#NPC system changes end
#Encounter types
enctype_fighting_against_village_raid = 1
enctype_catched_during_village_raid   = 2


### Troop occupations slot_troop_occupation
##slto_merchant           = 1
slto_inactive           = 0 #for companions at the beginning of the game

slto_kingdom_hero       = 2

slto_player_companion   = 5 #This is specifically for companions in the employ of the player -- ie, in the party, or on a mission
slto_kingdom_lady       = 6 #Usually inactive (Calradia is a traditional place). However, can be made potentially active if active_npcs are expanded to include ladies
slto_kingdom_seneschal  = 7
slto_robber_knight      = 8
slto_inactive_pretender = 9


stl_unassigned          = -1
stl_reserved_for_player = -2
stl_rejected_by_player  = -3

#NPC changes begin
slto_retirement      = 11
#slto_retirement_medium    = 12
#slto_retirement_short     = 13
#NPC changes end

########################################################
##  QUEST SLOTS            #############################
########################################################

slot_quest_target_center            = 1
slot_quest_target_troop             = 2
slot_quest_target_faction           = 3
slot_quest_object_troop             = 4
##slot_quest_target_troop_is_prisoner = 5
slot_quest_giver_troop              = 6
slot_quest_object_center            = 7
slot_quest_target_party             = 8
slot_quest_target_party_template    = 9
slot_quest_target_amount            = 10
slot_quest_current_state            = 11
slot_quest_giver_center             = 12
slot_quest_target_dna               = 13
slot_quest_target_item              = 14
slot_quest_object_faction           = 15

slot_quest_target_state             = 16
slot_quest_object_state             = 17

slot_quest_convince_value           = 19
slot_quest_importance               = 20
slot_quest_xp_reward                = 21
slot_quest_gold_reward              = 22
slot_quest_expiration_days          = 23
slot_quest_dont_give_again_period   = 24
slot_quest_dont_give_again_remaining_days = 25

slot_quest_failure_consequence      = 26
slot_quest_temp_slot      			= 27

########################################################
##  PARTY TEMPLATE SLOTS   #############################
########################################################

# Ryan BEGIN
slot_party_template_num_killed   = 1

slot_party_template_lair_type    	 	= 3
slot_party_template_lair_party    		= 4
slot_party_template_lair_spawnpoint     = 5


# Ryan END


########################################################
##  SCENE PROP SLOTS       #############################
########################################################

scene_prop_open_or_close_slot       = 1
scene_prop_smoke_effect_done        = 2
scene_prop_number_of_agents_pushing = 3 #for belfries only
scene_prop_next_entry_point_id      = 4 #for belfries only
scene_prop_belfry_platform_moved    = 5 #for belfries only
scene_prop_destroyed				= 6 #Borridian
scene_prop_assigned_agent			= 7 #Borridian
scene_prop_attached_agent			= 8 #Borridian
scene_prop_slots_end                = 9
scene_prop_placed_during_round		= 10 #Borridian
scene_prop_overlay_id               = 11 #Borridian
scene_prop_ammo_count               = 12 #Borridian
scene_prop_interval_counter         = 13 #Borridian

########################################################
rel_enemy   = 0
rel_neutral = 1
rel_ally    = 2


#Talk contexts
tc_town_talk                  = 0
tc_court_talk   	      	  = 1
tc_party_encounter            = 2
tc_castle_gate                = 3
tc_siege_commander            = 4
tc_join_battle_ally           = 5
tc_join_battle_enemy          = 6
tc_castle_commander           = 7
tc_hero_freed                 = 8
tc_hero_defeated              = 9
tc_entering_center_quest_talk = 10
tc_back_alley                 = 11
tc_siege_won_seneschal        = 12
tc_ally_thanks                = 13
tc_tavern_talk                = 14
tc_rebel_thanks               = 15
tc_garden            		  = 16
tc_courtship            	  = 16
tc_after_duel            	  = 17
tc_prison_break               = 18
tc_escape               	  = 19
tc_give_center_to_fief        = 20
tc_merchants_house            = 21


#Troop Commentaries begin
#Log entry types
#civilian
logent_village_raided            = 1
logent_village_extorted          = 2
logent_caravan_accosted          = 3 #in caravan accosted, center and troop object are -1, and the defender's faction is the object
logent_traveller_attacked        = 3 #in traveller attacked, origin and destination are center and troop object, and the attacker's faction is the object

logent_helped_peasants           = 4 

logent_party_traded              = 5

logent_castle_captured_by_player              = 10
logent_lord_defeated_by_player                = 11
logent_lord_captured_by_player                = 12
logent_lord_defeated_but_let_go_by_player     = 13
logent_player_defeated_by_lord                = 14
logent_player_retreated_from_lord             = 15
logent_player_retreated_from_lord_cowardly    = 16
logent_lord_helped_by_player                  = 17
logent_player_participated_in_siege           = 18
logent_player_participated_in_major_battle    = 19
logent_castle_given_to_lord_by_player         = 20

logent_pledged_allegiance          = 21
logent_liege_grants_fief_to_vassal = 22


logent_renounced_allegiance      = 23 

logent_player_claims_throne_1    		               = 24
logent_player_claims_throne_2    		               = 25


logent_troop_feels_cheated_by_troop_over_land		   = 26
logent_ruler_intervenes_in_quarrel                     = 27
logent_lords_quarrel_over_land                         = 28
logent_lords_quarrel_over_insult                       = 29
logent_marshal_vs_lord_quarrel                  	   = 30
logent_lords_quarrel_over_woman                        = 31

logent_lord_protests_marshall_appointment			   = 32
logent_lord_blames_defeat						   	   = 33

logent_player_suggestion_succeeded					   = 35
logent_player_suggestion_failed					       = 36

logent_liege_promises_fief_to_vassal				   = 37

logent_lord_insults_lord_for_cowardice                 = 38
logent_lord_insults_lord_for_rashness                  = 39
logent_lord_insults_lord_for_abandonment               = 40
logent_lord_insults_lord_for_indecision                = 41
logent_lord_insults_lord_for_cruelty                   = 42
logent_lord_insults_lord_for_dishonor                  = 43




logent_game_start                           = 45 
logent_poem_composed                        = 46 ##Not added
logent_tournament_distinguished             = 47 ##Not added
logent_tournament_won                       = 48 ##Not added

#logent courtship - lady is always actor, suitor is always troop object
logent_lady_favors_suitor                   = 51 #basically for gossip
logent_lady_betrothed_to_suitor_by_choice   = 52
logent_lady_betrothed_to_suitor_by_family   = 53
logent_lady_rejects_suitor                  = 54
logent_lady_father_rejects_suitor           = 55
logent_lady_marries_lord                    = 56
logent_lady_elopes_with_lord                = 57
logent_lady_rejected_by_suitor              = 58
logent_lady_betrothed_to_suitor_by_pressure = 59 #mostly for gossip

logent_lady_and_suitor_break_engagement		= 60
logent_lady_marries_suitor				    = 61

logent_lord_holds_lady_hostages             = 62
logent_challenger_defeats_lord_in_duel      = 63
logent_challenger_loses_to_lord_in_duel     = 64

logent_player_stole_cattles_from_village    = 66

logent_party_spots_wanted_bandits           = 70


logent_border_incident_cattle_stolen          = 72 #possibly add this to rumors for non-player faction
logent_border_incident_bride_abducted         = 73 #possibly add this to rumors for non-player faction
logent_border_incident_villagers_killed       = 74 #possibly add this to rumors for non-player faction
logent_border_incident_subjects_mistreated    = 75 #possibly add this to rumors for non-player faction

#These supplement caravans accosted and villages burnt, in that they create a provocation. So far, they only refer to the player
logent_border_incident_troop_attacks_neutral  = 76
logent_border_incident_troop_breaks_truce     = 77
logent_border_incident_troop_suborns_lord   = 78


logent_policy_ruler_attacks_without_provocation 			= 80
logent_policy_ruler_ignores_provocation         			= 81 #possibly add this to rumors for non-player factions
logent_policy_ruler_makes_peace_too_soon        			= 82
logent_policy_ruler_declares_war_with_justification         = 83
logent_policy_ruler_breaks_truce                            = 84
logent_policy_ruler_issues_indictment_just                  = 85 #possibly add this to rumors for non-player faction
logent_policy_ruler_issues_indictment_questionable          = 86 #possibly add this to rumors for non-player faction

logent_player_faction_declares_war						    = 90 #this doubles for declare war to extend power
logent_faction_declares_war_out_of_personal_enmity		    = 91
logent_faction_declares_war_to_regain_territory 		    = 92
logent_faction_declares_war_to_curb_power					= 93
logent_faction_declares_war_to_respond_to_provocation	    = 94
logent_war_declaration_types_end							= 95


#logent_lady_breaks_betrothal_with_lord      = 58
#logent_lady_betrothal_broken_by_lord        = 59

#lord reputation type, for commentaries
#"Martial" will be twice as common as the other types
lrep_none           = 0 
lrep_martial        = 1 #chivalrous but not terribly empathetic or introspective, - eg Richard Lionheart, your average 14th century French baron
lrep_quarrelsome    = 2 #spiteful, cynical, a bit paranoid, possibly hotheaded - eg Robert Graves' Tiberius, some of Charles VI's uncles
lrep_selfrighteous  = 3 #coldblooded, moralizing, often cruel - eg William the Conqueror, Timur, Octavian, Aurangzeb (although he is arguably upstanding instead, particularly after his accession)
lrep_cunning        = 4 #coldblooded, pragmatic, amoral - eg Louis XI, Guiscard, Akbar Khan, Abd al-Aziz Ibn Saud
lrep_debauched      = 5 #spiteful, amoral, sadistic - eg Caligula, Tuchman's Charles of Navarre
lrep_goodnatured    = 6 #chivalrous, benevolent, perhaps a little too decent to be a good warlord - eg Hussein ibn Ali. Few well-known historical examples maybe. because many lack the drive to rise to faction leadership. Ranjit Singh has aspects
lrep_upstanding     = 7 #moralizing, benevolent, pragmatic, - eg Bernard Cornwell's Alfred, Charlemagne, Salah al-Din, Sher Shah Suri

lrep_roguish        = 8 #used for commons, specifically ex-companions. Tries to live life as a lord to the full
lrep_benefactor     = 9 #used for commons, specifically ex-companions. Tries to improve lot of folks on land
lrep_custodian      = 10 #used for commons, specifically ex-companions. Tries to maximize fief's earning potential

#lreps specific to dependent noblewomen
lrep_conventional    = 21 #Charlotte York in SATC seasons 1-2, probably most medieval aristocrats
lrep_adventurous     = 22 #Tomboyish. However, this basically means that she likes to travel and hunt, and perhaps yearn for wider adventures. However, medieval noblewomen who fight are rare, and those that attempt to live independently of a man are rarer still, and best represented by pre-scripted individuals like companions
lrep_otherworldly    = 23 #Prone to mysticism, romantic. 
lrep_ambitious       = 24 #Lady Macbeth
lrep_moralist        = 25 #Equivalent of upstanding or benefactor -- takes nobless oblige, and her traditional role as repository of morality, very seriously. Based loosely on Christine de Pisa 

#a more complicated system of reputation could include the following...

#successful vs unlucky -- basic gauge of success
#daring vs cautious -- maybe not necessary
#honorable/pious/ideological vs unscrupulous -- character's adherance to an external code of conduct. Fails to capture complexity of people like Aurangzeb, maybe, but good for NPCs
	#(visionary/altruist and orthodox/unorthodox could be a subset of the above, or the specific external code could be another tag)
#generous/loyal vs manipulative/exploitative -- character's sense of duty to specific individuals, based on their relationship. Affects loyalty of troops, etc
#merciful vs cruel/ruthless/sociopathic -- character's general sense of compassion. Sher Shah is example of unscrupulous and merciful (the latter to a degree).
#dignified vs unconventional -- character's adherance to social conventions. Very important, given the times


courtship_poem_tragic      = 1 #Emphasizes longing, Laila and Majnoon
courtship_poem_heroic      = 2 #Norse sagas with female heroines
courtship_poem_comic       = 3 #Emphasis on witty repartee -- Contrasto (Sicilian school satire) 
courtship_poem_mystic      = 4 #Sufi poetry. Song of Songs
courtship_poem_allegoric   = 5 #Idealizes woman as a civilizing force -- the Romance of the Rose, Siege of the Castle of Love

#courtship gifts currently deprecated







#Troop Commentaries end

tutorial_fighters_begin = "trp_tutorial_fighter_1"
tutorial_fighters_end   = "trp_tutorial_archer_1"

#Walker types: 
walkert_default            = 0
walkert_needs_money        = 1
walkert_needs_money_helped = 2
walkert_spy                = 3
num_town_walkers = 8
town_walker_entries_start = 32

reinforcement_cost_easy = 600
reinforcement_cost_moderate = 450
reinforcement_cost_hard = 300

merchant_toll_duration        = 72 #Tolls are valid for 72 hours

hero_escape_after_defeat_chance = 70


raid_distance = 4

surnames_begin = "str_surname_1"
surnames_end = "str_surnames_end"
names_begin = "str_name_1"
names_end = surnames_begin
countersigns_begin = "str_countersign_1"
countersigns_end = names_begin
secret_signs_begin = "str_secret_sign_1"
secret_signs_end = countersigns_begin

kingdom_titles_male_begin = "str_faction_title_male_player"
kingdom_titles_female_begin = "str_faction_title_female_player"

kingdoms_begin = "fac_player_supporters_faction"
kingdoms_end = "fac_kingdoms_end"

bandits_begin = "trp_bandit"
bandits_end = "trp_black_khergit_horseman"

kingdom_ladies_begin = "trp_knight_1_1_wife"
kingdom_ladies_end = "trp_heroes_end"

#active NPCs in order: companions, kings, lords, pretenders

pretenders_begin = "trp_kingdom_1_pretender"
pretenders_end = kingdom_ladies_begin

lords_begin = "trp_knight_1_1"
lords_end = pretenders_begin

kings_begin = "trp_kingdom_1_lord"
kings_end = lords_begin

companions_begin = "trp_npc1"
companions_end = kings_begin

active_npcs_begin = "trp_npc1"
active_npcs_end = kingdom_ladies_begin
#"active_npcs_begin replaces kingdom_heroes_begin to allow for companions to become lords. Includes anyone who may at some point lead their own party: the original kingdom heroes, companions who may become kingdom heroes, and pretenders. (slto_kingdom_hero as an occupation means that you lead a party on the map. Pretenders have the occupation "slto_inactive_pretender", even if they are part of a player's party, until they have their own independent party)
#If you're a modder and you don't want to go through and switch every kingdom_heroes to active_npcs, simply define a constant: kingdom_heroes_begin = active_npcs_begin., and kingdom_heroes_end = active_npcs_end. I haven't tested for that, but I think it should work.

active_npcs_including_player_begin = "trp_kingdom_heroes_including_player_begin"
original_kingdom_heroes_begin = "trp_kingdom_1_lord"

heroes_begin = active_npcs_begin
heroes_end = kingdom_ladies_end

soldiers_begin = "trp_farmer"
soldiers_end = "trp_town_walker_1"

#Rebellion changes

##rebel_factions_begin = "fac_kingdom_1_rebels"
##rebel_factions_end =   "fac_kingdoms_end"

pretenders_begin = "trp_kingdom_1_pretender"
pretenders_end = active_npcs_end
#Rebellion changes

tavern_minstrels_begin = "trp_tavern_minstrel_1"
tavern_minstrels_end   = "trp_kingdom_heroes_including_player_begin"

tavern_booksellers_begin = "trp_tavern_bookseller_1"
tavern_booksellers_end   = tavern_minstrels_begin

tavern_travelers_begin = "trp_tavern_traveler_1"
tavern_travelers_end   = tavern_booksellers_begin

ransom_brokers_begin = "trp_ransom_broker_1"
ransom_brokers_end   = tavern_travelers_begin

mercenary_troops_begin = "trp_watchman"
mercenary_troops_end = "trp_mercenaries_end"

multiplayer_troops_begin = "trp_pentac_sentry_multiplayer"
multiplayer_troops_end = "trp_multiplayer_end"

multiplayer_ai_troops_begin = "trp_ae_combined_triax"
multiplayer_ai_troops_end = multiplayer_troops_begin

multiplayer_scenes_begin = "scn_multi_scene_1"
multiplayer_non_fi_scenes_begin = "scn_training_grounds"
multiplayer_fi_scenes_begin = "scn_ailando"
multiplayer_map_templates_begin = "scn_grassland_small"
multiplayer_standard_scenes_end = "scn_custom_map_1"
multiplayer_pollable_scenes_end = "scn_multiplayer_maps_end"
multiplayer_scenes_end = "scn_multiplayer_maps_end"

multiplayer_scene_names_begin = "str_multi_scene_1"
multiplayer_scene_names_end = "str_multi_scene_end"
multiplayer_custom_scene_names_refs_begin = trp_custom_map_1

multiplayer_flag_projections_begin = "mesh_flag_project_sw"
multiplayer_flag_projections_end = "mesh_flag_projects_end"

multiplayer_flag_taken_projections_begin = "mesh_flag_project_sw_miss"
multiplayer_flag_taken_projections_end = "mesh_flag_project_misses_end"

multiplayer_game_type_names_begin = "str_multi_game_type_1"
multiplayer_game_type_names_end = "str_multi_game_types_end"

multiplayer_poll_reasons_begin = "str_poll_reason_team_killing"
multiplayer_poll_reasons_end = "str_poll_reason_end"

quick_battle_troops_begin = "trp_quick_battle_troop_1"
quick_battle_troops_end = "trp_quick_battle_troops_end"

quick_battle_troop_texts_begin = "str_quick_battle_troop_1"
quick_battle_troop_texts_end = "str_quick_battle_troops_end"

quick_battle_scenes_begin = "scn_quick_battle_scene_1"
quick_battle_scenes_end = "scn_quick_battle_maps_end"

quick_battle_scene_images_begin = "mesh_cb_ui_maps_scene_01"

quick_battle_battle_scenes_begin = quick_battle_scenes_begin
quick_battle_battle_scenes_end = "scn_quick_battle_scene_4"

quick_battle_siege_scenes_begin = quick_battle_battle_scenes_end
quick_battle_siege_scenes_end = quick_battle_scenes_end

quick_battle_scene_names_begin = "str_quick_battle_scene_1"

lord_quests_begin = "qst_deliver_message"
lord_quests_end   = "qst_follow_army"

lord_quests_begin_2 = "qst_destroy_bandit_lair"
lord_quests_end_2   = "qst_blank_quest_2"

enemy_lord_quests_begin = "qst_lend_surgeon"
enemy_lord_quests_end   = lord_quests_end

village_elder_quests_begin = "qst_deliver_grain"
village_elder_quests_end = "qst_eliminate_bandits_infesting_village"

village_elder_quests_begin_2 = "qst_blank_quest_6"
village_elder_quests_end_2   = "qst_blank_quest_6"

mayor_quests_begin  = "qst_move_cattle_herd"
mayor_quests_end    = village_elder_quests_begin

mayor_quests_begin_2 = "qst_blank_quest_11"
mayor_quests_end_2   = "qst_blank_quest_11"

lady_quests_begin = "qst_rescue_lord_by_replace"
lady_quests_end   = mayor_quests_begin

lady_quests_begin_2 = "qst_blank_quest_16"
lady_quests_end_2   = "qst_blank_quest_16"

army_quests_begin = "qst_deliver_cattle_to_army"
army_quests_end   = lady_quests_begin

army_quests_begin_2 = "qst_blank_quest_21"
army_quests_end_2   = "qst_blank_quest_21"

player_realm_quests_begin = "qst_resolve_dispute"
player_realm_quests_end = "qst_blank_quest_1"

player_realm_quests_begin_2 = "qst_blank_quest_26"
player_realm_quests_end_2 = "qst_blank_quest_26"

all_items_begin = 0
all_items_end = "itm_items_end"

all_quests_begin = 0
all_quests_end = "qst_quests_end"

towns_begin = "p_town_1"
castles_begin = "p_castle_1"
villages_begin = "p_village_1"

towns_end = castles_begin
castles_end = villages_begin
villages_end   = "p_salt_mine"

walled_centers_begin = towns_begin
walled_centers_end   = castles_end

centers_begin = towns_begin
centers_end   = villages_end

training_grounds_begin   = "p_training_ground_1"
training_grounds_end     = "p_Bridge_1"

scenes_begin = "scn_town_1_center"
scenes_end = "scn_castle_1_exterior"

spawn_points_begin = "p_zendar"
spawn_points_end = "p_spawn_points_end"

regular_troops_begin       = "trp_novice_fighter"
regular_troops_end         = "trp_tournament_master"

swadian_merc_parties_begin = "p_town_1_mercs"
swadian_merc_parties_end   = "p_town_8_mercs"

vaegir_merc_parties_begin  = "p_town_8_mercs"
vaegir_merc_parties_end    = "p_zendar"

arena_masters_begin    = "trp_town_1_arena_master"
arena_masters_end      = "trp_town_1_armorer"

training_gound_trainers_begin    = "trp_trainer_1"
training_gound_trainers_end      = "trp_ransom_broker_1"

town_walkers_begin = "trp_town_walker_1"
town_walkers_end = "trp_village_walker_1"

village_walkers_begin = "trp_village_walker_1"
village_walkers_end   = "trp_spy_walker_1"

spy_walkers_begin = "trp_spy_walker_1"
spy_walkers_end = "trp_tournament_master"

walkers_begin = town_walkers_begin
walkers_end   = spy_walkers_end

armor_merchants_begin  = "trp_town_1_armorer"
armor_merchants_end    = "trp_town_1_weaponsmith"

weapon_merchants_begin = "trp_town_1_weaponsmith"
weapon_merchants_end   = "trp_town_1_tavernkeeper"

tavernkeepers_begin    = "trp_town_1_tavernkeeper"
tavernkeepers_end      = "trp_town_1_merchant"

goods_merchants_begin  = "trp_town_1_merchant"
goods_merchants_end    = "trp_town_1_horse_merchant"

horse_merchants_begin  = "trp_town_1_horse_merchant"
horse_merchants_end    = "trp_town_1_mayor"

mayors_begin           = "trp_town_1_mayor"
mayors_end             = "trp_village_1_elder"

village_elders_begin   = "trp_village_1_elder"
village_elders_end     = "trp_merchants_end"

startup_merchants_begin = "trp_swadian_merchant"
startup_merchants_end = "trp_startup_merchants_end"

num_max_items = 10000 #used for multiplayer mode

average_price_factor = 1000
minimum_price_factor = 100
maximum_price_factor = 10000

village_prod_min = 0 #was -5
village_prod_max = 20 #was 20

trade_goods_begin = "itm_spice"
trade_goods_end = "itm_siege_supply"
food_begin = "itm_smoked_fish"
food_end = "itm_siege_supply"
reference_books_begin = "itm_book_wound_treatment_reference"
reference_books_end   = trade_goods_begin
readable_books_begin = "itm_book_tactics"
readable_books_end   = reference_books_begin
books_begin = readable_books_begin
books_end = reference_books_end
horses_begin = "itm_sumpter_horse"
horses_end = "itm_arrows"
weapons_begin = "itm_wooden_stick"
weapons_end = "itm_wooden_shield"
ranged_weapons_begin = "itm_darts"
ranged_weapons_end = "itm_torch"
armors_begin = "itm_leather_gloves"
armors_end = "itm_wooden_stick"
shields_begin = "itm_wooden_shield"
shields_end = ranged_weapons_begin

# Banner constants

banner_meshes_begin = "mesh_banner_a01"
banner_meshes_end_minus_one = "mesh_banner_f21"

arms_meshes_begin = "mesh_arms_a01"
arms_meshes_end_minus_one = "mesh_arms_f21"

custom_banner_charges_begin = "mesh_custom_banner_charge_01"
custom_banner_charges_end = "mesh_tableau_mesh_custom_banner"

custom_banner_backgrounds_begin = "mesh_custom_banner_bg"
custom_banner_backgrounds_end = custom_banner_charges_begin

custom_banner_flag_types_begin = "mesh_custom_banner_01"
custom_banner_flag_types_end = custom_banner_backgrounds_begin

custom_banner_flag_map_types_begin = "mesh_custom_map_banner_01"
custom_banner_flag_map_types_end = custom_banner_flag_types_begin

custom_banner_flag_scene_props_begin = "spr_custom_banner_01"
custom_banner_flag_scene_props_end = "spr_banner_a"

custom_banner_map_icons_begin = "icon_custom_banner_01"
custom_banner_map_icons_end = "icon_banner_01"

banner_map_icons_begin = "icon_banner_01"
banner_map_icons_end_minus_one = "icon_banner_136"

banner_scene_props_begin = "spr_banner_a"
banner_scene_props_end_minus_one = "spr_banner_f21"

khergit_banners_begin_offset = 63
khergit_banners_end_offset = 84

sarranid_banners_begin_offset = 105
sarranid_banners_end_offset = 125

banners_end_offset = 136

# Some constants for merchant invenotries
merchant_inventory_space = 30
num_merchandise_goods = 40

num_max_river_pirates = 25
num_max_zendar_peasants = 25
num_max_zendar_manhunters = 10

num_max_dp_bandits = 10
num_max_refugees = 10
num_max_deserters = 10

num_max_militia_bands = 15
num_max_armed_bands = 12

num_max_vaegir_punishing_parties = 20
num_max_rebel_peasants = 25

num_max_frightened_farmers = 50
num_max_undead_messengers  = 20

num_forest_bandit_spawn_points = 1
num_mountain_bandit_spawn_points = 1
num_steppe_bandit_spawn_points = 1
num_taiga_bandit_spawn_points = 1
num_desert_bandit_spawn_points = 1
num_black_khergit_spawn_points = 1
num_sea_raider_spawn_points = 2

peak_prisoner_trains = 4
peak_kingdom_caravans = 12
peak_kingdom_messengers = 3


# Note positions
note_troop_location = 3

#battle tactics
btactic_hold = 1
btactic_follow_leader = 2
btactic_charge = 3
btactic_stand_ground = 4

#default right mouse menu orders
cmenu_move = -7
cmenu_follow = -6

# Town center modes - resets in game menus during the options
tcm_default 		= 0
tcm_disguised 		= 1
tcm_prison_break 	= 2
tcm_escape      	= 3


# Arena battle modes
#abm_fight = 0
abm_training = 1
abm_visit = 2
abm_tournament = 3

# Camp training modes
ctm_melee    = 1
ctm_ranged   = 2
ctm_mounted  = 3
ctm_training = 4

# Village bandits attack modes
vba_normal          = 1
vba_after_training  = 2

arena_tier1_opponents_to_beat = 3
arena_tier1_prize = 5
arena_tier2_opponents_to_beat = 6
arena_tier2_prize = 10
arena_tier3_opponents_to_beat = 10
arena_tier3_prize = 25
arena_tier4_opponents_to_beat = 20
arena_tier4_prize = 60
arena_grand_prize = 250


#Additions
price_adjustment = 25 #the percent by which a trade at a center alters price

fire_duration = 4 #fires takes 4 hours

#NORMAL ACHIEVEMENTS
multiplayer_pack_message_shift_size = 256
ACHIEVEMENT_NONE_SHALL_PASS = 1,
ACHIEVEMENT_MAN_EATER = 2,
ACHIEVEMENT_THE_HOLY_HAND_GRENADE = 3,
ACHIEVEMENT_LOOK_AT_THE_BONES = 4,
ACHIEVEMENT_KHAAAN = 5,
ACHIEVEMENT_GET_UP_STAND_UP = 6,
ACHIEVEMENT_BARON_GOT_BACK = 7,
ACHIEVEMENT_BEST_SERVED_COLD = 8,
ACHIEVEMENT_TRICK_SHOT = 9,
ACHIEVEMENT_GAMBIT = 10,
ACHIEVEMENT_OLD_SCHOOL_SNIPER = 11,
ACHIEVEMENT_CALRADIAN_ARMY_KNIFE = 12,
ACHIEVEMENT_MOUNTAIN_BLADE = 13,
ACHIEVEMENT_HOLY_DIVER = 14,
ACHIEVEMENT_FORCE_OF_NATURE = 15,

#SKILL RELATED ACHIEVEMENTS:
ACHIEVEMENT_BRING_OUT_YOUR_DEAD = 16,
ACHIEVEMENT_MIGHT_MAKES_RIGHT = 17,
ACHIEVEMENT_COMMUNITY_SERVICE = 18,
ACHIEVEMENT_AGILE_WARRIOR = 19,
ACHIEVEMENT_MELEE_MASTER = 20,
ACHIEVEMENT_DEXTEROUS_DASTARD = 21,
ACHIEVEMENT_MIND_ON_THE_MONEY = 22,
ACHIEVEMENT_ART_OF_WAR = 23,
ACHIEVEMENT_THE_RANGER = 24,
ACHIEVEMENT_TROJAN_BUNNY_MAKER = 25,

#MAP RELATED ACHIEVEMENTS:
ACHIEVEMENT_MIGRATING_COCONUTS = 26,
ACHIEVEMENT_HELP_HELP_IM_BEING_REPRESSED = 27,
ACHIEVEMENT_SARRANIDIAN_NIGHTS = 28,
ACHIEVEMENT_OLD_DIRTY_SCOUNDREL = 29,
ACHIEVEMENT_THE_BANDIT = 30,
ACHIEVEMENT_GOT_MILK = 31,
ACHIEVEMENT_SOLD_INTO_SLAVERY = 32,
ACHIEVEMENT_MEDIEVAL_TIMES = 33,
ACHIEVEMENT_GOOD_SAMARITAN = 34,
ACHIEVEMENT_MORALE_LEADER = 35,
ACHIEVEMENT_ABUNDANT_FEAST = 36,
ACHIEVEMENT_BOOK_WORM = 37,
ACHIEVEMENT_ROMANTIC_WARRIOR = 38,

#POLITICALLY ORIENTED ACHIEVEMENTS:
ACHIEVEMENT_HAPPILY_EVER_AFTER = 39,
ACHIEVEMENT_HEART_BREAKER = 40,
ACHIEVEMENT_AUTONOMOUS_COLLECTIVE = 41,
ACHIEVEMENT_I_DUB_THEE = 42,
ACHIEVEMENT_SASSY = 43,
ACHIEVEMENT_THE_GOLDEN_THRONE = 44,
ACHIEVEMENT_KNIGHTS_OF_THE_ROUND = 45,
ACHIEVEMENT_TALKING_HELPS = 46,
ACHIEVEMENT_KINGMAKER = 47,
ACHIEVEMENT_PUGNACIOUS_D = 48,
ACHIEVEMENT_GOLD_FARMER = 49,
ACHIEVEMENT_ROYALITY_PAYMENT = 50,
ACHIEVEMENT_MEDIEVAL_EMLAK = 51,
ACHIEVEMENT_CALRADIAN_TEA_PARTY = 52,
ACHIEVEMENT_MANIFEST_DESTINY = 53,
ACHIEVEMENT_CONCILIO_CALRADI = 54,
ACHIEVEMENT_VICTUM_SEQUENS = 55,

#MULTIPLAYER ACHIEVEMENTS:
ACHIEVEMENT_THIS_IS_OUR_LAND = 56,
ACHIEVEMENT_SPOIL_THE_CHARGE = 57,
ACHIEVEMENT_HARASSING_HORSEMAN = 58,
ACHIEVEMENT_THROWING_STAR = 59,
ACHIEVEMENT_SHISH_KEBAB = 60,
ACHIEVEMENT_RUIN_THE_RAID = 61,
ACHIEVEMENT_LAST_MAN_STANDING = 62,
ACHIEVEMENT_EVERY_BREATH_YOU_TAKE = 63,
ACHIEVEMENT_CHOPPY_CHOP_CHOP = 64,
ACHIEVEMENT_MACE_IN_YER_FACE = 65,
ACHIEVEMENT_THE_HUSCARL = 66,
ACHIEVEMENT_GLORIOUS_MOTHER_FACTION = 67,
ACHIEVEMENT_ELITE_WARRIOR = 68,

#COMBINED ACHIEVEMENTS
ACHIEVEMENT_SON_OF_ODIN = 69,
ACHIEVEMENT_KING_ARTHUR = 70,
ACHIEVEMENT_KASSAI_MASTER = 71,
ACHIEVEMENT_IRON_BEAR = 72,
ACHIEVEMENT_LEGENDARY_RASTAM = 73,
ACHIEVEMENT_SVAROG_THE_MIGHTY = 74,

ACHIEVEMENT_MAN_HANDLER = 75,
ACHIEVEMENT_GIRL_POWER = 76,
ACHIEVEMENT_QUEEN = 77,
ACHIEVEMENT_EMPRESS = 78,
ACHIEVEMENT_TALK_OF_THE_TOWN = 79,
ACHIEVEMENT_LADY_OF_THE_LAKE = 80,

##################################
###FULL INVASION MAIN CONSTANTS###
##################################

#Adminmod   #Selnix
admin_spawn_sword           = 1
admin_spawn_katana          = 2
admin_force_team_change     = 3
admin_give_health           = 4
admin_give_gold             = 5
admin_spawn_pistol          = 6
admin_slay_player           = 7
admin_spawn_stones          = 8
admin_spawn_shield          = 9
admin_give_gold_everyone    = 10
admin_health_everyone       = 11
admin_teleport              = 12
admin_ammo                  = 13
admin_teleport_to           = 14
admin_chat                  = 15
admin_change_all            = 16
admin_spawn_dagger          = 17
admin_spawn_horse           = 18
admin_kill_horse            = 19
admin_slay_all	            = 20
admin_teleport_all          = 21
admin_slay_bot			    = 22
admin_slay_disabled_classes = 23
admin_spawn_shotgun         = 24
admin_spawn_spectral_sword  = 25
admin_spawn_spectral_axe    = 26
admin_spawn_spectral_pike   = 27
admin_spawn_spectral_maul   = 28
admin_spawn_boulders        = 29
admin_spawn_giant_sword     = 30
admin_spawn_giant_mace      = 31
admin_spawn_balrog_sword    = 32
admin_spawn_sauron_mace     = 33
admin_spawn_lightsaber      = 34

#Maroon New Admin Commands###################
admin_spawn_potatoes     = 35
admin_slay_horse         = 36
admin_spawn_banhammer    = 37

admin_spawn_list_mask    = 50

admin_give_var_gold		 = 40
admin_give_var_gold_all	 = 41

#Borridian New Constants#######################################
dev_invulnerability		 = 23
dev_teleport			 = 24
dev_spawn				 = 25
dev_neutrality			 = 26
dev_invisibility		 = 27

player_cur_equipped_slots_begin	= slot_player_cur_equipped_item0
player_cur_equipped_slots_end	= slot_player_cur_equipped_horse + 1

npc_kingdoms_begin = "fac_defender_greeks"
npc_kingdoms_end = kingdoms_end

invader_factions_begin = "fac_invader_greeks"
invader_factions_end = kingdoms_end

defender_factions_begin = npc_kingdoms_begin
defender_factions_end = invader_factions_begin

### MAROON ### - fill in all the empty constants
random_items_1h_begin			= "itm_box_italian_sword"
random_items_2h_begin			= "itm_box_danish_greatsword"
random_items_polearms_begin		= "itm_box_simple_poleaxe"
random_items_bows_begin			= "itm_box_short_bow"
random_items_xbows_begin		= "itm_box_light_crossbow"
random_items_thrown_begin		= "itm_box_throwing_axes"
random_items_firearms_begin		= "itm_box_gun_empire_light_musket_c_p1_01"
random_items_shields_begin		= "itm_box_rhun_elite_shield"
random_items_arrows_begin		= "itm_box_arrows"
random_items_bolts_begin		= "itm_box_bolts"
random_items_bullets_begin		= "itm_box_cartridges"

legendary_items_1h_begin		= "itm_legendary_orc_slayer"
legendary_items_2h_begin		= "itm_legendary_boss_hellfire_sword_01"
legendary_items_polearms_begin	= "itm_legendary_waor_staff_d"
legendary_items_bows_begin		= "itm_legendary_bow_captain"
legendary_items_xbows_begin		= "itm_legendary_ballista"
legendary_items_thrown_begin	= "itm_legendary_chaos_throwing_axes"
legendary_items_firearms_begin	= "itm_legendary_blunderbuss"
legendary_items_shields_begin	= "itm_legendary_shield_captain"
legendary_items_arrows_begin	= "itm_legendary_black_arrows"
legendary_items_bolts_begin		= "itm_legendary_dragon_bolts"
legendary_items_bullets_begin	= "itm_legendary_pistol_ball_02"

legendary_items_begin	 		= legendary_items_1h_begin
legendary_items_end	 			= "itm_legendary_items_end"
lightsabers_begin	            = "itm_lightsaber_blue_bastard"
lightsabers_1h_begin            = "itm_lightsaber_blue_one_handed"
lightsabers_2h_begin            = lightsabers_begin
lightsabers_polearm_begin       = "itm_lightsaber_blue_pike"
lightsabers_1h_end              = lightsabers_polearm_begin
lightsabers_2h_end              = lightsabers_1h_begin
lightsabers_polearm_end         = "itm_lightsaber_lance"
lightsabers_end	                = legendary_items_1h_begin
random_items_begin				= random_items_1h_begin
random_items_end				= lightsabers_begin

random_box_legendary_chance		= 1 #(%)
random_box_melee_price			= 1000
random_box_ranged_price			= 1200
random_box_shield_price			= 750
legendary_box_price				= 40000
### MAROON ###

magic_items_begin			= itm_wand_weak
magic_items_end				= itm_engineer_turret_short_range_shot + 1

bombs_begin					= itm_arcana_bomb_blue_weak
bombs_end					= itm_arcana_bomb_strong_dummy_green + 1

turret_trigger_tick_interval    = 0.2
turret_items_begin              = "itm_engineer_turret_long_range"
turret_items_end                = "itm_engineer_turret_long_range_shot"
turret_ammo_begin               = turret_items_end
turret_ammo_end                 = "itm_shapka_jeleznaya"
turret_props_begin              = "spr_engineer_turret_long_range"
turret_props_end                = "spr_spawn_point_marker_1"
#####################################
turret_ammo_long_range          = 60
turret_ammo_medium_range        = 90    #Max ammo amount (what to set it to when initialised)
turret_ammo_short_range         = 120
#####################################
turret_los_long_range           = 80
turret_los_medium_range         = 45    #Max firing range in metres
turret_los_short_range          = 10
#####################################
turret_los_long_range_min       = 10
turret_los_medium_range_min     = 5    #Min firing range in metres
turret_los_short_range_min      = 0
#####################################
turret_velocity_long_range           = 150
turret_velocity_medium_range         = 80    #Missile Velocity
turret_velocity_short_range          = 60
##########################################
turret_tick_interval_long_range     = 10
turret_tick_interval_medium_range   = 6     #Fire rate = 1 round per (this_value * turret_trigger_tick_interval) seconds
turret_tick_interval_short_range    = 2
##########################################
turret_ammo_precision_long_range    = 10000
turret_ammo_precision_medium_range  = 1000  #These values correspond to the fixed point multiplier setting when calculating agent lead in targeting scripts. Higher values = greater precision; necessary for longer ranges
turret_ammo_precision_short_range   = 1000
##########################################

max_lightsaber_sounds		= 20
bot_callout_chance			= 20

dev_persistent_slots_begin		= 200
patron_persistent_slots_begin	= 400

polls_disabled_wave_start		= 1
polls_disabled_time_increment	= 10 #Seconds

wabbajack_effects_human	 	= 0	#All effects possible
wabbajack_effects_gobblyng	= 1 #All effects except for armour randomisation
wabbajack_effects_boss		= 2 #Limited effects for bosses only
wabbajack_effects_special	= 3 #No equipment swapping effects
wabbajack_effects_mount		= 4 #Mount-specific effects only

wabbajack_mounts_begin = 0
wabbajack_mounts_end = 0

wabbajack_helmets_begin = 0
wabbajack_helmets_end = 0

wabbajack_armours_begin = 0
wabbajack_armours_end = 0

wabbajack_boots_begin = 0
wabbajack_boots_end = 0

wabbajack_gloves_begin = 0
wabbajack_gloves_end = 0

max_correctly_displayed_gold = 131071 #Values sent by server to clients above this number will overflow and corrupt

destructible_props_begin	= "spr_invasion_door_right"
destructible_props_end		= "spr_invasion_weather_box"
destructible_doors_begin	= destructible_props_begin
destructible_doors_end	=   "spr_portcullis_E"

item_cost_percentage_change_threshold 	= 150000	#Any item with an override price between 150000 and 150200 has its price changed by a % (150000 - 150099 = % decrease, 150100 - 150199 = % increase)
item_cost_percent_decrease 				= item_cost_percentage_change_threshold
item_cost_percent_increase 				= item_cost_percentage_change_threshold + 100
item_cost_recruit_tier	 				= item_cost_percent_decrease + 50		#Recruit classes receive a 50% discount on all items

defender_melee_gold_per_kill	= 120
defender_melee_gold_per_hit		= 55
defender_ranged_gold_per_kill	= 100
defender_ranged_gold_per_hit	= 100
defender_gold_bombs				= 50
gold_bonus_per_wave				= 1000
invasion_damage_reduction		= 65		#(%)
invasion_horse_bump_attrition	= 4			#(hp)

INVASION_WAVE_COUNT				= 34
MAX_ACTIVE_INVADERS				= 200
MAX_PARTIES_PER_WAVE			= 10
INVASION_DEATH_COOLDOWN_WAVES	= 2
INVASION_STARTING_LIVES         = 2
INVASION_MAX_LIVES              = 3
INVASION_RESPAWN_SECONDS        = 90
INVASION_WAVES_PER_LIFE         = 1

INVASION_STAGGER_COUNTER_THRESHOLD = 5
INVASION_STAGGER_RESISTANCE_DURATION = 10

defender_spawn_points_begin		= 0
defender_infantry_spawn_points	= 6
defender_ranged_spawn_points	= 3
defender_mounted_spawn_points	= 3

invader_spawn_points_begin		= 32
invader_infantry_spawn_points	= 6
invader_ranged_spawn_points		= 3
invader_mounted_spawn_points	= 3

MIN_HORDE_SIZE	= 20
MAX_HORDE_SIZE	= 60

PLAYER_BOT_MULTIPLIER		= 6

group_agent_spawn_interval	= 1
group_agent_spawn_density	= 7

group_spawns_at_wave_start		= 3
group_spawn_cooldown_min		= 1#10
group_spawn_cooldown_max		= 5#20
chance_of_elite_spawn			= 5
dev_wave_spawn_time_multiplier	= 10

stagger_1h_damage_threshold = 22
stagger_2h_damage_threshold = 32

leadership_buff_radius		= 20

leadership_banner_items_begin	=	"itm_banner_swadia"
leadership_banner_items_end		=	"itm_banner_items_end"

invasion_parties_array_begin		= 0
invasion_parties_array_end			= invasion_parties_array_begin + 200

invasion_spawn_array_begin			= invasion_parties_array_end
invasion_spawn_array_end			= invasion_spawn_array_begin + MAX_HORDE_SIZE

invasion_temp_array_begin			= invasion_spawn_array_end
invasion_temp_array_end				= invasion_temp_array_begin + 200

invasion_messages_begin				= "str_a_s1_is_attacking_s2"

horde_spawn_sounds_begin			= "snd_osiris_horde_spawned"

# FACTION TROOP LISTS INDEXES #

troop_colour_starter			= 0x80FF80
troop_colour_regular			= 0xFFFFFF
troop_colour_elite				= 0xEC8222
troop_colour_hero				= 0xFFC90E

lotr_gondor_classes_start		= "trp_gondorian_conscript_multiplayer"
lotr_gondor_classes_end			= "trp_eorling_militiaman_multiplayer"

lotr_rohan_classes_start		= "trp_eorling_militiaman_multiplayer"
lotr_rohan_classes_end			= "trp_lorien_forest_warden_multiplayer"

lotr_elven_classes_start		= "trp_lorien_forest_warden_multiplayer"
lotr_elven_classes_end			= "trp_dwarven_miner_multiplayer"

lotr_dwarven_classes_start		= "trp_dwarven_miner_multiplayer"
lotr_dwarven_classes_end		= "trp_dalesman_multiplayer"

lotr_dale_classes_start			= "trp_dalesman_multiplayer"
lotr_dale_classes_end			= "trp_uruk_hai_raider_multiplayer"

lotr_isengard_classes_start     = "trp_uruk_hai_raider_multiplayer"
lotr_isengard_classes_end       = "trp_uruk_youngling_multiplayer"

lotr_fos_classes_start			= "trp_uruk_youngling_multiplayer"
lotr_fos_classes_end			= "trp_palestinian_levy_multiplayer"

# HERO POWER INDEXES FOR MOD CONCEPTS #

hero_power_ammunition_stocks	= 2
hero_power_arcane_turret		= 3
hero_power_barrier              = 4
hero_power_battle_frenzy        = 5
hero_power_blink                = 6
hero_power_bodyguards           = 7
hero_power_cheat_death          = 8
hero_power_clones               = 9
hero_power_combat_stance        = 10
hero_power_corrupt		        = 11
hero_power_curse                = 12
hero_power_decoy                = 13
hero_power_explosive_ammo       = 14
hero_power_fear                 = 15
hero_power_group_heal           = 16
hero_power_indomitable          = 17
hero_power_inspire              = 18
hero_power_invisibility         = 19
hero_power_iron_steed           = 20
hero_power_mend_shields         = 21
hero_power_monster_slayer       = 22
hero_power_necromantic_aura     = 23
hero_power_pulse_of_undeath     = 24
hero_power_reflect              = 25
hero_power_reinforce            = 26
hero_power_siphon               = 27
hero_power_slow                 = 28
hero_power_soulbound            = 29
hero_power_summon_mount         = 30
hero_power_windrider            = 31
hero_power_windwalker           = 32

### EQUIPMENT SCREEN CONSTANTS ###

draw_weapons = 0
draw_helmets = 1
draw_armors = 2
draw_gloves = 3
draw_boots = 4
draw_horses = 5

slots_array_size = 200

slot_available_items_count = 5000
slot_available_items_begin = 5001

slot_available_item_types_begin = slot_available_items_begin + slots_array_size

slot_filled_temp_slots = slot_available_item_types_begin + slots_array_size
slot_available_item_temp_slots_begin = slot_filled_temp_slots + slots_array_size + 1
slot_available_item_temp_slots_end = slot_available_item_temp_slots_begin + slots_array_size

slot_priced_ordered_items_count = slot_available_item_temp_slots_end + slots_array_size
slot_priced_ordered_slots_begin = slot_priced_ordered_items_count + 1
slot_priced_ordered_slots_end = slot_priced_ordered_slots_begin + slots_array_size

slot_es_overlays_begin = slot_priced_ordered_slots_end + slots_array_size
slot_es_overlay_selected_item_overlays_begin = slot_es_overlays_begin
slot_es_overlay_selected_item_name = slot_es_overlay_selected_item_overlays_begin
slot_es_overlay_selected_item_type_label = slot_es_overlay_selected_item_overlays_begin + 1
slot_es_overlay_selected_item_type = slot_es_overlay_selected_item_overlays_begin + 2
slot_es_overlay_selected_item_price_label = slot_es_overlay_selected_item_overlays_begin + 3
slot_es_overlay_selected_item_price = slot_es_overlay_selected_item_overlays_begin + 4
slot_es_overlay_selected_item_swing_label = slot_es_overlay_selected_item_overlays_begin + 5
slot_es_overlay_selected_item_swing = slot_es_overlay_selected_item_overlays_begin + 6
slot_es_overlay_selected_item_thrust_label = slot_es_overlay_selected_item_overlays_begin + 7
slot_es_overlay_selected_item_thrust = slot_es_overlay_selected_item_overlays_begin + 8
slot_es_overlay_selected_item_length_label = slot_es_overlay_selected_item_overlays_begin + 9
slot_es_overlay_selected_item_length = slot_es_overlay_selected_item_overlays_begin + 10
slot_es_overlay_selected_item_speed_label = slot_es_overlay_selected_item_overlays_begin + 11
slot_es_overlay_selected_item_speed = slot_es_overlay_selected_item_overlays_begin + 12
slot_es_overlay_selected_item_weight_label = slot_es_overlay_selected_item_overlays_begin + 13
slot_es_overlay_selected_item_weight = slot_es_overlay_selected_item_overlays_begin + 14
slot_es_overlay_selected_item_damage_label = slot_es_overlay_selected_item_overlays_begin + 15
slot_es_overlay_selected_item_damage = slot_es_overlay_selected_item_overlays_begin + 16
slot_es_overlay_selected_item_clip_label = slot_es_overlay_selected_item_overlays_begin + 17
slot_es_overlay_selected_item_clip = slot_es_overlay_selected_item_overlays_begin + 18
slot_es_overlay_selected_item_accuracy_label = slot_es_overlay_selected_item_overlays_begin + 19
slot_es_overlay_selected_item_accuracy = slot_es_overlay_selected_item_overlays_begin + 20
slot_es_overlay_selected_item_size_label = slot_es_overlay_selected_item_overlays_begin + 21
slot_es_overlay_selected_item_size = slot_es_overlay_selected_item_overlays_begin + 22
slot_es_overlay_selected_item_hit_points_label = slot_es_overlay_selected_item_overlays_begin + 23
slot_es_overlay_selected_item_hit_points = slot_es_overlay_selected_item_overlays_begin + 24
slot_es_overlay_selected_item_resistance_label = slot_es_overlay_selected_item_overlays_begin + 25
slot_es_overlay_selected_item_resistance = slot_es_overlay_selected_item_overlays_begin + 26
slot_es_overlay_selected_item_maneuver_label = slot_es_overlay_selected_item_overlays_begin + 27
slot_es_overlay_selected_item_maneuver = slot_es_overlay_selected_item_overlays_begin + 28
slot_es_overlay_selected_item_charge_label = slot_es_overlay_selected_item_overlays_begin + 29
slot_es_overlay_selected_item_charge = slot_es_overlay_selected_item_overlays_begin + 30
slot_es_overlay_selected_item_body_armor_label = slot_es_overlay_selected_item_overlays_begin + 31
slot_es_overlay_selected_item_body_armor = slot_es_overlay_selected_item_overlays_begin + 32
slot_es_overlay_selected_item_head_armor_label = slot_es_overlay_selected_item_overlays_begin + 33
slot_es_overlay_selected_item_head_armor = slot_es_overlay_selected_item_overlays_begin + 34
slot_es_overlay_selected_item_leg_armor_label = slot_es_overlay_selected_item_overlays_begin + 35
slot_es_overlay_selected_item_leg_armor = slot_es_overlay_selected_item_overlays_begin + 36
slot_es_overlay_selected_item_shotgun = slot_es_overlay_selected_item_overlays_begin + 37
slot_es_overlay_selected_item_crush_through = slot_es_overlay_selected_item_overlays_begin + 38
slot_es_overlay_selected_item_knock_down = slot_es_overlay_selected_item_overlays_begin + 39
slot_es_overlay_selected_item_penetrate_shield = slot_es_overlay_selected_item_overlays_begin + 40
slot_es_overlay_selected_item_bonus_against_shield = slot_es_overlay_selected_item_overlays_begin + 41
slot_es_overlay_selected_item_knock_back = slot_es_overlay_selected_item_overlays_begin + 42
slot_es_overlay_selected_item_no_reload_moving_horseback = slot_es_overlay_selected_item_overlays_begin + 43
slot_es_overlay_selected_item_no_reload_horseback = slot_es_overlay_selected_item_overlays_begin + 44
slot_es_overlay_selected_item_cant_use_on_horseback = slot_es_overlay_selected_item_overlays_begin + 45
slot_es_overlay_selected_item_unbalanced = slot_es_overlay_selected_item_overlays_begin + 46
slot_es_overlay_selected_item_no_reload_while_moving = slot_es_overlay_selected_item_overlays_begin + 47
slot_es_overlay_selected_item_penalty_with_shield = slot_es_overlay_selected_item_overlays_begin + 48
slot_es_overlay_selected_item_no_parry = slot_es_overlay_selected_item_overlays_begin + 49
slot_es_overlay_selected_item_buff_desc = slot_es_overlay_selected_item_overlays_begin + 50
slot_es_overlay_selected_item_buff_1_icon = slot_es_overlay_selected_item_overlays_begin + 51
slot_es_overlay_selected_item_buff_1_val = slot_es_overlay_selected_item_overlays_begin + 52
slot_es_overlay_selected_item_buff_2_icon = slot_es_overlay_selected_item_overlays_begin + 53
slot_es_overlay_selected_item_buff_2_val = slot_es_overlay_selected_item_overlays_begin + 54
slot_es_overlay_selected_item_buff_3_icon = slot_es_overlay_selected_item_overlays_begin + 55
slot_es_overlay_selected_item_buff_3_val = slot_es_overlay_selected_item_overlays_begin + 56
slot_es_overlay_selected_item_extra_text = slot_es_overlay_selected_item_overlays_begin + 57
slot_es_overlay_selected_item_turret_damage_label = slot_es_overlay_selected_item_overlays_begin + 58
slot_es_overlay_selected_item_turret_damage = slot_es_overlay_selected_item_overlays_begin + 59
slot_es_overlay_selected_item_turret_range_label = slot_es_overlay_selected_item_overlays_begin + 60
slot_es_overlay_selected_item_turret_range = slot_es_overlay_selected_item_overlays_begin + 61
slot_es_overlay_selected_item_turret_ammo_label = slot_es_overlay_selected_item_overlays_begin + 62
slot_es_overlay_selected_item_turret_ammo = slot_es_overlay_selected_item_overlays_begin + 63
slot_es_overlay_selected_item_turret_fire_rate_label = slot_es_overlay_selected_item_overlays_begin + 64
slot_es_overlay_selected_item_turret_fire_rate = slot_es_overlay_selected_item_overlays_begin + 65
slot_es_overlay_selected_item_turret_shoot_speed_label = slot_es_overlay_selected_item_overlays_begin + 66
slot_es_overlay_selected_item_turret_shoot_speed = slot_es_overlay_selected_item_overlays_begin + 67

slot_es_overlay_selected_item_separator = slot_es_overlay_selected_item_overlays_begin + 68

slot_es_overlay_selected_item_2_type_label = slot_es_overlay_selected_item_overlays_begin + 69
slot_es_overlay_selected_item_2_type = slot_es_overlay_selected_item_overlays_begin + 70
slot_es_overlay_selected_item_2_price_label = slot_es_overlay_selected_item_overlays_begin + 71
slot_es_overlay_selected_item_2_price = slot_es_overlay_selected_item_overlays_begin + 72
slot_es_overlay_selected_item_2_swing_label = slot_es_overlay_selected_item_overlays_begin + 73
slot_es_overlay_selected_item_2_swing = slot_es_overlay_selected_item_overlays_begin + 74
slot_es_overlay_selected_item_2_thrust_label = slot_es_overlay_selected_item_overlays_begin + 75
slot_es_overlay_selected_item_2_thrust = slot_es_overlay_selected_item_overlays_begin + 76
slot_es_overlay_selected_item_2_length_label = slot_es_overlay_selected_item_overlays_begin + 77
slot_es_overlay_selected_item_2_length = slot_es_overlay_selected_item_overlays_begin + 78
slot_es_overlay_selected_item_2_speed_label = slot_es_overlay_selected_item_overlays_begin + 79
slot_es_overlay_selected_item_2_speed = slot_es_overlay_selected_item_overlays_begin + 80
slot_es_overlay_selected_item_2_crush_through = slot_es_overlay_selected_item_overlays_begin + 81
slot_es_overlay_selected_item_2_knock_down = slot_es_overlay_selected_item_overlays_begin + 82
slot_es_overlay_selected_item_2_bonus_against_shield = slot_es_overlay_selected_item_overlays_begin + 83
slot_es_overlay_selected_item_2_knock_back = slot_es_overlay_selected_item_overlays_begin + 84
slot_es_overlay_selected_item_2_cant_use_on_horseback = slot_es_overlay_selected_item_overlays_begin + 85
slot_es_overlay_selected_item_2_unbalanced = slot_es_overlay_selected_item_overlays_begin + 86
slot_es_overlay_selected_item_2_penalty_with_shield = slot_es_overlay_selected_item_overlays_begin + 87
slot_es_overlay_selected_item_2_no_parry = slot_es_overlay_selected_item_overlays_begin + 88

slot_es_overlay_equipped_item_overlays_begin = slot_es_overlay_selected_item_overlays_begin + 89
slot_es_overlay_equipped_item_name = slot_es_overlay_equipped_item_overlays_begin
slot_es_overlay_equipped_item_type_label = slot_es_overlay_equipped_item_overlays_begin + 1
slot_es_overlay_equipped_item_type = slot_es_overlay_equipped_item_overlays_begin + 2
slot_es_overlay_equipped_item_price_label = slot_es_overlay_equipped_item_overlays_begin + 3
slot_es_overlay_equipped_item_price = slot_es_overlay_equipped_item_overlays_begin + 4
slot_es_overlay_equipped_item_swing_label = slot_es_overlay_equipped_item_overlays_begin + 5
slot_es_overlay_equipped_item_swing = slot_es_overlay_equipped_item_overlays_begin + 6
slot_es_overlay_equipped_item_thrust_label = slot_es_overlay_equipped_item_overlays_begin + 7
slot_es_overlay_equipped_item_thrust = slot_es_overlay_equipped_item_overlays_begin + 8
slot_es_overlay_equipped_item_length_label = slot_es_overlay_equipped_item_overlays_begin + 9
slot_es_overlay_equipped_item_length = slot_es_overlay_equipped_item_overlays_begin + 10
slot_es_overlay_equipped_item_speed_label = slot_es_overlay_equipped_item_overlays_begin + 11
slot_es_overlay_equipped_item_speed = slot_es_overlay_equipped_item_overlays_begin + 12
slot_es_overlay_equipped_item_weight_label = slot_es_overlay_equipped_item_overlays_begin + 13
slot_es_overlay_equipped_item_weight = slot_es_overlay_equipped_item_overlays_begin + 14
slot_es_overlay_equipped_item_damage_label = slot_es_overlay_equipped_item_overlays_begin + 15
slot_es_overlay_equipped_item_damage = slot_es_overlay_equipped_item_overlays_begin + 16
slot_es_overlay_equipped_item_clip_label = slot_es_overlay_equipped_item_overlays_begin + 17
slot_es_overlay_equipped_item_clip = slot_es_overlay_equipped_item_overlays_begin + 18
slot_es_overlay_equipped_item_accuracy_label = slot_es_overlay_equipped_item_overlays_begin + 19
slot_es_overlay_equipped_item_accuracy = slot_es_overlay_equipped_item_overlays_begin + 20
slot_es_overlay_equipped_item_size_label = slot_es_overlay_equipped_item_overlays_begin + 21
slot_es_overlay_equipped_item_size = slot_es_overlay_equipped_item_overlays_begin + 22
slot_es_overlay_equipped_item_hit_points_label = slot_es_overlay_equipped_item_overlays_begin + 23
slot_es_overlay_equipped_item_hit_points = slot_es_overlay_equipped_item_overlays_begin + 24
slot_es_overlay_equipped_item_resistance_label = slot_es_overlay_equipped_item_overlays_begin + 25
slot_es_overlay_equipped_item_resistance = slot_es_overlay_equipped_item_overlays_begin + 26
slot_es_overlay_equipped_item_maneuver_label = slot_es_overlay_equipped_item_overlays_begin + 27
slot_es_overlay_equipped_item_maneuver = slot_es_overlay_equipped_item_overlays_begin + 28
slot_es_overlay_equipped_item_charge_label = slot_es_overlay_equipped_item_overlays_begin + 29
slot_es_overlay_equipped_item_charge = slot_es_overlay_equipped_item_overlays_begin + 30
slot_es_overlay_equipped_item_body_armor_label = slot_es_overlay_equipped_item_overlays_begin + 31
slot_es_overlay_equipped_item_body_armor = slot_es_overlay_equipped_item_overlays_begin + 32
slot_es_overlay_equipped_item_head_armor_label = slot_es_overlay_equipped_item_overlays_begin + 33
slot_es_overlay_equipped_item_head_armor = slot_es_overlay_equipped_item_overlays_begin + 34
slot_es_overlay_equipped_item_leg_armor_label = slot_es_overlay_equipped_item_overlays_begin + 35
slot_es_overlay_equipped_item_leg_armor = slot_es_overlay_equipped_item_overlays_begin + 36
slot_es_overlay_equipped_item_shotgun = slot_es_overlay_equipped_item_overlays_begin + 37
slot_es_overlay_equipped_item_crush_through = slot_es_overlay_equipped_item_overlays_begin + 38
slot_es_overlay_equipped_item_knock_down = slot_es_overlay_equipped_item_overlays_begin + 39
slot_es_overlay_equipped_item_penetrate_shield = slot_es_overlay_equipped_item_overlays_begin + 40
slot_es_overlay_equipped_item_bonus_against_shield = slot_es_overlay_equipped_item_overlays_begin + 41
slot_es_overlay_equipped_item_knock_back = slot_es_overlay_equipped_item_overlays_begin + 42
slot_es_overlay_equipped_item_no_reload_moving_horseback = slot_es_overlay_equipped_item_overlays_begin + 43
slot_es_overlay_equipped_item_no_reload_horseback = slot_es_overlay_equipped_item_overlays_begin + 44
slot_es_overlay_equipped_item_cant_use_on_horseback = slot_es_overlay_equipped_item_overlays_begin + 45
slot_es_overlay_equipped_item_unbalanced = slot_es_overlay_equipped_item_overlays_begin + 46
slot_es_overlay_equipped_item_no_reload_while_moving = slot_es_overlay_equipped_item_overlays_begin + 47
slot_es_overlay_equipped_item_penalty_with_shield = slot_es_overlay_equipped_item_overlays_begin + 48
slot_es_overlay_equipped_item_no_parry = slot_es_overlay_equipped_item_overlays_begin + 49
slot_es_overlay_equipped_item_buff_desc = slot_es_overlay_equipped_item_overlays_begin + 50
slot_es_overlay_equipped_item_buff_1_icon = slot_es_overlay_equipped_item_overlays_begin + 51
slot_es_overlay_equipped_item_buff_1_val = slot_es_overlay_equipped_item_overlays_begin + 52
slot_es_overlay_equipped_item_buff_2_icon = slot_es_overlay_equipped_item_overlays_begin + 53
slot_es_overlay_equipped_item_buff_2_val = slot_es_overlay_equipped_item_overlays_begin + 54
slot_es_overlay_equipped_item_buff_3_icon = slot_es_overlay_equipped_item_overlays_begin + 55
slot_es_overlay_equipped_item_buff_3_val = slot_es_overlay_equipped_item_overlays_begin + 56
slot_es_overlay_equipped_item_extra_text = slot_es_overlay_equipped_item_overlays_begin + 57
slot_es_overlay_equipped_item_turret_damage_label = slot_es_overlay_equipped_item_overlays_begin + 58
slot_es_overlay_equipped_item_turret_damage = slot_es_overlay_equipped_item_overlays_begin + 59
slot_es_overlay_equipped_item_turret_range_label = slot_es_overlay_equipped_item_overlays_begin + 60
slot_es_overlay_equipped_item_turret_range = slot_es_overlay_equipped_item_overlays_begin + 61
slot_es_overlay_equipped_item_turret_ammo_label = slot_es_overlay_equipped_item_overlays_begin + 62
slot_es_overlay_equipped_item_turret_ammo = slot_es_overlay_equipped_item_overlays_begin + 63
slot_es_overlay_equipped_item_turret_fire_rate_label = slot_es_overlay_equipped_item_overlays_begin + 64
slot_es_overlay_equipped_item_turret_fire_rate = slot_es_overlay_equipped_item_overlays_begin + 65
slot_es_overlay_equipped_item_turret_shoot_speed_label = slot_es_overlay_equipped_item_overlays_begin + 66
slot_es_overlay_equipped_item_turret_shoot_speed = slot_es_overlay_equipped_item_overlays_begin + 67

slot_es_overlay_equipped_item_separator = slot_es_overlay_equipped_item_overlays_begin + 68

slot_es_overlay_equipped_item_2_type_label = slot_es_overlay_equipped_item_overlays_begin + 69
slot_es_overlay_equipped_item_2_type = slot_es_overlay_equipped_item_overlays_begin + 70
slot_es_overlay_equipped_item_2_price_label = slot_es_overlay_equipped_item_overlays_begin + 71
slot_es_overlay_equipped_item_2_price = slot_es_overlay_equipped_item_overlays_begin + 72
slot_es_overlay_equipped_item_2_swing_label = slot_es_overlay_equipped_item_overlays_begin + 73
slot_es_overlay_equipped_item_2_swing = slot_es_overlay_equipped_item_overlays_begin + 74
slot_es_overlay_equipped_item_2_thrust_label = slot_es_overlay_equipped_item_overlays_begin + 75
slot_es_overlay_equipped_item_2_thrust = slot_es_overlay_equipped_item_overlays_begin + 76
slot_es_overlay_equipped_item_2_length_label = slot_es_overlay_equipped_item_overlays_begin + 77
slot_es_overlay_equipped_item_2_length = slot_es_overlay_equipped_item_overlays_begin + 78
slot_es_overlay_equipped_item_2_speed_label = slot_es_overlay_equipped_item_overlays_begin + 79
slot_es_overlay_equipped_item_2_speed = slot_es_overlay_equipped_item_overlays_begin + 80
slot_es_overlay_equipped_item_2_crush_through = slot_es_overlay_equipped_item_overlays_begin + 81
slot_es_overlay_equipped_item_2_knock_down = slot_es_overlay_equipped_item_overlays_begin + 82
slot_es_overlay_equipped_item_2_bonus_against_shield = slot_es_overlay_equipped_item_overlays_begin + 83
slot_es_overlay_equipped_item_2_knock_back = slot_es_overlay_equipped_item_overlays_begin + 84
slot_es_overlay_equipped_item_2_cant_use_on_horseback = slot_es_overlay_equipped_item_overlays_begin + 85
slot_es_overlay_equipped_item_2_unbalanced = slot_es_overlay_equipped_item_overlays_begin + 86
slot_es_overlay_equipped_item_2_penalty_with_shield = slot_es_overlay_equipped_item_overlays_begin + 87
slot_es_overlay_equipped_item_2_no_parry = slot_es_overlay_equipped_item_overlays_begin + 88

slot_es_overlays_end = slot_es_overlay_equipped_item_2_no_parry + 1

slot_wp_array_begin = slot_es_overlays_end
slot_wp_array_end = slot_wp_array_begin + 7