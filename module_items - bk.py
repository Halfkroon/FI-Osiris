from module_constants import *
from ID_factions import *
from header_items import  *
from header_operations import *
from header_triggers import *

####################################################################################################################
#  Each item record contains the following fields:
#  1) Item id: used for referencing items in other files.
#     The prefix itm_ is automatically added before each item id.
#  2) Item name. Name of item as it'll appear in inventory window
#  3) List of meshes.  Each mesh record is a tuple containing the following fields:
#    3.1) Mesh name.
#    3.2) Modifier bits that this mesh matches.
#     Note that the first mesh record is the default.
#  4) Item flags. See header_items.py for a list of available flags.
#  5) Item capabilities. Used for which animations this item is used with. See header_items.py for a list of available flags.
#  6) Item value.
#  7) Item stats: Bitwise-or of various stats about the item such as:
#      weight, abundance, difficulty, head_armor, body_armor,leg_armor, etc...
#  8) Modifier bits: Modifiers that can be applied to this item.
#  9) [Optional] Triggers: List of simple triggers to be associated with the item.
#  10) [Optional] Factions: List of factions that item can be found as merchandise.
####################################################################################################################

# Some constants for ease of use.
imodbits_none = 0
imodbits_horse_basic = imodbit_swaybacked|imodbit_lame|imodbit_spirited|imodbit_heavy|imodbit_stubborn
imodbits_cloth  = imodbit_tattered | imodbit_ragged | imodbit_sturdy | imodbit_thick | imodbit_hardened
imodbits_armor  = imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_plate  = imodbit_cracked | imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_polearm = imodbit_cracked | imodbit_bent | imodbit_balanced
imodbits_shield  = imodbit_cracked | imodbit_battered |imodbit_thick | imodbit_reinforced
imodbits_sword   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered
imodbits_sword_high   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered|imodbit_masterwork
imodbits_axe   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_mace   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_pick   = imodbit_rusty | imodbit_chipped | imodbit_balanced | imodbit_heavy
imodbits_bow = imodbit_cracked | imodbit_bent | imodbit_strong |imodbit_masterwork
imodbits_crossbow = imodbit_cracked | imodbit_bent | imodbit_masterwork
imodbits_missile   = imodbit_bent | imodbit_large_bag
imodbits_thrown   = imodbit_bent | imodbit_heavy| imodbit_balanced| imodbit_large_bag
imodbits_thrown_minus_heavy = imodbit_bent | imodbit_balanced| imodbit_large_bag

imodbits_horse_good = imodbit_spirited|imodbit_heavy
imodbits_good   = imodbit_sturdy | imodbit_thick | imodbit_hardened | imodbit_reinforced
imodbits_bad    = imodbit_rusty | imodbit_chipped | imodbit_tattered | imodbit_ragged | imodbit_cracked | imodbit_bent
# Replace winged mace/spiked mace with: Flanged mace / Knobbed mace?
# Fauchard (majowski glaive) 
items = [
# item_name, mesh_name, item_properties, item_capabilities, slot_no, cost, bonus_flags, weapon_flags, scale, view_dir, pos_offset
 ["no_item","INVALID ITEM", [("invalid_item",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(90)|swing_damage(16,blunt)|thrust_damage(10,blunt),imodbits_none],

 ["tutorial_spear", "Spear", [("spear",0)], itp_type_polearm| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 0 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(0 , cut) | thrust_damage(19 ,  pierce),imodbits_polearm ],
 ["tutorial_club", "Club", [("club",0)], itp_type_one_handed_wpn| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 0 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
 ["tutorial_battle_axe", "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(5)|difficulty(0)|spd_rtng(88) | weapon_length(108)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["tutorial_arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(0,pierce)|max_ammo(20),imodbits_missile],
 ["tutorial_bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|abundance(90)|weapon_length(55)|thrust_damage(0,pierce)|max_ammo(18),imodbits_missile],
 ["tutorial_short_bow", "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 0 , weight(1)|difficulty(0)|spd_rtng(98) | shoot_speed(49) | thrust_damage(12 ,  pierce  ),imodbits_bow ],
 ["tutorial_crossbow", "Crossbow", [("crossbow",0)], itp_type_crossbow |itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0 , weight(3)|difficulty(0)|spd_rtng(42)|  shoot_speed(68) | thrust_damage(32,pierce)|max_ammo(1),imodbits_crossbow ],
 ["tutorial_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|difficulty(0)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16 ,  cut)|max_ammo(14)|weapon_length(0),imodbits_missile ],
 ["tutorial_saddle_horse", "Saddle Horse", [("saddle_horse",0)], itp_type_horse, 0, 0,abundance(90)|body_armor(3)|difficulty(0)|horse_speed(40)|horse_maneuver(38)|horse_charge(8),imodbits_horse_basic],
 ["tutorial_shield", "Kite Shield", [("shield_kite_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(150),imodbits_shield ],
 ["tutorial_staff_no_attack","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_parry_polearm|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(0,blunt) | thrust_damage(0,blunt),imodbits_none],
 ["tutorial_staff","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(16,blunt) | thrust_damage(16,blunt),imodbits_none],
 ["tutorial_sword", "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(18 , cut) | thrust_damage(15 ,  pierce),imodbits_sword ],
 ["tutorial_axe", "Axe", [("iron_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(4)|difficulty(0)|spd_rtng(91) | weapon_length(108)|swing_damage(19 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

 ["tutorial_dagger","Dagger", [("practice_dagger",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(40)|swing_damage(16,blunt)|thrust_damage(10,blunt),imodbits_none],


 ["horse_meat","Horse Meat", [("raw_meat",0)], itp_type_goods|itp_consumable|itp_food, 0, 12,weight(40)|food_quality(30)|max_ammo(40),imodbits_none],
# Items before this point are hardwired and their order should not be changed!
 ["practice_sword","Practice Sword", [("practice_sword",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_wooden_parry|itp_wooden_attack, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(90)|swing_damage(22,blunt)|thrust_damage(20,blunt),imodbits_none],
 ["heavy_practice_sword","Heavy Practice Sword", [("heavy_practicesword",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_greatsword,
    21, weight(6.25)|spd_rtng(94)|weapon_length(128)|swing_damage(30,blunt)|thrust_damage(24,blunt),imodbits_none],
 ["practice_dagger","Practice Dagger", [("practice_dagger",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_no_parry|itp_wooden_attack, itc_dagger|itcf_carry_dagger_front_left, 2,weight(0.5)|spd_rtng(110)|weapon_length(47)|swing_damage(16,blunt)|thrust_damage(14,blunt),imodbits_none],
 ["practice_axe", "Practice Axe", [("hatchet",0)], itp_type_one_handed_wpn| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 24 , weight(2) | spd_rtng(95) | weapon_length(75) | swing_damage(24, blunt) | thrust_damage(0, pierce), imodbits_axe],
 ["arena_axe", "Axe", [("arena_axe",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 137 , weight(1.5)|spd_rtng(100) | weapon_length(69)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["arena_sword", "Sword", [("arena_sword_one_handed",0),("sword_medieval_b_scabbard", ixmesh_carry),], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 243 , weight(1.5)|spd_rtng(99) | weapon_length(95)|swing_damage(22 , blunt) | thrust_damage(20 ,  blunt),imodbits_sword_high ],
 ["arena_sword_two_handed",  "Two Handed Sword", [("arena_sword_two_handed",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 670 , weight(2.75)|spd_rtng(93) | weapon_length(110)|swing_damage(30 , blunt) | thrust_damage(24 ,  blunt),imodbits_sword_high ],
 ["arena_lance",         "Lance", [("arena_lance",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear,
 90 , weight(2.5)|spd_rtng(96) | weapon_length(150)|swing_damage(20 , blunt) | thrust_damage(25 ,  blunt),imodbits_polearm ],
 ["practice_staff","Practice Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(2.5)|spd_rtng(103) | weapon_length(118)|swing_damage(18,blunt) | thrust_damage(18,blunt),imodbits_none],
 ["practice_lance","Practice Lance", [("joust_of_peace",0)], itp_couchable|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack, itc_greatlance, 18,weight(4.25)|spd_rtng(58)|weapon_length(240)|swing_damage(0,blunt)|thrust_damage(15,blunt),imodbits_none],
 ["practice_shield","Practice Shield", [("shield_round_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 20,weight(3.5)|body_armor(1)|hit_points(200)|spd_rtng(100)|shield_width(50),imodbits_none],
 ["practice_bow","Practice Bow", [("hunting_bow",0), ("hunting_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 0, weight(1.5)|spd_rtng(90) | shoot_speed(40) | thrust_damage(21, blunt),imodbits_bow ],
##                                                     ("hunting_bow",0)],                  itp_type_bow|itp_two_handed|itp_primary|itp_attach_left_hand, itcf_shoot_bow, 4,weight(1.5)|spd_rtng(90)|shoot_speed(40)|thrust_damage(19,blunt),imodbits_none],
 ["practice_crossbow", "Practice Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0, weight(3)|spd_rtng(42)| shoot_speed(68) | thrust_damage(32,blunt)|max_ammo(1),imodbits_crossbow],
 ["practice_javelin", "Practice Javelins", [("javelin",0),("javelins_quiver_new", ixmesh_carry)], itp_type_thrown |itp_primary|itp_next_item_as_melee,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 0, weight(5) | spd_rtng(91) | shoot_speed(28) | thrust_damage(27, blunt) | max_ammo(50) | weapon_length(75), imodbits_thrown],
 ["practice_javelin_melee", "practice_javelin_melee", [("javelin",0)], itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry , itc_staff, 0, weight(1)|difficulty(0)|spd_rtng(91) |swing_damage(12, blunt)| thrust_damage(14,  blunt)|weapon_length(75),imodbits_polearm ],
 ["practice_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16, blunt)|max_ammo(10)|weapon_length(0),imodbits_thrown ],
 ["practice_throwing_daggers_100_amount", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16, blunt)|max_ammo(100)|weapon_length(0),imodbits_thrown ],
# ["cheap_shirt","Cheap Shirt", [("shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 4,weight(1.25)|body_armor(3),imodbits_none],
 ["practice_horse","Practice Horse", [("saddle_horse",0)], itp_type_horse, 0, 37,body_armor(10)|horse_speed(40)|horse_maneuver(37)|horse_charge(14),imodbits_none],
 ["practice_arrows","Practice Arrows", [("arena_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_missile],
## ["practice_arrows","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo)], itp_type_arrows, 0, 31,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_none],
 ["practice_bolts","Practice Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|weapon_length(55)|max_ammo(49),imodbits_missile],
 ["practice_arrows_10_amount","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(10),imodbits_missile],
 ["practice_arrows_100_amount","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(100),imodbits_missile],
 ["practice_bolts_9_amount","Practice Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|weapon_length(55)|max_ammo(9),imodbits_missile],
 ["practice_boots", "Practice Boots", [("boot_nomad_a",0)], itp_type_foot_armor |itp_civilian  | itp_attach_armature,0, 11 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10), imodbits_cloth ],
 ["red_tourney_armor","Red Tourney Armor", [("tourn_armor_a",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["blue_tourney_armor","Blue Tourney Armor", [("mail_shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["green_tourney_armor","Green Tourney Armor", [("leather_vest",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["gold_tourney_armor","Gold Tourney Armor", [("padded_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["red_tourney_helmet","Red Tourney Helmet",[("flattop_helmet",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["blue_tourney_helmet","Blue Tourney Helmet",[("segmented_helm",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["green_tourney_helmet","Green Tourney Helmet",[("hood_c",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["gold_tourney_helmet","Gold Tourney Helmet",[("hood_a",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],

["arena_shield_red", "Shield", [("arena_shield_red",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_blue", "Shield", [("arena_shield_blue",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_green", "Shield", [("arena_shield_green",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_yellow", "Shield", [("arena_shield_yellow",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],

["arena_armor_white", "Arena Armor White", [("arena_armorW_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_red", "Arena Armor Red", [("arena_armorR_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_blue", "Arena Armor Blue", [("arena_armorB_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_green", "Arena Armor Green", [("arena_armorG_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_yellow", "Arena Armor Yellow", [("arena_armorY_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_tunic_white", "Arena Tunic White ", [("arena_tunicW_new",0)], itp_type_body_armor |itp_covers_legs ,0, 47 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_red", "Arena Tunic Red", [("arena_tunicR_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ], 
["arena_tunic_blue", "Arena Tunic Blue", [("arena_tunicB_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ], 
["arena_tunic_green", "Arena Tunic Green", [("arena_tunicG_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_yellow", "Arena Tunic Yellow", [("arena_tunicY_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
#headwear
["arena_helmet_red", "Arena Helmet Red", [("arena_helmetR",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_blue", "Arena Helmet Blue", [("arena_helmetB",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_green", "Arena Helmet Green", [("arena_helmetG",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_yellow", "Arena Helmet Yellow", [("arena_helmetY",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["steppe_helmet_white", "Steppe Helmet White", [("steppe_helmetW",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_red", "Steppe Helmet Red", [("steppe_helmetR",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_blue", "Steppe Helmet Blue", [("steppe_helmetB",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_green", "Steppe Helmet Green", [("steppe_helmetG",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_yellow", "Steppe Helmet Yellow", [("steppe_helmetY",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["tourney_helm_white", "Tourney Helm White", [("tourney_helmR",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_red", "Tourney Helm Red", [("tourney_helmR",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_blue", "Tourney Helm Blue", [("tourney_helmB",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_green", "Tourney Helm Green", [("tourney_helmG",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_yellow", "Tourney Helm Yellow", [("tourney_helmY",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_red", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_blue", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_green", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_yellow", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],

# A treatise on The Method of Mechanical Theorems Archimedes
 
#This book must be at the beginning of readable books
 ["book_tactics","De Re Militari", [("book_a",0)], itp_type_book, 0, 4000,weight(2)|abundance(100),imodbits_none],
 ["book_persuasion","Rhetorica ad Herennium", [("book_b",0)], itp_type_book, 0, 5000,weight(2)|abundance(100),imodbits_none],
 ["book_leadership","The Life of Alixenus the Great", [("book_d",0)], itp_type_book, 0, 4200,weight(2)|abundance(100),imodbits_none],
 ["book_intelligence","Essays on Logic", [("book_e",0)], itp_type_book, 0, 2900,weight(2)|abundance(100),imodbits_none],
 ["book_trade","A Treatise on the Value of Things", [("book_f",0)], itp_type_book, 0, 3100,weight(2)|abundance(100),imodbits_none],
 ["book_weapon_mastery", "On the Art of Fighting with Swords", [("book_d",0)], itp_type_book, 0, 4200,weight(2)|abundance(100),imodbits_none],
 ["book_engineering","Method of Mechanical Theorems", [("book_open",0)], itp_type_book, 0, 4000,weight(2)|abundance(100),imodbits_none],

#Reference books
#This book must be at the beginning of reference books
 ["book_wound_treatment_reference","The Book of Healing", [("book_c",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],
 ["book_training_reference","Manual of Arms", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],
 ["book_surgery_reference","The Great Book of Surgery", [("book_c",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],

 #other trade goods (first one is spice)
 ["spice","Spice", [("spice_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 880,weight(40)|abundance(25)|max_ammo(50),imodbits_none],
 ["salt","Salt", [("salt_sack",0)], itp_merchandise|itp_type_goods, 0, 255,weight(50)|abundance(120),imodbits_none],


 #["flour","Flour", [("salt_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 40,weight(50)|abundance(100)|food_quality(45)|max_ammo(50),imodbits_none],

 ["oil","Oil", [("oil",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 450,weight(50)|abundance(60)|max_ammo(50),imodbits_none],

 ["pottery","Pottery", [("jug",0)], itp_merchandise|itp_type_goods, 0, 100,weight(50)|abundance(90),imodbits_none],

 ["raw_flax","Flax Bundle", [("raw_flax",0)], itp_merchandise|itp_type_goods, 0, 150,weight(40)|abundance(90),imodbits_none],
 ["linen","Linen", [("linen",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbits_none],

 ["wool","Wool", [("wool_sack",0)], itp_merchandise|itp_type_goods, 0, 130,weight(40)|abundance(90),imodbits_none],
 ["wool_cloth","Wool Cloth", [("wool_cloth",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbits_none],

 ["raw_silk","Raw Silk", [("raw_silk_bundle",0)], itp_merchandise|itp_type_goods, 0, 600,weight(30)|abundance(90),imodbits_none],
 ["raw_dyes","Dyes", [("dyes",0)], itp_merchandise|itp_type_goods, 0, 200,weight(10)|abundance(90),imodbits_none],
 ["velvet","Velvet", [("velvet",0)], itp_merchandise|itp_type_goods, 0, 1025,weight(40)|abundance(30),imodbits_none],

 ["iron","Iron", [("iron",0)], itp_merchandise|itp_type_goods, 0,264,weight(60)|abundance(60),imodbits_none],
 ["tools","Tools", [("iron_hammer",0)], itp_merchandise|itp_type_goods, 0, 410,weight(50)|abundance(90),imodbits_none],

 ["raw_leather","Hides", [("leatherwork_inventory",0)], itp_merchandise|itp_type_goods, 0, 120,weight(40)|abundance(90),imodbits_none],
 ["leatherwork","Leatherwork", [("leatherwork_frame",0)], itp_merchandise|itp_type_goods, 0, 220,weight(40)|abundance(90),imodbits_none],
 
 ["raw_date_fruit","Date Fruit", [("date_inventory",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 120,weight(40)|food_quality(10)|max_ammo(10),imodbits_none],
 ["furs","Furs", [("fur_pack",0)], itp_merchandise|itp_type_goods, 0, 391,weight(40)|abundance(90),imodbits_none],

 ["wine","Wine", [("amphora_slim",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 220,weight(30)|abundance(60)|max_ammo(50),imodbits_none],
 ["ale","Ale", [("ale_barrel",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 120,weight(30)|abundance(70)|max_ammo(50),imodbits_none],

# ["dry_bread", "wheat_sack", itp_type_goods|itp_consumable, 0, slt_none,view_goods,95,weight(2),max_ammo(50),imodbits_none],
#foods (first one is smoked_fish)
 ["smoked_fish","Smoked Fish", [("smoked_fish",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(110)|food_quality(50)|max_ammo(50),imodbits_none],
 ["cheese","Cheese", [("cheese_b",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 ["honey","Honey", [("honey_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 220,weight(5)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 ["sausages","Sausages", [("sausages",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(10)|abundance(110)|food_quality(40)|max_ammo(40),imodbits_none],
 ["cabbages","Cabbages", [("cabbage",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 30,weight(15)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["dried_meat","Dried Meat", [("smoked_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbits_none],
 ["apples","Fruit", [("apple_basket",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 44,weight(20)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["raw_grapes","Grapes", [("grapes_inventory",0)], itp_merchandise|itp_consumable|itp_type_goods, 0, 75,weight(40)|abundance(90)|food_quality(10)|max_ammo(10),imodbits_none], #x2 for wine
 ["raw_olives","Olives", [("olive_inventory",0)], itp_merchandise|itp_consumable|itp_type_goods, 0, 100,weight(40)|abundance(90)|food_quality(10)|max_ammo(10),imodbits_none], #x3 for oil
 ["grain","Grain", [("wheat_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 30,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],

 ["cattle_meat","Beef", [("raw_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 80,weight(20)|abundance(100)|food_quality(80)|max_ammo(50),imodbits_none],
 ["bread","Bread", [("bread_a",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 50,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["chicken","Chicken", [("chicken",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 95,weight(10)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["pork","Pork", [("pork",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbits_none],
 ["butter","Butter", [("butter_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 150,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 

 #Would like to remove flour altogether and reduce chicken, pork and butter (perishables) to non-trade items. Apples could perhaps become a generic "fruit", also representing dried fruit and grapes
 # Armagan: changed order so that it'll be easier to remove them from trade goods if necessary.
#************************************************************************************************
# ITEMS before this point are hardcoded into item_codes.h and their order should not be changed!
#************************************************************************************************

# Quest Items

 ["siege_supply","Supplies", [("ale_barrel",0)], itp_type_goods, 0, 96,weight(40)|abundance(70),imodbits_none],
 ["quest_wine","Wine", [("amphora_slim",0)], itp_type_goods, 0, 46,weight(40)|abundance(60)|max_ammo(50),imodbits_none],
 ["quest_ale","Ale", [("ale_barrel",0)], itp_type_goods, 0, 31,weight(40)|abundance(70)|max_ammo(50),imodbits_none],

 
# Tutorial Items

 ["tutorial_sword", "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(18 , cut) | thrust_damage(15 ,  pierce),imodbits_sword ],
 ["tutorial_axe", "Axe", [("iron_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(4)|difficulty(0)|spd_rtng(91) | weapon_length(108)|swing_damage(19 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["tutorial_spear", "Spear", [("spear",0)], itp_type_polearm| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 0 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(0 , cut) | thrust_damage(19 ,  pierce),imodbits_polearm ],
 ["tutorial_club", "Club", [("club",0)], itp_type_one_handed_wpn| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 0 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
 ["tutorial_battle_axe", "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(5)|difficulty(0)|spd_rtng(88) | weapon_length(108)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["tutorial_arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(0,pierce)|max_ammo(20),imodbits_missile],
 ["tutorial_bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(0,pierce)|max_ammo(18),imodbits_missile],
 ["tutorial_short_bow", "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 0 , weight(1)|difficulty(0)|spd_rtng(98) | shoot_speed(49) | thrust_damage(12 ,  pierce  ),imodbits_bow ],
 ["tutorial_crossbow", "Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0 , weight(3)|difficulty(0)|spd_rtng(42)|  shoot_speed(68) | thrust_damage(32,pierce)|max_ammo(1),imodbits_crossbow ],
 ["tutorial_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|difficulty(0)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16 ,  cut)|max_ammo(14)|weapon_length(0),imodbits_missile ],
 ["tutorial_saddle_horse", "Saddle Horse", [("saddle_horse",0)], itp_type_horse, 0, 0,abundance(90)|body_armor(3)|difficulty(0)|horse_speed(40)|horse_maneuver(38)|horse_charge(8),imodbits_horse_basic],
 ["tutorial_shield", "Kite Shield", [("shield_kite_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(150),imodbits_shield ],
 ["tutorial_staff_no_attack","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_parry_polearm|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(0,blunt) | thrust_damage(0,blunt),imodbits_none],
 ["tutorial_staff","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(16,blunt) | thrust_damage(16,blunt),imodbits_none],

# Horses: sumpter horse/ pack horse, saddle horse, steppe horse, warm blood, geldling, stallion,   war mount, charger, 
# Carthorse, hunter, heavy hunter, hackney, palfrey, courser, destrier.
 ["sumpter_horse","Sumpter Horse", [("sumpter_horse",0)], itp_merchandise|itp_type_horse, 0, 600,abundance(90)|hit_points(100)|body_armor(14)|difficulty(1)|horse_speed(37)|horse_maneuver(39)|horse_charge(9)|horse_scale(100),imodbits_horse_basic],
 ["saddle_horse","Saddle Horse", [("saddle_horse",0),("horse_c",imodbits_horse_good)], itp_merchandise|itp_type_horse, 0, 675,abundance(90)|hit_points(100)|body_armor(8)|difficulty(1)|horse_speed(45)|horse_maneuver(44)|horse_charge(10)|horse_scale(104),imodbits_horse_basic],
 ["steppe_horse","Steppe Horse", [("steppe_horse",0)], itp_merchandise|itp_type_horse, 0, 750,abundance(80)|hit_points(120)|body_armor(10)|difficulty(2)|horse_speed(40)|horse_maneuver(51)|horse_charge(8)|horse_scale(98),imodbits_horse_basic, [], [fac_kingdom_2, fac_kingdom_3]],
 ["arabian_horse_a","Desert Horse", [("arabian_horse_a",0)], itp_merchandise|itp_type_horse, 0, 850,abundance(80)|hit_points(110)|body_armor(10)|difficulty(2)|horse_speed(42)|horse_maneuver(50)|horse_charge(12)|horse_scale(100),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3, fac_kingdom_6]],
 ["courser","Courser", [("courser",0)], itp_merchandise|itp_type_horse, 0, 925,abundance(70)|body_armor(12)|hit_points(110)|difficulty(2)|horse_speed(50)|horse_maneuver(44)|horse_charge(12)|horse_scale(106),imodbits_horse_basic|imodbit_champion],
 ["arabian_horse_b","Sarranid Horse", [("arabian_horse_b",0)], itp_merchandise|itp_type_horse, 0, 1075,abundance(80)|hit_points(120)|body_armor(10)|difficulty(3)|horse_speed(43)|horse_maneuver(54)|horse_charge(16)|horse_scale(100),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6]],
 ["hunter","Hunter", [("hunting_horse",0),("hunting_horse",imodbits_horse_good)], itp_merchandise|itp_type_horse, 0, 1525,abundance(60)|hit_points(160)|body_armor(18)|difficulty(3)|horse_speed(43)|horse_maneuver(44)|horse_charge(24)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
 ["warhorse","War Horse", [("warhorse_chain",0)], itp_merchandise|itp_type_horse, 0, 2125,abundance(50)|hit_points(165)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(41)|horse_charge(28)|horse_scale(110),imodbits_horse_basic|imodbit_champion],
 ["charger","Charger", [("charger_new",0)], itp_merchandise|itp_type_horse, 0, 2700,abundance(40)|hit_points(165)|body_armor(58)|difficulty(4)|horse_speed(40)|horse_maneuver(44)|horse_charge(32)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1, fac_kingdom_5]],



#whalebone crossbow, yew bow, war bow, arming sword 
 ["arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows|itp_merchandise|itp_default_ammo, itcf_carry_quiver_back, 
 100,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(1,pierce)|max_ammo(30),imodbits_missile],
 ["khergit_arrows","Khergit Arrows", [("arrow_b",0),("flying_missile",ixmesh_flying_ammo),("quiver_b", ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 
 300,weight(3.5)|abundance(30)|weapon_length(95)|thrust_damage(3,pierce)|max_ammo(28),imodbits_missile],
 ["barbed_arrows","Barbed Arrows", [("barbed_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_d", ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 
 200,weight(3)|abundance(70)|weapon_length(95)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile],
 ["bodkin_arrows","Bodkin Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c", ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 
 300,weight(3)|abundance(50)|weapon_length(91)|thrust_damage(3,pierce)|max_ammo(28),imodbits_missile],
 ["bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise|itp_default_ammo|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 
 100,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(1,pierce)|max_ammo(30),imodbits_missile],
 ["steel_bolts","Steel Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag_c", ixmesh_carry)], itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 
 200,weight(2.5)|abundance(20)|weapon_length(63)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile],
 ["cartridges","Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 
 100,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(1,pierce)|max_ammo(50),imodbits_missile],

["pilgrim_disguise", "Pilgrim Disguise", [("pilgrim_outfit",0)], 0| itp_type_body_armor |itp_covers_legs |itp_civilian ,0, 2180 , weight(2)|abundance(100)|head_armor(0)|body_armor(19)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["pilgrim_hood", "Pilgrim Hood", [("pilgrim_hood",0)], 0| itp_type_head_armor |itp_civilian  ,0, 700 , weight(1.25)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],

# ARMOR
#handwear
["leather_gloves","Leather Gloves", [("leather_gloves_L",0)], itp_merchandise|itp_type_hand_armor,0, 150, weight(0.25)|abundance(120)|body_armor(1)|difficulty(0),imodbits_cloth],
["mail_mittens","Mail Mittens", [("mail_mittens_L",0)], itp_merchandise|itp_type_hand_armor,0, 300, weight(0.5)|abundance(100)|body_armor(2)|difficulty(0),imodbits_armor],
["scale_gauntlets","Scale Gauntlets", [("scale_gauntlets_b_L",0)], itp_merchandise|itp_type_hand_armor,0, 750, weight(0.75)|abundance(100)|body_armor(5)|difficulty(0),imodbits_armor],
["lamellar_gauntlets","Lamellar Gauntlets", [("scale_gauntlets_a_L",0)], itp_merchandise|itp_type_hand_armor,0, 900, weight(0.9)|abundance(100)|body_armor(6)|difficulty(0),imodbits_armor],
["gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_merchandise|itp_type_hand_armor,0, 1050, weight(1.0)|abundance(100)|body_armor(7)|difficulty(0),imodbits_armor],

#footwear
["wrapping_boots", "Wrapping Boots", [("wrapping_boots_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 105 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0) ,imodbits_cloth ],
["woolen_hose", "Woolen Hose", [("woolen_hose_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 140 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(4)|difficulty(0) ,imodbits_cloth ],
["blue_hose", "Blue Hose", [("blue_hose_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 175 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(5)|difficulty(0) ,imodbits_cloth ],
["hunter_boots", "Hunter Boots", [("hunter_boots_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature,0,
 315 , weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["hide_boots", "Hide Boots", [("hide_boots_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian  | itp_attach_armature,0,
 350 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["ankle_boots", "Ankle Boots", [("ankle_boots_a_new",0)], itp_merchandise| itp_type_foot_armor |itp_civilian  | itp_attach_armature,0,
 420 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["nomad_boots", "Nomad Boots", [("nomad_boots_a",0)], itp_merchandise| itp_type_foot_armor  |itp_civilian | itp_attach_armature,0,
 490 , weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(14)|difficulty(0) ,imodbits_cloth ],
["leather_boots", "Leather Boots", [("leather_boots_a",0)], itp_merchandise| itp_type_foot_armor  |itp_civilian | itp_attach_armature,0,
 560 , weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(0) ,imodbits_cloth ],
["splinted_leather_greaves", "Splinted Leather Greaves", [("leather_greaves_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature,0,
 980 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(0) ,imodbits_armor ],
["mail_chausses", "Mail Chausses", [("mail_chausses_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature  ,0,
 840 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(24)|difficulty(0) ,imodbits_armor ],
["splinted_greaves", "Splinted Greaves", [("splinted_greaves_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature,0,
 735 , weight(2.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(7) ,imodbits_armor ],
["mail_boots", "Mail Boots", [("mail_boots_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature  ,0,
 1085 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(31)|difficulty(8) ,imodbits_armor ],
["iron_greaves", "Iron Greaves", [("iron_greaves_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature,0,
 1155 , weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(9) ,imodbits_armor ],
["black_greaves", "Black Greaves", [("black_greaves",0)], itp_type_foot_armor  | itp_attach_armature,0,
 1225 , weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(35)|difficulty(0) ,imodbits_armor ],
["khergit_leather_boots", "Khergit Leather Boots", [("khergit_leather_boots",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 630 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["sarranid_boots_a", "Sarranid Shoes", [("sarranid_shoes",0)], itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 280 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["sarranid_boots_b", "Sarranid Leather Boots", [("sarranid_boots",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 560 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(0) ,imodbits_cloth ],
["sarranid_boots_c", "Plated Boots", [("sarranid_camel_boots",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 700 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(20)|difficulty(0) ,imodbits_plate ],
["sarranid_boots_d", "Sarranid Mail Boots", [("sarranid_mail_chausses",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 1050 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(30)|difficulty(0) ,imodbits_armor ],

["sarranid_head_cloth", "Lady Head Cloth", [("tulbent",0)],  itp_type_head_armor | itp_doesnt_cover_hair |itp_civilian |itp_attach_armature,0, 200 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["sarranid_head_cloth_b", "Lady Head Cloth", [("tulbent_b",0)],  itp_type_head_armor | itp_doesnt_cover_hair |itp_civilian |itp_attach_armature,0, 200 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["sarranid_felt_head_cloth", "Head Cloth", [("common_tulbent",0)],  itp_type_head_armor  |itp_civilian |itp_attach_armature,0, 200 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["sarranid_felt_head_cloth_b", "Head Cloth", [("common_tulbent_b",0)],  itp_type_head_armor  |itp_civilian |itp_attach_armature,0, 200 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],


#bodywear
["lady_dress_ruby", "Lady Dress", [("lady_dress_r",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["lady_dress_green", "Lady Dress", [("lady_dress_g",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["lady_dress_blue", "Lady Dress", [("lady_dress_b",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["red_dress", "Red Dress", [("red_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["brown_dress", "Brown Dress", [("brown_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["green_dress", "Green Dress", [("green_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["khergit_lady_dress", "Khergit Lady Dress", [("khergit_lady_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["khergit_lady_dress_b", "Khergit Leather Lady Dress", [("khergit_lady_dress_b",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["sarranid_lady_dress", "Sarranid Lady Dress", [("sarranid_lady_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["sarranid_lady_dress_b", "Sarranid Lady Dress", [("sarranid_lady_dress_b",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["sarranid_common_dress", "Sarranid Dress", [("sarranid_common_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["sarranid_common_dress_b", "Sarranid Dress", [("sarranid_common_dress_b",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 1350 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["courtly_outfit", "Courtly Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian   ,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nobleman_outfit", "Nobleman Outfit", [("nobleman_outfit_b_new",0)], itp_type_body_armor|itp_covers_legs|itp_civilian   ,0, 1920 , weight(4)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(12)|difficulty(0) ,imodbits_cloth ], 
["nomad_armor", "Nomad Armor", [("nomad_armor_new",0)], itp_merchandise| itp_type_body_armor |itp_covers_legs   ,0, 2400 , weight(2)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["khergit_armor", "Khergit Armor", [("khergit_armor_new",0)], itp_merchandise| itp_type_body_armor | itp_covers_legs ,0, 2400 , weight(2)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["khergit_armor2", "Armor", [("khergit_armor_new",0)], itp_merchandise| itp_type_body_armor | itp_covers_legs ,0, 2400 , weight(2)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["leather_jacket", "Leather Jacket", [("leather_jacket_new",0)], itp_merchandise| itp_type_body_armor | itp_covers_legs  |itp_civilian ,0, 2000 , weight(3)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],

#NEW:
["rawhide_coat", "Rawhide Coat", [("coat_of_plates_b",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0, 1000 , weight(5)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
#NEW: was lthr_armor_a
["leather_armor", "Leather Armor", [("tattered_leather_armor_a",0)], itp_merchandise| itp_type_body_armor |itp_covers_legs  ,0, 1800 , weight(7)|abundance(100)|head_armor(0)|body_armor(18)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["fur_coat", "Fur Coat", [("fur_coat",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0, 1510 , weight(6)|abundance(100)|head_armor(0)|body_armor(13)|leg_armor(6)|difficulty(0) ,imodbits_armor ],



#for future:
["coat", "Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["leather_coat", "Leather Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["mail_coat", "Coat of Mail", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["long_mail_coat", "Long Coat of Mail", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["mail_with_tunic_red", "Mail with Tunic", [("arena_armorR_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 4180 , weight(16)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(8), imodbits_armor ],
["mail_with_tunic_green", "Mail with Tunic", [("arena_armorG_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 4180 , weight(16)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(8), imodbits_armor ],
["hide_coat", "Hide Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["merchant_outfit", "Merchant Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["homespun_dress", "Homespun Dress", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["thick_coat", "Thick Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["coat_with_cape", "Coat with Cape", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["steppe_outfit", "Steppe Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nordic_outfit", "Nordic Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nordic_armor", "Nordic Armor", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["hide_armor", "Hide Armor", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["cloaked_tunic", "Cloaked Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["sleeveless_tunic", "Sleeveless Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["sleeveless_leather_tunic", "Sleeveless Leather Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["linen_shirt", "Linen Shirt", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["wool_coat", "Wool Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 1750 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
#end

["dress", "Dress", [("dress",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 670 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0) ,imodbits_cloth ],
["blue_dress", "Blue Dress", [("blue_dress_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 670 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0) ,imodbits_cloth ],
["peasant_dress", "Peasant Dress", [("peasant_dress_b_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 670 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0) ,imodbits_cloth ], 
["woolen_dress", "Woolen Dress", [("woolen_dress",0)], itp_merchandise| itp_type_body_armor|itp_civilian  |itp_covers_legs ,0,
 870 , weight(1.75)|abundance(100)|head_armor(0)|body_armor(8)|leg_armor(2)|difficulty(0) ,imodbits_cloth ],
["shirt", "Shirt", [("shirt",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 500 , weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
 #NEW: was "linen_tunic"
["linen_tunic", "Linen Tunic", [("shirt_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 635 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
 #NEW was cvl_costume_a
["short_tunic", "Red Tunic", [("rich_tunic_a",0)], itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 735 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
 ["red_shirt", "Red Shirt", [("rich_tunic_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 735 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
 ["red_tunic", "Red Tunic", [("arena_tunicR_new",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 735 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],

 ["green_tunic", "Green Tunic", [("arena_tunicG_new",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 735 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
 ["blue_tunic", "Blue Tunic", [("arena_tunicB_new",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 735 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
["robe", "Robe", [("robe",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1010 , weight(1.5)|abundance(100)|head_armor(0)|body_armor(8)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
#NEW: was coarse_tunic
["coarse_tunic", "Tunic with vest", [("coarse_tunic_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 1310 , weight(2)|abundance(100)|head_armor(0)|body_armor(11)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["leather_apron", "Leather Apron", [("leather_apron",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 1445 , weight(3)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(7)|difficulty(0) ,imodbits_cloth ],
#NEW: was tabard_a
["tabard", "Tabard", [("tabard_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
#NEW: was leather_vest
["leather_vest", "Leather Vest", [("leather_vest_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 1745 , weight(4)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(7)|difficulty(0) ,imodbits_cloth ],
["steppe_armor", "Steppe Armor", [("lamellar_leather",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["steppe_armor2", "Lamellar Harness", [("lamellar_leather",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["gambeson", "Gambeson", [("white_gambeson",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs|itp_civilian,0,
 2175 , weight(5)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(5)|difficulty(0) ,imodbits_cloth ],
["blue_gambeson", "Blue Gambeson", [("blue_gambeson",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs|itp_civilian,0,
 2275 , weight(5)|abundance(100)|head_armor(0)|body_armor(21)|leg_armor(5)|difficulty(0) ,imodbits_cloth ],
#NEW: was red_gambeson
["red_gambeson", "Red Gambeson", [("red_gambeson_a",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs|itp_civilian,0,
 2275 , weight(5)|abundance(100)|head_armor(0)|body_armor(21)|leg_armor(5)|difficulty(0) ,imodbits_cloth ],
#NEW: was aketon_a
["padded_cloth", "Aketon", [("padded_cloth_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 2410 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
#NEW:
 ["aketon_green", "Padded Cloth", [("padded_cloth_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 2410 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
 #NEW: was "leather_jerkin"
["leather_jerkin", "Leather Jerkin", [("ragged_leather_jerkin",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 2510 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["nomad_vest", "Nomad Vest", [("nomad_vest_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 2480 , weight(7)|abundance(50)|head_armor(0)|body_armor(22)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["ragged_outfit", "Ragged Outfit", [("ragged_outfit_a_new",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 2615 , weight(7)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
 #NEW: was padded_leather
["padded_leather", "Padded Leather", [("leather_armor_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian,0,
 3050 , weight(12)|abundance(100)|head_armor(0)|body_armor(27)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["tribal_warrior_outfit", "Tribal Warrior Outfit", [("tribal_warrior_outfit_a_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 3350 , weight(14)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nomad_robe", "Nomad Robe", [("nomad_robe_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs |itp_civilian,0,
 3550 , weight(15)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
#["heraldric_armor", "Heraldric Armor", [("tourn_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 442 , weight(17)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#NEW: was "std_lthr_coat"
["studded_leather_coat", "Studded Leather Coat", [("leather_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3950 , weight(14)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(10)|difficulty(7) ,imodbits_armor ],

["byrnie", "Byrnie", [("byrnie_a_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4110 , weight(17)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(6)|difficulty(7) ,imodbits_armor ],
#["blackwhite_surcoat", "Black and White Surcoat", [("surcoat_blackwhite",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 348 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["green_surcoat", "Green Surcoat", [("surcoat_green",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 348 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["blue_surcoat", "Blue Surcoat", [("surcoat_blue",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 350 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["red_surcoat", "Red Surcoat", [("surcoat_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 350 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#NEW: was "haubergeon_a"
["haubergeon", "Haubergeon", [("haubergeon_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4310 , weight(18)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(6)|difficulty(6) ,imodbits_armor ],

["lamellar_vest", "Lamellar Vest", [("lamellar_vest_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 4280 , weight(18)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(8)|difficulty(7) ,imodbits_cloth ],

["lamellar_vest_khergit", "Khergit Lamellar Vest", [("lamellar_vest_b",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 4280 , weight(18)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(8)|difficulty(7) ,imodbits_cloth ],

 #NEW: was mail_shirt
["mail_shirt", "Mail Shirt", [("mail_shirt_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4120 , weight(19)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(12)|difficulty(7) ,imodbits_armor ],

["mail_hauberk_a", "Mail Hauberk", [("hauberk_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4420 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(7) ,imodbits_armor ],
["mail_hauberk", "Mail Hauberk", [("hauberk_a_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4420 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(7) ,imodbits_armor ],
 
["hauberk", "Mail Hauberk", [("hauberk",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4420 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(7) ,imodbits_armor ],

["mail_with_surcoat", "Mail with Surcoat", [("mail_long_surcoat_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail", "Surcoat over Mail", [("surcoat_over_mail_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
#["lamellar_cuirass", "Lamellar Cuirass", [("lamellar_armor",0)], itp_type_body_armor  |itp_covers_legs,0, 1020 , weight(25)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(15)|difficulty(9) ,imodbits_armor ],
#NEW: was "brigandine_a"
["brigandine_red", "Brigandine", [("brigandine_b",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs,0,
 5020 , weight(19)|abundance(100)|head_armor(0)|body_armor(46)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["lamellar_armor", "Lamellar Armor", [("lamellar_armor_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5255 , weight(25)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(13)|difficulty(0) ,imodbits_armor ],
["scale_armor", "Scale Armor", [("lamellar_armor_e",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5655 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(13)|difficulty(8) ,imodbits_armor ],
 #NEW: was "reinf_jerkin"
["banded_armor", "Banded Armor", [("banded_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5390 , weight(23)|abundance(100)|head_armor(0)|body_armor(49)|leg_armor(14)|difficulty(8) ,imodbits_armor ],
#NEW: was hard_lthr_a
["cuir_bouilli", "Cuir Bouilli", [("cuir_bouilli_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5525 , weight(24)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(15)|difficulty(8) ,imodbits_armor ],
["coat_of_plates", "Coat of Plates", [("coat_of_plates_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["coat_of_plates_red", "Coat of Plates", [("coat_of_plates_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["plate_armor", "Plate Armor", [("full_plate_armor",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 6095 , weight(27)|abundance(100)|head_armor(0)|body_armor(55)|leg_armor(17)|difficulty(9) ,imodbits_plate ],
["black_armor", "Black Armor", [("black_armor",0)], itp_type_body_armor  |itp_covers_legs ,0,
 6330 , weight(28)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(18)|difficulty(10) ,imodbits_plate ],

##armors_d
["pelt_coat", "Pelt Coat", [("thick_coat_a",0)],  itp_merchandise|itp_type_body_armor  |itp_covers_legs ,0,
 935, weight(2)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
##armors_e
["khergit_elite_armor", "Khergit Elite Armor", [("lamellar_armor_d",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["vaegir_elite_armor", "Vaegir Elite Armor", [("lamellar_armor_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["sarranid_elite_armor", "Sarranid Elite Armor", [("tunic_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian ,0,
 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],


["sarranid_dress_a", "Dress", [("woolen_dress",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1215 , weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["sarranid_dress_b", "Dress", [("woolen_dress",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1215 , weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["sarranid_cloth_robe", "Worn Robe", [("sar_robe",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1215 , weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["sarranid_cloth_robe_b", "Worn Robe", [("sar_robe_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1215 , weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["skirmisher_armor", "Skirmisher Armor", [("skirmisher_armor",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1815 , weight(3)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["archers_vest", "Archer's Padded Vest", [("archers_vest",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 2720 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["sarranid_leather_armor", "Sarranid Leather Armor", [("sarranid_leather_armor",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3620 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["sarranid_cavalry_robe", "Cavalry Robe", [("arabian_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3880 , weight(15)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(8)|difficulty(0) ,imodbits_armor ],
["arabian_armor_b", "Sarranid Guard Armor", [("arabian_armor_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4080 , weight(19)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8)|difficulty(0) ,imodbits_armor],
["arabian_armor_b2b", "Guard Armor", [("arabian_armor_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4080 , weight(19)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8)|difficulty(0) ,imodbits_armor],
["sarranid_mail_shirt", "Sarranid Mail Shirt", [("sarranian_mail_shirt",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 4490 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mamluke_mail", "Mamluke Mail", [("sarranid_elite_cavalary",0)], itp_merchandise| itp_type_body_armor |itp_covers_legs|itp_civilian  ,0, 
 5360 , weight(24)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(16)|difficulty(8) ,imodbits_armor ],

#Quest-specific - perhaps can be used for prisoners, 
["burlap_tunic", "Burlap Tunic", [("shirt",0)], itp_type_body_armor  |itp_covers_legs ,0,
 335 , weight(1)|abundance(100)|head_armor(0)|body_armor(3)|leg_armor(1)|difficulty(0) ,imodbits_armor ],


["heraldic_mail_with_surcoat", "Heraldic Mail with Surcoat", [("heraldic_armor_new_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5495 , weight(22)|abundance(100)|head_armor(0)|body_armor(49)|leg_armor(17)|difficulty(7) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_a", ":agent_no", ":troop_no")])]],
["heraldic_mail_with_tunic", "Heraldic Mail", [("heraldic_armor_new_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5560 , weight(22)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(16)|difficulty(7) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_b", ":agent_no", ":troop_no")])]],
["heraldic_mail_with_tunic_b", "Heraldic Mail", [("heraldic_armor_new_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5560 , weight(22)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(16)|difficulty(7) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_c", ":agent_no", ":troop_no")])]],
["heraldic_mail_with_tabard", "Heraldic Mail with Tabard", [("heraldic_armor_new_d",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5625 , weight(21)|abundance(100)|head_armor(0)|body_armor(51)|leg_armor(15)|difficulty(7) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_d", ":agent_no", ":troop_no")])]],
["turret_hat_ruby", "Turret Hat", [("turret_hat_r",0)], itp_type_head_armor  |itp_civilian|itp_fit_to_head ,0, 400 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ], 
["turret_hat_blue", "Turret Hat", [("turret_hat_b",0)], itp_type_head_armor  |itp_civilian|itp_fit_to_head ,0, 400 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ], 
["turret_hat_green", "Barbette", [("barbette_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head,0,300, weight(0.5)|abundance(100)|head_armor(6)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["head_wrappings","Head Wrapping",[("head_wrapping",0)],itp_type_head_armor|itp_fit_to_head,0,150, weight(0.25)|head_armor(3),imodbit_tattered | imodbit_ragged | imodbit_sturdy | imodbit_thick],
["court_hat", "Turret Hat", [("court_hat",0)], itp_type_head_armor  |itp_civilian|itp_fit_to_head ,0, 400 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ], 
["wimple_a", "Wimple", [("wimple_a_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head,0,200, weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["wimple_with_veil", "Wimple with Veil", [("wimple_b_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head,0,200, weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["straw_hat", "Straw Hat", [("straw_hat_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,100, weight(1)|abundance(100)|head_armor(2)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["common_hood", "Hood", [("hood_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["hood_b", "Hood", [("hood_b",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["hood_c", "Hood", [("hood_c",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["hood_d", "Hood", [("hood_d",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["headcloth", "Headcloth", [("headcloth_a_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 200 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["woolen_hood", "Woolen Hood", [("woolen_hood",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 400 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["arming_cap", "Arming Cap", [("arming_cap_a_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 350 , weight(1)|abundance(100)|head_armor(7)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["fur_hat", "Fur Hat", [("fur_hat_a_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 400 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["nomad_cap", "Nomad Cap", [("nomad_cap_a_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 500 , weight(0.75)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["nomad_cap_b", "Nomad Cap", [("nomad_cap_b_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 650 , weight(0.75)|abundance(100)|head_armor(13)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["steppe_cap", "Steppe Cap", [("steppe_cap_a_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 700 , weight(1)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["padded_coif", "Padded Coif", [("padded_coif_a_new",0)], itp_merchandise| itp_type_head_armor   ,0, 550 , weight(1)|abundance(100)|head_armor(11)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["woolen_cap", "Woolen Cap", [("woolen_cap_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 600 , weight(1)|abundance(100)|head_armor(6)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["felt_hat", "Felt Hat", [("felt_hat_a_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian,0, 400 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["felt_hat_b", "Felt Hat", [("felt_hat_b_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian,0, 400 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["leather_cap", "Leather Cap", [("leather_cap_a_new",0)], itp_merchandise| itp_type_head_armor|itp_civilian ,0, 900, weight(1)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["female_hood", "Lady's Hood", [("ladys_hood_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 500 , weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["leather_steppe_cap_a", "Steppe Cap", [("leather_steppe_cap_a_new",0)], itp_merchandise|itp_type_head_armor   ,0, 
750 , weight(1)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["leather_steppe_cap_b", "Steppe Cap ", [("tattered_steppe_cap_b_new",0)], itp_merchandise|itp_type_head_armor   ,0, 
800 , weight(1)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["leather_steppe_cap_c", "Steppe Cap", [("steppe_cap_a_new",0)], itp_merchandise|itp_type_head_armor   ,0, 800 , weight(1)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["leather_warrior_cap", "Leather Warrior Cap", [("skull_cap_new_b",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 900 , weight(1)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["skullcap", "Skullcap", [("skull_cap_new_a",0)], itp_merchandise| itp_type_head_armor   ,0, 1000 , weight(1.0)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["mail_coif", "Mail Coif", [("mail_coif_new",0)], itp_merchandise| itp_type_head_armor   ,0, 1100 , weight(1.25)|abundance(100)|head_armor(22)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_armor ],
["footman_helmet", "Footman's Helmet", [("skull_cap_new",0)], itp_merchandise| itp_type_head_armor   ,0, 1200 , weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["nasal_helmet", "Nasal Helmet", [("nasal_helmet_b",0)], itp_merchandise| itp_type_head_armor   ,0, 1300 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["norman_helmet", "Helmet with Cap", [("norman_helmet_a",0)], itp_merchandise| itp_type_head_armor|itp_fit_to_head ,0, 1400 , weight(1.25)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["segmented_helmet", "Segmented Helmet", [("segmented_helm_new",0)], itp_merchandise| itp_type_head_armor   ,0, 1550 , weight(1.25)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["helmet_with_neckguard", "Helmet with Neckguard", [("neckguard_helm_new",0)], itp_merchandise| itp_type_head_armor   ,0, 
1600 , weight(1.5)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["flat_topped_helmet", "Flat Topped Helmet", [("flattop_helmet_new",0)], itp_merchandise| itp_type_head_armor   ,0, 
1650 , weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["kettle_hat", "Kettle Hat", [("kettle_hat_new",0)], itp_merchandise| itp_type_head_armor,0, 
1750 , weight(1.75)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["spiked_helmet", "Spiked Helmet", [("spiked_helmet_new",0)], itp_merchandise| itp_type_head_armor   ,0, 1900 , weight(2)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_helmet", "Nordic Helmet", [("helmet_w_eyeguard_new",0)], itp_merchandise| itp_type_head_armor   ,0, 2000 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["khergit_lady_hat", "Khergit Lady Hat", [("khergit_lady_hat",0)],  itp_type_head_armor   |itp_civilian |itp_doesnt_cover_hair | itp_fit_to_head,0, 200 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["khergit_lady_hat_b", "Khergit Lady Leather Hat", [("khergit_lady_hat_b",0)], itp_type_head_armor  | itp_doesnt_cover_hair | itp_fit_to_head  |itp_civilian ,0, 200 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["sarranid_felt_hat", "Sarranid Felt Hat", [("sar_helmet3",0)], itp_merchandise| itp_type_head_armor   ,0, 250 , weight(2)|abundance(100)|head_armor(5)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["turban", "Turban", [("tuareg_open",0)], itp_merchandise| itp_type_head_armor   ,0, 550 , weight(1)|abundance(100)|head_armor(11)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["desert_turban", "Desert Turban", [("tuareg",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard ,0, 700 , weight(1.50)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["sarranid_warrior_cap", "Sarranid Warrior Cap", [("tuareg_helmet",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 750 , weight(2)|abundance(100)|head_armor(19)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_warrior_cap2", "Warrior Cap", [("tuareg_helmet",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 750 , weight(2)|abundance(100)|head_armor(19)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_horseman_helmet", "Horseman Helmet", [("sar_helmet2",0)], itp_merchandise| itp_type_head_armor   ,0, 1250 , weight(2.75)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_helmet1", "Sarranid Keffiyeh Helmet", [("sar_helmet1",0)], itp_merchandise| itp_type_head_armor   ,0, 1750 , weight(2.50)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_mail_coif", "Sarranid Mail Coif", [("tuareg_helmet2",0)], itp_merchandise| itp_type_head_armor ,0, 2050 , weight(3)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_veiled_helmet", "Sarranid Veiled Helmet", [("sar_helmet4",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 2350 , weight(3.50)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_archer_helmet", "Nordic Leather Helmet", [("Helmet_A_vs2",0)], itp_merchandise| itp_type_head_armor    ,0, 700 , weight(1.25)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_veteran_archer_helmet", "Nordic Leather Helmet", [("Helmet_A",0)], itp_merchandise| itp_type_head_armor,0, 1000 , weight(1.5)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_archer_helmet_b", "Leather Helmet", [("Helmet_A_vs2",0)], itp_merchandise| itp_type_head_armor    ,0, 700 , weight(1.25)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_veteran_archer_helmet_b", "Leather Helmet", [("Helmet_A",0)], itp_merchandise| itp_type_head_armor,0, 1000 , weight(1.5)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_footman_helmet", "Nordic Footman Helmet", [("Helmet_B_vs2",0)], itp_merchandise| itp_type_head_armor |itp_fit_to_head ,0, 1500 , weight(1.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_fighter_helmet", "Nordic Fighter Helmet", [("Helmet_B",0)], itp_merchandise| itp_type_head_armor|itp_fit_to_head ,0, 1700 , weight(2)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_footman_helmet_b", "Warrior Helmet", [("Helmet_B_vs2",0)], itp_merchandise| itp_type_head_armor |itp_fit_to_head ,0, 1500 , weight(1.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_fighter_helmet_b", "Fighter Helmet", [("Helmet_B",0)], itp_merchandise| itp_type_head_armor|itp_fit_to_head ,0, 1700 , weight(2)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_huscarl_helmet", "Nordic Huscarl's Helmet", [("Helmet_C_vs2",0)], itp_merchandise| itp_type_head_armor   ,0, 2000 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_warlord_helmet", "Nordic Warlord Helmet", [("Helmet_C",0)], itp_merchandise| itp_type_head_armor ,0, 2400 , weight(2.25)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["vaegir_fur_cap", "Cap with Fur", [("vaeg_helmet3",0)], itp_merchandise| itp_type_head_armor   ,0, 750 , weight(1)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_fur_helmet", "Vaegir Helmet", [("vaeg_helmet2",0)], itp_merchandise| itp_type_head_armor   ,0, 1050 , weight(2)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_spiked_helmet", "Spiked Cap", [("vaeg_helmet1",0)], itp_merchandise| itp_type_head_armor   ,0, 1600 , weight(2.50)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_lamellar_helmet", "Helmet with Lamellar Guard", [("vaeg_helmet4",0)], itp_merchandise| itp_type_head_armor   ,0, 1900 , weight(2.75)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_noble_helmet", "Vaegir Nobleman Helmet", [("vaeg_helmet7",0)], itp_merchandise| itp_type_head_armor   ,0, 2250, weight(2.75)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_war_helmet", "Vaegir War Helmet", [("vaeg_helmet6",0)], itp_merchandise| itp_type_head_armor   ,0, 2350 , weight(3)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_footman_helmet", "Vaegir Footman Helmet", [("vaeg_helmet5",0)], itp_merchandise| itp_type_head_armor   ,0, 2150 , weight(2.75)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_mask", "Vaegir War Mask", [("vaeg_helmet9",0)], itp_merchandise| itp_type_head_armor |itp_covers_beard ,0, 2600 , weight(3.50)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_open_mask", "Vaegir Open War Mask", [("vaeg_helmet8",0)], itp_merchandise| itp_type_head_armor ,0, 2500 , weight(3.25)|abundance(100)|head_armor(50)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

 
["bascinet_1", "Bascinet", [("bascinet_avt_new1",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet", "Bascinet", [("bascinet_avt_new",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_2", "Bascinet with Aventail", [("bascinet_new_a",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_3", "Bascinet with Nose Guard", [("bascinet_new_b",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["guard_helmet", "Guard Helmet", [("reinf_helmet_new",0)], itp_merchandise| itp_type_head_armor   ,0, 2350 , weight(2.5)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(9) ,imodbits_plate ],
["black_helmet", "Black Helmet", [("black_helm",0)], itp_type_head_armor,0, 2500 , weight(2.75)|abundance(100)|head_armor(50)|body_armor(0)|leg_armor(0)|difficulty(9) ,imodbits_plate ],
["full_helm", "Full Helm", [("great_helmet_new_b",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2550 , weight(2.5)|abundance(100)|head_armor(51)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet", "Great Helmet", [("great_helmet_new",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["winged_great_helmet", "Winged Great Helmet", [("maciejowski_helmet_new",0)], itp_merchandise|itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],


#WEAPONS
["wooden_stick",         "Wooden Stick", [("wooden_stick",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 
375 , weight(2.5)|difficulty(0)|spd_rtng(99) | weapon_length(63)|swing_damage(13 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["cudgel",         "Cudgel", [("club",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 
400 , weight(2.5)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(13 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["hammer",         "Hammer", [("iron_hammer_new",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar, 
725 , weight(2)|difficulty(0)|spd_rtng(100) | weapon_length(55)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["club",         "Club", [("club",0)], itp_type_one_handed_wpn|itp_merchandise| itp_can_knock_down|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 
750 , weight(2.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(20 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["winged_mace",         "Flanged Mace", [("flanged_mace",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
925 , weight(3.5)|difficulty(0)|spd_rtng(103) | weapon_length(70)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["spiked_mace",         "Spiked Mace", [("spiked_mace_new",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
1025 , weight(3.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(28 , blunt) | thrust_damage(0 ,  pierce),imodbits_pick ],
["military_hammer", "Military Hammer", [("military_hammer",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
1125 , weight(2)|difficulty(0)|spd_rtng(95) | weapon_length(70)|swing_damage(31 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["maul",         "Maul", [("maul_b",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down |itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear, 
1250 , weight(6)|difficulty(11)|spd_rtng(83) | weapon_length(79)|swing_damage(36 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["sledgehammer", "Sledgehammer", [("maul_c",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear, 
1375 , weight(7)|difficulty(12)|spd_rtng(81) | weapon_length(82)|swing_damage(39, blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["warhammer",         "Great Hammer", [("maul_d",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear, 
1425 , weight(9)|difficulty(14)|spd_rtng(79) | weapon_length(75)|swing_damage(45 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["pickaxe",         "Pickaxe", [("fighting_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
500 , weight(3)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(19 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["spiked_club",         "Spiked Club", [("spiked_club",0)], itp_type_one_handed_wpn|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
525 , weight(3)|difficulty(0)|spd_rtng(97) | weapon_length(70)|swing_damage(21 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],
["fighting_pick", "Fighting Pick", [("fighting_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
650 , weight(1.0)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(25 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["military_pick", "Military Pick", [("steel_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
800 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(70)|swing_damage(31 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["morningstar",         "Morningstar", [("mace_morningstar_new",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_carry_mace_left_hip, 
1150 , weight(4.5)|difficulty(13)|spd_rtng(95) | weapon_length(85)|swing_damage(38 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],


["sickle",         "Sickle", [("sickle",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver, 
250 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(40)|swing_damage(20 , cut) | thrust_damage(0 ,  pierce),imodbits_none ],
["cleaver",         "Cleaver", [("cleaver_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver, 
250 , weight(1.5)|difficulty(0)|spd_rtng(103) | weapon_length(35)|swing_damage(24 , cut) | thrust_damage(0 ,  pierce),imodbits_none ],
["knife",         "Knife", [("peasant_knife_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 
125 , weight(0.5)|difficulty(0)|spd_rtng(110) | weapon_length(40)|swing_damage(21 , cut) | thrust_damage(13 ,  pierce),imodbits_sword ],
["butchering_knife", "Butchering Knife", [("khyber_knife_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_right, 
225 , weight(0.75)|difficulty(0)|spd_rtng(108) | weapon_length(60)|swing_damage(24 , cut) | thrust_damage(17 ,  pierce),imodbits_sword ],
["dagger",         "Dagger", [("dagger_b",0),("dagger_b_scabbard",ixmesh_carry),("dagger_b",imodbits_good),("dagger_b_scabbard",ixmesh_carry|imodbits_good)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left|itcf_show_holster_when_drawn, 
175 , weight(0.75)|difficulty(0)|spd_rtng(109) | weapon_length(47)|swing_damage(22 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
#["nordic_sword", "Nordic Sword", [("viking_sword",0),("scab_vikingsw", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 142 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(98)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
#["arming_sword", "Arming Sword", [("b_long_sword",0),("scab_longsw_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 156 , weight(1.5)|difficulty(0)|spd_rtng(101) | weapon_length(100)|swing_damage(25 , cut) | thrust_damage(22 ,  pierce),imodbits_sword ],
#["sword",         "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 148 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(26 , cut) | thrust_damage(23 ,  pierce),imodbits_sword ],
["falchion",         "Falchion", [("falchion_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 
650 , weight(2.5)|difficulty(8)|spd_rtng(98) | weapon_length(73)|swing_damage(30 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["broadsword",         "Broadsword", [("broadsword",0),("scab_broadsword", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 122 , weight(2.5)|difficulty(8)|spd_rtng(91) | weapon_length(101)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["scimitar",         "Scimitar", [("scimeter",0),("scab_scimeter", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
#108 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(29 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["scimitar",         "Scimitar", [("scimitar_a",0),("scab_scimeter_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
875 , weight(1.5)|difficulty(0)|spd_rtng(101) | weapon_length(97)|swing_damage(30 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["scimitar_b",         "Elite Scimitar", [("scimitar_b",0),("scab_scimeter_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
1000 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(103)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["arabian_sword_a",         "Sarranid Sword", [("arabian_sword_a",0),("scab_arabian_sword_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
725 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(26 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["arabian_sword_b",         "Sarranid Arming Sword", [("arabian_sword_b",0),("scab_arabian_sword_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
775 , weight(1.7)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(28 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["sarranid_cavalry_sword",         "Sarranid Cavalry Sword", [("arabian_sword_c",0),("scab_arabian_sword_c", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
950 , weight(1.5)|difficulty(0)|spd_rtng(98) | weapon_length(105)|swing_damage(31 , cut) | thrust_damage(25 ,  pierce),imodbits_sword_high ],
["arabian_sword_d",         "Sarranid Guard Sword", [("arabian_sword_d",0),("scab_arabian_sword_d", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
900 , weight(1.7)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(34 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ],

["arabian_sword_a2",         "Sword", [("arabian_sword_a",0),("scab_arabian_sword_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
725 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(26 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["arabian_sword_b2",         "Arming Sword", [("arabian_sword_b",0),("scab_arabian_sword_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
775 , weight(1.7)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(28 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["sarranid_cavalry_sword2",         "Cavalry Sword", [("arabian_sword_c",0),("scab_arabian_sword_c", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
950 , weight(1.5)|difficulty(0)|spd_rtng(98) | weapon_length(105)|swing_damage(31 , cut) | thrust_damage(25 ,  pierce),imodbits_sword_high ],
["arabian_sword_d2",         "Guard Sword", [("arabian_sword_d",0),("scab_arabian_sword_d", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
900 , weight(1.7)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(34 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ],

#["nomad_sabre",         "Nomad Sabre", [("shashqa",0),("scab_shashqa", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 115 , weight(1.75)|difficulty(0)|spd_rtng(101) | weapon_length(100)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["bastard_sword", "Bastard Sword", [("bastard_sword",0),("scab_bastardsw", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 279 , weight(2.25)|difficulty(9)|spd_rtng(102) | weapon_length(120)|swing_damage(33 , cut) | thrust_damage(27 ,  pierce),imodbits_sword ],
["great_sword",         "Great Sword", [("b_bastard_sword",0),("scab_bastardsw_b", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1250 , weight(2.75)|difficulty(10)|spd_rtng(95) | weapon_length(125)|swing_damage(39 , cut) | thrust_damage(31 ,  pierce),imodbits_sword_high ],
["sword_of_war", "Sword of War", [("b_bastard_sword",0),("scab_bastardsw_b", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1325 , weight(3)|difficulty(11)|spd_rtng(94) | weapon_length(130)|swing_damage(40 , cut) | thrust_damage(31 ,  pierce),imodbits_sword_high ],
["hatchet",         "Hatchet", [("hatchet",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 
450 , weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(60)|swing_damage(23 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["hand_axe",         "Hand Axe", [("hatchet",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 
675 , weight(2)|difficulty(7)|spd_rtng(95) | weapon_length(75)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["fighting_axe", "Fighting Axe", [("fighting_ax",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 
875 , weight(2.5)|difficulty(9)|spd_rtng(92) | weapon_length(90)|swing_damage(31 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["axe",                 "Axe", [("iron_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 
975 , weight(4)|difficulty(8)|spd_rtng(91) | weapon_length(108)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["voulge_a",         "Voulge", [("voulge",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 
1125 , weight(4.5)|difficulty(8)|spd_rtng(87) | weapon_length(119)|swing_damage(35 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["battle_axe",         "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 
1225 , weight(5)|difficulty(9)|spd_rtng(88) | weapon_length(108)|swing_damage(41 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["war_axe",         "War Axe", [("war_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 
1275 , weight(5)|difficulty(10)|spd_rtng(86) | weapon_length(110)|swing_damage(43 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["double_axe",         "Double Axe", [("dblhead_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 1075 , weight(6.5)|difficulty(12)|spd_rtng(85) | weapon_length(95)|swing_damage(43 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
#["great_axe",         "Great Axe", [("great_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 415 , weight(7)|difficulty(13)|spd_rtng(82) | weapon_length(120)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

["sword_two_handed_b",         "Two Handed Sword", [("sword_two_handed_b",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 1100 , weight(2.75)|difficulty(10)|spd_rtng(97) | weapon_length(110)|swing_damage(40 , cut) | thrust_damage(28 ,  pierce),imodbits_sword_high ],
["sword_two_handed_a",         "Great Sword", [("sword_two_handed_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 1250 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(42 , cut) | thrust_damage(29 ,  pierce),imodbits_sword_high ],


["khergit_sword_two_handed_a",         "Two Handed Sabre", [("khergit_sword_two_handed_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back,
 1250 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(40 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["khergit_sword_two_handed_b",         "Two Handed Sabre", [("khergit_sword_two_handed_b",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back,
 1375 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(44 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["two_handed_cleaver", "War Cleaver", [("military_cleaver_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back,
 1375 , weight(2.75)|difficulty(10)|spd_rtng(93) | weapon_length(120)|swing_damage(45 , cut) | thrust_damage(0 ,  cut),imodbits_sword_high ],
["military_cleaver_b", "Soldier's Cleaver", [("military_cleaver_b",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,
 850 , weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(31 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["military_cleaver_c", "Military Cleaver", [("military_cleaver_c",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,
 950 , weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["military_sickle_a", "Military Sickle", [("military_sickle_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 725 , weight(1.0)|difficulty(9)|spd_rtng(100) | weapon_length(75)|swing_damage(26 , pierce) | thrust_damage(0 ,  pierce),imodbits_axe ],


["bastard_sword_a", "Bastard Sword", [("bastard_sword_a",0),("bastard_sword_a_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 925 , weight(2.0)|difficulty(9)|spd_rtng(98) | weapon_length(101)|swing_damage(35 , cut) | thrust_damage(26 ,  pierce),imodbits_sword_high ],
["bastard_sword_b", "Heavy Bastard Sword", [("bastard_sword_b",0),("bastard_sword_b_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1000 , weight(2.25)|difficulty(9)|spd_rtng(97) | weapon_length(105)|swing_damage(37 , cut) | thrust_damage(27 ,  pierce),imodbits_sword_high ],

["one_handed_war_axe_a", "One Handed Axe", [("one_handed_war_axe_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 775 , weight(1.5)|difficulty(9)|spd_rtng(98) | weapon_length(71)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_a", "One Handed Battle Axe", [("one_handed_battle_axe_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 850 , weight(1.5)|difficulty(9)|spd_rtng(98) | weapon_length(73)|swing_damage(34 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_war_axe_b", "One Handed War Axe", [("one_handed_war_axe_b",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 875 , weight(1.5)|difficulty(9)|spd_rtng(98) | weapon_length(76)|swing_damage(34 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_b", "One Handed Battle Axe", [("one_handed_battle_axe_b",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 875 , weight(1.75)|difficulty(9)|spd_rtng(98) | weapon_length(72)|swing_damage(36 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_c", "One Handed Battle Axe", [("one_handed_battle_axe_c",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 950 , weight(2.0)|difficulty(9)|spd_rtng(98) | weapon_length(76)|swing_damage(37 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],


["two_handed_axe",         "Two Handed Axe", [("two_handed_battle_axe_a",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 900 , weight(4.5)|difficulty(10)|spd_rtng(96) | weapon_length(90)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["two_handed_battle_axe_2",         "Two Handed War Axe", [("two_handed_battle_axe_b",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1050 , weight(4.5)|difficulty(10)|spd_rtng(96) | weapon_length(92)|swing_damage(44 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["shortened_voulge",         "Shortened Voulge", [("two_handed_battle_axe_c",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1125 , weight(4.5)|difficulty(10)|spd_rtng(92) | weapon_length(100)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["great_axe",         "Great Axe", [("two_handed_battle_axe_e",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1150 , weight(4.5)|difficulty(10)|spd_rtng(94) | weapon_length(96)|swing_damage(47 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["long_axe",         "Long Axe", [("long_axe_a",0)], itp_type_polearm|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_next_item_as_melee|itp_unbalanced|itp_merchandise,itc_staff|itcf_carry_axe_back,
 1050 , weight(4.75)|difficulty(10)|spd_rtng(93) | weapon_length(120)|swing_damage(46 , cut) | thrust_damage(19 ,  blunt),imodbits_axe ],
["long_axe_alt",         "Long Axe", [("long_axe_a",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1050 , weight(4.75)|difficulty(10)|spd_rtng(88) | weapon_length(120)|swing_damage(46 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["long_axe_b",         "Long War Axe", [("long_axe_b",0)], itp_type_polearm| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_next_item_as_melee|itp_unbalanced|itp_merchandise, itc_staff|itcf_carry_axe_back,
 1150 , weight(5.0)|difficulty(10)|spd_rtng(92) | weapon_length(125)|swing_damage(50 , cut) | thrust_damage(18 ,  blunt),imodbits_axe ],
["long_axe_b_alt",         "Long War Axe", [("long_axe_b",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1150 , weight(5.0)|difficulty(10)|spd_rtng(87) | weapon_length(125)|swing_damage(50 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["long_axe_c",         "Great Long Axe", [("long_axe_c",0)], itp_type_polearm| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_next_item_as_melee|itp_unbalanced|itp_merchandise, itc_staff|itcf_carry_axe_back,
 1225 , weight(5.5)|difficulty(10)|spd_rtng(91) | weapon_length(127)|swing_damage(54 , cut) | thrust_damage(19 ,  blunt),imodbits_axe ],
["long_axe_c_alt",      "Great Long Axe", [("long_axe_c",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1225 , weight(5.5)|difficulty(10)|spd_rtng(85) | weapon_length(127)|swing_damage(54 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

 ["bardiche",         "Bardiche", [("two_handed_battle_axe_d",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1125 , weight(4.75)|difficulty(10)|spd_rtng(91) | weapon_length(102)|swing_damage(47 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["great_bardiche",         "Great Bardiche", [("two_handed_battle_axe_f",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1400 , weight(5.0)|difficulty(10)|spd_rtng(89) | weapon_length(116)|swing_damage(50 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],




["voulge",         "Voulge", [("two_handed_battle_long_axe_a",0)], itp_type_polearm|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_staff,
 1425 , weight(3.0)|difficulty(10)|spd_rtng(88) | weapon_length(175)|swing_damage(40 , cut) | thrust_damage(18 ,  pierce),imodbits_axe ],
["long_bardiche",         "Long Bardiche", [("two_handed_battle_long_axe_b",0)], itp_type_polearm|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_staff,
 1150 , weight(4.75)|difficulty(11)|spd_rtng(89) | weapon_length(140)|swing_damage(48 , cut) | thrust_damage(17 ,  pierce),imodbits_axe ],
["great_long_bardiche",         "Great Long Bardiche", [("two_handed_battle_long_axe_c",0)], itp_type_polearm|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_staff,
 1300 , weight(5.0)|difficulty(12)|spd_rtng(88) | weapon_length(155)|swing_damage(50 , cut) | thrust_damage(17 ,  pierce),imodbits_axe ],

 ["hafted_blade_b",         "Hafted Blade", [("khergit_pike_b",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_two_handed|itp_penalty_with_shield|itp_wooden_parry, itcf_carry_spear|itc_guandao,
 800 , weight(2.75)|difficulty(0)|spd_rtng(95) | weapon_length(135)|swing_damage(37 , cut) | thrust_damage(20 ,  pierce),imodbits_polearm ],
 ["hafted_blade_a",         "Hafted Blade", [("khergit_pike_a",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_two_handed|itp_penalty_with_shield|itp_wooden_parry, itcf_carry_spear|itc_guandao,
 900 , weight(3.25)|difficulty(0)|spd_rtng(93) | weapon_length(153)|swing_damage(39 , cut) | thrust_damage(19 ,  pierce),imodbits_polearm ],

["shortened_military_scythe",         "Shortened Military Scythe", [("two_handed_battle_scythe_a",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back,
 1225 , weight(3.0)|difficulty(10)|spd_rtng(90) | weapon_length(112)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["sword_medieval_a", "Sword", [("sword_medieval_a",0),("sword_medieval_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 775 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(27 , cut) | thrust_damage(22 ,  pierce),imodbits_sword_high ],
#["sword_medieval_a_long", "Sword", [("sword_medieval_a_long",0),("sword_medieval_a_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 156 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(105)|swing_damage(25 , cut) | thrust_damage(22 ,  pierce),imodbits_sword ],
["sword_medieval_b", "Sword", [("sword_medieval_b",0),("sword_medieval_b_scabbard", ixmesh_carry),("sword_rusty_a",imodbit_rusty),("sword_rusty_a_scabbard", ixmesh_carry|imodbit_rusty)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 800 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(28 , cut) | thrust_damage(23 ,  pierce),imodbits_sword_high ],
["sword_medieval_b_small", "Short Sword", [("sword_medieval_b_small",0),("sword_medieval_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 700 , weight(1)|difficulty(0)|spd_rtng(102) | weapon_length(85)|swing_damage(26, cut) | thrust_damage(24, pierce),imodbits_sword_high ],
["sword_medieval_c", "Arming Sword", [("sword_medieval_c",0),("sword_medieval_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 825 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29 , cut) | thrust_damage(24 ,  pierce),imodbits_sword_high ],
["sword_medieval_c_small", "Short Arming Sword", [("sword_medieval_c_small",0),("sword_medieval_c_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 725 , weight(1)|difficulty(0)|spd_rtng(103) | weapon_length(86)|swing_damage(26, cut) | thrust_damage(24 ,  pierce),imodbits_sword_high ],
["sword_medieval_c_long", "Arming Sword", [("sword_medieval_c_long",0),("sword_medieval_c_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 925 , weight(1.7)|difficulty(0)|spd_rtng(99) | weapon_length(100)|swing_damage(29 , cut) | thrust_damage(28 ,  pierce),imodbits_sword_high ],
["sword_medieval_d_long", "Long Arming Sword", [("sword_medieval_d_long",0),("sword_medieval_d_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1025 , weight(1.8)|difficulty(0)|spd_rtng(96) | weapon_length(105)|swing_damage(33 , cut) | thrust_damage(28 ,  pierce),imodbits_sword ],
 
#["sword_medieval_d", "sword_medieval_d", [("sword_medieval_d",0),("sword_medieval_d_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 131 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(24 , cut) | thrust_damage(21 ,  pierce),imodbits_sword ],
#["sword_medieval_e", "sword_medieval_e", [("sword_medieval_e",0),("sword_medieval_e_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 131 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(24 , cut) | thrust_damage(21 ,  pierce),imodbits_sword ],

["sword_viking_1", "Nordic Sword", [("sword_viking_c",0),("sword_viking_c_scabbard ", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 750 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(94)|swing_damage(28 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ] ,
["sword_viking_2", "Nordic Sword", [("sword_viking_b",0),("sword_viking_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 800 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_2_small", "Nordic Short Sword", [("sword_viking_b_small",0),("sword_viking_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 725 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(85)|swing_damage(28 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_3", "Nordic War Sword", [("sword_viking_a",0),("sword_viking_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 800 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(30 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
#["sword_viking_a_long", "sword_viking_a_long", [("sword_viking_a_long",0),("sword_viking_a_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 142 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(105)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
["sword_viking_3_small", "Nordic Short War Sword", [("sword_viking_a_small",0),("sword_viking_a_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 750 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(86)|swing_damage(29 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
#["sword_viking_c_long", "sword_viking_c_long", [("sword_viking_c_long",0),("sword_viking_c_long_scabbard ", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 142 , weight(1.5)|difficulty(0)|spd_rtng(95) | weapon_length(105)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ] ,

["sword_viking_1b", "Sword", [("sword_viking_c",0),("sword_viking_c_scabbard ", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 750 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(94)|swing_damage(28 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ] ,
["sword_viking_2b", "Sword", [("sword_viking_b",0),("sword_viking_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 800 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_2b_small", "Short Sword", [("sword_viking_b_small",0),("sword_viking_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 725 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(85)|swing_damage(28 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_3b", "War Sword", [("sword_viking_a",0),("sword_viking_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 800 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(30 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_3b_small", "Short War Sword", [("sword_viking_a_small",0),("sword_viking_a_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 750 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(86)|swing_damage(29 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],

["sword_khergit_1", "Nomad Sabre", [("khergit_sword_b",0),("khergit_sword_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 850 , weight(1.25)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(29 , cut),imodbits_sword_high ],
["sword_khergit_1b", "Sabre", [("khergit_sword_b",0),("khergit_sword_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 850 , weight(1.25)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(29 , cut),imodbits_sword_high ],
["sword_khergit_2", "Sabre", [("khergit_sword_c",0),("khergit_sword_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 875 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(30 , cut),imodbits_sword_high ],
["sword_khergit_3", "Sabre", [("khergit_sword_a",0),("khergit_sword_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 900 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(98)|swing_damage(31 , cut),imodbits_sword_high ],
["sword_khergit_4", "Heavy Sabre", [("khergit_sword_d",0),("khergit_sword_d_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 925 , weight(1.75)|difficulty(0)|spd_rtng(98) | weapon_length(96)|swing_damage(33 , cut),imodbits_sword_high ],



["mace_1",         "Spiked Club", [("mace_d",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 600 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(19 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_2",         "Knobbed_Mace", [("mace_a",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 775 , weight(2.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(21 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_3",         "Spiked Mace", [("mace_c",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 850 , weight(2.75)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(23 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_4",         "Winged_Mace", [("mace_b",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 900 , weight(2.75)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
# Goedendag
 ["club_with_spike_head",  "Spiked Staff", [("mace_e",0)],  itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_wooden_parry, itc_bastardsword|itcf_carry_axe_back,
 1175 , weight(2.80)|difficulty(9)|spd_rtng(95) | weapon_length(117)|swing_damage(24 , blunt) | thrust_damage(20 ,  pierce),imodbits_mace ],

["long_spiked_club",         "Long Spiked Club", [("mace_long_c",0)], itp_type_polearm|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_axe_back,
 1350 , weight(3)|difficulty(0)|spd_rtng(96) | weapon_length(126)|swing_damage(29 , pierce) | thrust_damage(20 ,  blunt),imodbits_mace ],
["long_hafted_knobbed_mace",         "Long Hafted Knobbed Mace", [("mace_long_a",0)], itp_type_polearm| itp_can_knock_down|itp_primary|itp_wooden_parry, itc_staff|itcf_carry_axe_back,
 1625 , weight(3)|difficulty(0)|spd_rtng(95) | weapon_length(133)|swing_damage(31 , blunt) | thrust_damage(25 ,  blunt),imodbits_mace ],
["long_hafted_spiked_mace",         "Long Hafted Spiked Mace", [("mace_long_b",0)], itp_type_polearm|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_axe_back,
 1750 , weight(3)|difficulty(0)|spd_rtng(92) | weapon_length(140)|swing_damage(34 , blunt) | thrust_damage(28 ,  blunt),imodbits_mace ],

["sarranid_two_handed_mace_1",         "Iron Mace", [("mace_long_d",0)], itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_merchandise| itp_primary|itp_crush_through|itp_unbalanced, itc_greatsword|itcf_carry_axe_back,
1550 , weight(4.5)|difficulty(0)|spd_rtng(90) | weapon_length(95)|swing_damage(39 , blunt) | thrust_damage(25 ,  blunt),imodbits_mace ],


["sarranid_mace_1",         "Iron Mace", [("mace_small_d",0)], itp_type_one_handed_wpn|itp_merchandise|itp_can_knock_down |itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 1175 , weight(2.0)|difficulty(0)|spd_rtng(99) | weapon_length(73)|swing_damage(30 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["sarranid_axe_a", "Iron Battle Axe", [("one_handed_battle_axe_g",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 825 , weight(1.65)|difficulty(9)|spd_rtng(97) | weapon_length(71)|swing_damage(35 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["sarranid_axe_b", "Iron War Axe", [("one_handed_battle_axe_h",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 900 , weight(1.75)|difficulty(9)|spd_rtng(97) | weapon_length(71)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

["sarranid_two_handed_axe_a",         "Sarranid Battle Axe", [("two_handed_battle_axe_g",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1125 , weight(3.0)|difficulty(10)|spd_rtng(89) | weapon_length(95)|swing_damage(49 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["sarranid_two_handed_axe_b",         "Sarranid War Axe", [("two_handed_battle_axe_h",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1025 , weight(2.50)|difficulty(0)|spd_rtng(90) | weapon_length(90)|swing_damage(46 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
 
["sarranid_two_handed_axe_a2",         "Battle Axe", [("two_handed_battle_axe_g",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1125 , weight(3.0)|difficulty(10)|spd_rtng(89) | weapon_length(95)|swing_damage(49 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["sarranid_two_handed_axe_b2",         "War Axe", [("two_handed_battle_axe_h",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 1025 , weight(2.50)|difficulty(0)|spd_rtng(90) | weapon_length(90)|swing_damage(46 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],




["scythe",         "Scythe", [("scythe",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 
700 , weight(2)|difficulty(0)|spd_rtng(95) | weapon_length(182)|swing_damage(24 , cut) | thrust_damage(14 ,  pierce),imodbits_polearm ],
["pitch_fork",         "Pitch Fork", [("pitch_fork",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_staff_overhead_shield, 
675 , weight(1.5)|difficulty(0)|spd_rtng(87) | weapon_length(154)|swing_damage(16 , blunt) | thrust_damage(22 ,  pierce),imodbits_polearm ],
["military_fork", "Military Fork", [("military_fork",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry,itc_staff_overhead_shield, 
975 , weight(2)|difficulty(0)|spd_rtng(95) | weapon_length(135)|swing_damage(15 , blunt) | thrust_damage(30 ,  pierce),imodbits_polearm ],
["battle_fork",         "Battle Fork", [("battle_fork",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry,itc_staff_overhead_shield, 
1125 , weight(2.25)|difficulty(0)|spd_rtng(90) | weapon_length(144)|swing_damage(15, blunt) | thrust_damage(35 ,  pierce),imodbits_polearm ],
["boar_spear",         "Boar Spear", [("spear",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear, 
725 , weight(1.5)|difficulty(0)|spd_rtng(90) | weapon_length(157)|swing_damage(26 , cut) | thrust_damage(23 ,  pierce),imodbits_polearm ],
#["spear",         "Spear", [("spear",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear, 173 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(17 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],


["jousting_lance", "Jousting Lance", [("joust_of_peace",0)], itp_couchable|itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_greatlance, 1275 , weight(5)|difficulty(0)|spd_rtng(61) | weapon_length(240)|swing_damage(0 , cut) | thrust_damage(17 ,  blunt),imodbits_polearm ],
#["lance",         "Lance", [("pike",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 196 , weight(5)|difficulty(0)|spd_rtng(72) | weapon_length(170)|swing_damage(0 , cut) | thrust_damage(20 ,  pierce),imodbits_polearm ],
["double_sided_lance", "Double Sided Lance", [("lance_dblhead",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff_overhead_shield, 875 , weight(4.0)|difficulty(0)|spd_rtng(95) | weapon_length(128)|swing_damage(25, cut) | thrust_damage(27 ,  pierce),imodbits_polearm ],
#["pike",         "Pike", [("pike",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_spear,
# 212 , weight(6)|difficulty(0)|spd_rtng(77) | weapon_length(167)|swing_damage(0 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
["glaive_a",         "Glaive", [("glaive",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 1150 , weight(4.5)|difficulty(0)|spd_rtng(90) | weapon_length(157)|swing_damage(39 , cut) | thrust_damage(21 ,  pierce),imodbits_polearm ],
["glaive",         "Glaive", [("glaive_b",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 1150 , weight(4.5)|difficulty(0)|spd_rtng(90) | weapon_length(157)|swing_damage(39 , cut) | thrust_damage(21 ,  pierce),imodbits_polearm ],
["poleaxe",         "Poleaxe", [("pole_ax",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff,
 1325 , weight(4.5)|difficulty(13)|spd_rtng(77) | weapon_length(180)|swing_damage(50 , cut) | thrust_damage(15 ,  blunt),imodbits_polearm ],
["polehammer",         "Polehammer", [("pole_hammer",0)], itp_type_polearm|itp_offset_lance| itp_primary|itp_two_handed|itp_wooden_parry|itp_unbalanced|itp_crush_through|itp_can_knock_down, itc_staff,
 1950 , weight(7)|difficulty(18)|spd_rtng(75) | weapon_length(126)|swing_damage(50 , blunt) | thrust_damage(35 ,  blunt),imodbits_polearm ],
["staff",         "Staff", [("wooden_staff",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack, itc_staff|itcf_carry_sword_back,
 675 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(130)|swing_damage(18 , blunt) | thrust_damage(19 ,  blunt),imodbits_polearm ],
["quarter_staff", "Quarter Staff", [("quarter_staff",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_staff|itcf_carry_sword_back,
 1025 , weight(2)|difficulty(0)|spd_rtng(104) | weapon_length(140)|swing_damage(20 , blunt) | thrust_damage(20 ,  blunt),imodbits_polearm ],
["iron_staff",         "Iron Staff", [("iron_staff",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary, itc_staff|itcf_carry_sword_back,
 1225 , weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(140)|swing_damage(25 , blunt) | thrust_damage(26 ,  blunt),imodbits_polearm ],

#["glaive_b",         "Glaive_b", [("glaive_b",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
# 352 , weight(4.5)|difficulty(0)|spd_rtng(83) | weapon_length(157)|swing_damage(38 , cut) | thrust_damage(21 ,  pierce),imodbits_polearm ],


["shortened_spear",         "Shortened Spear", [("spear_g_1-9m",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff_overhead_shield|itcf_carry_spear,
 875 , weight(2.0)|difficulty(0)|spd_rtng(102) | weapon_length(120)|swing_damage(19 , blunt) | thrust_damage(25 ,  pierce),imodbits_polearm ],
["spear",         "Spear", [("spear_h_2-15m",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff_overhead_shield|itcf_carry_spear,
 975 , weight(2.25)|difficulty(0)|spd_rtng(98) | weapon_length(135)|swing_damage(20 , blunt) | thrust_damage(26 ,  pierce),imodbits_polearm ],

["bamboo_spear",         "Bamboo Spear", [("arabian_spear_a_3m",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff_overhead_shield|itcf_carry_spear,
 800, weight(2.0)|difficulty(0)|spd_rtng(88) | weapon_length(200)|swing_damage(15 , blunt) | thrust_damage(20 ,  pierce),imodbits_polearm ],




["war_spear",         "War Spear", [("spear_i_2-3m",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff_overhead_shield|itcf_carry_spear,
 1100 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(150)|swing_damage(20 , blunt) | thrust_damage(27 ,  pierce),imodbits_polearm ],
["military_scythe",         "Military Scythe", [("spear_e_2-5m",0),("spear_c_2-5m",imodbits_bad)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear,
 900 , weight(2.5)|difficulty(0)|spd_rtng(90) | weapon_length(155)|swing_damage(36 , cut) | thrust_damage(25 ,  pierce),imodbits_polearm ],
["light_lance",         "Light Lance", [("spear_b_2-75m",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_musket_melee_bayonet,
 1125 , weight(2.5)|difficulty(0)|spd_rtng(85) | weapon_length(175)|swing_damage(16 , blunt) | thrust_damage(27 ,  pierce),imodbits_polearm ],
["lance",         "Lance", [("spear_d_2-8m",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_musket_melee_bayonet,
 1125 , weight(2.5)|difficulty(0)|spd_rtng(80) | weapon_length(180)|swing_damage(16 , blunt) | thrust_damage(26 ,  pierce),imodbits_polearm ],
["heavy_lance",         "Heavy Lance", [("spear_f_2-9m",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_musket_melee_bayonet,
 1175 , weight(2.75)|difficulty(10)|spd_rtng(75) | weapon_length(190)|swing_damage(16 , blunt) | thrust_damage(26 ,  pierce),imodbits_polearm ],
["great_lance",         "Great Lance", [("heavy_lance",0)], itp_couchable|itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_greatlance, 
 1325 , weight(5)|difficulty(11)|spd_rtng(55) | weapon_length(240)|swing_damage(0 , cut) | thrust_damage(21 ,  pierce),imodbits_polearm ],
["pike",         "Pike", [("spear_a_3m",0)], itp_type_polearm|itp_merchandise| itp_cant_use_on_horseback|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_two_handed, itc_musket_melee_bayonet,
 900 , weight(3.0)|difficulty(0)|spd_rtng(81) | weapon_length(245)|swing_damage(16 , blunt) | thrust_damage(26 ,  pierce),imodbits_polearm ],
##["spear_e_3-25m",         "Spear_3-25m", [("spear_e_3-25m",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear,
## 150 , weight(4.5)|difficulty(0)|spd_rtng(81) | weapon_length(225)|swing_damage(19 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
["ashwood_pike", "Ashwood Pike", [("pike",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_musket_melee_bayonet,
 1200 , weight(3.5)|difficulty(9)|spd_rtng(90) | weapon_length(170)|swing_damage(19 , blunt) | thrust_damage(29,  pierce),imodbits_polearm ],
["awlpike",    "Awlpike", [("awl_pike_b",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_musket_melee_bayonet|itcf_carry_spear,
 1075 , weight(2.25)|difficulty(0)|spd_rtng(92) | weapon_length(165)|swing_damage(20 , blunt) | thrust_damage(33 ,  pierce),imodbits_polearm ],
["awlpike_long",  "Long Awlpike", [("awl_pike_a",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_musket_melee_bayonet|itcf_carry_spear,
 1125 , weight(2.25)|difficulty(0)|spd_rtng(89) | weapon_length(185)|swing_damage(20 , blunt) | thrust_damage(32 ,  pierce),imodbits_polearm ],
#["awlpike",         "Awlpike", [("pike",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear,
# 378 , weight(3.5)|difficulty(12)|spd_rtng(92) | weapon_length(160)|swing_damage(20 ,blunt) | thrust_damage(31 ,  pierce),imodbits_polearm ],

["bec_de_corbin_a",  "War Hammer", [("bec_de_corbin_a",0)], itp_type_polearm|itp_merchandise|itp_cant_use_on_horseback|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_two_handed, itc_cutting_spear|itcf_carry_spear,
 800 , weight(4.0)|difficulty(0)|spd_rtng(85)| weapon_length(120)|swing_damage(38, blunt) | thrust_damage(38 ,  pierce),imodbits_polearm ],



# SHIELDS

["wooden_shield", "Wooden Shield", [("shield_round_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  50 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield, 
	[
		(ti_on_shield_hit, 
		[
			(store_trigger_param_1, ":receiver_id"),
			(store_trigger_param_2, ":inflicter_id"),
			(store_trigger_param_3, ":damage"),
			
			(call_script, "script_shield_damage_modifiers", ":receiver_id", ":inflicter_id", ":damage"),
			(try_begin),
				(gt, reg20, 0),
				(set_trigger_result, reg20),
			(try_end),
		])
	],
],
##["wooden_shield", "Wooden Shield", [("shield_round_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield,




#["round_shield", "Round Shield", [("shield_round_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  64 , weight(2)|hit_points(400)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield ],
["nordic_shield", "Nordic Shield", [("shield_round_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  50 , weight(2)|hit_points(440)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield, 
	[
		(ti_on_shield_hit, 
		[
			(store_trigger_param_1, ":receiver_id"),
			(store_trigger_param_2, ":inflicter_id"),
			(store_trigger_param_3, ":damage"),
			
			(call_script, "script_shield_damage_modifiers", ":receiver_id", ":inflicter_id", ":damage"),
			(try_begin),
				(gt, reg20, 0),
				(set_trigger_result, reg20),
			(try_end),
		])
	],
],
#["kite_shield",         "Kite Shield", [("shield_kite_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["kite_shield_", "Kite Shield", [("shield_kite_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["large_shield", "Large Shield", [("shield_kite_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  165 , weight(2.5)|hit_points(520)|body_armor(1)|spd_rtng(80)|shield_width(92),imodbits_shield ],
#["battle_shield", "Battle Shield", [("shield_kite_d",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  196 , weight(3)|hit_points(560)|body_armor(1)|spd_rtng(78)|shield_width(94),imodbits_shield ],
["fur_covered_shield",  "Fur Covered Shield", [("shield_kite_m",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  125 , weight(3.5)|hit_points(600)|body_armor(1)|spd_rtng(76)|shield_width(81),imodbits_shield, 
	[
		(ti_on_shield_hit, 
		[
			(store_trigger_param_1, ":receiver_id"),
			(store_trigger_param_2, ":inflicter_id"),
			(store_trigger_param_3, ":damage"),
			
			(call_script, "script_shield_damage_modifiers", ":receiver_id", ":inflicter_id", ":damage"),
			(try_begin),
				(gt, reg20, 0),
				(set_trigger_result, reg20),
			(try_end),
		])
	],
],
#["heraldric_shield", "Heraldric Shield", [("shield_heraldic",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  301 , weight(3.5)|hit_points(640)|body_armor(1)|spd_rtng(83)|shield_width(65),imodbits_shield ],
#["heater_shield", "Heater Shield", [("shield_heater_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  477 , weight(3.5)|hit_points(710)|body_armor(4)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["steel_shield", "Steel Shield", [("shield_dragon",0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield,  1275 , weight(4)|hit_points(700)|body_armor(17)|spd_rtng(61)|shield_width(40),imodbits_shield, 
	[
		(ti_on_shield_hit, 
		[
			(store_trigger_param_1, ":receiver_id"),
			(store_trigger_param_2, ":inflicter_id"),
			(store_trigger_param_3, ":damage"),
			
			(call_script, "script_shield_damage_modifiers", ":receiver_id", ":inflicter_id", ":damage"),
			(try_begin),
				(gt, reg20, 0),
				(set_trigger_result, reg20),
			(try_end),
		])
	],
],
#["nomad_shield", "Nomad Shield", [("shield_wood_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  12 , weight(2)|hit_points(260)|body_armor(6)|spd_rtng(110)|shield_width(30),imodbits_shield ],

["plate_covered_round_shield", "Plate Covered Round Shield", [("shield_round_e",0)], itp_type_shield, itcf_carry_round_shield,  575 , weight(4)|hit_points(330)|body_armor(16)|spd_rtng(90)|shield_width(40),imodbits_shield ],
["leather_covered_round_shield", "Leather Covered Round Shield", [("shield_round_d",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  275 , weight(2.5)|hit_points(310)|body_armor(8)|spd_rtng(96)|shield_width(40),imodbits_shield ],
["hide_covered_round_shield", "Hide Covered Round Shield", [("shield_round_f",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  75 , weight(2)|hit_points(260)|body_armor(3)|spd_rtng(100)|shield_width(40),imodbits_shield ],

["shield_heater_c", "Heater Shield", [("shield_heater_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  100 , weight(3.5)|hit_points(410)|body_armor(2)|spd_rtng(80)|shield_width(50),imodbits_shield ],
["shield_heater_d", "Heater Shield", [("shield_heater_d",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  450 , weight(3.5)|hit_points(710)|body_armor(4)|spd_rtng(80)|shield_width(60),imodbits_shield ],

#["shield_kite_g",         "Kite Shield g", [("shield_kite_g",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_h",         "Kite Shield h", [("shield_kite_h",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_i",         "Kite Shield i ", [("shield_kite_i",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_k",         "Kite Shield k", [("shield_kite_k",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],

["tab_shield_round_a", "Old Round Shield", [("tableau_shield_round_5",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
100 , weight(2.5)|hit_points(195)|body_armor(4)|spd_rtng(93)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_round_shield_5", ":agent_no", ":troop_no")])]],
["tab_shield_round_b", "Plain Round Shield", [("tableau_shield_round_3",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
275 , weight(3)|hit_points(260)|body_armor(8)|spd_rtng(90)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_round_shield_3", ":agent_no", ":troop_no")])]],
["tab_shield_round_c", "Round Shield", [("tableau_shield_round_2",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
500 , weight(3.5)|hit_points(310)|body_armor(12)|spd_rtng(87)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner","tableau_round_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_round_d", "Heavy Round Shield", [("tableau_shield_round_1",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
800 , weight(4)|hit_points(400)|body_armor(15)|spd_rtng(84)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_round_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_round_e", "Huscarl's Round Shield", [("tableau_shield_round_4",0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield,  
1300 , weight(4.5)|hit_points(510)|body_armor(19)|spd_rtng(81)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_round_shield_4", ":agent_no", ":troop_no")])]],

["tab_shield_kite_a", "Old Kite Shield",   [("tableau_shield_kite_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
125 , weight(2)|hit_points(165)|body_armor(5)|spd_rtng(96)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_kite_b", "Plain Kite Shield",   [("tableau_shield_kite_3" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
300 , weight(2.5)|hit_points(215)|body_armor(10)|spd_rtng(93)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_3", ":agent_no", ":troop_no")])]],
["tab_shield_kite_c", "Kite Shield",   [("tableau_shield_kite_2" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
475 , weight(3)|hit_points(265)|body_armor(13)|spd_rtng(90)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_kite_d", "Heavy Kite Shield",   [("tableau_shield_kite_2" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
800 , weight(3.5)|hit_points(310)|body_armor(18)|spd_rtng(87)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_kite_cav_a", "Horseman's Kite Shield",   [("tableau_shield_kite_4" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
250 , weight(2)|hit_points(165)|body_armor(14)|spd_rtng(103)|shield_width(30)|shield_height(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_4", ":agent_no", ":troop_no")])]],
["tab_shield_kite_cav_b", "Knightly Kite Shield",   [("tableau_shield_kite_4" ,0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield,  
550 , weight(2.5)|hit_points(225)|body_armor(23)|spd_rtng(100)|shield_width(30)|shield_height(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_4", ":agent_no", ":troop_no")])]],

["tab_shield_heater_a", "Old Heater Shield",   [("tableau_shield_heater_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
125 , weight(2)|hit_points(160)|body_armor(6)|spd_rtng(96)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_heater_b", "Plain Heater Shield",   [("tableau_shield_heater_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
325 , weight(2.5)|hit_points(210)|body_armor(11)|spd_rtng(93)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_heater_c", "Heater Shield",   [("tableau_shield_heater_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
525 , weight(3)|hit_points(260)|body_armor(14)|spd_rtng(90)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_heater_d", "Heavy Heater Shield",   [("tableau_shield_heater_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
825 , weight(3.5)|hit_points(305)|body_armor(19)|spd_rtng(87)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_heater_cav_a", "Horseman's Heater Shield",   [("tableau_shield_heater_2" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
350 , weight(2)|hit_points(160)|body_armor(16)|spd_rtng(103)|shield_width(30)|shield_height(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_heater_cav_b", "Knightly Heater Shield",   [("tableau_shield_heater_2" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
675 , weight(2.5)|hit_points(220)|body_armor(23)|spd_rtng(100)|shield_width(30)|shield_height(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_2", ":agent_no", ":troop_no")])]],

["tab_shield_pavise_a", "Old Board Shield",   [("tableau_shield_pavise_2" ,0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield,  
225 , weight(3.5)|hit_points(280)|body_armor(4)|spd_rtng(89)|shield_width(43)|shield_height(100),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_pavise_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_pavise_b", "Plain Board Shield",   [("tableau_shield_pavise_2" ,0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield,  
550 , weight(4)|hit_points(360)|body_armor(8)|spd_rtng(85)|shield_width(43)|shield_height(100),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_pavise_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_pavise_c", "Board Shield",   [("tableau_shield_pavise_1" ,0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield,  
825 , weight(4.5)|hit_points(430)|body_armor(10)|spd_rtng(81)|shield_width(43)|shield_height(100),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_pavise_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_pavise_d", "Heavy Board Shield",   [("tableau_shield_pavise_1" ,0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield,  
1475, weight(5)|hit_points(550)|body_armor(14)|spd_rtng(78)|shield_width(43)|shield_height(100),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_pavise_shield_1", ":agent_no", ":troop_no")])]],

["tab_shield_small_round_a", "Plain Cavalry Shield", [("tableau_shield_small_round_3",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
125 , weight(2)|hit_points(160)|body_armor(8)|spd_rtng(105)|shield_width(40),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_small_round_shield_3", ":agent_no", ":troop_no")])]],
["tab_shield_small_round_b", "Round Cavalry Shield", [("tableau_shield_small_round_1",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
300 , weight(2.5)|hit_points(200)|body_armor(14)|spd_rtng(103)|shield_width(40),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_small_round_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_small_round_c", "Elite Cavalry Shield", [("tableau_shield_small_round_2",0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield,  
575 , weight(3)|hit_points(250)|body_armor(22)|spd_rtng(100)|shield_width(40),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_small_round_shield_2", ":agent_no", ":troop_no")])]],


 #RANGED



["darts",         "Darts", [("dart_b",0),("dart_b_bag", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_javelin|itcf_carry_quiver_right_vertical|itcf_show_holster_when_drawn, 
575 , weight(4)|difficulty(1)|spd_rtng(95) | shoot_speed(28) | thrust_damage(22 ,  pierce)|max_ammo(24)|weapon_length(32),imodbits_thrown ],
["war_darts",         "War Darts", [("dart_a",0),("dart_a_bag", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
650 , weight(5)|difficulty(1)|spd_rtng(93) | shoot_speed(27) | thrust_damage(25 ,  pierce)|max_ammo(21)|weapon_length(45),imodbits_thrown ],

["javelin",         "Javelins", [("javelin",0),("javelins_quiver_new", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
850, weight(4)|difficulty(1)|spd_rtng(91) | shoot_speed(25) | thrust_damage(34 ,  pierce)|max_ammo(18)|weapon_length(75),imodbits_thrown ],
["javelin_melee",         "Javelin", [("javelin",0)], itp_type_polearm|itp_primary|itp_wooden_parry , itc_staff_overhead_shield, 
850, weight(1)|difficulty(0)|spd_rtng(95) |swing_damage(12, cut)| thrust_damage(14,  pierce)|weapon_length(75),imodbits_polearm ],

["throwing_spears",         "Throwing Spears", [("jarid_new_b",0),("jarid_new_b_bag", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
1075 , weight(3)|difficulty(2)|spd_rtng(87) | shoot_speed(22) | thrust_damage(44 ,  pierce)|max_ammo(15)|weapon_length(65),imodbits_thrown ],
["throwing_spear_melee",         "Throwing Spear", [("jarid_new_b",0),("javelins_quiver", ixmesh_carry)],itp_type_polearm|itp_primary|itp_wooden_parry , itc_staff_overhead_shield, 
1075 , weight(1)|difficulty(1)|spd_rtng(91) | swing_damage(18, cut) | thrust_damage(23 ,  pierce)|weapon_length(75),imodbits_thrown ],

["jarid",         "Jarids", [("jarid_new",0),("jarid_quiver", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
1175 , weight(2.75)|difficulty(2)|spd_rtng(89) | shoot_speed(24) | thrust_damage(48 ,  pierce)|max_ammo(12)|weapon_length(65),imodbits_thrown ],
["jarid_melee",         "Jarid", [("jarid_new",0),("jarid_quiver", ixmesh_carry)], itp_type_polearm|itp_primary|itp_wooden_parry , itc_staff_overhead_shield,
1175 , weight(1)|difficulty(2)|spd_rtng(93) | swing_damage(16, cut) | thrust_damage(20 ,  pierce)|weapon_length(65),imodbits_thrown ],



["stones",         "Stones", [("throwing_stone",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_stone, 350 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],

["throwing_knives", "Throwing Knives", [("throwing_knife",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_knife, 500 , weight(2.5)|difficulty(0)|spd_rtng(121) | shoot_speed(25) | thrust_damage(19 ,  cut)|max_ammo(42)|weapon_length(0),imodbits_thrown ],
["throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_knife, 600 , weight(2.5)|difficulty(0)|spd_rtng(110) | shoot_speed(24) | thrust_damage(25 ,  cut)|max_ammo(39)|weapon_length(0),imodbits_thrown ],
["light_throwing_axes", "Light Throwing Axes", [("francisca",0)], itp_type_thrown |itp_bonus_against_shield|itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_axe,
875, weight(5)|difficulty(2)|spd_rtng(99) | shoot_speed(18) | thrust_damage(35,cut)|max_ammo(16)|weapon_length(53),imodbits_thrown_minus_heavy ],
["light_throwing_axes_melee", "Light Throwing Axe", [("francisca",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,
875, weight(1)|difficulty(2)|spd_rtng(99)|weapon_length(53)| swing_damage(26,cut),imodbits_thrown_minus_heavy ],
["throwing_axes", "Throwing Axes", [("throwing_axe_a",0)], itp_type_thrown |itp_bonus_against_shield|itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_axe,
975, weight(5)|difficulty(3)|spd_rtng(98) | shoot_speed(18) | thrust_damage(39,cut)|max_ammo(14)|weapon_length(53),imodbits_thrown_minus_heavy ],
["throwing_axes_melee", "Throwing Axe", [("throwing_axe_a",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,
975, weight(1)|difficulty(3)|spd_rtng(98) | swing_damage(29,cut)|weapon_length(53),imodbits_thrown_minus_heavy ],
["heavy_throwing_axes", "Heavy Throwing Axes", [("throwing_axe_b",0)], itp_type_thrown |itp_bonus_against_shield|itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_axe,
1100, weight(5)|difficulty(4)|spd_rtng(97) | shoot_speed(18) | thrust_damage(44,cut)|max_ammo(12)|weapon_length(53),imodbits_thrown_minus_heavy ],
["heavy_throwing_axes_melee", "Heavy Throwing Axe", [("throwing_axe_b",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,
1100, weight(1)|difficulty(4)|spd_rtng(97) | swing_damage(32,cut)|weapon_length(53),imodbits_thrown_minus_heavy ],



["hunting_bow",         "Hunting Bow", [("hunting_bow",0),("hunting_bow_carry",ixmesh_carry)],itp_type_bow |itp_merchandise|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 
1050 , weight(1)|difficulty(0)|spd_rtng(100) | shoot_speed(52) | thrust_damage(15 ,  pierce),imodbits_bow ],
["short_bow",         "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 
1225 , weight(1)|difficulty(1)|spd_rtng(97) | shoot_speed(55) | thrust_damage(18 ,  pierce  ),imodbits_bow ],
["nomad_bow",         "Nomad Bow", [("nomad_bow",0),("nomad_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 
1300 , weight(1.25)|difficulty(2)|spd_rtng(94) | shoot_speed(56) | thrust_damage(20 ,  pierce),imodbits_bow ],
["long_bow",         "Long Bow", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 
1200 , weight(1.75)|difficulty(3)|spd_rtng(79) | shoot_speed(56) | thrust_damage(22 ,  pierce),imodbits_bow ],
["khergit_bow",         "Khergit Bow", [("khergit_bow",0),("khergit_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 
1325 , weight(1.25)|difficulty(3)|spd_rtng(90) | shoot_speed(57) | thrust_damage(21 ,pierce),imodbits_bow ],
["strong_bow",         "Strong Bow", [("strong_bow",0),("strong_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 
1400 , weight(1.25)|difficulty(3)|spd_rtng(88) | shoot_speed(58) | thrust_damage(23 ,pierce),imodbit_cracked | imodbit_bent | imodbit_masterwork ],
["war_bow",         "War Bow", [("war_bow",0),("war_bow_carry",ixmesh_carry)],itp_type_bow|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 
1450 , weight(1.5)|difficulty(4)|spd_rtng(84) | shoot_speed(59) | thrust_damage(25 ,pierce),imodbits_bow ],
["hunting_crossbow", "Hunting Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
900 , weight(2.25)|difficulty(0)|spd_rtng(47) | shoot_speed(50) | thrust_damage(37 ,  pierce)|max_ammo(1),imodbits_crossbow ],
["light_crossbow", "Light Crossbow", [("crossbow_b",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
1050 , weight(2.5)|difficulty(8)|spd_rtng(45) | shoot_speed(59) | thrust_damage(44 ,  pierce)|max_ammo(1),imodbits_crossbow ],
["crossbow",         "Crossbow",         [("crossbow_a",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
1000 , weight(3)|difficulty(8)|spd_rtng(43) | shoot_speed(66) | thrust_damage(49,pierce)|max_ammo(1),imodbits_crossbow ],
["heavy_crossbow", "Heavy Crossbow", [("crossbow_c",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
1150 , weight(3.5)|difficulty(9)|spd_rtng(41) | shoot_speed(68) | thrust_damage(58 ,pierce)|max_ammo(1),imodbits_crossbow ],
["sniper_crossbow", "Siege Crossbow", [("crossbow_c",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
1175 , weight(3.75)|difficulty(10)|spd_rtng(37) | shoot_speed(70) | thrust_damage(63 ,pierce)|max_ammo(1),imodbits_crossbow ],
["flintlock_pistol", "Flintlock Pistol", [("flintlock_pistol",0)], itp_type_pistol |itp_merchandise|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 275 , weight(1.5)|difficulty(0)|spd_rtng(38) | shoot_speed(160) | thrust_damage(45 ,pierce)|max_ammo(1)|accuracy(65),imodbits_none,
 [(ti_on_weapon_attack, [(play_sound,"snd_pistol_shot"),(position_move_x, pos1,27),(position_move_y, pos1,36),(particle_system_burst, "psys_pistol_smoke", pos1, 15)])]],
["torch",         "Torch", [("club",0)], itp_type_one_handed_wpn|itp_primary, itc_scimitar, 11 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none,
 [(ti_on_init_item, [(set_position_delta,0,60,0),(particle_system_add_new, "psys_torch_fire"),(particle_system_add_new, "psys_torch_smoke"),(set_current_color,150, 130, 70),(add_point_light, 10, 30),
])]],

["lyre",         "Lyre", [("lyre",0)], itp_type_shield|itp_wooden_parry|itp_civilian, itcf_carry_bow_back,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90),0 ],
["lute",         "Lute", [("lute",0)], itp_type_shield|itp_wooden_parry|itp_civilian, itcf_carry_bow_back,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90),0 ],

##["short_sword", "Short Sword",
## [("sword_norman",0),("sword_norman_scabbard", ixmesh_carry),("sword_norman_rusty",imodbit_rusty),("sword_norman_rusty_scabbard", ixmesh_carry|imodbit_rusty)],
## itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 183 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(75)|swing_damage(25 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],

["strange_armor",  "Strange Armor", [("samurai_armor",0)], itp_type_body_armor  |itp_covers_legs ,0, 4465 , weight(18)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(19)|difficulty(7) ,imodbits_armor ],
["strange_boots",  "Strange Boots", [("samurai_boots",0)], itp_type_foot_armor | itp_attach_armature,0, 735 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(0) ,imodbits_cloth ],
["strange_helmet", "Strange Helmet", [("samurai_helmet",0)], itp_type_head_armor   ,0, 2200 , weight(2)|abundance(100)|head_armor(44)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["strange_sword", "Strange Sword", [("katana",0),("katana_scabbard",ixmesh_carry)], itp_type_two_handed_wpn| itp_primary, itc_bastardsword|itcf_carry_katana|itcf_show_holster_when_drawn, 800 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(95)|swing_damage(32 , cut) | thrust_damage(18 ,  pierce),imodbits_sword ],
["strange_great_sword",  "Strange Great Sword", [("no_dachi",0),("no_dachi_scabbard",ixmesh_carry)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back|itcf_show_holster_when_drawn, 1200 , weight(3.5)|difficulty(11)|spd_rtng(92) | weapon_length(125)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["strange_short_sword", "Strange Short Sword", [("wakizashi",0),("wakizashi_scabbard",ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_wakizashi|itcf_show_holster_when_drawn, 525 , weight(1.25)|difficulty(0)|spd_rtng(108) | weapon_length(65)|swing_damage(25 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
["court_dress", "Court Dress", [("court_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian   ,0, 1540 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(4)|difficulty(0) ,imodbits_cloth ],
["rich_outfit", "Rich Outfit", [("merchant_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian   ,0, 1740 , weight(4)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(4)|difficulty(0) ,imodbits_cloth ],
["khergit_guard_armor", "Khergit Guard Armor", [("lamellar_armor_a",0)], itp_type_body_armor|itp_covers_legs   ,0, 
5630 , weight(25)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(18)|difficulty(0) ,imodbits_armor ],
#["leather_steppe_cap_c", "Leather Steppe Cap", [("leather_steppe_cap_c",0)], itp_type_head_armor   ,0, 51 , weight(2)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["felt_steppe_cap", "Felt Steppe Cap", [("felt_steppe_cap",0)], itp_type_head_armor   ,0, 800 , weight(2)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["khergit_war_helmet", "Khergit War Helmet", [("tattered_steppe_cap_a_new",0)], itp_type_head_armor | itp_merchandise   ,0, 1550 , weight(2)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["khergit_helmet", "Khergit Helmet", [("khergit_guard_helmet",0)], itp_type_head_armor   ,0, 1650 , weight(2)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
#["khergit_sword", "Khergit Sword", [("khergit_sword",0),("khergit_sword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 183 , weight(1.25)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(23 , cut) | thrust_damage(14 ,  pierce),imodbits_sword ],
["khergit_guard_boots",  "Khergit Guard Boots", [("lamellar_boots_a",0)], itp_type_foot_armor | itp_attach_armature,0, 700 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(20)|difficulty(0) ,imodbits_cloth ],
["khergit_guard_helmet", "Khergit Guard Helmet", [("lamellar_helmet_a",0)], itp_type_head_armor |itp_merchandise   ,0, 2000 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["khergit_cavalry_helmet", "Khergit Cavalry Helmet", [("lamellar_helmet_b",0)], itp_type_head_armor | itp_merchandise   ,0, 1800 , weight(2)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],

["black_hood", "Black Hood", [("hood_black",0)], itp_type_head_armor|itp_merchandise   ,0, 900 , weight(2)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["light_leather", "Light Leather", [("light_leather",0)], itp_type_body_armor|itp_covers_legs|itp_merchandise   ,0, 2845 , weight(5)|abundance(100)|head_armor(0)|body_armor(26)|leg_armor(7)|difficulty(0) ,imodbits_armor ],
["light_leather_boots",  "Light Leather Boots", [("light_leather_boots",0)], itp_type_foot_armor |itp_merchandise| itp_attach_armature,0, 525 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(15)|difficulty(0) ,imodbits_cloth ],
["mail_and_plate", "Mail and Plate", [("mail_and_plate",0)], itp_type_body_armor|itp_covers_legs   ,0, 4420 , weight(16)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["light_mail_and_plate", "Light Mail and Plate", [("light_mail_and_plate",0)], itp_type_body_armor|itp_covers_legs   ,0, 3620, weight(10)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],

["byzantion_helmet_a", "Byzantion Helmet", [("byzantion_helmet_a",0)], itp_type_head_armor   ,0, 1000 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["magyar_helmet_a", "Magyar Helmet", [("magyar_helmet_a",0)], itp_type_head_armor   ,0, 1000 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["rus_helmet_a", "Rus Helmet", [("rus_helmet_a",0)], itp_type_head_armor   ,0, 1000 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["sipahi_helmet_a", "Sipahi Helmet", [("sipahi_helmet_a",0)], itp_type_head_armor   ,0, 1000 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["shahi", "Shahi", [("shahi",0)], itp_type_head_armor   ,0, 1000 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["rabati", "Rabati", [("rabati",0)], itp_type_head_armor   ,0, 1000 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],

["tunic_with_green_cape", "Tunic with Green Cape", [("peasant_man_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 6 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0) ,imodbits_cloth ], 
["keys", "Ring of Keys", [("throwing_axe_a",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,
240, weight(5)|spd_rtng(98) | swing_damage(29,cut)|max_ammo(5)|weapon_length(53),imodbits_thrown ], 
["bride_dress", "Bride Dress", [("bride_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["bride_crown", "Crown of Flowers", [("bride_crown",0)],  itp_type_head_armor | itp_doesnt_cover_hair |itp_civilian |itp_attach_armature,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["bride_shoes", "Bride Shoes", [("bride_shoes",0)], itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 30 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],

["practice_bow_2","Practice Bow", [("hunting_bow",0), ("hunting_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 0, weight(1.5)|spd_rtng(90) | shoot_speed(40) | thrust_damage(21, blunt),imodbits_bow ],
["practice_arrows_2","Practice Arrows", [("arena_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_missile],


["plate_boots", "Plate Boots", [("plate_boots",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature,0,
 1155 , weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(9) ,imodbits_plate ], 

["heraldic_mail_with_surcoat_for_tableau", "{!}Heraldic Mail with Surcoat", [("heraldic_armor_new_a",0)], itp_type_body_armor |itp_covers_legs ,0,
 135, weight(22)|abundance(100)|head_armor(0)|body_armor(1)|leg_armor(1),imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_a", ":agent_no", ":troop_no")])]],
["mail_boots_for_tableau", "Mail Boots", [("mail_boots_a",0)], itp_type_foot_armor | itp_attach_armature  ,0,
 35, weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(1) ,imodbits_armor ],
["warhorse_sarranid","Sarranian War Horse", [("warhorse_sarranid",0)], itp_merchandise|itp_type_horse, 0, 2700,abundance(40)|hit_points(165)|body_armor(58)|difficulty(4)|horse_speed(40)|horse_maneuver(44)|horse_charge(32)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6]],
["warhorse_steppe","Steppe Charger", [("warhorse_steppe",0)], itp_merchandise|itp_type_horse, 0, 2175,abundance(45)|hit_points(150)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(50)|horse_charge(28)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3,fac_kingdom_2]],

###############################
###FULL INVASION ITEMS BEGIN###
###############################

["wabbajack_cast", "Legendary Wabbajack", [("wabbajack",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_cant_use_on_horseback|itp_next_item_as_melee, itcf_thrust_onehanded|itcf_carry_sword_back, 0, weight(2)|spd_rtng(97)|weapon_length(1)|thrust_damage(1,blunt),imodbits_polearm,[														
	(ti_on_weapon_attack,													
	[													
		(multiplayer_is_server),												
		(store_trigger_param_1, ":aggressor_id"),												
		(call_script, "script_wabbajack_cast", ":aggressor_id"),												
	]),													
]],															
["wabbajack_melee", "Legendary Wabbajack", [("wabbajack",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_cant_use_on_horseback, itc_staff|itcf_carry_sword_back, 0, weight(2)|spd_rtng(97)|weapon_length(133)|swing_damage(28,blunt)|thrust_damage(20, blunt),imodbits_polearm ],
["wabbajack_missile","Wabbajack Spell", [("invalid_item",0),("huojian_fly4",ixmesh_flying_ammo),("invalid_item", ixmesh_carry)], itp_type_bolts|itp_merchandise|itp_default_ammo|itp_can_penetrate_shield|itp_ignore_gravity|itp_no_pick_up_from_ground, 0,0,weight(1)|weapon_length(95)|thrust_damage(100,pierce)|max_ammo(100),imodbits_missile,[														
	(ti_on_missile_hit,													
	[													
		(particle_system_burst, "psys_war_smoke_tall", pos1, 1),												
	]),													
]],

### Banner Items ###

["banner_swadia","Swadian Banner", [("banner_swadia",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry, itcf_carry_spear|itc_slashing_polearm,
 1125, weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20, blunt),imodbits_polearm ],
["banner_vaegir","Vaegir Banner", [("banner_vaegir",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry, itcf_carry_spear|itc_slashing_polearm,
 1125, weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20, blunt),imodbits_polearm ],
["banner_khergit","Khergit Banner", [("banner_khergit",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry, itcf_carry_spear|itc_slashing_polearm,
 1125, weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20, blunt),imodbits_polearm ],
["banner_nord","Nord Banner", [("banner_nord",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry, itcf_carry_spear|itc_slashing_polearm,
 1125, weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20, blunt),imodbits_polearm ],
["banner_rhodok","Rhodok Banner", [("banner_rhodok",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry, itcf_carry_spear|itc_slashing_polearm,
 1125, weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20, blunt),imodbits_polearm ],
["banner_sarranid","Sarranian Banner", [("banner_sarranid",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry, itcf_carry_spear|itc_slashing_polearm,
 1125, weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20, blunt),imodbits_polearm ],
 
 ### Native Retextured Items ###
 
 ## Native Retextured Body Armor ##
 
["coat_of_plates_c", "Coat of Plates", [("coat_of_plates_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["coat_of_plates_d", "Coat of Plates", [("coat_of_plates_d",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["coat_of_plates_e", "Coat of Plates", [("coat_of_plates_e",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
 
["haubergeon_e", "Haubergeon", [("haubergeon_e",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4310 , weight(18)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(6)|difficulty(6) ,imodbits_armor ],
["haubergeon_f", "Haubergeon", [("haubergeon_f",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4310 , weight(18)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(6)|difficulty(6) ,imodbits_armor ],
["haubergeon_h", "Haubergeon", [("haubergeon_h",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4310 , weight(18)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(6)|difficulty(6) ,imodbits_armor ],
 
["byrnie_b_new", "Byrnie", [("byrnie_b_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4110 , weight(17)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(6)|difficulty(7) ,imodbits_armor ],
["byrnie_d_new", "Byrnie", [("byrnie_d_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4110 , weight(17)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(6)|difficulty(7) ,imodbits_armor ],
["byrnie_e_new", "Byrnie", [("byrnie_e_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4110 , weight(17)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(6)|difficulty(7) ,imodbits_armor ],
["byrnie_g_new", "Byrnie", [("byrnie_g_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4110 , weight(17)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(6)|difficulty(7) ,imodbits_armor ],
 
["tabard_blu", "Tabard", [("tabard_blu",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_blu3", "Tabard", [("tabard_blu3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_red2", "Tabard", [("tabard_red2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_grn2", "Tabard", [("tabard_grn2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_grn4", "Tabard", [("tabard_grn4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_prp2", "Tabard", [("tabard_prp2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_prp3", "Tabard", [("tabard_prp3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_blk", "Tabard", [("tabard_blk",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_blk2", "Tabard", [("tabard_blk2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["tabard_blk3", "Tabard", [("tabard_blk3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 1610 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
 
["tabard_mail_a", "Tabard With Mail", [("tabard_mail_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_b", "Nordic Tabard With Mail", [("tabard_mail_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_b3", "Nordic Tabard With Mail", [("tabard_mail_b3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_b4", "Nordic Tabard With Mail", [("tabard_mail_b4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_c", "Swadian Tabard With Mail", [("tabard_mail_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_c2", "Swadian Tabard With Mail", [("tabard_mail_c2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_c4", "Swadian Tabard With Mail", [("tabard_mail_c4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_d", "Rhodok Tabard With Mail", [("tabard_mail_d",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_d2", "Rhodok Tabard With Mail", [("tabard_mail_d2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_d3", "Rhodok Tabard With Mail", [("tabard_mail_d3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_d4", "Rhodok Tabard With Mail", [("tabard_mail_d4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_f", "Tabard With Mail", [("tabard_mail_f",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_f2", "Tabard With Mail", [("tabard_mail_f2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["tabard_mail_f3", "Tabard With Mail", [("tabard_mail_f3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 3520 , weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
 
["mail_and_plateb", "Green Mail and Plate", [("mail_and_plateb",0)], itp_type_body_armor|itp_covers_legs   ,0, 4420 , weight(16)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["mail_and_platec", "Red Mail and Plate", [("mail_and_platec",0)], itp_type_body_armor|itp_covers_legs   ,0, 4420 , weight(16)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["mail_and_plated", "Purple Mail and Plate", [("mail_and_plated",0)], itp_type_body_armor|itp_covers_legs   ,0, 4420 , weight(16)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["mail_and_platee", "Yellow Mail and Plate", [("mail_and_platee",0)], itp_type_body_armor|itp_covers_legs   ,0, 4420 , weight(16)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["mail_and_plateg", "Black Mail and Plate", [("mail_and_plateg",0)], itp_type_body_armor|itp_covers_legs   ,0, 4420 , weight(16)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(0) ,imodbits_armor ],

["padded_cloth_b_blu", "Nord Padded Cloth", [("padded_cloth_b_blu",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2410 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["padded_cloth_b_red", "Swadian Padded Cloth", [("padded_cloth_b_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2410 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["padded_cloth_b_ylw", "Yellow Padded Cloth", [("padded_cloth_b_ylw",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2410 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["padded_cloth_b_blk", "Black Padded Cloth", [("padded_cloth_b_blk",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2410 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["padded_cloth_b_grn", "Rhodok Padded Cloth", [("padded_cloth_b_grn",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2410 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],

["mail_vest_b", "Mail Vest", [("mail_vest_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4025 , weight(19)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(15)|difficulty(7) ,imodbits_armor ],
["mail_vest_blu", "Blue Mail Vest", [("mail_vest_blu",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4025 , weight(19)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(15)|difficulty(7) ,imodbits_armor ],
["mail_vest_red", "Red Mail Vest", [("mail_vest_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4025 , weight(19)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(15)|difficulty(7) ,imodbits_armor ],
["mail_vest_wht", "White Mail Vest", [("mail_vest_wht",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4025 , weight(19)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(15)|difficulty(7) ,imodbits_armor ],
["mail_vest_blk", "Black Mail Vest", [("mail_vest_blk",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4025 , weight(19)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(15)|difficulty(7) ,imodbits_armor ],

["mail_shirt_grn", "Green Mail Shirt", [("mail_shirt_grn",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4120 , weight(19)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(12)|difficulty(7) ,imodbits_armor ],
["mail_shirt_red", "Red Mail Shirt", [("mail_shirt_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4120 , weight(19)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(12)|difficulty(7) ,imodbits_armor ],
["mail_shirt_blk", "Black Mail Shirt", [("mail_shirt_blk",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4120 , weight(19)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(12)|difficulty(7) ,imodbits_armor ],
["mail_shirt_wht", "White Mail Shirt", [("mail_shirt_wht",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4120 , weight(19)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(12)|difficulty(7) ,imodbits_armor ],

["leather_armor_a4", "Studded Leather Coat", [("leather_armor_a4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3950 , weight(14)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(10)|difficulty(7) ,imodbits_armor ],

["light_armor1b", "Swadian Skirmisher Armor", [("light_armor1b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3280 , weight(8)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
["light_armor2", "Swadian Skirmisher Armor", [("light_armor2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3280 , weight(8)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
["light_armor2b", "Swadian Skirmisher Armor", [("light_armor2b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3280 , weight(8)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
["light_armor3b", "Swadian Skirmisher Armor", [("light_armor3b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3280 , weight(8)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
["light_armor4", "Swadian Skirmisher Armor", [("light_armor4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3280 , weight(8)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
["light_armor6", "Swadian Skirmisher Armor", [("light_armor6",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3280 , weight(8)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
["light_armor7", "Swadian Skirmisher Armor", [("light_armor7",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3280 , weight(8)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(8)|difficulty(7) ,imodbits_armor ],

["cuir_bouilli_blu2", "Nordic Cuir Bouilli", [("cuir_bouilli_blu2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5525 , weight(24)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(15)|difficulty(8) ,imodbits_armor ],
["cuir_bouilli_blu3", "Nordic Cuir Bouilli", [("cuir_bouilli_blu3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5525 , weight(24)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(15)|difficulty(8) ,imodbits_armor ],
["cuir_bouilli_blu4", "Nordic Cuir Bouilli", [("cuir_bouilli_blu4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5525 , weight(24)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(15)|difficulty(8) ,imodbits_armor ],

["mail_long_surcoat_new_b", "Mail with Surcoat", [("mail_long_surcoat_new_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_b2", "Mail with Surcoat", [("mail_long_surcoat_new_b2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_b3", "Mail with Surcoat", [("mail_long_surcoat_new_b3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_c", "Mail with Surcoat", [("mail_long_surcoat_new_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_c2", "Mail with Surcoat", [("mail_long_surcoat_new_c2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_c3", "Mail with Surcoat", [("mail_long_surcoat_new_c3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_e", "Mail with Surcoat", [("mail_long_surcoat_new_e",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_f", "Mail with Surcoat", [("mail_long_surcoat_new_f",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_f2", "Mail with Surcoat", [("mail_long_surcoat_new_f2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new_f3", "Mail with Surcoat", [("mail_long_surcoat_new_f3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new2", "Mail with Surcoat", [("mail_long_surcoat_new2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new3", "Mail with Surcoat", [("mail_long_surcoat_new3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mail_long_surcoat_new4", "Mail with Surcoat", [("mail_long_surcoat_new4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],

["surcoat_over_mail_newb", "Surcoat over Mail", [("surcoat_over_mail_newb",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newb2", "Surcoat over Mail", [("surcoat_over_mail_newb2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newd", "Surcoat over Mail", [("surcoat_over_mail_newd",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newf", "Surcoat over Mail", [("surcoat_over_mail_newf",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newb3", "Surcoat over Mail", [("surcoat_over_mail_newb3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newc2", "Surcoat over Mail", [("surcoat_over_mail_newc2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newc3", "Surcoat over Mail", [("surcoat_over_mail_newc3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newc4", "Surcoat over Mail", [("surcoat_over_mail_newc4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newd2", "Surcoat over Mail", [("surcoat_over_mail_newd2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newd3", "Surcoat over Mail", [("surcoat_over_mail_newd3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newd4", "Surcoat over Mail", [("surcoat_over_mail_newd4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newe2", "Surcoat over Mail", [("surcoat_over_mail_newe2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_newe3", "Surcoat over Mail", [("surcoat_over_mail_newe3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_new2", "Surcoat over Mail", [("surcoat_over_mail_new2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail_new3", "Surcoat over Mail", [("surcoat_over_mail_new3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4790 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],

["lamellar_vest_a5", "Lamellar Vest", [("lamellar_vest_a5",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0, 4280 , weight(18)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(8)|difficulty(7) ,imodbits_cloth ],
["lamellar_vest_a6", "Lamellar Vest", [("lamellar_vest_a6",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0, 4280 , weight(18)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(8)|difficulty(7) ,imodbits_cloth ],
["lamellar_vest_a7", "Lamellar Vest", [("lamellar_vest_a7",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0, 4280 , weight(18)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(8)|difficulty(7) ,imodbits_cloth ],

["tunic_armor_a2", "Sarranid Elite Armor", [("tunic_armor_a2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian ,0, 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["tunic_armor_a3", "Sarranid Elite Armor", [("tunic_armor_a3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian ,0, 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["tunic_armor_a4", "Sarranid Elite Armor", [("tunic_armor_a4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian ,0, 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["tunic_armor_a5", "Sarranid Elite Armor", [("tunic_armor_a5",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian ,0, 5760 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],

["scale_armor_e3", "Scale Armor", [("scale_armor_e3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4385 , weight(8)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(11)|difficulty(7) ,imodbits_armor ],
["scale_armor_e4", "Scale Armor", [("scale_armor_e4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4385 , weight(8)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(11)|difficulty(7) ,imodbits_armor ],
["scale_armor_e6", "Scale Armor", [("scale_armor_e6",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4385 , weight(8)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(11)|difficulty(7) ,imodbits_armor ],

["archers_vest_2", "Archer's Padded Vest", [("archers_vest_2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2720 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["archers_vest_4", "Archer's Padded Vest", [("archers_vest_4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2720 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["archers_vest_5", "Archer's Padded Vest", [("archers_vest_5",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2720 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["archers_vest_6", "Archer's Padded Vest", [("archers_vest_6",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 2720 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],

["sarranid_leather_armor_3", "Sarranid Leather Armor", [("sarranid_leather_armor_3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3620 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["sarranid_leather_armor_5", "Sarranid Leather Armor", [("sarranid_leather_armor_5",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3620 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["sarranid_leather_armor_3b", "Leather Armor", [("sarranid_leather_armor_3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3620 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["sarranid_leather_armor_5b", "Leather Armor", [("sarranid_leather_armor_5",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3620 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["sarranid_leather_armor_6", "Sarranid Leather Armor", [("sarranid_leather_armor_6",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3620 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],

["arabian_armor_b2", "Sarranid Guard Armor", [("arabian_armor_b2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4080 , weight(19)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8)|difficulty(0) ,imodbits_armor],
["arabian_armor_b3", "Sarranid Guard Armor", [("arabian_armor_b3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4080 , weight(19)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8)|difficulty(0) ,imodbits_armor],
["arabian_armor_b4", "Sarranid Guard Armor", [("arabian_armor_b4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4080 , weight(19)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8)|difficulty(0) ,imodbits_armor],
["arabian_armor_b3b", "Guard Armor", [("arabian_armor_b3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4080 , weight(19)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8)|difficulty(0) ,imodbits_armor],
["arabian_armor_b4b", "Guard Armor", [("arabian_armor_b4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4080 , weight(19)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8)|difficulty(0) ,imodbits_armor],

["sarranian_mail_shirt4", "Sarranid Mail Shirt", [("sarranian_mail_shirt4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 4490 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["sarranian_mail_shirt5", "Sarranid Mail Shirt", [("sarranian_mail_shirt5",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 4490 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["sarranian_mail_shirt6", "Sarranid Mail Shirt", [("sarranian_mail_shirt6",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 4490 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["sarranian_mail_shirt8", "Sarranid Mail Shirt", [("sarranian_mail_shirt8",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 4490 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14)|difficulty(7) ,imodbits_armor ],

["skirmisher_armor2", "Skirmisher Armor", [("skirmisher_armor2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1815 , weight(3)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],

["lamellar_armor_b2", "Lamellar Armor", [("lamellar_armor_b2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5255 , weight(25)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(13)|difficulty(0) ,imodbits_armor ],
["lamellar_armor_b3", "Lamellar Armor", [("lamellar_armor_b3",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5255 , weight(25)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(13)|difficulty(0) ,imodbits_armor ],
["lamellar_armor_b4", "Lamellar Armor", [("lamellar_armor_b4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5255 , weight(25)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(13)|difficulty(0) ,imodbits_armor ],
["lamellar_armor_b6", "Lamellar Armor", [("lamellar_armor_b6",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5255 , weight(25)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(13)|difficulty(0) ,imodbits_armor ],

["lamellar_leather2", "Steppe Armor", [("lamellar_leather2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["lamellar_leather4", "Steppe Armor", [("lamellar_leather4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["lamellar_leather5", "Steppe Armor", [("lamellar_leather5",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["lamellar_leather6", "Steppe Armor", [("lamellar_leather6",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["lamellar_leather2b", "Lamellar Harness", [("lamellar_leather2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["lamellar_leather4b", "Lamellar Harness", [("lamellar_leather4",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["lamellar_leather5b", "Lamellar Harness", [("lamellar_leather5",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["lamellar_leather6b", "Lamellar Harness", [("lamellar_leather6",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1880 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],

["nomad_robe_ab", "Nomad Robe", [("nomad_robe_ab",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs |itp_civilian,0, 3550 , weight(15)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nomad_robe_ac", "Nomad Robe", [("nomad_robe_ac",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs |itp_civilian,0, 3550 , weight(15)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nomad_robe_ad", "Nomad Robe", [("nomad_robe_ad",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs |itp_civilian,0, 3550 , weight(15)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nomad_robe_ae", "Nomad Robe", [("nomad_robe_ae",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs |itp_civilian,0, 3550 , weight(15)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nomad_robe_af", "Nomad Robe", [("nomad_robe_af",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs |itp_civilian,0, 3550 , weight(15)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],

["lamellar_armor_elite", "Lamellar Armor", [("lamellar_armor_elite",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 6060 , weight(25)|abundance(100)|head_armor(0)|body_armor(55)|leg_armor(16)|difficulty(0) ,imodbits_armor ],
["lamellar_armor_elite2", "Lamellar Armor", [("lamellar_armor_elite2",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 6060 , weight(25)|abundance(100)|head_armor(0)|body_armor(55)|leg_armor(16)|difficulty(0) ,imodbits_armor ],

["nobleman_armor_swadian", "Swadian Nobleman's Cloak", [("nobleman_armor_swadian",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4990 , weight(22)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["nobleman_armor_rhodok", "Rhodok Nobleman's Cloak", [("nobleman_armor_rhodok",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4990 , weight(22)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["nobleman_armor_nord", "Nordic Nobleman's Cloak", [("nobleman_armor_nord",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4990 , weight(22)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(14)|difficulty(7) ,imodbits_armor ],


 ## Native Retextured Head Armor ##
 
["neckguard_helm_newblu", "Blue Helmet with Neckguard", [("neckguard_helm_newblu",0)], itp_merchandise| itp_type_head_armor   ,0, 1600 , weight(1.5)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["neckguard_helm_newgrn", "Green Helmet with Neckguard", [("neckguard_helm_newgrn",0)], itp_merchandise| itp_type_head_armor   ,0, 1600 , weight(1.5)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["neckguard_helm_newred", "Red Helmet with Neckguard", [("neckguard_helm_newred",0)], itp_merchandise| itp_type_head_armor   ,0, 1600 , weight(1.5)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["neckguard_helm_newylw", "Yellow Helmet with Neckguard", [("neckguard_helm_newylw",0)], itp_merchandise| itp_type_head_armor   ,0, 1600 , weight(1.5)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["skull_cap_new_b", "Red Footman's Helmet", [("skull_cap_new_b_1",0)], itp_merchandise| itp_type_head_armor   ,0, 1200 , weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["skull_cap_new_c", "Blue Footman's Helmet", [("skull_cap_new_c",0)], itp_merchandise| itp_type_head_armor   ,0, 1200 , weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["skull_cap_new_d", "Green Footman's Helmet", [("skull_cap_new_d",0)], itp_merchandise| itp_type_head_armor   ,0, 1200 , weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["skull_cap_new_e", "Yellow Footman's Helmet", [("skull_cap_new_e",0)], itp_merchandise| itp_type_head_armor   ,0, 1200 , weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],

["reinf_helmet_newblu", "Blue Guard Helmet", [("reinf_helmet_newblu",0)], itp_merchandise| itp_type_head_armor   ,0, 2350 , weight(2.5)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(9) ,imodbits_plate ],
["reinf_helmet_newgrn", "Green Guard Helmet", [("reinf_helmet_newgrn",0)], itp_merchandise| itp_type_head_armor   ,0, 2350 , weight(2.5)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(9) ,imodbits_plate ],
["reinf_helmet_newred", "Red Guard Helmet", [("reinf_helmet_newred",0)], itp_merchandise| itp_type_head_armor   ,0, 2350 , weight(2.5)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(9) ,imodbits_plate ],
["reinf_helmet_newylw", "Yellow Guard Helmet", [("reinf_helmet_newylw",0)], itp_merchandise| itp_type_head_armor   ,0, 2350 , weight(2.5)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(9) ,imodbits_plate ],

["flattop_helmet_new_grn", "Flat Topped Helmet", [("flattop_helmet_new_grn",0)], itp_merchandise| itp_type_head_armor   ,0, 1650 , weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["flattop_helmet_new_wht", "Flat Topped Helmet", [("flattop_helmet_new_wht",0)], itp_merchandise| itp_type_head_armor   ,0, 1650 , weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["flattop_helmet_new_ylw", "Flat Topped Helmet", [("flattop_helmet_new_ylw",0)], itp_merchandise| itp_type_head_armor   ,0, 1650 , weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["kettle_hat_new_b", "Kettle Hat", [("kettle_hat_new_b",0)], itp_merchandise| itp_type_head_armor,0, 1750 , weight(1.75)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["kettle_hat_new_c", "Kettle Hat", [("kettle_hat_new_c",0)], itp_merchandise| itp_type_head_armor,0, 1750 , weight(1.75)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["kettle_hat_new_d", "Kettle Hat", [("kettle_hat_new_d",0)], itp_merchandise| itp_type_head_armor,0, 1750 , weight(1.75)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["kettle_hat_new_e", "Kettle Hat", [("kettle_hat_new_e",0)], itp_merchandise| itp_type_head_armor,0, 1750 , weight(1.75)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["maciejowski_helmet_new_b", "Horned Great Helmet", [("maciejowski_helmet_new_b",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2650 , weight(2.75)|abundance(100)|head_armor(53)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_bblu", "Horned Blue Great Helmet", [("maciejowski_helmet_new_bblu",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2650 , weight(2.75)|abundance(100)|head_armor(53)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_bgrn", "Horned Green Great Helmet", [("maciejowski_helmet_new_bgrn",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2650 , weight(2.75)|abundance(100)|head_armor(53)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_bred", "Horned Red Great Helmet", [("maciejowski_helmet_new_bred",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2650 , weight(2.75)|abundance(100)|head_armor(53)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_bylw", "Horned Yellow Great Helmet", [("maciejowski_helmet_new_bylw",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2650 , weight(2.75)|abundance(100)|head_armor(53)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["maciejowski_helmet_new_b2", "Great Helmet", [("maciejowski_helmet_new_b2",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_b2blu", "Blue Great Helmet", [("maciejowski_helmet_new_b2blu",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_b2grn", "Green Great Helmet", [("maciejowski_helmet_new_b2grn",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_b2red", "Red Great Helmet", [("maciejowski_helmet_new_b2red",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_b2ylw", "Yellow Great Helmet", [("maciejowski_helmet_new_b2ylw",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["maciejowski_helmet_new_grn", "Green Winged Great Helmet", [("maciejowski_helmet_new_grn",0)], itp_merchandise|itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_wht", "White Winged Great Helmet", [("maciejowski_helmet_new_wht",0)], itp_merchandise|itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["maciejowski_helmet_new_ylw", "Yellow Winged Great Helmet", [("maciejowski_helmet_new_ylw",0)], itp_merchandise|itp_type_head_armor|itp_covers_head,0, 2750 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["great_helmet_new_plain", "Plain Great Helmet", [("great_helmet_new_plain",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newb", "Blue Great Helmet", [("great_helmet_newb",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newc", "Yellow Great Helmet", [("great_helmet_newc",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newd", "Great Helmet", [("great_helmet_newd",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newf3", "Green Great Helmet", [("great_helmet_newf3",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newg", "Red Great Helmet", [("great_helmet_newg",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newg2", "Red Great Helmet", [("great_helmet_newg2",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newg3", "Red Great Helmet", [("great_helmet_newg3",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newi", "White Helmet", [("great_helmet_newi",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_newj", "Black Great Helmet", [("great_helmet_newj",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["bascinet_new_a10", "Bascinet with Aventail", [("bascinet_new_a10",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_new_a12", "Bascinet with Aventail", [("bascinet_new_a12",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_new_a3", "Bascinet with Aventail", [("bascinet_new_a3",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_new_a6", "Bascinet with Aventail", [("bascinet_new_a6",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_new_a7", "Bascinet with Aventail", [("bascinet_new_a7",0)], itp_merchandise|itp_type_head_armor   ,0, 2250 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

["bascinet_new_b10", "Bascinet with Nose Guard", [("bascinet_new_b10",0)], itp_merchandise|itp_type_head_armor   ,0, 2300 , weight(2.25)|abundance(100)|head_armor(46)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_new_b12", "Bascinet with Nose Guard", [("bascinet_new_b12",0)], itp_merchandise|itp_type_head_armor   ,0, 2300 , weight(2.25)|abundance(100)|head_armor(46)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_new_b3", "Bascinet with Nose Guard", [("bascinet_new_b3",0)], itp_merchandise|itp_type_head_armor   ,0, 2300 , weight(2.25)|abundance(100)|head_armor(46)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_new_b6", "Bascinet with Nose Guard", [("bascinet_new_b6",0)], itp_merchandise|itp_type_head_armor   ,0, 2300 , weight(2.25)|abundance(100)|head_armor(46)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_new_b7", "Bascinet with Nose Guard", [("bascinet_new_b7",0)], itp_merchandise|itp_type_head_armor   ,0, 2300 , weight(2.25)|abundance(100)|head_armor(46)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

["great_helmet_new2", "Great Helmet", [("great_helmet_new2",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["great_helmet_new_b2", "Full Helm", [("great_helmet_new_b2",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2600 , weight(2.5)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b3", "Red Full Helm", [("great_helmet_new_b3",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2600 , weight(2.5)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b4", "Blue Full Helm", [("great_helmet_new_b4",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2600 , weight(2.5)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b5", "Yellow Full Helm", [("great_helmet_new_b5",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2600 , weight(2.5)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b6", "Green Full Helm", [("great_helmet_new_b6",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2600 , weight(2.5)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b7", "Black Full Helm", [("great_helmet_new_b7",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2600 , weight(2.5)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["great_helmet_new_b8", "Full Helm", [("great_helmet_new_b8",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2550 , weight(2.5)|abundance(100)|head_armor(51)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b9", "Red Full Helm", [("great_helmet_new_b9",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2550 , weight(2.5)|abundance(100)|head_armor(51)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b10", "Blue Full Helm", [("great_helmet_new_b10",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2550 , weight(2.5)|abundance(100)|head_armor(51)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b11", "Yellow Full Helm", [("great_helmet_new_b11",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2550 , weight(2.5)|abundance(100)|head_armor(51)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b12", "Green Full Helm", [("great_helmet_new_b12",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2550 , weight(2.5)|abundance(100)|head_armor(51)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet_new_b13", "Black Full Helm", [("great_helmet_new_b13",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 2550 , weight(2.5)|abundance(100)|head_armor(51)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["sugarloaf_helmet_new2", "Sugarloaf Helmet", [("sugarloaf_helmet_new2",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["sugarloaf_helmet_new3", "Sugarloaf Helmet", [("sugarloaf_helmet_new3",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2700 , weight(2.75)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["sar_helmet_blu1", "Sarranid Keffiyeh Helmet", [("sar_helmet_blu1",0)], itp_merchandise| itp_type_head_armor   ,0, 1750 , weight(2.50)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sar_helmet_grn1", "Sarranid Keffiyeh Helmet", [("sar_helmet_grn1",0)], itp_merchandise| itp_type_head_armor   ,0, 1750 , weight(2.50)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sar_helmet_red1", "Sarranid Keffiyeh Helmet", [("sar_helmet_red1",0)], itp_merchandise| itp_type_head_armor   ,0, 1750 , weight(2.50)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["sar_helmet_blu2", "Horseman Helmet", [("sar_helmet_blu2",0)], itp_merchandise| itp_type_head_armor   ,0, 1250 , weight(2.75)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sar_helmet_grn2", "Horseman Helmet", [("sar_helmet_grn2",0)], itp_merchandise| itp_type_head_armor   ,0, 1250 , weight(2.75)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sar_helmet_red2", "Horseman Helmet", [("sar_helmet_red2",0)], itp_merchandise| itp_type_head_armor   ,0, 1250 , weight(2.75)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["sar_helmet_blu3", "Sarranid Felt Hat", [("sar_helmet_blu3",0)], itp_merchandise| itp_type_head_armor   ,0, 250 , weight(2)|abundance(100)|head_armor(5)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["sar_helmet_grn3", "Sarranid Felt Hat", [("sar_helmet_grn3",0)], itp_merchandise| itp_type_head_armor   ,0, 250 , weight(2)|abundance(100)|head_armor(5)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["sar_helmet_red3", "Sarranid Felt Hat", [("sar_helmet_red3",0)], itp_merchandise| itp_type_head_armor   ,0, 250 , weight(2)|abundance(100)|head_armor(5)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],

["tuareg_helmet2_blu", "Sarranid Mail Coif", [("tuareg_helmet2_blu",0)], itp_merchandise| itp_type_head_armor ,0, 2050 , weight(3)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["tuareg_helmet2_grn", "Sarranid Mail Coif", [("tuareg_helmet2_grn",0)], itp_merchandise| itp_type_head_armor ,0, 2050 , weight(3)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["tuareg_helmet2_red", "Sarranid Mail Coif", [("tuareg_helmet2_red",0)], itp_merchandise| itp_type_head_armor ,0, 2050 , weight(3)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["tuareg_helmet_blu", "Sarranid Warrior Cap", [("tuareg_helmet_blu",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 950 , weight(2)|abundance(100)|head_armor(19)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["tuareg_helmet_grn", "Sarranid Warrior Cap", [("tuareg_helmet_grn",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 950 , weight(2)|abundance(100)|head_armor(19)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["tuareg_helmet_red", "Sarranid Warrior Cap", [("tuareg_helmet_red",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 950 , weight(2)|abundance(100)|head_armor(19)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["tuareg_helmet_red2", "Warrior Cap", [("tuareg_helmet_red",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 950 , weight(2)|abundance(100)|head_armor(19)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["tuareg_open_blu", "Turban", [("tuareg_open_blu",0)], itp_merchandise| itp_type_head_armor   ,0, 550 , weight(1)|abundance(100)|head_armor(11)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["tuareg_open_grn", "Turban", [("tuareg_open_grn",0)], itp_merchandise| itp_type_head_armor   ,0, 550 , weight(1)|abundance(100)|head_armor(11)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["tuareg_open_red", "Turban", [("tuareg_open_red",0)], itp_merchandise| itp_type_head_armor   ,0, 550 , weight(1)|abundance(100)|head_armor(11)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],

["tuareg_blu", "Desert Turban", [("tuareg_blu",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard ,0, 700 , weight(1.50)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["tuareg_grn", "Desert Turban", [("tuareg_grn",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard ,0, 700 , weight(1.50)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["tuareg_red", "Desert Turban", [("tuareg_red",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard ,0, 700 , weight(1.50)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],

 ## Native Retextured Shields ##
 
["shield_kite_swad", "Swadian Kite Shield", [("shield_kite_swad",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  350 , weight(2.5)|hit_points(480)|body_armor(8)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["shield_kite_swad3", "Swadian Kite Shield", [("shield_kite_swad3",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  350 , weight(2.5)|hit_points(480)|body_armor(8)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["shield_kite_swad4", "Swadian Kite Shield", [("shield_kite_swad4",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  350 , weight(2.5)|hit_points(480)|body_armor(8)|spd_rtng(82)|shield_width(90),imodbits_shield ],

["shield_kite_rhod", "Rhodok Kite Shield", [("shield_kite_rhod",0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield,  550 , weight(2.5)|hit_points(600)|body_armor(10)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["shield_kite_rhod2", "Rhodok Kite Shield", [("shield_kite_rhod2",0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield,  550 , weight(2.5)|hit_points(600)|body_armor(10)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["shield_kite_rhod3", "Rhodok Kite Shield", [("shield_kite_rhod3",0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield,  550 , weight(2.5)|hit_points(600)|body_armor(10)|spd_rtng(82)|shield_width(90),imodbits_shield ],

["shield_kite_nord", "Nord Kite Shield", [("shield_kite_nord",0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield,  550 , weight(2.5)|hit_points(600)|body_armor(10)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["shield_kite_nord2", "Nord Kite Shield", [("shield_kite_nord2",0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield,  550 , weight(2.5)|hit_points(600)|body_armor(10)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["shield_kite_nord3", "Nord Kite Shield", [("shield_kite_nord3",0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield,  550 , weight(2.5)|hit_points(600)|body_armor(10)|spd_rtng(82)|shield_width(90),imodbits_shield ],

["shield_round_swad", "Swadian Round Shield", [("shield_round_swad",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  300 , weight(2.5)|hit_points(360)|body_armor(14)|spd_rtng(96)|shield_width(40),imodbits_shield ],
["shield_round_rhod", "Rhodok Round Shield", [("shield_round_rhod",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  300 , weight(2.5)|hit_points(360)|body_armor(14)|spd_rtng(96)|shield_width(40),imodbits_shield ],
["shield_round_nord", "Nord Round Shield", [("shield_round_nord",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  300 , weight(2.5)|hit_points(360)|body_armor(14)|spd_rtng(96)|shield_width(40),imodbits_shield ],

["shield_knight_zs1", "Swadian Knight Shield", [("shield_knight_zs1",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_zr2", "Rhodok Knight Shield", [("shield_knight_zr2",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_zn3", "Nord Knight Shield", [("shield_knight_zn3",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],

["shield_knight_ds", "Knight Shield", [("shield_knight_ds",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_es", "Knight Shield", [("shield_knight_es",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_fn", "Knight Shield", [("shield_knight_fn",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_gr", "Knight Shield", [("shield_knight_gr",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_ir", "Knight Shield", [("shield_knight_ir",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_js", "Knight Shield", [("shield_knight_js",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_ks", "Knight Shield", [("shield_knight_ks",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_ln", "Knight Shield", [("shield_knight_ln",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_tr", "Knight Shield", [("shield_knight_tr",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_ur", "Knight Shield", [("shield_knight_ur",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_vs", "Knight Shield", [("shield_knight_vs",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_ws", "Knight Shield", [("shield_knight_ws",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["shield_knight_xr", "Knight Shield", [("shield_knight_xr",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  425 , weight(3.5)|hit_points(720)|body_armor(10)|spd_rtng(80)|shield_width(60),imodbits_shield ],

### Native Retextured Items End ###

## Narf head armor ##

["chapel_de_fer_mail1", "Mailed Kettle Hat", [("chapel_de_fer_mail1",0)], itp_merchandise|itp_type_head_armor|itp_attach_armature   ,0, 2100 , weight(2.75)|abundance(100)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["chapel_de_fer_mail2", "Mailed Kettle Hat", [("chapel_de_fer_mail2",0)], itp_merchandise|itp_type_head_armor|itp_attach_armature   ,0, 2100 , weight(2.75)|abundance(100)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["chapel_de_fer_mail3", "Mailed Kettle Hat", [("chapel_de_fer_mail3",0)], itp_merchandise|itp_type_head_armor|itp_attach_armature   ,0, 2100 , weight(2.75)|abundance(100)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

["hounskull", "Houndskull Helmet", [("hounskull",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 2900 , weight(2.75)|abundance(100)|head_armor(58)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["sallet_a_closed1", "Sallet Helmet with Gorget", [("sallet_a_closed1",0)], itp_covers_head|itp_type_head_armor|itp_attach_armature   ,0, 2600 , weight(2.25)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["sallet_a_closed2", "Sallet Helmet with Gorget", [("sallet_a_closed2",0)], itp_covers_head|itp_type_head_armor|itp_attach_armature   ,0, 2600 , weight(2.25)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["sallet_a_open1", "Open Sallet Helmet with Gorget", [("sallet_a_open1",0)], itp_type_head_armor|itp_attach_armature   ,0, 2400 , weight(2.25)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["sallet_a_open2", "Open Sallet Helmet with Gorget", [("sallet_a_open2",0)], itp_type_head_armor|itp_attach_armature   ,0, 2400 , weight(2.25)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["sallet_b_closed1", "Sallet Helmet with Gorget", [("sallet_a_closed1",0)], itp_covers_head|itp_type_head_armor|itp_attach_armature   ,0, 2600 , weight(2.25)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["sallet_b_closed2", "Sallet Helmet with Gorget", [("sallet_a_closed2",0)], itp_covers_head|itp_type_head_armor|itp_attach_armature   ,0, 2600 , weight(2.25)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["sallet_b_open1", "Open Sallet Helmet with Gorget", [("sallet_a_open1",0)], itp_type_head_armor|itp_attach_armature   ,0, 2400 , weight(2.25)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["sallet_b_open2", "Open Sallet Helmet with Gorget", [("sallet_a_open2",0)], itp_type_head_armor|itp_attach_armature   ,0, 2400 , weight(2.25)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

["zitta_bascinet", "Closed Bascinet Helmet", [("zitta_bascinet",0)], itp_attach_armature| itp_type_head_armor|itp_covers_head,0, 2800 , weight(2.75)|abundance(100)|head_armor(56)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["zitta_bascinet_novisor", "Bascinet Helmet", [("zitta_bascinet_novisor",0)], itp_attach_armature| itp_type_head_armor,0, 2250 , weight(2.75)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],

["visored_salet", "Visored Sallet", [("visored_salet",0)], itp_type_head_armor|itp_covers_head   ,0, 2900 , weight(2.75)|abundance(100)|head_armor(58)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

## Narf gauntlets ##

["leather_gauntlet","Leather Gloves", [("leather_gauntlet_L",0)], itp_merchandise|itp_type_hand_armor,0, 150, weight(0.25)|abundance(100)|body_armor(1)|difficulty(0),imodbits_armor],
["mail_gauntlets","Mail Gauntlets", [("mail_gauntlets_L",0)], itp_merchandise|itp_type_hand_armor,0, 300, weight(0.5)|abundance(100)|body_armor(2)|difficulty(0),imodbits_armor],
["demi_gauntlets","Demi Gauntlets", [("demi_gauntlets_L",0)], itp_merchandise|itp_type_hand_armor,0, 300, weight(0.5)|abundance(100)|body_armor(2)|difficulty(0),imodbits_armor],
["wisby_gauntlets_black","Black Wisby Gauntlets", [("wisby_gauntlets_black_L",0)], itp_merchandise|itp_type_hand_armor,0, 450, weight(0.75)|abundance(100)|body_armor(3)|difficulty(0),imodbits_armor],
["wisby_gauntlets_red","Red Wisby Gauntlets", [("wisby_gauntlets_red_L",0)], itp_merchandise|itp_type_hand_armor,0, 450, weight(0.75)|abundance(100)|body_armor(3)|difficulty(0),imodbits_armor],
["finger_gauntlets","Lobstered Gauntlets", [("finger_gauntlets_L",0)], itp_merchandise|itp_type_hand_armor,0, 450, weight(0.75)|abundance(100)|body_armor(3)|difficulty(0),imodbits_armor],
["hourglass_gauntlets","Hourglass Gauntlets", [("hourglass_gauntlets_L",0)], itp_merchandise|itp_type_hand_armor,0, 600, weight(0.75)|abundance(100)|body_armor(4)|difficulty(0),imodbits_armor],
["hourglass_gauntlets_ornate","Decorated Hourglass Gauntlets", [("hourglass_gauntlets_ornate_L",0)], itp_merchandise|itp_type_hand_armor,0, 600, weight(0.75)|abundance(100)|body_armor(4)|difficulty(0),imodbits_armor],

## Narf body armor ##

["aketon_kneecops", "Aketon with Kneecops", [("aketon_kneecops",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3060 , weight(11)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(16)|difficulty(0) ,imodbits_cloth ],

["brigandine_blue", "Blue Brigandine", [("brigandine_blue",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3780 , weight(11)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["brigandine_black", "Black Brigandine", [("brigandine_black",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3780 , weight(11)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["brigandine_brown", "Brown Brigandine", [("brigandine_brown",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3780 , weight(11)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["brigandine_green", "Green Brigandine", [("brigandine_green",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3780 , weight(11)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["brigandine_red", "Red Brigandine", [("brigandine_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3780 , weight(11)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],

["brigandine_blue_mail", "Mailed Blue Brigandine", [("brigandine_blue_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4830 , weight(13)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_black_mail", "Mailed Black Brigandine", [("brigandine_black_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4830 , weight(13)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_brown_mail", "Mailed Brown Brigandine", [("brigandine_brown_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4830 , weight(13)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_green_mail", "Mailed Green Brigandine", [("brigandine_green_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4830 , weight(13)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_red_mail", "Mailed Red Brigandine", [("brigandine_red_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 4830 , weight(13)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],

["brigandine_blue_plate_mail", "Plated Blue Brigandine", [("brigandine_blue_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5430 , weight(15)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_black_plate_mail", "Plated Black Brigandine", [("brigandine_black_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5430 , weight(15)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_brown_plate_mail", "Plated Brown Brigandine", [("brigandine_brown_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5430 , weight(15)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_green_plate_mail", "Plated Green Brigandine", [("brigandine_green_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5430 , weight(15)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_red_plate_mail", "Plated Red Brigandine", [("brigandine_red_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5430 , weight(15)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],

["brigandine_blue_heavy_plate_mail", "Heavy Plated Blue Brigandine", [("brigandine_blue_heavy_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5930 , weight(17)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_black_heavy_plate_mail", "Heavy Plated Black Brigandine", [("brigandine_black_heavy_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5930 , weight(17)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_brown_heavy_plate_mail", "Heavy Plated Brown Brigandine", [("brigandine_brown_heavy_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5930 , weight(17)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_green_heavy_plate_mail", "Heavy Plated Green Brigandine", [("brigandine_green_heavy_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5930 , weight(17)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["brigandine_red_heavy_plate_mail", "Heavy Plated Red Brigandine", [("brigandine_red_heavy_plate_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5930 , weight(17)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],

["corrazina_red", "Red Corrazina", [("corrazina_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 6300 , weight(17)|abundance(100)|head_armor(0)|body_armor(56)|leg_armor(20)|difficulty(0) ,imodbits_cloth ],
["corrazina_grey", "Grey Corrazina", [("corrazina_grey",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 6300 , weight(17)|abundance(100)|head_armor(0)|body_armor(56)|leg_armor(20)|difficulty(0) ,imodbits_cloth ],
["corrazina_green", "Green Corrazina", [("corrazina_green",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 6300 , weight(17)|abundance(100)|head_armor(0)|body_armor(56)|leg_armor(20)|difficulty(0) ,imodbits_cloth ],

["churburg_13_mail", "Mailed Plate Armor", [("churburg_13_mail",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 6870 , weight(17)|abundance(100)|head_armor(0)|body_armor(61)|leg_armor(22)|difficulty(0) ,imodbits_cloth ],

["gothic_armour", "Gothic Armor", [("gothic_armour",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 7070 , weight(22)|abundance(100)|head_armor(0)|body_armor(63)|leg_armor(22)|difficulty(0) ,imodbits_cloth ],

["kuyak_a", "Nordic Brigandine", [("kuyak_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 5930 , weight(17)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["kuyak_b", "Nordic Heavy Brigandine", [("kuyak_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 6330 , weight(17)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],

## Narf boots ##

["steel_greaves",  "Steel Greaves", [("steel_greaves",0)], itp_type_foot_armor | itp_attach_armature,0, 1435 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(41)|difficulty(0) ,imodbits_cloth ],
["steel_greaves2",  "Steel Shinguards", [("steel_greaves2",0)], itp_type_foot_armor | itp_attach_armature,0, 1120 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(32)|difficulty(0) ,imodbits_cloth ],

## Narf items end ##

## 15th Century Weapons pack ##

["longsword_b", "Long Sword", [("longsword_b",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip,
 1000 , weight(1.8)|difficulty(0)|spd_rtng(97) | weapon_length(100)|swing_damage(34 , cut) | thrust_damage(28 ,  pierce),imodbits_sword ],
["english_longsword", "Long Sword", [("english_longsword",0),("english_longsword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 950 , weight(1.8)|difficulty(0)|spd_rtng(95) | weapon_length(102)|swing_damage(33 , cut) | thrust_damage(25 ,  pierce),imodbits_sword ],
["italian_sword", "Lengthened Arming Sword", [("italian_sword",0),("italian_sword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 900 , weight(1.8)|difficulty(0)|spd_rtng(98) | weapon_length(98)|swing_damage(31 , cut) | thrust_damage(26 ,  pierce),imodbits_sword ],
["irish_sword", "Long Sword", [("irish_sword",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip,
 1000 , weight(1.8)|difficulty(0)|spd_rtng(95) | weapon_length(107)|swing_damage(32 , cut) | thrust_damage(27 ,  pierce),imodbits_sword ],
["side_sword", "Side Sword", [("side_sword",0),("side_sword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 950 , weight(1.8)|difficulty(0)|spd_rtng(100) | weapon_length(95)|swing_damage(26 , cut) | thrust_damage(30 ,  pierce),imodbits_sword ],
["milanese_sword", "Short Broad Sword", [("milanese_sword",0),("milanese_sword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 825 , weight(1.5)|difficulty(0)|spd_rtng(105) | weapon_length(74)|swing_damage(32 , cut) | thrust_damage(22 ,  pierce),imodbits_sword ],
["scottish_sword", "Short Nordic Sword", [("scottish_sword",0),("scottish_sword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 850 , weight(1.5)|difficulty(0)|spd_rtng(103) | weapon_length(81)|swing_damage(34 , cut) | thrust_damage(26 ,  pierce),imodbits_sword ],
["grosse_messer", "Short Cleaver", [("grosse_messer",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,
 875 , weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(85)|swing_damage(36 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
["grosse_messer_b", "Butcher Sword", [("grosse_messer_b",0),("grosse_messer_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 925 , weight(1.5)|difficulty(0)|spd_rtng(94) | weapon_length(93)|swing_damage(37 , cut) | thrust_damage(26 ,  pierce),imodbits_sword ],

["german_bastard_sword", "Elegant Bastard Sword", [("german_bastard_sword",0),("german_bastard_sword_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1050 , weight(1.8)|difficulty(0)|spd_rtng(98) | weapon_length(106)|swing_damage(39 , cut) | thrust_damage(26 ,  pierce),imodbits_sword_high ],
["danish_greatsword", "Elegant Great Sword", [("danish_greatsword",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 1300 , weight(2.75)|difficulty(10)|spd_rtng(94) | weapon_length(115)|swing_damage(46 , cut) | thrust_damage(32 ,  pierce),imodbits_sword_high ],

["glaive1", "Small Glaive", [("glaive1",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 1100 , weight(4)|difficulty(0)|spd_rtng(92) | weapon_length(148)|swing_damage(36 , cut) | thrust_damage(25 ,  pierce),imodbits_polearm ],
["glaive2", "Glaive with Crossguard", [("glaive2",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 1175 , weight(4)|difficulty(0)|spd_rtng(92) | weapon_length(157)|swing_damage(33 , cut) | thrust_damage(31 ,  pierce),imodbits_polearm ],
["partisan", "Partisan", [("partisan",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 1150 , weight(3.5)|difficulty(0)|spd_rtng(94) | weapon_length(159)|swing_damage(31 , cut) | thrust_damage(29 ,  pierce),imodbits_polearm ],

["english_bill", "Bill Hook", [("english_bill",0)], itp_type_polearm|itp_offset_lance|itp_bonus_against_shield| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 2050 , weight(4.5)|difficulty(0)|spd_rtng(90) | weapon_length(186)|swing_damage(38 , pierce) | thrust_damage(32 ,  pierce),imodbits_polearm ],
["poleaxe_a", "Poleaxe", [("poleaxe_a",0)], itp_type_polearm|itp_offset_lance|itp_bonus_against_shield| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 1650 , weight(4.5)|difficulty(0)|spd_rtng(91) | weapon_length(156)|swing_damage(44 , cut) | thrust_damage(33 ,  pierce),imodbits_polearm ],
["swiss_halberd", "Halberd", [("swiss_halberd",0)], itp_type_polearm|itp_offset_lance|itp_bonus_against_shield| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 1775 , weight(5)|difficulty(0)|spd_rtng(88) | weapon_length(179)|swing_damage(46 , cut) | thrust_damage(26 ,  pierce),imodbits_polearm ],
["simple_poleaxe", "Simple Poleaxe", [("simple_poleaxe",0)], itp_type_polearm|itp_offset_lance|itp_bonus_against_shield| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 1600 , weight(4)|difficulty(0)|spd_rtng(92) | weapon_length(150)|swing_damage(42 , cut) | thrust_damage(35 ,  pierce),imodbits_polearm ],
 
 ## Lui swords ##
 
["lui_empirerapier", "Long Imperial Rapier", [("lui_empirerapier",0),("lui_empirerapier_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1525 , weight(2)|difficulty(0)|spd_rtng(92) | weapon_length(126)|swing_damage(35 , cut) | thrust_damage(39 ,  pierce),imodbits_sword_high ],
["lui_empirerapierb", "Long Imperial Rapier", [("lui_empirerapierb",0),("lui_empirerapierb_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1550 , weight(2)|difficulty(0)|spd_rtng(93) | weapon_length(126)|swing_damage(29 , cut) | thrust_damage(41 ,  pierce),imodbits_sword_high ],
["lui_rapier", "Long Rapier", [("lui_rapier",0),("lui_rapier_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1350 , weight(2)|difficulty(0)|spd_rtng(95) | weapon_length(117)|swing_damage(28 , cut) | thrust_damage(37 ,  pierce),imodbits_sword_high ],
["lui_rapierb", "Long Rapier", [("lui_rapierb",0),("lui_rapierb_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1350 , weight(2)|difficulty(0)|spd_rtng(93) | weapon_length(121)|swing_damage(28 , cut) | thrust_damage(37 ,  pierce),imodbits_sword_high ],
["lui_rapierc", "Rapier", [("lui_rapierc",0),("lui_rapierc_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1150 , weight(1.75)|difficulty(0)|spd_rtng(98) | weapon_length(98)|swing_damage(31 , cut) | thrust_damage(35 ,  pierce),imodbits_sword_high ],
["lui_rapierd", "Long Rapier", [("lui_rapierd",0),("lui_rapierd_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1375 , weight(2)|difficulty(0)|spd_rtng(94) | weapon_length(119)|swing_damage(27 , cut) | thrust_damage(38 ,  pierce),imodbits_sword_high ],
["lui_rapiere", "Long Rapier", [("lui_rapiere",0),("lui_rapiere_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1325 , weight(2)|difficulty(0)|spd_rtng(94) | weapon_length(120)|swing_damage(27 , cut) | thrust_damage(37 ,  pierce),imodbits_sword_high ],
["lui_rapierg", "Long Rapier", [("lui_rapierg",0),("lui_rapierg_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1375 , weight(2)|difficulty(0)|spd_rtng(93) | weapon_length(125)|swing_damage(28 , cut) | thrust_damage(36 ,  pierce),imodbits_sword_high ],

["lui_greatswordc", "Imperial Great Sword", [("lui_greatswordc",0),("lui_greatswordc_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1475 , weight(3)|difficulty(0)|spd_rtng(92) | weapon_length(135)|swing_damage(43 , cut) | thrust_damage(36 ,  pierce),imodbits_sword_high ],
["lui_greatswordd", "Imperial Great Sword", [("lui_greatswordd",0),("lui_greatswordd_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1575 , weight(3)|difficulty(0)|spd_rtng(91) | weapon_length(140)|swing_damage(48 , cut) | thrust_damage(32 ,  pierce),imodbits_sword_high ],
["lui_nordictwoh", "Nordic Great Sword", [("lui_nordictwoh",0),("lui_nordictwoh_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1400 , weight(3)|difficulty(0)|spd_rtng(98) | weapon_length(121)|swing_damage(44 , cut) | thrust_damage(33 ,  pierce),imodbits_sword_high ],

["lui_cimeterre", "Sarranid Scimitar", [("lui_cimeterre",0),("lui_cimeterre_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1125 , weight(1.5)|difficulty(0)|spd_rtng(103) | weapon_length(99)|swing_damage(37 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["lui_cimeterreb", "Sarranid Dark Scimitar", [("lui_cimeterreb",0),("lui_cimeterreb_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1375 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(121)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["lui_cimeterrec", "Sarranid Two Handed Scimitar", [("lui_cimeterrec",0),("lui_cimeterrec_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1525 , weight(2.5)|difficulty(0)|spd_rtng(100) | weapon_length(122)|swing_damage(46 , cut) | thrust_damage(0 ,  cut),imodbits_sword_high ],
 
["lui_perfectsword", "Serindiar", [("lui_perfectsword",0),("lui_perfectsword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1350 , weight(2)|difficulty(0)|spd_rtng(98) | weapon_length(123)|swing_damage(40 , cut) | thrust_damage(35 ,  pierce),imodbits_sword_high ],
["lui_swadianlordsword", "Veidar", [("lui_swadianlordsword",0),("lui_swadianlordsword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1375 , weight(2)|difficulty(0)|spd_rtng(101) | weapon_length(115)|swing_damage(39 , cut) | thrust_damage(33 ,  pierce),imodbits_sword_high ],
["lui_kingswordtwoh", "Tadsamesh", [("lui_kingswordtwoh",0),("lui_kingswordtwoh_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1500 , weight(3)|difficulty(0)|spd_rtng(97) | weapon_length(118)|swing_damage(50 , cut) | thrust_damage(35 ,  pierce),imodbits_sword_high ],
["lui_nordicbastard", "Bryntvari", [("lui_nordicbastard",0),("lui_nordicbastard_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1425 , weight(2)|difficulty(0)|spd_rtng(102) | weapon_length(111)|swing_damage(42 , cut) | thrust_damage(34 ,  pierce),imodbits_sword_high ],
["lui_sultancimitar", "Tazjunat", [("lui_sultancimitar",0),("lui_sultancimitar_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1400 , weight(1.5)|difficulty(0)|spd_rtng(105) | weapon_length(104)|swing_damage(43 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["lui_vaegirlordsword", "Yaragar", [("lui_vaegirlordsword",0),("lui_vaegirlordsword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1350 , weight(2)|difficulty(0)|spd_rtng(100) | weapon_length(114)|swing_damage(39 , cut) | thrust_damage(32 ,  pierce),imodbits_sword_high ],
["lui_khergitlordexecutioner", "Tismirr", [("lui_khergitlordexecutioner",0),("lui_khergitlordexecutioner_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1725 , weight(2.5)|difficulty(0)|spd_rtng(100) | weapon_length(122)|swing_damage(52 , cut) | thrust_damage(0 ,  cut),imodbits_sword_high ],

["lui_highelflongsword", "Tirion", [("lui_highelflongsword",0),("lui_highelflongsword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 1200, weight(1)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(39, cut) | thrust_damage(32, pierce),imodbits_sword_high ],
["lui_highelftwohanderb", "Orophin", [("lui_highelftwohanderb",0),("lui_highelftwohanderb_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 1500 , weight(2.25)|difficulty(0)|spd_rtng(98) | weapon_length(126)|swing_damage(45, cut) | thrust_damage(35, pierce),imodbits_sword_high ],

 

## Misc CWE items ##

["boot_light_crusader_c",  "Light Boots", [("boot_light_crusader_c",0)], itp_type_foot_armor | itp_attach_armature,0, 280 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["boot_light_crusader_d",  "Light Boots", [("boot_light_crusader_d",0)], itp_type_foot_armor | itp_attach_armature,0, 280 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["boot_average_crusader_c",  "Leather Boots", [("boot_average_crusader_c",0)], itp_type_foot_armor | itp_attach_armature,0, 420 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["boot_average_crusader_d",  "Leather Boots", [("boot_average_crusader_d",0)], itp_type_foot_armor | itp_attach_armature,0, 420 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],

["crusade_bolts","Sharp Bolts", [("crusade_bolt",0),("flying_missile",ixmesh_flying_ammo),("crusade_bolt_bag", ixmesh_carry),("crusade_bolt_bag", ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 
 200,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile],
["crusade_bolts_heavy","Heavy Bolts", [("crusade_bolt_heavy",0),("flying_missile",ixmesh_flying_ammo),("crusade_bolt_bag_heavy", ixmesh_carry),("crusade_bolt_bag_heavy", ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 
 300,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(3,pierce)|max_ammo(28),imodbits_missile],
["crusade_bolts_heavy_pierce","Piercing Bolts", [("crusade_bolt_heavy_pierce",0),("flying_missile",ixmesh_flying_ammo),("crusade_bolt_bag_heavy_pierce", ixmesh_carry),("crusade_bolt_bag_heavy_pierce", ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 
 400,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(4,pierce)|max_ammo(27),imodbits_missile],
 
## Readded Native Shields ## 

["norman_shield_1", "Painted Kite Shield", [("norman_shield_1",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  650 , weight(2)|hit_points(450)|body_armor(10)|spd_rtng(88)|shield_width(37)|shield_height(70),imodbits_shield ],
["norman_shield_2", "Metal Rimmed Kite Shield", [("norman_shield_2",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  1125 , weight(2.5)|hit_points(530)|body_armor(15)|spd_rtng(82)|shield_width(37)|shield_height(70),imodbits_shield ],
["norman_shield_3", "Painted Kite Shield", [("norman_shield_3",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  650 , weight(2)|hit_points(450)|body_armor(10)|spd_rtng(88)|shield_width(37)|shield_height(70),imodbits_shield ],
["norman_shield_4", "Metal Rimmed Kite Shield", [("norman_shield_4",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  1125 , weight(2.5)|hit_points(530)|body_armor(15)|spd_rtng(82)|shield_width(37)|shield_height(70),imodbits_shield ],
["norman_shield_5", "Painted Kite Shield", [("norman_shield_5",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  650 , weight(2)|hit_points(450)|body_armor(10)|spd_rtng(88)|shield_width(37)|shield_height(70),imodbits_shield ],
["norman_shield_6", "Metal Rimmed Kite Shield", [("norman_shield_6",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  1125 , weight(2.5)|hit_points(530)|body_armor(15)|spd_rtng(82)|shield_width(37)|shield_height(70),imodbits_shield ],
["norman_shield_7", "Metal Rimmed Kite Shield", [("norman_shield_7",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  1125 , weight(2.5)|hit_points(530)|body_armor(15)|spd_rtng(82)|shield_width(37)|shield_height(70),imodbits_shield ],
["norman_shield_8", "Metal Rimmed Kite Shield", [("norman_shield_8",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  1125 , weight(2.5)|hit_points(530)|body_armor(15)|spd_rtng(82)|shield_width(37)|shield_height(70),imodbits_shield ],

## Brytenwalda Valsgarde Helmets ##

["bl_01_valsgarde02", "Conical Nordic Helmet", [("BL_01_Valsgarde02",0)], itp_type_head_armor   ,0, 1350 , weight(1.5)|abundance(100)|head_armor(27)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde05", "Nordic Helmet with Neckguard", [("BL_01_Valsgarde05",0)], itp_type_head_armor   ,0, 1450 , weight(1.5)|abundance(100)|head_armor(29)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde06", "Nordic Helmet with Neckguard", [("BL_01_Valsgarde06",0)], itp_type_head_armor   ,0, 1650 , weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde04", "Nordic Helmet", [("BL_01_Valsgarde04",0)], itp_type_head_armor   ,0, 1800 , weight(2)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde03", "Nordic Helmet", [("BL_01_Valsgarde03",0)], itp_type_head_armor   ,0, 1850 , weight(2)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde09", "Nordic Helmet with Neckguard", [("BL_01_Valsgarde09",0)], itp_type_head_armor   ,0, 1950 , weight(2)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde10", "Plated Nordic Helmet with Neckguard", [("BL_01_Valsgarde10",0)], itp_type_head_armor   ,0, 2000 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde07", "Plated Nordic Helmet with Neckguard", [("BL_01_Valsgarde07",0)], itp_type_head_armor   ,0, 2000 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde08", "Plated Nordic Helmet with Neckguard", [("BL_01_Valsgarde08",0)], itp_type_head_armor   ,0, 2050 , weight(2)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_01_valsgarde01", "Valsgarde Helmet", [("BL_01_Valsgarde01",0)], itp_type_head_armor|itp_covers_head   ,0, 2400 , weight(2)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

["bl_02_valsgarde02", "Conical Nordic Helmet", [("BL_02_Valsgarde02",0)], itp_type_head_armor   ,0, 1350 , weight(1.5)|abundance(100)|head_armor(27)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde05", "Nordic Helmet with Neckguard", [("BL_02_Valsgarde05",0)], itp_type_head_armor   ,0, 1450 , weight(1.5)|abundance(100)|head_armor(29)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde06", "Nordic Helmet with Neckguard", [("BL_02_Valsgarde06",0)], itp_type_head_armor   ,0, 1650 , weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde04", "Nordic Helmet", [("BL_02_Valsgarde04",0)], itp_type_head_armor   ,0, 1800 , weight(2)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde03", "Nordic Helmet", [("BL_02_Valsgarde03",0)], itp_type_head_armor   ,0, 1850 , weight(2)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde09", "Nordic Helmet with Neckguard", [("BL_02_Valsgarde09",0)], itp_type_head_armor   ,0, 1950 , weight(2)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde10", "Plated Nordic Helmet with Neckguard", [("BL_02_Valsgarde10",0)], itp_type_head_armor   ,0, 2000 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde07", "Plated Nordic Helmet with Neckguard", [("BL_02_Valsgarde07",0)], itp_type_head_armor   ,0, 2000 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde08", "Plated Nordic Helmet with Neckguard", [("BL_02_Valsgarde08",0)], itp_type_head_armor   ,0, 2050 , weight(2)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bl_02_valsgarde01", "Valsgarde Helmet", [("BL_02_Valsgarde01",0)], itp_type_head_armor|itp_covers_head   ,0, 2400 , weight(2)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

## Plated Charger ##

["charger_plate_1","Plated Charger", [("charger_plate_1",0)], itp_merchandise|itp_type_horse, 0, 2700,abundance(40)|hit_points(180)|body_armor(65)|difficulty(4)|horse_speed(38)|horse_maneuver(40)|horse_charge(39)|horse_scale(115),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1, fac_kingdom_5]],

### TLD Items ###

## TLD Helmets ##

# Arnor #

["arnor_hood", "Arnorian Hood", [("arnor_hood",0)],itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["dunedain_helm_a", "Blue Arnorian Helmet", [("dunedain_helm_a",0)], itp_type_head_armor   ,0, 1550 , weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dunedain_helm_b", "Red Arnorian Helmet", [("dunedain_helm_b",0)], itp_type_head_armor   ,0, 1550 , weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dunedain_helm_c", "Arnorian Helmet with Mail", [("dunedain_helm_c",0)], itp_type_head_armor   ,0, 1700 , weight(1.75)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["arnor_helm_a", "Heavy Arnorian Helmet", [("arnor_helm_a",0)], itp_type_head_armor   ,0, 1900 , weight(2)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

["dunedain_ranger_hood", "Dunedain Ranger Hood", [("gondor_ranger_hood",0)],itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["dunedain_ranger_hood_mask", "Masked Dunedain Ranger Hood", [("gondor_ranger_hood_mask",0)],itp_type_head_armor|itp_civilian|itp_covers_beard,0,600, weight(1)|abundance(100)|head_armor(12)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],

# Dale #

["dwarven_inf_helmet_t1", "Dale Leather Helmet", [("dwarven_inf_helmet_t1",0)], itp_type_head_armor   ,0, 1500 , weight(1.5)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_cloth ],
["dale_inf_helm_b", "Dale Footman Helmet", [("dale_inf_helm_b",0)], itp_type_head_armor   ,0, 1800 , weight(1.75)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dale_inf_helm_a", "Dale Infantry Helmet", [("dale_inf_helm_a",0)], itp_type_head_armor   ,0, 1850 , weight(1.75)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dale_inf_helm_c", "Dale Heavy Helmet", [("dale_inf_helm_c",0)], itp_type_head_armor|itp_covers_beard  ,0, 1950 , weight(2)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dwarven_inf_helmet_t3", "Dwarfmade Rune Helmet", [("dwarven_inf_helmet_t3",0)], itp_type_head_armor|itp_covers_beard   ,0, 2200 , weight(2.25)|abundance(100)|head_armor(44)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

["dale_archer_helm_b", "Dale Bowman Helmet", [("dale_archer_helm_b",0)], itp_type_head_armor   ,0, 1600 , weight(1.75)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dale_archer_helm_a", "Dale Kettle Helmet", [("dale_archer_helm_a",0)], itp_type_head_armor   ,0, 1800 , weight(1.75)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dale_archer_helm_c", "Bardian Marksman Helmet", [("dale_archer_helm_c",0)], itp_type_head_armor   ,0, 1950 , weight(2)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

# Dunland #

["dunland_antlercap", "Dunnish Antlered Cap", [("dunland_antlercap",0)],itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["dunland_wolfcap", "Dunnish Wolf Guard Helm", [("dunland_wolfcap",0)],itp_type_head_armor|itp_civilian,0,600, weight(1)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["dunland_helm_a", "Dunnish Helmet", [("dunland_helm_a",0)], itp_type_head_armor   ,0, 1500 , weight(1.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dunland_helm_b", "Dunnish Heavy Helmet", [("dunland_helm_b",0)], itp_type_head_armor   ,0, 1650 , weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["dunland_helm_c", "Dunnish Antlered Heavy Helmet", [("dunland_helm_c",0)], itp_type_head_armor   ,0, 1650 , weight(2)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],

# Dwarves #

["dwarfhelmhood", "Dwarven Hood", [("DwarfHelmHood",0)],itp_type_head_armor|itp_civilian,0,300, weight(1)|abundance(100)|head_armor(6)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["dwarfhelmminercap", "Dwarven Miner Cap", [("DwarfHelmMinerCap",0)],itp_type_head_armor|itp_civilian,0,400, weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["dwarfhelmminer", "Dwarven Miner Hat", [("DwarfHelmMiner",0)],itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["dwarfhelmroundleather", "Dwarven Watchman Helmet", [("DwarfHelmRoundLeather",0)],itp_covers_beard|itp_type_head_armor,0,1000, weight(1.5)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmconical_bleather", "Dwarven Nasal Helmet", [("DwarfHelmConical_BLeather",0)],itp_type_head_armor,0,1050, weight(1.5)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmconicalleather", "Dwarven Militia Helmet", [("DwarfHelmConicalLeather",0)],itp_covers_beard|itp_type_head_armor,0,1050, weight(1.5)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmcoif", "Dwarven Mail Coif", [("DwarfHelmCoif",0)],itp_covers_beard|itp_type_head_armor,0,1250, weight(1.75)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmcoifmask_b", "Dwarven Masked Coif", [("DwarfHelmCoifMask_B",0)],itp_type_head_armor|itp_covers_beard,0,1400, weight(1.75)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmcoifmask", "Dwarven Dragon Coif", [("DwarfHelmCoifMask",0)],itp_type_head_armor|itp_covers_beard,0,1750, weight(1.75)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmroundchain", "Dwarven Watchman Coif", [("DwarfHelmRoundChain",0)],itp_type_head_armor,0,1500, weight(2)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmconical_bchain", "Dwarven Nasal Coif", [("DwarfHelmConical_BChain",0)],itp_covers_beard|itp_type_head_armor,0,1550, weight(2)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmconicalchain", "Dwarven Militia Coif", [("DwarfHelmConicalChain",0)],itp_type_head_armor,0,1550, weight(2)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmfrisianchain", "Dwarven Swordsman Helmet", [("DwarfHelmFrisianChain",0)],itp_covers_beard|itp_type_head_armor,0,1900, weight(2)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmsalletchain", "Dwarven Mine Guard Helmet", [("DwarfHelmSalletChain",0)],itp_covers_beard|itp_type_head_armor,0,1900, weight(2)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmconicalmask", "Dwarven Vault Warden Helmet", [("DwarfHelmConicalMask",0)],itp_covers_beard|itp_type_head_armor,0,2050, weight(2)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmfrisianmask_a", "Dwarven Veteran Swordsman Helmet", [("DwarfHelmFrisianMask_A",0)],itp_covers_beard|itp_type_head_armor,0,2250, weight(2.5)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmfrisianmask_b", "Dwarven Mountain Guard Helmet", [("DwarfHelmFrisianMask_B",0)],itp_covers_beard|itp_type_head_armor,0,2450, weight(2.5)|abundance(100)|head_armor(49)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmroundmask", "Dwarven Guardsman Helmet", [("DwarfHelmRoundMask",0)],itp_covers_beard|itp_type_head_armor,0,2150, weight(2)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmsalletsargeant", "Dwarven Iron Guard Helmet", [("DwarfHelmSalletSargeant",0)],itp_covers_beard|itp_type_head_armor,0,2700, weight(3.25)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmironheadface", "Dwarven Mithril Guard Helmet", [("DwarfHelmIronheadFace",0)],itp_covers_beard|itp_type_head_armor,0,2600, weight(3)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmironheadnasal", "Dwarven Dragon Guard Helmet", [("DwarfHelmIronheadNasal",0)],itp_covers_beard|itp_type_head_armor,0,2600, weight(3)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["dwarfhelmkingcrown", "Dwarven King's Helm", [("DwarfHelmKingCrown",0)],itp_covers_beard|itp_type_head_armor,0,3000, weight(3)|abundance(100)|head_armor(60)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Gondor #

["gondor_auxila_helm", "Gondorian Recruit Helmet", [("gondor_auxila_helm",0)],itp_type_head_armor,0,1300, weight(1.5)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_footman_helm", "Gondorian Footman Helmet", [("gondor_footman_helm",0)],itp_type_head_armor,0,1650, weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_squire_helm", "Gondorian Squire Helmet", [("gondor_squire_helm",0)],itp_type_head_armor,0,1850, weight(1.75)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_regular_helm", "Gondorian Sergeant Helmet", [("gondor_regular_helm",0)],itp_type_head_armor,0,2000, weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_knight_helm", "Gondorian Knight Helmet", [("gondor_knight_helm",0)],itp_type_head_armor,0,2100, weight(2)|abundance(100)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_bowman_helm", "Gondorian Bowman Helmet", [("gondor_bowman_helm",0)],itp_type_head_armor,0,1350, weight(1.5)|abundance(100)|head_armor(27)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_archer_helm", "Gondorian Archer Helmet", [("gondor_archer_helm",0)],itp_type_head_armor,0,1400, weight(1.5)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_tower_archer_helm", "Gondorian Tower Archer Helmet", [("gondor_tower_archer_helm",0)],itp_type_head_armor,0,1700, weight(1.5)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_tower_guard_helm", "Gondorian Steward Guard Helmet", [("gondor_tower_guard_helm",0)],itp_type_head_armor,0,2350, weight(2)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_citadel_knight_helm", "Gondorian Citadel Guard Helmet", [("gondor_citadel_knight_helm",0)],itp_type_head_armor,0,2400, weight(2)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_leader_helm", "Denethor's Helmet", [("gondor_leader_helm",0)],itp_type_head_armor,0,2700, weight(2)|abundance(100)|head_armor(54)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["gondor_dolamroth_helm", "Dol Amroth Helmet", [("gondor_dolamroth_helm",0)],itp_type_head_armor,0,2000, weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_dolamroth_knight_helm", "Swan Knight Helmet", [("gondor_dolamroth_knight_helm",0)],itp_type_head_armor,0,2450, weight(2.25)|abundance(100)|head_armor(49)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["imrahil_helmet", "Prince Imrahil's Helmet", [("imrahil_helmet",0)],itp_type_head_armor|itp_attach_armature,0,2750, weight(2.5)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["ithilien_ranger_hood", "Ithilien Ranger Hood", [("gondor_ranger_hood",0)],itp_type_head_armor|itp_civilian,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["ithilien_ranger_hood_mask", "Masked Ithilien Ranger Hood", [("gondor_ranger_hood_mask",0)],itp_type_head_armor|itp_civilian|itp_covers_beard,0,600, weight(1)|abundance(100)|head_armor(12)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],

["lamedon_hood", "Lamedon Hood", [("lamedon_hood",0)],itp_type_head_armor,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["gondor_lamedon_helm", "Lamedon Warrior Helmet", [("gondor_lamedon_helm",0)],itp_type_head_armor,0,1800, weight(1.75)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["gondor_lamedon_guard_helm", "Lamedon Guard Helmet", [("gondor_lamedon_guard_helm",0)],itp_type_head_armor,0,2050, weight(2)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["blackroot_hood", "Blackroot Hood", [("blackroot_hood",0)],itp_type_head_armor,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],

["lossarnach_cloth_cap", "Lossarnach Cap", [("lossarnach_cloth_cap",0)],itp_type_head_armor,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["lossarnach_leather_cap", "Lossarnach Leather Cap", [("lossarnach_leather_cap",0)],itp_type_head_armor,0,1100, weight(1)|abundance(100)|head_armor(22)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["lossarnach_scale_cap", "Lossarnach Scaled Cap", [("lossarnach_scale_cap",0)],itp_type_head_armor,0,1600, weight(1)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["pelargir_hood", "Pelargir Hood", [("pelargir_hood",0)],itp_type_head_armor,0,500, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["pelargir_helmet_light", "Pelargir Helmet", [("pelargir_helmet_light",0)],itp_type_head_armor,0,1500, weight(1)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["pelargir_helmet_heavy", "Pelargir Marine Helmet", [("pelargir_helmet_heavy",0)],itp_type_head_armor,0,1950, weight(1)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Rohan #

["rohan_light_helmet_a", "Rohirric Recruit Helmet", [("rohan_light_helmet_a",0)],itp_type_head_armor,0,1100, weight(1.25)|abundance(100)|head_armor(22)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rohan_light_helmet_b", "Rohirric Light Helmet", [("rohan_light_helmet_b",0)],itp_type_head_armor,0,1300, weight(1.5)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rohan_med_helmet_a", "Rohirric Engraved Helmet", [("rohan_med_helmet_a",0)],itp_type_head_armor,0,1700, weight(1.75)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rohan_med_helmet_b", "Rohirric Engraved Helmet", [("rohan_med_helmet_b",0)],itp_type_head_armor,0,1700, weight(1.75)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["rohan_archer_helmet_a", "Rohirric Archer Helmet", [("rohan_archer_helmet_a",0)],itp_type_head_armor,0,1400, weight(1.5)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rohan_archer_helmet_b", "Rohirric Archer Helmet", [("rohan_archer_helmet_b",0)],itp_type_head_armor,0,1400, weight(1.5)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rohan_archer_helmet_c", "Rohirric Veteran Archer Helmet", [("rohan_archer_helmet_c",0)],itp_type_head_armor,0,1500, weight(1.5)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["rohan_cav_helmet_a", "Rohirrim Rider Helmet", [("rohan_cav_helmet_a",0)],itp_type_head_armor,0,1850, weight(2)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rohan_cav_helmet_b", "Rohirrim Rider Helmet", [("rohan_cav_helmet_b",0)],itp_type_head_armor,0,1850, weight(2)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rohan_cav_helmet_c", "Rohirrim Elite Rider Helmet", [("rohan_cav_helmet_c",0)],itp_type_head_armor,0,1950, weight(2)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["rohan_captain_helmet", "Eomer's Helmet", [("rohan_captain_helmet",0)],itp_type_head_armor,0,2100, weight(2)|abundance(100)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Mirkwood #

["mirkwood_helm", "Mirkwood Helmet", [("mirkwood_helm",0)],itp_type_head_armor,0,1400, weight(1)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["mirkwood_archer", "Mirkwood Archer Helmet", [("mirkwood_archer",0)],itp_type_head_armor,0,1600, weight(1.25)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["mirkwood_royal_archer", "Mirkwood Royal Archer Helmet", [("mirkwood_royal_archer",0)],itp_type_head_armor,0,1650, weight(1.25)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["mirkwood_spearman", "Mirkwood Spearman Helmet", [("mirkwood_spearman",0)],itp_type_head_armor,0,1750, weight(1.5)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["mirkwood_royal_spearman", "Mirkwood Royal Spearman Helmet", [("mirkwood_royal_spearman",0)],itp_type_head_armor,0,1950, weight(1.5)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Lorien #

["lorienhelmetarcherlow", "Lorien Helmet", [("lorienhelmetarcherlow",0)],itp_type_head_armor,0,1700, weight(1.5)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["lorienhelmetarcherhigh", "Lorien Mail Helmet", [("lorienhelmetarcherhigh",0)],itp_type_head_armor,0,1900, weight(1.5)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["lorienhelmetinf", "Lorien Plated Helmet", [("lorienhelmetinf",0)],itp_type_head_armor,0,2000, weight(1.5)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Rivendell #

["rivendell_helmet_coif", "Rivendell Coif", [("rivendell_helmet_coif",0)],itp_type_head_armor,0,1200, weight(1)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rivendell_helmet_archer", "Rivendell Archer Helmet", [("rivendell_helmet_archer",0)],itp_type_head_armor,0,1650, weight(1.5)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rivendell_helmet", "Rivendell Helmet", [("mirkwood_spearman",0)],itp_type_head_armor,0,1750, weight(1.5)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rivendell_helmet_swordfighter", "Rivendell Swordsman Helmet", [("rivendell_helmet_swordfighter",0)],itp_type_head_armor,0,1850, weight(1.75)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Khand #

["khand_helmet_mask1", "Variag Gladiator Mask", [("Khand_Helmet_Mask1",0)],itp_type_head_armor|itp_covers_beard|itp_doesnt_cover_hair,0,500, weight(0.5)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["khand_helmet_mask2", "Variag Gladiator Mask", [("Khand_Helmet_Mask2",0)],itp_type_head_armor|itp_covers_beard|itp_doesnt_cover_hair,0,500, weight(0.5)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],

["khand_helmet_f1", "Variag Keffiyeh", [("Khand_Helmet_F1",0)],itp_type_head_armor|itp_covers_beard,0,500, weight(0.5)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["khand_helmet_b3", "Variag Keffiyeh Helmet", [("Khand_Helmet_B3",0)],itp_type_head_armor|itp_covers_beard,0,1050, weight(1)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_e4", "Variag Keffiyeh Helmet", [("Khand_Helmet_E4",0)],itp_type_head_armor|itp_covers_beard,0,1050, weight(1)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_e1", "Variag Keffiyeh Helmet", [("Khand_Helmet_E1",0)],itp_type_head_armor|itp_covers_beard,0,1050, weight(1)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_a3", "Variag Keffiyeh Helmet", [("Khand_Helmet_A3",0)],itp_type_head_armor|itp_covers_beard,0,1050, weight(1)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_a2", "Variag Mail Helmet", [("Khand_Helmet_A2",0)],itp_type_head_armor,0,1300, weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_e3", "Variag Mail Helmet", [("Khand_Helmet_E3",0)],itp_type_head_armor,0,1300, weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_e2", "Variag Helmet with Coif", [("Khand_Helmet_E2",0)],itp_type_head_armor|itp_covers_beard,0,1400, weight(1.5)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_c3", "Variag Mail Keffiyeh Helmet", [("Khand_Helmet_C3",0)],itp_type_head_armor|itp_covers_beard,0,1550, weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_c4", "Variag Mail Keffiyeh Helmet", [("Khand_Helmet_C4",0)],itp_type_head_armor|itp_covers_beard,0,1550, weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_b4", "Variag Helmet with Coif", [("Khand_Helmet_B4",0)],itp_type_head_armor|itp_covers_beard,0,1550, weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_a1", "Variag Helmet with Coif", [("Khand_Helmet_A1",0)],itp_type_head_armor|itp_covers_beard,0,1550, weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_d2", "Variag Helmet with Coif", [("Khand_Helmet_D2",0)],itp_type_head_armor|itp_covers_beard,0,1550, weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_d1", "Variag Faceplate Helmet", [("Khand_Helmet_D1",0)],itp_type_head_armor|itp_covers_beard,0,1700, weight(1.75)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_d3", "Variag Faceplate Helmet", [("Khand_Helmet_D3",0)],itp_type_head_armor|itp_covers_beard,0,1850, weight(1.75)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_b2", "Variag Faceplate Helmet", [("Khand_Helmet_B2",0)],itp_type_head_armor|itp_covers_beard,0,1850, weight(1.75)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["khand_helmet_b1", "Variag Decorated Helmet", [("Khand_Helmet_B1",0)],itp_type_head_armor|itp_covers_beard,0,2000, weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Harad #

["harad_pantherhelm", "Haradrim Panther Hood", [("harad_pantherhelm",0)],itp_type_head_armor,0, 400 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["harad_heavy_inf_helm", "Haradrim Helmet", [("harad_heavy_inf_helm",0)],itp_type_head_armor,0,1400, weight(1.25)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["harad_wavy_helm", "Haradrim Tall Helmet", [("harad_wavy_helm",0)],itp_type_head_armor,0,1900, weight(1.5)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["lion_helm", "Haradrim Lion Helmet", [("lion_helm",0)],itp_type_head_armor,0,2000, weight(1.5)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["harad_finhelm", "Haradrim Veiled Helmet", [("harad_finhelm",0)],itp_type_head_armor|itp_covers_beard,0,2150, weight(1.75)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["harad_dragon_helm", "Haradrim Heavy Tall Helmet", [("harad_dragon_helm",0)],itp_type_head_armor,0,2250, weight(1.75)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["black_snake_helm", "Serpent Guard Helmet", [("black_snake_helm",0)],itp_type_head_armor|itp_covers_beard,0,2250, weight(1.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["harad_cav_helm_a", "Haradrim Cavalry Helmet", [("harad_cav_helm_a",0)],itp_type_head_armor,0,1250, weight(1)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["harad_cav_helm_b", "Haradrim Mail Cavalry Helmet", [("harad_cav_helm_b",0)],itp_type_head_armor,0,1750, weight(1.5)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

["eagle_guard_helmet", "Eagle Guard Helmet", [("eagle_guard_helmet",0)],itp_type_head_armor,0,2250, weight(2)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Umbar #

["umbar_militia_helmet", "Umbar Militia Helmet", [("umbar_militia_helmet",0)],itp_type_head_armor,0,1200, weight(1.25)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["umbar_militia_helmet_b", "Umbar Militia Helmet", [("umbar_militia_helmet_b",0)],itp_type_head_armor,0,1200, weight(1.25)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["raider_helmet", "Umbar Raider Helmet", [("raider_helmet",0)],itp_type_head_armor,0,1450, weight(1.25)|abundance(100)|head_armor(29)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["raider_helmet_b", "Umbar Raider Helmet", [("raider_helmet_b",0)],itp_type_head_armor,0,1450, weight(1.25)|abundance(100)|head_armor(29)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["umbar_tall_helmet", "Umbar Tall Shell Helmet", [("umbar_tall_helmet",0)],itp_type_head_armor,0,1550, weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["umbar_tall_helmet_b", "Umbar Tall Shell Helmet", [("umbar_tall_helmet_b",0)],itp_type_head_armor,0,1550, weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["shell_helmet", "Umbar Shell Helmet", [("shell_helmet",0)],itp_type_head_armor,0,1550, weight(1.5)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["shell_helmet_blue", "Umbar Shell Helmet", [("shell_helmet_blue",0)],itp_type_head_armor,0,1550, weight(1.5)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],


# Rhun #
 
["rhunhelmleather1", "Rhun Leather Halfmask", [("RhunHelmLeather1",0)],itp_type_head_armor,0,1000, weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmleather2", "Rhun Plumed Leather Halfmask", [("RhunHelmLeather2",0)],itp_type_head_armor,0,1050, weight(1.25)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmleather3", "Rhun Topped Leather Halfmask", [("RhunHelmLeather3",0)],itp_type_head_armor,0,1050, weight(1.25)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmround1", "Rhun Spiked Pot Helmet", [("RhunHelmRound1",0)],itp_type_head_armor,0,1250, weight(1.25)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmhorde1", "Rhun Pot Helmet", [("RhunHelmHorde1",0)],itp_type_head_armor,0,1300, weight(1.5)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmhorde3", "Rhun Horned Helmet", [("RhunHelmHorde3",0)],itp_type_head_armor,0,1400, weight(1.5)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmround2", "Rhun Spiked Pot Helmet", [("RhunHelmRound2",0)],itp_type_head_armor|itp_covers_beard,0,1750, weight(1.5)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmconical1", "Rhun Conical Helmet", [("RhunHelmConical1",0)],itp_type_head_armor,0,1550, weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmpot3", "Rhun Horned Helmet", [("RhunHelmPot3",0)],itp_type_head_armor|itp_covers_beard,0,2000, weight(1.5)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmdeathdealer1", "Rhun Berserker Helmet", [("RhunHelmDeathDealer1",0)],itp_type_head_armor,0,1850, weight(1.5)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmdeathdealer2", "Rhun Berserker Helmet", [("RhunHelmDeathDealer2",0)],itp_type_head_armor,0,1900, weight(1.5)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmconical2", "Rhun Mailed Conical Helmet", [("RhunHelmConical2",0)],itp_type_head_armor|itp_covers_beard,0,2050, weight(1.75)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhunhelmpot2", "Rhun Horned Helmet", [("RhunHelmPot2",0)],itp_type_head_armor|itp_covers_beard,0,2150, weight(1.75)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
 
["rhun_helm_light1", "Rhun Pot Helmet", [("rhun_helm_light1",0)],itp_type_head_armor,0,1300, weight(1.5)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_light2", "Rhun Pot Helmet", [("rhun_helm_light2",0)],itp_type_head_armor,0,1300, weight(1.5)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_light3", "Rhun Pot Helmet", [("rhun_helm_light3",0)],itp_type_head_armor,0,1300, weight(1.5)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_horde1", "Rhun Decorated Helmet", [("rhun_helm_horde1",0)],itp_type_head_armor,0,1750, weight(2)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_horde2", "Rhun Decorated Helmet", [("rhun_helm_horde2",0)],itp_type_head_armor,0,1750, weight(2)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_pot1", "Rhun Masked Helmet", [("rhun_helm_pot1",0)],itp_type_head_armor|itp_covers_beard,0,2050, weight(2)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_pot2", "Rhun Open Mask Helmet", [("rhun_helm_pot2",0)],itp_type_head_armor|itp_covers_beard,0,2050, weight(2)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_conical1", "Rhun Conical Helmet", [("rhun_helm_conical1",0)],itp_type_head_armor|itp_covers_beard,0,2200, weight(2.25)|abundance(100)|head_armor(44)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_conical2", "Rhun Conical Helmet", [("rhun_helm_conical2",0)],itp_type_head_armor|itp_covers_beard,0,2200, weight(2.25)|abundance(100)|head_armor(44)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_plumed1", "Rhun Open Cataphract Helmet", [("rhun_helm_plumed1",0)],itp_type_head_armor,0,2000, weight(2.25)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_plumed2", "Rhun Masked Cataphract Helmet", [("rhun_helm_plumed2",0)],itp_type_head_armor|itp_covers_beard,0,2150, weight(2.25)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_plumed3", "Rhun Silver Cataphract Helmet", [("rhun_helm_plumed3",0)],itp_type_head_armor,0,2000, weight(2.25)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["rhun_helm_plumed4", "Rhun Silver Cataphract Helmet", [("rhun_helm_plumed4",0)],itp_type_head_armor|itp_covers_beard,0,2150, weight(2.25)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Mordor #
 
["mordor_helmet_a", "Black Numenorean Helmet", [("mordor_helmet_a",0)],itp_type_head_armor,0,1800,weight(2)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["old_mordor_helmet_a", "Small Black Numenorean Helmet", [("old_mordor_helmet_a",0)],itp_type_head_armor,0,1700,weight(2)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["black_numenor_helmet", "Black Numenorean Helmet", [("black_numenor_helmet",0)],itp_type_head_armor,0,1900,weight(2)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["old_black_numenor_helmet", "Small Black Numenorean Helmet", [("old_black_numenor_helmet",0)],itp_type_head_armor,0,1800,weight(2)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["mordor_captain_helmet", "Morgul Knight's Helmet", [("mordor_captain_helmet",0)],itp_type_head_armor|itp_covers_beard,0,2100,weight(2.5)|abundance(100)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["old_mordor_captain_helmet", "Morgul Knight's Helmet", [("old_mordor_captain_helmet",0)],itp_type_head_armor|itp_covers_beard,0,2100,weight(2.5)|abundance(100)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["mouth_sauron_helm", "Mouth of Sauron's Helmet", [("mouth_sauron_helm",0)],itp_type_head_armor|itp_covers_beard,0,2400,weight(3)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["helmet_khamul_small", "Khamul's Helmet", [("helmet_khamul_small",0)],itp_type_head_armor|itp_covers_head,0,2500,weight(3.5)|abundance(100)|head_armor(50)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["witchking_helmet", "Witch-king's Helmet", [("witchking_helmet",0)],itp_type_head_armor|itp_covers_head,0,2750,weight(4)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Mordor Orcs #

["orc_gunda_cap", "Gundabad Cap", [("orc_gunda_cap",0)],itp_type_head_armor,0,700,weight(0.5)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_gunda_helm_a", "Gundabad Helm", [("orc_gunda_helm_a",0)],itp_type_head_armor,0,800,weight(0.6)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_gunda_helm_b", "Gundabad Helm", [("orc_gunda_helm_b",0)],itp_type_head_armor,0,850,weight(0.7)|abundance(100)|head_armor(17)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_gunda_helm_c", "Gundabad Helm", [("orc_gunda_helm_c",0)],itp_type_head_armor,0,900,weight(0.8)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_scavenged_leather_cap", "Scavenged Leather Cap", [("orc_scavenged_leather_cap",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,900,weight(1)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth ],
["orc_coif", "Orc Coif", [("orc_coif",0)],itp_type_head_armor,0,1000,weight(1)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_wargrider_helm", "Warg Rider Helm", [("orc_wargrider_helm",0)],itp_type_head_armor,0,1050,weight(0.75)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_c", "Orc Helm", [("orc_helm_c",0)],itp_type_head_armor,0,1050,weight(1)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_shaman_helm", "Orc Shaman Helm", [("orc_shaman_helm",0)],itp_type_head_armor,0,1050,weight(1)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_a", "Orc Helm", [("orc_helm_a",0)],itp_type_head_armor,0,1150,weight(1.5)|abundance(100)|head_armor(23)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_d", "Orc Helm", [("orc_helm_d",0)],itp_type_head_armor,0,1150,weight(1.5)|abundance(100)|head_armor(23)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_e", "Orc Helm", [("orc_helm_e",0)],itp_type_head_armor,0,1150,weight(1.25)|abundance(100)|head_armor(23)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_b", "Orc Helm", [("orc_helm_b",0)],itp_type_head_armor,0,1250,weight(1.75)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_f", "Orc Helm", [("orc_helm_f",0)],itp_type_head_armor,0,1300,weight(1.8)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["old_orc_helm_e", "Orc Helm", [("old_orc_helm_e",0)],itp_type_head_armor,0,1400,weight(1.9)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["old_orc_helm_f", "Orc Helm", [("old_orc_helm_f",0)],itp_type_head_armor,0,1400,weight(1.9)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["old_orc_helm_i", "Orc Helm", [("old_orc_helm_i",0)],itp_type_head_armor,0,1500,weight(2)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_g", "Orc Helm", [("orc_helm_g",0)],itp_type_head_armor,0,1550,weight(2)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_h", "Orc Helm", [("orc_helm_h",0)],itp_type_head_armor,0,1650,weight(2.1)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_i", "Orc Helm", [("orc_helm_i",0)],itp_type_head_armor,0,1650,weight(2.1)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_scavenged_spiked_helmet", "Scavenged Spiked Helmet", [("orc_scavenged_spiked_helmet",0)],itp_merchandise|itp_type_head_armor,0,1700,weight(2.15)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(7),imodbits_plate ],
["orc_helm_j", "Orc Helm", [("orc_helm_j",0)],itp_type_head_armor,0,1750,weight(2.25)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_helm_k", "Orc Helm", [("orc_helm_k",0)],itp_type_head_armor,0,1850,weight(2.25)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Mordor Uruks #

["uruk_head", "Uruk Head", [("uruk_head",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["uruk_head_b", "Uruk Head", [("uruk_head_b",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["uruk_head_c", "Uruk Head", [("uruk_head_c",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["uruk_head_d", "Uruk Head", [("uruk_head_d",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["uruk_head_e", "Uruk Head", [("uruk_head_e",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["uruk_head_f", "Uruk Head", [("uruk_head_f",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],

["uruk_helm_a", "Uruk Helm", [("uruk_helm_a",0)],itp_type_head_armor|itp_covers_head,0,1200,weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["uruk_helm_b", "Uruk Helm", [("uruk_helm_b",0)],itp_type_head_armor|itp_covers_head,0,1200,weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["uruk_helm_c", "Uruk Helm", [("uruk_helm_c",0)],itp_type_head_armor|itp_covers_head,0,1500,weight(1.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["uruk_helm_d", "Uruk Helm", [("uruk_helm_d",0)],itp_type_head_armor|itp_covers_head,0,1500,weight(1.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["uruk_helm_e", "Uruk Helm", [("uruk_helm_e",0)],itp_type_head_armor|itp_covers_head,0,1900,weight(2.25)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["uruk_helm_f", "Uruk Helm", [("uruk_helm_f",0)],itp_type_head_armor|itp_covers_head,0,1900,weight(2.25)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Oathbreakers #

["aod_helm_a1","Spectral Helmet", [("aod_helm_a1", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_a2","Spectral Helmet", [("aod_helm_a2", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_b1","Spectral Helmet", [("aod_helm_b1", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_b2","Spectral Helmet", [("aod_helm_b2", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_c1","Spectral Helmet", [("aod_helm_c1", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_c2","Spectral Helmet", [("aod_helm_c2", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_d1","Spectral Helmet", [("aod_helm_d1", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_d2","Spectral Helmet", [("aod_helm_d2", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_e1","Spectral Helmet", [("aod_helm_e1", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_e2","Spectral Helmet", [("aod_helm_e2", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_f1","Spectral Helmet", [("aod_helm_f1", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_f2","Spectral Helmet", [("aod_helm_f2", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_g1","Spectral Helmet", [("aod_helm_g1", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_g2","Spectral Helmet", [("aod_helm_g2", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_h1","Spectral Helmet", [("aod_helm_h1", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],															
["aod_helm_h2","Spectral Helmet", [("aod_helm_h2", 0)], itp_type_head_armor|itp_covers_head, 0,1500, weight(2)|abundance(100)|head_armor(30), imodbits_armor, []],		
["king_of_the_dead_helmet","King of the Dead's Helmet", [("king_of_the_dead_helmet", 0)], itp_type_head_armor|itp_covers_head, 0,0, weight(2.5)|abundance(100)|head_armor(45), imodbits_armor, []],

# Sauron #
["sauron_helmet","Sauron's Helmet", [("sauron_helmet", 0)], itp_type_head_armor|itp_covers_head, 0,0, weight(4)|abundance(100)|head_armor(60), imodbits_armor, []],

# Isengard Orcs #
["orc_isen_helm_a", "Isengard Orc Helm", [("orc_isen_helm_a",0)],itp_type_head_armor,0,1000,weight(1.5)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_isen_helm_b", "Isengard Orc Helm", [("orc_isen_helm_b",0)],itp_type_head_armor,0,1050,weight(1.5)|abundance(100)|head_armor(23)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orc_isen_helm_c", "Isengard Orc Helm", [("orc_isen_helm_c",0)],itp_type_head_armor,0,1100,weight(1.5)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],

# Isengard Uruk-hai #
["urukhai_head_a", "Uruk-hai Head", [("urukhai_head_a",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["urukhai_head_b", "Uruk-hai Head", [("urukhai_head_b",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["urukhai_head_c", "Uruk-hai Head", [("urukhai_head_c",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],

["urukhai_trackerhelm_a", "Uruk-hai Tracker Helm", [("urukhai_trackerhelm_a",0)],itp_type_head_armor|itp_covers_head,0,1100,weight(1)|abundance(100)|head_armor(22)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["urukhai_trackerhelm_b", "Uruk-hai Tracker Helm", [("urukhai_trackerhelm_b",0)],itp_type_head_armor|itp_covers_head,0,1250,weight(1)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["urukhai_helm_a", "Uruk-hai Helm", [("urukhai_helm_a",0)],itp_type_head_armor|itp_covers_head,0,1750,weight(2)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["urukhai_helm_b", "Uruk-hai Helm", [("urukhai_helm_b",0)],itp_type_head_armor|itp_covers_head,0,1900,weight(2)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["urukhai_helm_c", "Uruk-hai Helm", [("urukhai_helm_c",0)],itp_type_head_armor|itp_covers_head,0,2000,weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["urukhai_guardhelm", "Uruk-hai Guard Helm", [("urukhai_guardhelm",0)],itp_type_head_armor|itp_covers_head,0,2000,weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orthanc_guard_helm1", "Orthanc Guard Helm", [("orthanc_guard_helm1",0)],itp_type_head_armor|itp_covers_head,0,2150,weight(2)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orthanc_guard_helm2", "Orthanc Guard Helm", [("orthanc_guard_helm2",0)],itp_type_head_armor|itp_covers_head,0,2150,weight(2)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orthanc_guard_helm3", "Orthanc Guard Helm", [("orthanc_guard_helm3",0)],itp_type_head_armor|itp_covers_head,0,2150,weight(2)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["orthanc_guard_helm4", "Orthanc Guard Helm", [("orthanc_guard_helm4",0)],itp_type_head_armor|itp_covers_head,0,2150,weight(2)|abundance(100)|head_armor(43)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["urukhai_captainhelm", "Uruk-hai Captain Helm", [("urukhai_captainhelm",0)],itp_type_head_armor|itp_covers_head,0,2250,weight(2)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
											
## TLD Helmets end ##

## TLD Boots ##

["arnor_splinted", "Arnorian Leather Greaves", [("arnor_splinted",0)], itp_type_foot_armor|itp_attach_armature,0,735,weight(1.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(0) ,imodbits_armor ],
["arnor_greaves", "Arnorian Decorated Greaves", [("arnor_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,980,weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(0) ,imodbits_armor ],

["black_leather_boots", "Black Leather Boots", [("black_leather_boots",0)], itp_type_foot_armor|itp_attach_armature,0,560,weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(0) ,imodbits_cloth ],

["dunland_wolfboots", "Dunnish Fur Boots", [("dunland_wolfboots",0)], itp_type_foot_armor|itp_attach_armature,0,630,weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(18)|difficulty(0) ,imodbits_armor ],

["dwarf_chain_boots", "Dwarven Chain Boots", [("dwarf_chain_boots",0)], itp_type_foot_armor|itp_attach_armature,0,910,weight(1.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(26)|difficulty(0) ,imodbits_armor ],
["dwarf_scale_boots", "Dwarven Scale Boots", [("dwarf_scale_boots",0)], itp_type_foot_armor|itp_attach_armature,0,1225,weight(2.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(35)|difficulty(0) ,imodbits_armor ],

["gondor_light_greaves", "Gondorian Boots", [("gondor_light_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,630,weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(18)|difficulty(0) ,imodbits_armor ],
["gondor_medium_greaves", "Gondorian Plated Boots", [("gondor_medium_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,840,weight(1.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(24)|difficulty(0) ,imodbits_armor ],
["gondor_boots", "Gondorian Greaves", [("gondor_boots",0)],itp_type_foot_armor|itp_attach_armature,0,1050,weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(30)|difficulty(0) ,imodbits_armor ],
["gondor_heavy_greaves", "Gondorian Plated Greaves", [("gondor_heavy_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,1155,weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_armor ],
["dol_shoes", "Dol Amroth Shoes", [("dol_shoes",0)], itp_type_foot_armor|itp_attach_armature,0,350,weight(0.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10)|difficulty(0) ,imodbits_armor ],
["dol_greaves", "Dol Amroth Greaves", [("dol_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,1225,weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(35)|difficulty(0) ,imodbits_armor ],
["lossarnach_greaves", "Lossarnach Boots", [("lossarnach_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,945,weight(1.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(27)|difficulty(0) ,imodbits_armor ],
["pelargir_greaves", "Pelargir Boots", [("pelargir_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,945,weight(1.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(27)|difficulty(0) ,imodbits_armor ],

["rohan_shoes", "Rohirric Shoes", [("rohan_shoes",0)], itp_type_foot_armor|itp_attach_armature,0,350,weight(0.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10)|difficulty(0) ,imodbits_armor ],
["rohan_light_greaves", "Rohirric Leather Greaves", [("rohan_light_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,770,weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(22)|difficulty(0) ,imodbits_armor ],
["rohan_scale_greaves", "Rohirric Scale Greaves", [("rohan_scale_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,1085,weight(1.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(31)|difficulty(0) ,imodbits_armor ],
["rohan_steel_scale_greaves", "Rohirric Reinforced Scale Greaves", [("rohan_steel_scale_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,1190,weight(1.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(34)|difficulty(0) ,imodbits_armor ],

["mirkwood_boots", "Mirkwood Shoes", [("mirkwood_boots",0)], itp_type_foot_armor|itp_attach_armature,0,525,weight(0.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(15)|difficulty(0) ,imodbits_armor ],
["lorien_boots", "Lorien Boots", [("lorien_boots",0)], itp_type_foot_armor|itp_attach_armature,0,805,weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(23)|difficulty(0) ,imodbits_armor ],
["rivendell_boots", "Rivendell Boots", [("rivendell_boots",0)], itp_type_foot_armor|itp_attach_armature,0,630,weight(0.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(18)|difficulty(0) ,imodbits_armor ],

["variag_boots", "Variag Greaves", [("variag_boots",0)], itp_type_foot_armor|itp_attach_armature,0,1120,weight(1.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(32)|difficulty(0) ,imodbits_armor ],

["rhun_boots", "Rhunic Greaves", [("rhun_boots",0)], itp_type_foot_armor|itp_attach_armature,0,980,weight(1.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(0) ,imodbits_armor ],
["rhun_boots2", "Rhunic Greaves", [("rhun_boots2",0)], itp_type_foot_armor|itp_attach_armature,0,980,weight(1.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(0) ,imodbits_armor ],

["desert_boots", "Desert Shoes", [("desert_boots",0)], itp_type_foot_armor|itp_attach_armature,0,280,weight(0.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(8)|difficulty(0) ,imodbits_armor ],
["harad_leather_greaves", "Harad Leather Boots", [("harad_leather_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,700,weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(20)|difficulty(0) ,imodbits_armor ],
["harad_lamellar_greaves", "Harad Lamellar Greaves", [("harad_lamellar_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,875,weight(1.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(25)|difficulty(0) ,imodbits_armor ],
["harad_scale_greaves", "Harad Scale Greaves", [("harad_scale_greaves",0)], itp_type_foot_armor|itp_attach_armature,0,980,weight(1.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(0) ,imodbits_armor ],

["corsair_boots", "Umbar Shoes", [("corsair_boots",0)], itp_type_foot_armor|itp_attach_armature,0,350,weight(0.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10)|difficulty(0) ,imodbits_armor ],

["orc_ragwrap_lr", "Orc Boots Tier 1", [("orc_ragwrap_lr",0)], itp_type_foot_armor|itp_attach_armature,0,105,weight(0.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0) ,imodbits_armor ],
["orc_furboot_tall", "Orc Boots Tier 2", [("orc_furboot_tall",0)], itp_type_foot_armor|itp_attach_armature,0,315,weight(0.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(9)|difficulty(0) ,imodbits_armor ],
["orc_chain_greaves_lr", "Orc Boots Tier 3", [("orc_chain_greaves_lr",0)], itp_type_foot_armor|itp_attach_armature,0,735,weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(0) ,imodbits_armor ],

["uruk_calf_lr", "Uruk Feet", [("uruk_calf_lr",0)], itp_type_foot_armor|itp_attach_armature,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_none ],
["urukhai_calf_lr", "Uruk-hai Feet", [("urukhai_calf_lr",0)], itp_type_foot_armor|itp_attach_armature,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_none ],
["uruk_ragwrap_lr", "Uruk Boots Tier 1", [("uruk_ragwrap_lr",0)], itp_type_foot_armor|itp_attach_armature,0,140,weight(0.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(4)|difficulty(0) ,imodbits_armor ],
["uruk_furboot_lr", "Uruk Boots Tier 2", [("uruk_furboot_lr",0)], itp_type_foot_armor|itp_attach_armature,0,350,weight(0.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10)|difficulty(0) ,imodbits_armor ],
["uruk_chain_greave_lr", "Uruk Boots Tier 3", [("uruk_chain_greave_lr",0)], itp_type_foot_armor|itp_attach_armature,0,770,weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(22)|difficulty(0) ,imodbits_armor ],
["uruk_greave_lr", "Uruk Boots Tier 4", [("uruk_greave_lr",0)], itp_type_foot_armor|itp_attach_armature,0,1270,weight(1.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(27)|difficulty(0) ,imodbits_armor ],

["sauron_boots", "Sauron's Boots", [("sauron_boots",0)], itp_type_foot_armor|itp_attach_armature,0,0,weight(4)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(60)|difficulty(0) ,imodbits_armor ],

## TLD Boots end ##

## TLD Body Armor ##

# Arnor #

["arnor_blue", "Arnorian Surcoat over Mail", [("arnor_blue",0)], itp_type_body_armor  |itp_covers_legs ,0, 3920 , weight(10)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(12), imodbits_armor ],
["arnor_brown", "Arnorian Surcoat over Mail", [("arnor_brown",0)], itp_type_body_armor  |itp_covers_legs ,0, 3920 , weight(10)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(12), imodbits_armor ],
["arnor_reinf_jerkin", "Arnorian Reinforced Jerkin", [("arnor_reinf_jerkin",0)], itp_type_body_armor  |itp_covers_legs ,0, 4925 , weight(13)|abundance(100)|head_armor(0)|body_armor(44)|leg_armor(15), imodbits_armor ],
["arnor_knight", "Arnorian Knight Armor", [("arnor_knight",0)], itp_type_body_armor  |itp_covers_legs ,0, 5590 , weight(19)|abundance(100)|head_armor(0)|body_armor(51)|leg_armor(14), imodbits_armor ],

["arnor_ranger", "Dunedain Ranger Armor", [("arnor_ranger",0)], itp_type_body_armor  |itp_covers_legs ,0, 3220 , weight(4)|abundance(100)|head_armor(0)|body_armor(28)|leg_armor(12), imodbits_armor ],
["arnor_ranger_b", "Dunedain Mailed Ranger Armor", [("arnor_ranger_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 3760 , weight(6)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(16), imodbits_armor ],

# Dale #

["dale_footman", "Dale Recruit Armor", [("dale_footman",0)], itp_type_body_armor  |itp_covers_legs ,0, 3220 , weight(8)|abundance(100)|head_armor(0)|body_armor(28)|leg_armor(12), imodbits_armor ],
["dale_footman_cloak", "Dale Cloaked Recruit Armor", [("dale_footman_cloak",0)], itp_type_body_armor  |itp_covers_legs ,0, 3320 , weight(8.5)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(12), imodbits_armor ],
["dale_heavy_belt_a", "Dale Mailed Heavy Cloak", [("dale_heavy_belt_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 3760 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(16), imodbits_armor ],
["dale_heavy_belt_b", "Dale Mailed Heavy Cloak", [("dale_heavy_belt_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 3760 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(16), imodbits_armor ],
["dale_heavy_lvam_a", "Dale Mailed Jerkin", [("dale_heavy_lvam_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 3960 , weight(9)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(16), imodbits_armor ],
["dale_heavy_lvam_b", "Dale Mailed Jerkin", [("dale_heavy_lvam_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 3960 , weight(9)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(16), imodbits_armor ],
["dale_heavy_lvam_d", "Dale Mailed Jerkin", [("dale_heavy_lvam_d",0)], itp_type_body_armor  |itp_covers_legs ,0, 3960 , weight(9)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(16), imodbits_armor ],
["dale_heavy_cloak_a", "Dale Mailed Heavy Cloak", [("dale_heavy_cloak_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 4260 , weight(9)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(16), imodbits_armor ],
["dale_heavy_cloak_b", "Dale Mailed Heavy Cloak", [("dale_heavy_cloak_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 4260 , weight(9)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(16), imodbits_armor ],
["dale_heavy_cloak_d", "Dale Mailed Heavy Cloak", [("dale_heavy_cloak_d",0)], itp_type_body_armor  |itp_covers_legs ,0, 4260 , weight(9)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(16), imodbits_armor ],

["dale_new_archer_c", "Dale Archer Armor", [("dale_new_archer_c",0)], itp_type_body_armor  |itp_covers_legs ,0, 2820 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(12), imodbits_armor ],
["dale_new_archer_f", "Dale Archer Armor", [("dale_new_archer_f",0)], itp_type_body_armor  |itp_covers_legs ,0, 2820 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(12), imodbits_armor ],

# Dunland #
 
["dunland_fur_a", "Dunnish Fur Armor", [("dunland_fur_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dunland_fur_b", "Dunnish Fur Armor", [("dunland_fur_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dunland_fur_c", "Dunnish Fur Armor", [("dunland_fur_c",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dunland_fur_d", "Dunnish Fur Armor", [("dunland_fur_d",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dunland_fur_e", "Dunnish Fur Armor", [("dunland_fur_e",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dunland_fur_f", "Dunnish Fur Armor", [("dunland_fur_f",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dunland_long_fur", "Dunnish Fur Armor", [("dunland_long_fur",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dunland_hauberk_a", "Dunnish Hauberk", [("dunland_hauberk_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 3820 , weight(8)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(12), imodbits_armor ],
["dunland_hauberk_b", "Dunnish Hauberk", [("dunland_hauberk_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 3820 , weight(8)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(12), imodbits_armor ],
["dunland_chieftain", "Dunnish Chieftain Armor", [("dunland_chieftain",0)], itp_type_body_armor  |itp_covers_legs ,0, 4490 , weight(8)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14), imodbits_armor ],

# Dwarves #

["dwarf_tunic_erebor", "Dwarven Erebor Tunic", [("dwarf_tunic_erebor",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dwarf_tunic_ironhills", "Dwarven Iron Hills Tunic", [("dwarf_tunic_ironhills",0)], itp_type_body_armor  |itp_covers_legs ,0, 2680 , weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8), imodbits_armor ],
["dwarf_padtunic", "Dwarven Cloaked Leather Jerkin", [("dwarf_padtunic",0)], itp_type_body_armor  |itp_covers_legs ,0, 3420 , weight(11)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(12), imodbits_armor ],
["dwarf_padmail", "Dwarven Jerkin over Mail", [("dwarf_padmail",0)], itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(14)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14), imodbits_armor ],
["dwarf_paddedlongcoat", "Dwarven Padded Coat over Mail", [("dwarf_paddedlongcoat",0)], itp_type_body_armor  |itp_covers_legs ,0, 4690 , weight(14)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14), imodbits_armor ],
["dwarf_tunicovermaillight", "Dwarven Tunic over Mail", [("dwarf_tunicovermaillight",0)], itp_type_body_armor  |itp_covers_legs ,0, 4290 , weight(13)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(14), imodbits_armor ],
["dwarf_tunicovermail", "Dwarven Tunic over Mail", [("dwarf_tunicovermail",0)], itp_type_body_armor  |itp_covers_legs ,0, 4490 , weight(13)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14), imodbits_armor ],
["dwarf_tunicmail", "Dwarven Decorated Armor", [("dwarf_tunicmail",0)], itp_type_body_armor  |itp_covers_legs ,0, 5025 , weight(18)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(15), imodbits_armor ],
["dwarf_tunicmailarcher", "Dwarven Cloaked Decorated Armor", [("dwarf_tunicmailarcher",0)], itp_type_body_armor  |itp_covers_legs ,0, 5025 , weight(18)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(15), imodbits_armor ],
["dwarf_scalemail", "Dwarven Scale Armor", [("dwarf_scalemail",0)], itp_type_body_armor  |itp_covers_legs ,0, 5555 , weight(20)|abundance(100)|head_armor(0)|body_armor(51)|leg_armor(13), imodbits_armor ],
["dwarf_heavy_scalemail", "Dwarven Heavy Armor", [("dwarf_heavy_scalemail",0)], itp_type_body_armor  |itp_covers_legs ,0, 6030 , weight(28)|abundance(100)|head_armor(0)|body_armor(54)|leg_armor(18), imodbits_armor ],

# Gondor #

["gondor_jerkin","Gondorian Recruit Armor",[("gondor_jerkin",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["gondor_noble_cloak","Gondorian Cloaked Recruit Armor",[("gondor_noble_cloak",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["gondor_footman","Gondorian Footman Armor",[("gondor_footman",0)], itp_type_body_armor|itp_covers_legs,0,3855,weight(12)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(13),imodbits_armor],
["gondor_squire","Gondorian Squire Armor",[("gondor_squire",0)], itp_type_body_armor|itp_covers_legs,0,4055,weight(12.5)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(13),imodbits_armor],
["gondor_regular","Gondorian Sergeant Armor",[("gondor_regular",0)], itp_type_body_armor|itp_covers_legs,0,4455,weight(15)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(13),imodbits_armor],
["gondor_knight","Gondorian Knight Armor",[("gondor_knight",0)], itp_type_body_armor|itp_covers_legs,0,4855,weight(15.5)|abundance(100)|head_armor(0)|body_armor(44)|leg_armor(13),imodbits_armor],

["gondor_bowman","Gondorian Bowman Armor",[("gondor_bowman",0)],itp_type_body_armor|itp_covers_legs,0,3055,weight(6)|abundance(100)|head_armor(0)|body_armor(26)|leg_armor(13),imodbits_armor],
["gondor_archer","Gondorian Archer Armor",[("gondor_archer",0)],itp_type_body_armor|itp_covers_legs,0,3255,weight(6.5)|abundance(100)|head_armor(0)|body_armor(28)|leg_armor(13),imodbits_armor],

["gondor_ranger_cloak","Ithilien Ranger Armor",[("gondor_ranger_cloak",0)],itp_type_body_armor|itp_covers_legs,0,3350,weight(4)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(10),imodbits_armor],
["gondor_ranger_skirt","Ithilien Padded Ranger Armor",[("gondor_ranger_skirt",0)],itp_type_body_armor|itp_covers_legs,0,3655,weight(4.5)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(13),imodbits_armor],

["gondor_tower_guard","Gondorian Fountain Guard Surcoat",[("gondor_tower_guard",0)], itp_type_body_armor|itp_covers_legs,0,5360,weight(17)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(16),imodbits_armor],
["gondor_tower_knight","Gondorian Fountain Knight Surcoat",[("gondor_tower_knight",0)], itp_type_body_armor|itp_covers_legs,0,5360,weight(17)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(16),imodbits_armor],

["gondor_steward_guard","Gondorian Citadel Guard Surcoat",[("gondor_steward_guard",0)], itp_type_body_armor|itp_covers_legs,0,5560,weight(17)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(16),imodbits_armor],
["gondor_steward_knight","Gondorian Citadel Knight Surcoat",[("gondor_steward_knight",0)], itp_type_body_armor|itp_covers_legs,0,5560,weight(17)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(16),imodbits_armor],

["gondor_leader_surcoat_cloak","Denethor's Cloaked Armor",[("gondor_leader_surcoat_cloak",0)],itp_type_body_armor|itp_covers_legs,0,4525,weight(8)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(15),imodbits_armor],

# Dol Amroth #

["dol_shirt", "Dol Amroth Tunic", [("dol_shirt",0)], itp_type_body_armor  |itp_covers_legs ,0, 1610 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6), imodbits_armor ],
["dol_padded_coat", "Dol Amroth Padded Coat", [("dol_padded_coat",0)], itp_type_body_armor  |itp_covers_legs ,0, 2650 , weight(7)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(10), imodbits_armor ],
["dol_hauberk", "Dol Amroth Hauberk", [("dol_hauberk",0)], itp_type_body_armor  |itp_covers_legs ,0, 3990 , weight(13)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(14), imodbits_armor ],
["dol_heavy_mail", "Dol Amroth Plated Armor", [("dol_heavy_mail",0)], itp_type_body_armor  |itp_covers_legs ,0, 5260 , weight(16)|abundance(100)|head_armor(0)|body_armor(47)|leg_armor(16), imodbits_armor ],
["dol_very_heavy_mail", "Swan Knight Armor", [("dol_very_heavy_mail",0)], itp_type_body_armor  |itp_covers_legs ,0, 6060 , weight(18)|abundance(100)|head_armor(0)|body_armor(55)|leg_armor(16), imodbits_armor ],

["imrahil_armour", "Prince Imrahil's Armor", [("imrahil_armour",0)], itp_type_body_armor  |itp_covers_legs ,0, 6665 , weight(20)|abundance(100)|head_armor(0)|body_armor(60)|leg_armor(19), imodbits_armor ],

# Lamedon #

["lamedon_clansman","Lamedon Clansman Armor",[("lamedon_clansman",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["lamedon_footman","Lamedon Cloaked Clansman Armor",[("lamedon_footman",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8.5)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["lamedon_veteran","Lamedon Footman Armor",[("lamedon_veteran",0)],itp_type_body_armor|itp_covers_legs,0,3220,weight(9)|abundance(100)|head_armor(0)|body_armor(28)|leg_armor(12),imodbits_armor],
["lamedon_warrior","Lamedon Warrior Armor",[("lamedon_warrior",0)],itp_type_body_armor|itp_covers_legs,0,3990,weight(12)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(14),imodbits_armor],
["lamedon_vet_warrior","Lamedon Heavy Warrior Armor",[("lamedon_vet_warrior",0)],itp_type_body_armor|itp_covers_legs,0,4125,weight(13)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(15),imodbits_armor],
["lamedon_regular","Lamedon Sergeant Armor",[("lamedon_regular",0)],itp_type_body_armor|itp_covers_legs,0,4490,weight(15)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14),imodbits_armor],
["lamedon_knight","Lamedon Knight Armor",[("lamedon_knight",0)],itp_type_body_armor|itp_covers_legs,0,4890,weight(15.5)|abundance(100)|head_armor(0)|body_armor(44)|leg_armor(14),imodbits_armor],
["lamedon_leader_surcoat_cloak","Angbor's Cloaked Armor",[("lamedon_leader_surcoat_cloak",0)],itp_type_body_armor|itp_covers_legs,0,4525,weight(8)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(15),imodbits_armor],

# Blackroot #

["blackroot_footman","Blackroot Huntsman Armor",[("blackroot_footman",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["blackroot_bowman","Blackroot Cloaked Huntsman Armor",[("blackroot_bowman",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8.5)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["blackroot_warrior","Blackroot Bowman Armor",[("blackroot_warrior",0)],itp_type_body_armor|itp_covers_legs,0,3055,weight(6)|abundance(100)|head_armor(0)|body_armor(26)|leg_armor(13),imodbits_armor],
["blackroot_archer","Blackroot Archer Armor",[("blackroot_archer",0)],itp_type_body_armor|itp_covers_legs,0,3255,weight(6.5)|abundance(100)|head_armor(0)|body_armor(28)|leg_armor(13),imodbits_armor],
["blackroot_archer_caped","Blackroot Heavy Archer Armor",[("blackroot_archer_caped",0)],itp_type_body_armor|itp_covers_legs,0,3590,weight(7)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(14),imodbits_armor],
["blackroot_leader","Duinhir's Cloak",[("blackroot_leader",0)],itp_type_body_armor|itp_covers_legs,0,4690,weight(12)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14),imodbits_armor],

# Lossarnach #

["lossarnach_shirt","Lossarnach Tunic",[("lossarnach_shirt",0)],itp_type_body_armor|itp_covers_legs ,0, 1610 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6), imodbits_armor ],
["lossarnach_axeman","Lossarnach Recruit Armor",[("lossarnach_axeman",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["lossarnach_vet_axeman","Lossarnach Footman Armor",[("lossarnach_vet_axeman",0)],itp_type_body_armor|itp_covers_legs,0,3220,weight(9)|abundance(100)|head_armor(0)|body_armor(28)|leg_armor(12),imodbits_armor],
["lossarnach_warrior","Lossarnach Warrior Armor",[("lossarnach_warrior",0)],itp_type_body_armor|itp_covers_legs,0,3990,weight(9)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(14),imodbits_armor],
["lossarnach_vet_warrior","Lossarnach Veteran Armor",[("lossarnach_vet_warrior",0)],itp_type_body_armor|itp_covers_legs,0,3990,weight(9)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(17),imodbits_armor],
["lossarnach_leader","Forlong's Scale Armor",[("lossarnach_leader",0)],itp_type_body_armor|itp_covers_legs,0,5095,weight(10)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(17),imodbits_armor],

# Pelargir #

["pelargir_jerkin","Pelargir Recruit Armor",[("pelargir_jerkin",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["pelargir_light_marine","Pelargir Skirmisher Armor",[("pelargir_light_marine",0)],itp_type_body_armor|itp_covers_legs,0,3385,weight(10)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(11),imodbits_armor],
["pelargir_footman","Pelargir Footman Armor",[("pelargir_footman",0)],itp_type_body_armor|itp_covers_legs,0,3620,weight(11)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12),imodbits_armor],
["pelargir_marine","Pelargir Marine Armor",[("pelargir_marine",0)],itp_type_body_armor|itp_covers_legs,0,4220,weight(11)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(12),imodbits_armor],
["pelargir_vet_marine","Pelargir Marine Sergeant Armor",[("pelargir_vet_marine",0)],itp_type_body_armor|itp_covers_legs,0,4620,weight(11)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(12),imodbits_armor],

# Pinnath Gelin #

["pinnath_footman","Pinnath Gelin Padded Armor",[("pinnath_footman",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["pinnath_vet_footman","Pinnath Gelin Cloaked Padded Armor",[("pinnath_vet_footman",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(8.5)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["pinnath_warrior","Pinnath Gelin Heavy Armor",[("pinnath_warrior",0)],itp_type_body_armor|itp_covers_legs,0,3990,weight(9)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(14),imodbits_armor],
["pinnath_archer","Pinnath Gelin Cloaked Heavy Armor",[("pinnath_archer",0)],itp_type_body_armor|itp_covers_legs,0,3990,weight(9.5)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(14),imodbits_armor],

# Rohan #

["rohan_shirt", "Rohirric Tunic", [("rohan_shirt",0)], itp_type_body_armor  |itp_covers_legs ,0, 1610 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6), imodbits_armor ],
["rohan_shirt_cape", "Rohirric Cloaked Tunic", [("rohan_shirt_cape",0)], itp_type_body_armor  |itp_covers_legs ,0, 1610 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6), imodbits_armor ],
["rohan_long_shirt_cape", "Rohirric Cloaked Shirt", [("rohan_long_shirt_cape",0)], itp_type_body_armor  |itp_covers_legs ,0, 1710 , weight(4)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(6), imodbits_armor ],
["rohan_shirt_cape_b", "Rohirric Padded Tunic", [("rohan_shirt_cape_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 2410 , weight(5)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6), imodbits_armor ],
["rohan_long_shirt_cape_b", "Rohirric Padded Shirt", [("rohan_long_shirt_cape_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 2510 , weight(5)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(6), imodbits_armor ],
["rohan_long_shirt_cape_c", "Rohirric Padded Shirt", [("rohan_long_shirt_cape_c",0)], itp_type_body_armor  |itp_covers_legs ,0, 2510 , weight(5)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(6), imodbits_armor ],
["rohan_hauberk_a_cape", "Rohirric Tunic over Hauberk", [("rohan_hauberk_a_cape",0)], itp_type_body_armor  |itp_covers_legs ,0, 4090 , weight(11)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(14), imodbits_armor ],
["rohan_hauberk_b_cape", "Rohirric Tunic over Hauberk", [("rohan_hauberk_b_cape",0)], itp_type_body_armor  |itp_covers_legs ,0, 4090 , weight(11)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(14), imodbits_armor ],
["rohan_hauberk_c_cape", "Rohirric Tunic over Hauberk", [("rohan_hauberk_c_cape",0)], itp_type_body_armor  |itp_covers_legs ,0, 4090 , weight(11)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(14), imodbits_armor ],
["rohan_hauberk_cape_a", "Rohirric Cloaked Hauberk", [("rohan_hauberk_cape_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 4355 , weight(12)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(13), imodbits_armor ],
["rohan_hauberk_cape_b", "Rohirric Cloaked Hauberk", [("rohan_hauberk_cape_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 4355 , weight(12)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(13), imodbits_armor ],
["rohan_hauberk_cape_c", "Rohirric Cloaked Hauberk", [("rohan_hauberk_cape_c",0)], itp_type_body_armor  |itp_covers_legs ,0, 4355 , weight(12)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(13), imodbits_armor ],
["rohan_scale_cape_a", "Rohirric Scale Armor", [("rohan_scale_cape_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 4990 , weight(16)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(14), imodbits_armor ],
["rohan_scale_cape_b", "Rohirric Scale Armor", [("rohan_scale_cape_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 4990 , weight(16)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(14), imodbits_armor ],
["rohan_scale_cape_c", "Rohirric Scale Armor", [("rohan_scale_cape_c",0)], itp_type_body_armor  |itp_covers_legs ,0, 4990 , weight(16)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(14), imodbits_armor ],
["rohan_steel_scale_cape_a", "Rohirric Reinforced Scale Armor", [("rohan_steel_scale_cape_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 5360 , weight(16)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(17), imodbits_armor ],
["rohan_steel_scale_cape_b", "Rohirric Reinforced Scale Armor", [("rohan_steel_scale_cape_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 5360 , weight(16)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(17), imodbits_armor ],
["rohan_steel_scale_cape_c", "Rohirric Reinforced Scale Armor", [("rohan_steel_scale_cape_c",0)], itp_type_body_armor  |itp_covers_legs ,0, 5360 , weight(16)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(17), imodbits_armor ],
["rohan_hauberk_leather_cape_a", "Rohirric Decorated Hauberk", [("rohan_hauberk_leather_cape_a",0)], itp_type_body_armor  |itp_covers_legs ,0, 5790 , weight(14)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(14), imodbits_armor ],
["rohan_hauberk_leather_cape_b", "Rohirric Decorated Hauberk", [("rohan_hauberk_leather_cape_b",0)], itp_type_body_armor  |itp_covers_legs ,0, 5790 , weight(14)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(14), imodbits_armor ],
["rohan_hauberk_leather_cape_c", "Rohirric Heavy Decorated Hauberk", [("rohan_hauberk_leather_cape_c",0)], itp_type_body_armor  |itp_covers_legs ,0, 5990 , weight(14)|abundance(100)|head_armor(0)|body_armor(55)|leg_armor(14), imodbits_armor ],

["heraldic_rohan_armor", "Rohirric Heraldic Mail", [("heraldic_rohan_armor",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 4825 , weight(14)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(15)|difficulty(7) ,imodbits_armor,
 [
	(ti_on_init_item,	
	[
		(store_trigger_param_1, ":agent_no"),
		(store_trigger_param_2, ":troop_no"),
		
		(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_rohan", ":agent_no", ":troop_no")
	])
 ]],



# Mirkwood #

["mirkwood_leather","Mirkwood Leather Jerkin",[("mirkwood_leather",0)],itp_type_body_armor|itp_covers_legs,0,2715,weight(4)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(9),imodbits_armor],
["mirkwood_quiltedsurcoat_01","Mirkwood Leather Surcoat",[("mirkwood_quiltedsurcoat_01",0)],itp_type_body_armor|itp_covers_legs,0,3015,weight(4.5)|abundance(100)|head_armor(0)|body_armor(27)|leg_armor(9),imodbits_armor],
["mirkwood_maillewithsurcoat_01","Mirkwood Mailed Leather Surcoat",[("mirkwood_maillewithsurcoat_01",0)],itp_type_body_armor|itp_covers_legs,0,3655,weight(6)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(13),imodbits_armor],
["mirkwood_light_scale","Mirkwood Scale Armor",[("mirkwood_light_scale",0)],itp_type_body_armor|itp_covers_legs,0,3885,weight(7)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(11),imodbits_armor],
["mirkwood_scalequilted_01","Mirkwood Quilted Scale Armor",[("mirkwood_scalequilted_01",0)],itp_type_body_armor|itp_covers_legs,0,4090,weight(7)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(14),imodbits_armor],
["mirkwood_scaleovermaille_01","Mirkwood Mailed Scale Armor",[("mirkwood_scaleovermaille_01",0)],itp_type_body_armor|itp_covers_legs,0,4860,weight(9)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(16),imodbits_armor],

# Lorien #

["lorien_archer","Lorien Archer's Tunic",[("lorien_archer",0)],itp_type_body_armor|itp_covers_legs,0,3085,weight(4)|abundance(100)|head_armor(0)|body_armor(27)|leg_armor(11),imodbits_armor],
["lorien_warden_cloak","Lorien Warden Cloak",[("lorien_warden_cloak",0)],itp_type_body_armor|itp_covers_legs,0,3420,weight(4.5)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(12),imodbits_armor],
["lorien_royalarcher_01","Lorien Royal Warden Armor",[("lorien_royalarcher_01",0)],itp_type_body_armor|itp_covers_legs,0,5130,weight(14)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(18),imodbits_armor],

["lorien_light_infantry_01","Lorien Horseman Armor",[("lorien_light_infantry_01",0)],itp_type_body_armor|itp_covers_legs,0,3520,weight(8)|abundance(100)|head_armor(0)|body_armor(31)|leg_armor(12),imodbits_armor],
["lorien_light_vetinfantry_01","Lorien Light Horseman Armor",[("lorien_light_vetinfantry_01",0)],itp_type_body_armor|itp_covers_legs,0,3620,weight(8.5)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12),imodbits_armor],
["lorien_light_eliteinfantry_01","Lorien Horseman Armor",[("lorien_light_eliteinfantry_01",0)],itp_type_body_armor|itp_covers_legs,0,3955,weight(9)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(13),imodbits_armor],
["lorien_light_swordsman_01","Lorien Veteran Horseman Armor",[("lorien_light_swordsman_01",0)],itp_type_body_armor|itp_covers_legs,0,4055,weight(9.5)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(13),imodbits_armor],
["lorien_infantry_01","Lorien Warrior Armor",[("lorien_infantry_01",0)],itp_type_body_armor|itp_covers_legs,0,4830,weight(14)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(18),imodbits_armor],
["lorien_vetinfantry_01","Lorien Veteran Warrior Armor",[("lorien_vetinfantry_01",0)],itp_type_body_armor|itp_covers_legs,0,4930,weight(14)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(18),imodbits_armor],
["lorien_eliteinfantry_01","Lorien Elite Swordsman Armor",[("lorien_eliteinfantry_01",0)],itp_type_body_armor|itp_covers_legs,0,5130,weight(14)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(18),imodbits_armor],
["lorien_royalswordsman_01","Lorien Royal Guard Armor",[("lorien_royalswordsman_01",0)],itp_type_body_armor|itp_covers_legs,0,5430,weight(14)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(18),imodbits_armor],

# Rivendell #

["rivendell_recruit_swordfighter","Rivendell Soldier Armor",[("rivendell_recruit_swordfighter",0)],itp_type_body_armor|itp_covers_legs,0,3890,weight(10)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(14),imodbits_armor],
["rivendell_normal_swordfighter","Rivendell Warrior Armor",[("rivendell_normal_swordfighter",0)],itp_type_body_armor|itp_covers_legs,0,4190,weight(11)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(14),imodbits_armor],
["rivendell_elite_swordfighter","Rivendell Heavy Armor",[("rivendell_elite_swordfighter",0)],itp_type_body_armor|itp_covers_legs,0,4825,weight(13)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(15),imodbits_armor],
["rivendell_leader","Rivendell Elite Armor",[("rivendell_leader",0)],itp_type_body_armor|itp_covers_legs,0,4925,weight(13.5)|abundance(100)|head_armor(0)|body_armor(44)|leg_armor(15),imodbits_armor],
["rivendell_reward_armour","Lord Elrond's Armor",[("rivendell_reward_armour",0)],itp_type_body_armor|itp_covers_legs,0,5225,weight(14.5)|abundance(100)|head_armor(0)|body_armor(47)|leg_armor(15),imodbits_armor],

["rivendell_recruit_archer","Rivendell Padded Tunic",[("rivendell_recruit_archer",0)],itp_type_body_armor|itp_covers_legs,0,3085,weight(4)|abundance(100)|head_armor(0)|body_armor(27)|leg_armor(11),imodbits_armor],
["rivendell_normal_archer","Rivendell Padded Armor",[("rivendell_normal_archer",0)],itp_type_body_armor|itp_covers_legs,0,3420,weight(4.5)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(12),imodbits_armor],
["rivendell_elite_archer","Rivendell Archer Armor",[("rivendell_elite_archer",0)],itp_type_body_armor|itp_covers_legs,0,4290,weight(12)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(14),imodbits_armor],
["rivendell_mounted_archer","Rivendell Heavy Archer Armor",[("rivendell_mounted_archer",0)],itp_type_body_armor|itp_covers_legs,0,4690,weight(12)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14),imodbits_armor],

# Harad # 
 
["harad_tunic","Haradrim Tunic",[("harad_tunic",0)],itp_type_body_armor|itp_covers_legs,0,1610,weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6),imodbits_armor],
["harad_padded","Haradrim Padded Tunic",[("harad_padded",0)],itp_type_body_armor|itp_covers_legs,0,2280,weight(5)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(8),imodbits_armor],
["harad_skirmisher","Haradrim Skirmisher Tunic",[("harad_skirmisher",0)],itp_type_body_armor|itp_covers_legs,0,2680,weight(4.5)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(8),imodbits_armor],
["harad_archer","Haradrim Archer Vest",[("harad_archer",0)],itp_type_body_armor|itp_covers_legs,0,3790,weight(6.5)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(14),imodbits_armor],
["harad_hauberk","Haradrim Mail Hauberk",[("harad_hauberk",0)],itp_type_body_armor|itp_covers_legs,0,4220,weight(8)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(12),imodbits_armor],
["harad_lamellar","Haradrim Lamellar Hauberk",[("harad_lamellar",0)],itp_type_body_armor|itp_covers_legs,0,4420,weight(8)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12),imodbits_armor],
["harad_scale","Haradrim Scale Armor",[("harad_scale",0)],itp_type_body_armor|itp_covers_legs,0,4450,weight(9)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(10),imodbits_armor],
["harad_tiger_scale","Haradrim Scale Armor",[("harad_tiger_scale",0)],itp_type_body_armor|itp_covers_legs,0,4450,weight(9)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(10),imodbits_armor],
["harad_lion_scale","Haradrim Scale Armor",[("harad_lion_scale",0)],itp_type_body_armor|itp_covers_legs,0,4450,weight(9)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(10),imodbits_armor],
["harad_hashari_mail","Hashari Mailed Robe",[("harad_hashari_mail",0)],itp_type_body_armor|itp_covers_legs,0,4525,weight(8)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(15),imodbits_armor],
["harad_heavy","Eagle Guard Armor",[("harad_heavy",0)],itp_type_body_armor|itp_covers_legs,0,4955,weight(9)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(13),imodbits_armor],
["black_snake_armor","Serpent Guard Armor",[("black_snake_armor",0)],itp_type_body_armor|itp_covers_legs,0,5725,weight(9)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(15),imodbits_armor],

# Khand # 
 
["khand_light","Variag Leather Skirt",[("khand_light",0)],itp_type_body_armor|itp_covers_legs,0,450,weight(2)|abundance(100)|head_armor(0)|body_armor(1)|leg_armor(10),imodbits_armor],
["khand_light_lam","Variag Light Lamellar Armor",[("khand_light_lam",0)],itp_type_body_armor|itp_covers_legs,0,3320,weight(8)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(12),imodbits_armor],
["khand_med_lam_b","Variag Lamellar over Surcoat",[("khand_med_lam_b",0)],itp_type_body_armor|itp_covers_legs,0,4490,weight(12)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14),imodbits_armor],
["khand_med_lam_c","Variag Lamellar over Surcoat",[("khand_med_lam_c",0)],itp_type_body_armor|itp_covers_legs,0,4590,weight(12)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(14),imodbits_armor],
["khand_med_lam_d","Variag Lamellar over Surcoat",[("khand_med_lam_d",0)],itp_type_body_armor|itp_covers_legs,0,4690,weight(12)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14),imodbits_armor],
["khand_foot_lam_a","Variag Lamellar Footman Armor",[("khand_foot_lam_a",0)],itp_type_body_armor|itp_covers_legs,0,3785,weight(9)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(11),imodbits_armor],
["khand_foot_lam_b","Variag Lamellar Footman Armor",[("khand_foot_lam_b",0)],itp_type_body_armor|itp_covers_legs,0,3785,weight(9)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(11),imodbits_armor],
["khand_foot_lam_c","Variag Lamellar Footman Armor",[("khand_foot_lam_c",0)],itp_type_body_armor|itp_covers_legs,0,3785,weight(9)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(11),imodbits_armor],
["khand_heavy_lam","Variag Heavy Lamellar Armor",[("khand_heavy_lam",0)],itp_type_body_armor|itp_covers_legs,0,4990,weight(14)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(14),imodbits_armor],
["khand_noble_lam","Variag Champion Armor",[("khand_noble_lam",0)],itp_type_body_armor|itp_covers_legs,0,5290,weight(14)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(14),imodbits_armor],

 
# Umbar # 
 
["corsair_leather","Umbar Leather Vest",[("corsair_leather",0)],itp_type_body_armor|itp_covers_legs,0,2580,weight(5)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(8),imodbits_armor],
["corsair_leather_cape","Umbar Leather Vest",[("corsair_leather_cape",0)],itp_type_body_armor|itp_covers_legs,0,2580,weight(5)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(8),imodbits_armor],
["corsair_leather_pauldron","Umbar Leather Vest",[("corsair_leather_pauldron",0)],itp_type_body_armor|itp_covers_legs,0,2780,weight(5.25)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(8),imodbits_armor],
["corsair_padded","Umbar Scale Armor",[("corsair_padded",0)],itp_type_body_armor|itp_covers_legs,0,3780,weight(8)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8),imodbits_armor],
["corsair_padded_cape","Umbar Scale Armor",[("corsair_padded_cape",0)],itp_type_body_armor|itp_covers_legs,0,3780,weight(8)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8),imodbits_armor],
["corsair_padded_pauldron","Umbar Scale Armor",[("corsair_padded_pauldron",0)],itp_type_body_armor|itp_covers_legs,0,4080,weight(8.5)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8),imodbits_armor],
["corsair_chain","Umbar Scale Armor over Chain",[("corsair_chain",0)],itp_type_body_armor|itp_covers_legs,0,4380,weight(10)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(8),imodbits_armor],
["corsair_chain_cape","Umbar Scale Armor over Chain",[("corsair_chain_cape",0)],itp_type_body_armor|itp_covers_legs,0,4380,weight(10)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(8),imodbits_armor],
["corsair_chain_pauldron","Umbar Scale Armor over Chain",[("corsair_chain_pauldron",0)],itp_type_body_armor|itp_covers_legs,0,4780,weight(10.5)|abundance(100)|head_armor(0)|body_armor(44)|leg_armor(8),imodbits_armor],
["corsair_chain_leader","Umbar Nobleman Armor",[("corsair_chain_leader",0)],itp_type_body_armor|itp_covers_legs,0,5080,weight(11)|abundance(100)|head_armor(0)|body_armor(47)|leg_armor(8),imodbits_armor],
 
# Rhun #
 
["rhunarmorlight1","Rhun Leggings",[("RhunArmorLight1",0)],itp_type_body_armor|itp_covers_legs,0,750,weight(1)|abundance(100)|head_armor(0)|body_armor(4)|leg_armor(10),imodbits_armor],
["rhunarmorlight2","Rhun Belted Leggings",[("RhunArmorLight2",0)],itp_type_body_armor|itp_covers_legs,0,985,weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(11),imodbits_armor],
["rhunarmorlight4","Rhun Belted Leggings",[("RhunArmorLight4",0)],itp_type_body_armor|itp_covers_legs,0,1085,weight(1.25)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(11),imodbits_armor],
["rhunarmormedium1","Rhun Pauldrons",[("RhunArmorMedium1",0)],itp_type_body_armor|itp_covers_legs,0,1585,weight(1.5)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(11),imodbits_armor],
["rhunarmormedium3","Rhun Pauldrons",[("RhunArmorMedium3",0)],itp_type_body_armor|itp_covers_legs,0,1685,weight(1.5)|abundance(100)|head_armor(0)|body_armor(13)|leg_armor(11),imodbits_armor],
["rhunarmormedium4","Rhun Pauldrons",[("RhunArmorMedium4",0)],itp_type_body_armor|itp_covers_legs,0,1685,weight(1.5)|abundance(100)|head_armor(0)|body_armor(13)|leg_armor(11),imodbits_armor],
["rhunarmormedium5","Rhun Caped Tunic",[("RhunArmorMedium5",0)],itp_type_body_armor|itp_covers_legs,0,1950,weight(1.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(10),imodbits_armor],
["rhunarmorheavy2","Rhun Scale Hauberk",[("RhunArmorHeavy2",0)],itp_type_body_armor|itp_covers_legs,0,4060,weight(8)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(16),imodbits_armor],
["rhunarmorheavy3","Rhun Scale Hauberk",[("RhunArmorHeavy3",0)],itp_type_body_armor|itp_covers_legs,0,4060,weight(8)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(16),imodbits_armor],
["rhunarmornoble1A","Rhun Decorated Armor",[("RhunArmorNoble1A",0)],itp_type_body_armor|itp_covers_legs,0,4960,weight(10)|abundance(100)|head_armor(0)|body_armor(44)|leg_armor(16),imodbits_armor],
["rhunarmornoble1B","Rhun Heavy Decorated Armor",[("RhunArmorNoble1B",0)],itp_type_body_armor|itp_covers_legs,0,5360,weight(11)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(16),imodbits_armor],
 
["rhun_light_armor1","Rhun Scale Armor",[("rhun_light_armor1",0)],itp_type_body_armor|itp_covers_legs,0,5050,weight(9)|abundance(100)|head_armor(0)|body_armor(47)|leg_armor(10),imodbits_armor],
["rhun_light_armor2","Rhun Scale Armor",[("rhun_light_armor2",0)],itp_type_body_armor|itp_covers_legs,0,5050,weight(9)|abundance(100)|head_armor(0)|body_armor(47)|leg_armor(10),imodbits_armor],
["rhun_medium_armor1","Rhun Plated Cloth Armor",[("rhun_medium_armor1",0)],itp_type_body_armor|itp_covers_legs,0,4760,weight(6)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(16),imodbits_armor],
["rhun_medium_armor2","Rhun Plated Cloth Armor",[("rhun_medium_armor2",0)],itp_type_body_armor|itp_covers_legs,0,4760,weight(6)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(16),imodbits_armor],
["rhun_medium_armor3","Rhun Plated Cloth Armor",[("rhun_medium_armor3",0)],itp_type_body_armor|itp_covers_legs,0,4760,weight(6)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(16),imodbits_armor],
["rhun_medium_armor4","Rhun Plated Cloth Armor",[("rhun_medium_armor4",0)],itp_type_body_armor|itp_covers_legs,0,4760,weight(6)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(16),imodbits_armor],
["rhun_heavy_armor1","Rhun Chainmail Armor",[("rhun_heavy_armor1",0)],itp_type_body_armor|itp_covers_legs,0,5490,weight(9)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(14),imodbits_armor],
["rhun_heavy_armor2","Rhun Heavy Chainmail Armor",[("rhun_heavy_armor2",0)],itp_type_body_armor|itp_covers_legs,0,5790,weight(9)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(14),imodbits_armor],
["rhun_noble_armor1","Dragonclaw Cataphract Armor",[("rhun_noble_armor1",0)],itp_type_body_armor|itp_covers_legs,0,6295,weight(9)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(17),imodbits_armor],
["rhun_noble_armor2","Dragonclaw Cataphract Armor",[("rhun_noble_armor2",0)],itp_type_body_armor|itp_covers_legs,0,6295,weight(9)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(17),imodbits_armor],
["rhun_noble_armor3","Shadow Legion Armor",[("rhun_noble_armor3",0)],itp_type_body_armor|itp_covers_legs,0,6295,weight(9)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(17),imodbits_armor],
["rhun_noble_armor4","Shadow Legion Armor",[("rhun_noble_armor4",0)],itp_type_body_armor|itp_covers_legs,0,6295,weight(9)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(17),imodbits_armor],

# Mordor #
 
["evil_light_armor","Light Black Numenorean Hauberk",[("evil_light_armor",0)],itp_type_body_armor|itp_covers_legs,0,4060,weight(8)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(16),imodbits_armor],
["mordor_armor_a","Black Numenorean Hauberk",[("mordor_armor_a",0)],itp_type_body_armor|itp_covers_legs,0,4495,weight(10)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(17),imodbits_armor],
["mordor_armor_b","Black Numenorean Scaled Hauberk",[("mordor_armor_b",0)],itp_type_body_armor|itp_covers_legs,0,4695,weight(10)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(17),imodbits_armor],
["black_numenor_armor","Black Numenorean Scaled Armor",[("black_numenor_armor",0)],itp_type_body_armor|itp_covers_legs,0,4925,weight(10)|abundance(100)|head_armor(0)|body_armor(44)|leg_armor(15),imodbits_armor],
["mordor_captain_armor","Morgul Knight's Armor",[("mordor_captain_armor",0)],itp_type_body_armor|itp_covers_legs,0,5260,weight(10)|abundance(100)|head_armor(0)|body_armor(47)|leg_armor(16),imodbits_armor],
["armor","Morgul Knight's Armor",[("mordor_captain_armor",0)],itp_type_body_armor|itp_covers_legs,0,5260,weight(10)|abundance(100)|head_armor(0)|body_armor(47)|leg_armor(16),imodbits_armor],
["nazgulrobe","Nazgul's Robes",[("nazgulrobe",0)],itp_type_body_armor|itp_covers_head|itp_covers_legs,0,7975,weight(6)|abundance(100)|head_armor(35)|body_armor(50)|leg_armor(35),imodbits_armor],
["old_nazgulrobe","Nazgul's Robes",[("old_nazgulrobe",0)],itp_type_body_armor|itp_covers_legs,0,7975,weight(6)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(35),imodbits_armor],

# Mordor Orcs #
["orc_tribal_c","Tribal Orc Armour Tier 1",[("orc_tribal_c",0)],itp_type_body_armor|itp_covers_legs,0,410,weight(0.5)|abundance(100)|head_armor(0)|body_armor(2)|leg_armor(6),imodbits_armor],
["orc_tribal_b","Tribal Orc Armour Tier 2",[("orc_tribal_b",0)],itp_type_body_armor|itp_covers_legs,0,1040,weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(4),imodbits_armor],
["orc_tribal_a","Tribal Orc Armour Tier 3",[("orc_tribal_a",0)],itp_type_body_armor|itp_covers_legs,0,1340,weight(1)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(4),imodbits_armor],

["orc_mordor_a","Mordor Orc Armour Tier 1",[("orc_mordor_a",0)],itp_type_body_armor|itp_covers_legs,0,780,weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(8),imodbits_armor],
["orc_mordor_b","Mordor Orc Armour Tier 2",[("orc_mordor_b",0)],itp_type_body_armor|itp_covers_legs,0,1380,weight(2)|abundance(100)|head_armor(0)|body_armor(11)|leg_armor(8),imodbits_armor],
["orc_mordor_c","Mordor Orc Armour Tier 3",[("orc_mordor_c",0)],itp_type_body_armor|itp_covers_legs,0,1885,weight(2.75)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(11),imodbits_armor],
["orc_mordor_d","Mordor Orc Armour Tier 4",[("orc_mordor_d",0)],itp_type_body_armor|itp_covers_legs,0,2255,weight(3)|abundance(100)|head_armor(0)|body_armor(18)|leg_armor(13),imodbits_armor],
["orc_mordor_e","Mordor Orc Armour Tier 5",[("orc_mordor_e",0)],itp_type_body_armor|itp_covers_legs,0,2555,weight(3.25)|abundance(100)|head_armor(0)|body_armor(21)|leg_armor(13),imodbits_armor],
["orc_mordor_f","Mordor Orc Armour Tier 6",[("orc_mordor_f",0)],itp_type_body_armor|itp_covers_legs,0,2390,weight(4.5)|abundance(100)|head_armor(0)|body_armor(28)|leg_armor(14),imodbits_armor],
["orc_mordor_g","Mordor Orc Armour Tier 7",[("orc_mordor_g",0)],itp_type_body_armor|itp_covers_legs,0,3525,weight(4.75)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(15),imodbits_armor],
["orc_mordor_h","Mordor Orc Armour Tier 8",[("orc_mordor_h",0)],itp_type_body_armor|itp_covers_legs,0,3830,weight(5.25)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(18),imodbits_armor],
["orc_mordor_i","Mordor Orc Armour Tier 9",[("orc_mordor_i",0)],itp_type_body_armor|itp_covers_legs,0,4030,weight(5.75)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(18),imodbits_armor],
["orc_mordor_j","Mordor Orc Armour Tier 10",[("orc_mordor_j",0)],itp_type_body_armor|itp_covers_legs,0,4500,weight(6.5)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(20),imodbits_armor],

["orc_gunda_a","Gundabad Orc Armour Tier 1",[("orc_gunda_a",0)],itp_type_body_armor|itp_covers_legs,0,610,weight(0.75)|abundance(100)|head_armor(0)|body_armor(4)|leg_armor(6),imodbits_armor],
["orc_gunda_b","Gundabad Orc Armour Tier 2",[("orc_gunda_b",0)],itp_type_body_armor|itp_covers_legs,0,1310,weight(1.5)|abundance(100)|head_armor(0)|body_armor(11)|leg_armor(6),imodbits_armor],
["orc_gunda_c","Gundabad Orc Armour Tier 3",[("orc_gunda_c",0)],itp_type_body_armor|itp_covers_legs,0,1880,weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8),imodbits_armor],
["orc_gunda_d","Gundabad Orc Armour Tier 4",[("orc_gunda_d",0)],itp_type_body_armor|itp_covers_legs,0,2220,weight(2.25)|abundance(100)|head_armor(0)|body_armor(18)|leg_armor(12),imodbits_armor],
["orc_gunda_e","Gundabad Orc Armour Tier 5",[("orc_gunda_e",0)],itp_type_body_armor|itp_covers_legs,0,2960,weight(4)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(16),imodbits_armor],

# Moria Orcs #
["orc_moria_a","Moria Orc Armour Tier 1",[("orc_moria_a",0)],itp_type_body_armor|itp_covers_legs,0,780,weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(8),imodbits_armor],
["orc_moria_b","Moria Orc Armour Tier 2",[("orc_moria_b",0)],itp_type_body_armor|itp_covers_legs,0,1350,weight(1.25)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10),imodbits_armor],
["orc_moria_c","Moria Orc Armour Tier 3",[("orc_moria_c",0)],itp_type_body_armor|itp_covers_legs,0,2420,weight(3)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(12),imodbits_armor],
["orc_moria_d","Moria Orc Armour Tier 4",[("orc_moria_d",0)],itp_type_body_armor|itp_covers_legs,0,2560,weight(3.25)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(16),imodbits_armor],
["orc_moria_e","Moria Orc Armour Tier 5",[("orc_moria_e",0)],itp_type_body_armor|itp_covers_legs,0,3060,weight(4)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(16),imodbits_armor],

# Mordor Uruks #
["uruk_body","Mordor Uruk Body",[("uruk_body",0)],itp_type_body_armor|itp_covers_legs,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0),imodbits_none],
["uruk_mordor_a","Mordor Uruk Armour Tier 1",[("uruk_mordor_a",0)],itp_type_body_armor|itp_covers_legs,0,780,weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(8),imodbits_armor],
["uruk_mordor_b","Mordor Uruk Armour Tier 2",[("uruk_mordor_b",0)],itp_type_body_armor|itp_covers_legs,0,1080,weight(1.25)|abundance(100)|head_armor(0)|body_armor(8)|leg_armor(8),imodbits_armor],
["uruk_mordor_c","Mordor Uruk Armour Tier 3",[("uruk_mordor_c",0)],itp_type_body_armor|itp_covers_legs,0,2480,weight(4)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(8),imodbits_armor],
["uruk_mordor_d","Mordor Uruk Armour Tier 4",[("uruk_mordor_d",0)],itp_type_body_armor|itp_covers_legs,0,2960,weight(5)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(16),imodbits_armor],
["uruk_mordor_e","Mordor Uruk Armour Tier 5",[("uruk_mordor_e",0)],itp_type_body_armor|itp_covers_legs,0,3460,weight(5.5)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(16),imodbits_armor],
["uruk_mordor_f","Mordor Uruk Armour Tier 6",[("uruk_mordor_f",0)],itp_type_body_armor|itp_covers_legs,0,3890,weight(5.75)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(14),imodbits_armor],
["uruk_mordor_g","Mordor Uruk Armour Tier 7",[("uruk_mordor_g",0)],itp_type_body_armor|itp_covers_legs,0,3960,weight(5.85)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(16),imodbits_armor],
["uruk_mordor_h","Mordor Uruk Armour Tier 8",[("uruk_mordor_h",0)],itp_type_body_armor|itp_covers_legs,0,4490,weight(7)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14),imodbits_armor],
["uruk_mordor_i","Mordor Uruk Armour Tier 9",[("uruk_mordor_i",0)],itp_type_body_armor|itp_covers_legs,0,4760,weight(7.25)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(16),imodbits_armor],
["uruk_mordor_j","Mordor Uruk Armour Tier 10",[("uruk_mordor_j",0)],itp_type_body_armor|itp_covers_legs,0,5060,weight(8)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(16),imodbits_armor],
["uruk_mordor_k","Mordor Uruk Armour Tier 11",[("uruk_mordor_k",0)],itp_type_body_armor|itp_covers_legs,0,5260,weight(8.25)|abundance(100)|head_armor(0)|body_armor(47)|leg_armor(16),imodbits_armor],

["u_handl","Uruk Hands", [("u_handL",0)], itp_merchandise|itp_type_hand_armor,0,0, weight(0)|abundance(100)|body_armor(0)|difficulty(0),imodbits_none],
["uh_handl","Uruk-hai Hands", [("uh_handL",0)], itp_merchandise|itp_type_hand_armor,0,0, weight(0)|abundance(100)|body_armor(0)|difficulty(0),imodbits_none],

# Oathbreakers #
["aod_armor_a1","Spectral Armour", [("aod_armor_a1", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_a2","Spectral Armour", [("aod_armor_a2", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_b1","Spectral Armour", [("aod_armor_b1", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_b2","Spectral Armour", [("aod_armor_b2", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_c1","Spectral Armour", [("aod_armor_c1", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_c2","Spectral Armour", [("aod_armor_c2", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_d1","Spectral Armour", [("aod_armor_d1", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_d2","Spectral Armour", [("aod_armor_d2", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_e1","Spectral Armour", [("aod_armor_e1", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_e2","Spectral Armour", [("aod_armor_e2", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_f1","Spectral Armour", [("aod_armor_f1", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_f2","Spectral Armour", [("aod_armor_f2", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_g1","Spectral Armour", [("aod_armor_g1", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_g2","Spectral Armour", [("aod_armor_g2", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_h1","Spectral Armour", [("aod_armor_h1", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["aod_armor_h2","Spectral Armour", [("aod_armor_h2", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,3525, weight(12)|abundance(100)|body_armor(30)|leg_armor(15), imodbits_armor, []],															
["king_of_the_dead_armour","King of the Dead's Armour", [("king_of_the_dead_armour", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,0, weight(15)|abundance(100)|body_armor(45)|leg_armor(25), imodbits_armor, []],
																													
["aod_gloves_a","Spectral Gloves", [("aod_gloves_a_L", 0)], itp_type_hand_armor|itp_merchandise, 0,750, weight(0.25)|abundance(120)|body_armor(5), imodbits_armor, []],															
["aod_gloves_b","Spectral Gloves", [("aod_gloves_b_L", 0)], itp_type_hand_armor|itp_merchandise, 0,750, weight(0.25)|abundance(120)|body_armor(5), imodbits_armor, []],

# Trolls #
["troll","Troll Body",[("troll",0)],itp_type_body_armor|itp_covers_legs,0,0,weight(15)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(10),imodbits_none],
["leather_hat_troll","Troll Body",[("leather_hat_troll",0)],itp_type_body_armor|itp_covers_legs,0,0,weight(15)|abundance(100)|head_armor(20)|body_armor(10)|leg_armor(10),imodbits_none],
["suspenders_troll","Troll Body",[("suspenders_troll",0)],itp_type_body_armor|itp_covers_legs,0,0,weight(18)|abundance(100)|head_armor(20)|body_armor(20)|leg_armor(10),imodbits_none],
["fully_armoured_troll","Troll Body",[("fully_armoured_troll",0)],itp_type_body_armor|itp_covers_legs,0,0,weight(30)|abundance(100)|head_armor(40)|body_armor(45)|leg_armor(10),imodbits_none],

["invis_head","Invisible Head",[("invalid_item",0)],itp_type_head_armor|itp_covers_head,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["invis_hands","Invisible Hands", [("invalid_item",0)], itp_merchandise|itp_type_hand_armor,0,0, weight(0)|abundance(100)|body_armor(0)|difficulty(0),imodbits_none],
["invis_feet","Invisible Feet", [("invalid_item",0)], itp_type_foot_armor|itp_attach_armature,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_none],
["no_feet","No Feet", [("invalid_item",0)], itp_type_foot_armor|itp_attach_armature,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(0)|difficulty(0),imodbits_none],

# Balrog #
["balrog","Balrog Body",[("balrog",0)],itp_type_body_armor|itp_covers_head|itp_covers_legs,0,0,weight(30)|abundance(100)|head_armor(40)|body_armor(60)|leg_armor(40),imodbits_none],

# Sauron #
["sauron_armour","Sauron's Armour",[("sauron_armour",0)],itp_type_body_armor|itp_covers_legs,0,0,weight(20)|abundance(100)|head_armor(0)|body_armor(70)|leg_armor(40),imodbits_none],

# Isengard Orcs #
["orc_isen_a","Isengard Orc Armour Tier 1",[("orc_isen_a",0)],itp_type_body_armor|itp_covers_legs,0,780,weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(8),imodbits_armor],
["orc_isen_b","Isengard Orc Armour Tier 2",[("orc_isen_b",0)],itp_type_body_armor|itp_covers_legs,0,1825,weight(2.5)|abundance(100)|head_armor(0)|body_armor(13)|leg_armor(15),imodbits_armor],
["orc_isen_c","Isengard Orc Armour Tier 3",[("orc_isen_c",0)],itp_type_body_armor|itp_covers_legs,0,2360,weight(2.5)|abundance(100)|head_armor(0)|body_armor(18)|leg_armor(16),imodbits_armor],
["orc_isen_d","Isengard Orc Armour Tier 4",[("orc_isen_d",0)],itp_type_body_armor|itp_covers_legs,0,3060,weight(3.5)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(16),imodbits_armor],
["orc_isen_e","Isengard Orc Armour Tier 5",[("orc_isen_e",0)],itp_type_body_armor|itp_covers_legs,0,3100,weight(3.25)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(20),imodbits_armor],
["orc_isen_f","Isengard Orc Armour Tier 6",[("orc_isen_f",0)],itp_type_body_armor|itp_covers_legs,0,3500,weight(3.5)|abundance(100)|head_armor(0)|body_armor(28)|leg_armor(20),imodbits_armor],
["orc_isen_g","Isengard Orc Armour Tier 7",[("orc_isen_g",0)],itp_type_body_armor|itp_covers_legs,0,4200,weight(4.5)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(20),imodbits_armor],

# Isengard Uruk-hai #
["urukhai_body","Isengard Uruk-hai Body",[("urukhai_body",0)],itp_type_body_armor|itp_covers_legs,0,0,weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0),imodbits_none],
["urukhai_isen_a","Isengard Uruk-hai Armour Tier 1",[("urukhai_isen_a",0)],itp_type_body_armor|itp_covers_legs,0,850,weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(10),imodbits_none],
["urukhai_isen_b","Isengard Uruk-hai Armour Tier 2",[("urukhai_isen_b",0)],itp_type_body_armor|itp_covers_legs,0,2350,weight(3.5)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(10),imodbits_none],
["urukhai_isen_c","Isengard Uruk-hai Armour Tier 3",[("urukhai_isen_c",0)],itp_type_body_armor|itp_covers_legs,0,2820,weight(3.5)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(12),imodbits_none],
["urukhai_isen_e","Isengard Uruk-hai Armour Tier 4",[("urukhai_isen_e",0)],itp_type_body_armor|itp_covers_legs,0,3525,weight(5.5)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(15),imodbits_none],
["urukhai_isen_d","Isengard Uruk-hai Armour Tier 5",[("urukhai_isen_d",0)],itp_type_body_armor|itp_covers_legs,0,4900,weight(7)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(20),imodbits_none],
["urukhai_isen_f","Isengard Uruk-hai Armour Tier 6",[("urukhai_isen_f",0)],itp_type_body_armor|itp_covers_legs,0,5025,weight(7)|abundance(100)|head_armor(0)|body_armor(45)|leg_armor(15),imodbits_none],
["urukhai_isen_g","Isengard Uruk-hai Armour Tier 7",[("urukhai_isen_g",0)],itp_type_body_armor|itp_covers_legs,0,5500,weight(7.5)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(20),imodbits_none],
["urukhai_isen_h","Isengard Uruk-hai Armour Tier 8",[("urukhai_isen_h",0)],itp_type_body_armor|itp_covers_legs,0,5605,weight(7.75)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(23),imodbits_none],

["urukhai_isen_tracker_a","Isengard Uruk-hai Tracker Armour Tier 1",[("urukhai_isen_tracker_a",0)],itp_type_body_armor|itp_covers_legs,0,2620,weight(1.5)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(12),imodbits_none],
["urukhai_isen_tracker_b","Isengard Uruk-hai Tracker Armour Tier 2",[("urukhai_isen_tracker_b",0)],itp_type_body_armor|itp_covers_legs,0,2920,weight(1.5)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(12),imodbits_none],

["urukhai_guardarmour","Isengard Uruk-hai Guard Armour",[("urukhai_guardarmour",0)],itp_type_body_armor|itp_covers_legs,0,6175,weight(8.5)|abundance(100)|head_armor(0)|body_armor(53)|leg_armor(25),imodbits_none],

["urukhai_berserker","Isengard Berserker Body",[("urukhai_berserker",0)],itp_type_body_armor|itp_covers_head|itp_covers_legs,0,1250,weight(0.5)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0),imodbits_none],
["urukhai_berserker_elite","Isengard Berserker Armour",[("urukhai_berserker_elite",0)],itp_type_body_armor|itp_covers_head|itp_covers_legs,0,7300,weight(9.25)|abundance(100)|head_armor(32)|body_armor(50)|leg_armor(20),imodbits_none],

["lurtz","Lurtz",[("lurtz",0)],itp_type_body_armor|itp_covers_head|itp_covers_legs,0,4025,weight(3)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(15),imodbits_none],
["ugluk","Ugluk",[("ugluk",0)],itp_type_body_armor|itp_covers_head|itp_covers_legs,0,4825,weight(6)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(15),imodbits_none],
["mauhur","Mauhur",[("mauhur",0)],itp_type_body_armor|itp_covers_head|itp_covers_legs,0,7700,weight(8)|abundance(100)|head_armor(40)|body_armor(50)|leg_armor(20),imodbits_none],
["saruman","Saruman",[("saruman",0)],itp_type_body_armor|itp_covers_head|itp_covers_legs,0,7700,weight(3)|abundance(100)|head_armor(60)|body_armor(60)|leg_armor(60),imodbits_none],

## TLD Body armor end ##

## TLD Weapons ##
 
# TLD melee weapons #

# Arnor #

["dunedain_1h", "Arnorian Arming Sword", [("dunedain_1h",0),("scab_dunedain_1h",ixmesh_carry)], itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,750,weight(1.5)|difficulty(0)|spd_rtng(102)|weapon_length(81)|swing_damage(29,cut)|thrust_damage(26,pierce),imodbits_sword],
["dunedain_long", "Arnorian Longsword", [("dunedain_long",0),("scab_dunedain_long",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,975,weight(1.75)|difficulty(0)|spd_rtng(98)|weapon_length(101)|swing_damage(32,cut)|thrust_damage(28,pierce),imodbits_sword],
 
["dunedain_bastard","Arnorian Bastard Sword",[("dunedain_bastard",0),("scab_dunedain_bastard",ixmesh_carry)],itp_type_two_handed_wpn|itp_primary,itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1175,weight(2)|difficulty(0)|spd_rtng(96)|weapon_length(122)|swing_damage(35,cut)|thrust_damage(32,pierce),imodbits_sword],
 
# Dale #
 
["dale_sword_a","Dale Clansman's Sword",[("dale_sword_a",0),("scab_dale_sword_a",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip,900,weight(1.5)|difficulty(0)|spd_rtng(101)|weapon_length(95)|swing_damage(30,cut)|thrust_damage(26,pierce),imodbits_sword],
["dale_sword_b","Dale Arming Sword",[("dale_sword_b",0),("scab_dale_sword_b",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip,850,weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(87)|swing_damage(30,cut)|thrust_damage(28,pierce),imodbits_sword],
["dale_sword_c","Dale Shortsword",[("dale_sword_c",0),("scab_dale_sword_c",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip,750,weight(1.25)|difficulty(0)|spd_rtng(105)|weapon_length(76)|swing_damage(29,cut)|thrust_damage(28,pierce),imodbits_sword],
 
["dale_spear","Dale Spear",[("dale_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1275,weight(2.25)|difficulty(0)|spd_rtng(93)|weapon_length(170)|swing_damage(24,blunt)|thrust_damage(27,pierce),imodbits_polearm],
["dale_billhook","Dale Billhook",[("dale_billhook",0)],itp_type_polearm|itp_offset_lance|itp_bonus_against_shield|itp_primary|itp_two_handed|itp_wooden_parry,itc_staff|itcf_carry_spear,1425,weight(2.5)|difficulty(0)|spd_rtng(90)|weapon_length(180)|swing_damage(31,pierce)|thrust_damage(27,pierce),imodbits_polearm],
 
# Dunland #
 
["dunland_sword","Dunnish Sword",[("dunland_sword",0)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip,850,weight(2)|difficulty(0)|spd_rtng(94)|weapon_length(98)|swing_damage(32,cut)|thrust_damage(23,pierce),imodbits_sword],
["dunland_antleraxe","Dunnish Antler Axe",[("dunland_antleraxe",0)],itp_type_one_handed_wpn|itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,575,weight(1.5)|difficulty(0)|spd_rtng(98)|weapon_length(66)|swing_damage(24,pierce)|thrust_damage(0,pierce),imodbits_axe],
["dunland_axe_a","Dunnish Wildman's Axe",[("dunland_axe_a",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,550,weight(1.5)|difficulty(0)|spd_rtng(102)|weapon_length(59)|swing_damage(27,cut)|thrust_damage(0,pierce),imodbits_axe],
["dunland_axe_b","Dunnish Shortaxe",[("dunland_axe_b",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,600,weight(1.5)|difficulty(0)|spd_rtng(102)|weapon_length(57)|swing_damage(29,cut)|thrust_damage(0,pierce),imodbits_axe],
["dunland_axe_c","Dunnish Pointed Battleaxe",[("dunland_axe_c",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,825,weight(1.5)|difficulty(0)|spd_rtng(104)|weapon_length(58)|swing_damage(32,pierce)|thrust_damage(0,pierce),imodbits_axe],
 
["dunland_spear","Dunnish Crude Spear",[("dunland_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,975,weight(2)|difficulty(0)|spd_rtng(93)|weapon_length(150)|swing_damage(20,blunt)|thrust_damage(24,pierce),imodbits_polearm],
["dunland_pike","Dunnish Crude Pike",[("dunland_pike",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry|itp_two_handed,itc_musket_melee_bayonet|itcf_carry_spear,1200,weight(2)|difficulty(0)|spd_rtng(90)|weapon_length(204)|swing_damage(19,blunt)|thrust_damage(22,pierce),imodbits_polearm],

["banner_dunland","Dunnish Banner", [("banner_dunland",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry,itcf_carry_spear|itc_slashing_polearm,1125,weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20,blunt),imodbits_polearm ],

# Dwarves #

["dwarf_sword_a","Dwarven Shortsword",[("dwarf_sword_a",0),("scab_dwarf_sword_a",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,600,weight(1.25)|difficulty(0)|spd_rtng(104)|weapon_length(61)|swing_damage(32,cut)|thrust_damage(23,pierce),imodbits_sword],
["dwarf_sword_b","Dwarven Shortsword",[("dwarf_sword_b",0),("scab_dwarf_sword_b",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,650,weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(66)|swing_damage(31,cut)|thrust_damage(28,pierce),imodbits_sword],
["dwarf_sword_c","Dwarven Shortsword",[("dwarf_sword_c",0),("scab_dwarf_sword_c",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,650,weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(66)|swing_damage(33,cut)|thrust_damage(24,pierce),imodbits_sword],
["dwarf_sword_d","Dwarven Shortsword",[("dwarf_sword_d",0),("scab_dwarf_sword_d",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,550,weight(1)|difficulty(0)|spd_rtng(107)|weapon_length(57)|swing_damage(30,cut)|thrust_damage(24,pierce),imodbits_sword],
 
["dwarf_mattock","Dwarven Mattock",[("dwarf_mattock",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,675,weight(1.5)|difficulty(0)|spd_rtng(104)|weapon_length(51)|swing_damage(30,pierce)|thrust_damage(0,pierce),imodbits_axe],
["dwarf_1h_axe","Dwarven Waraxe",[("dwarf_1h_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,600,weight(1.5)|difficulty(0)|spd_rtng(101)|weapon_length(55)|swing_damage(34,cut)|thrust_damage(0,pierce),imodbits_axe],
["dwarf_adz","Dwarven Warpick",[("dwarf_adz",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,1050,weight(1.75)|difficulty(0)|spd_rtng(95)|weapon_length(76)|swing_damage(34,pierce)|thrust_damage(0,pierce),imodbits_axe],
["dwarf_war_mattock","Dwarven Long Mattock",[("dwarf_war_mattock",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,975,weight(1.75)|difficulty(0)|spd_rtng(93)|weapon_length(98)|swing_damage(31,cut)|thrust_damage(0,pierce),imodbits_axe],
["dwarf_battle_axe","Dwarven Rounded Axe",[("dwarf_battle_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,800,weight(1.5)|difficulty(0)|spd_rtng(98)|weapon_length(72)|swing_damage(33,cut)|thrust_damage(0,pierce),imodbits_axe],
["dwarf_war_pick","Dwarven Steel Warpick",[("dwarf_war_pick",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield,itc_longsword|itcf_carry_axe_left_hip,1375,weight(2)|difficulty(0)|spd_rtng(94)|weapon_length(95)|swing_damage(36,pierce)|thrust_damage(31,pierce),imodbits_axe],
 
["dwarf_great_axe","Dwarven Great Axe",[("dwarf_great_axe",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced,itc_nodachi|itcf_carry_axe_back,1300,weight(4.5)|difficulty(0)|spd_rtng(93)|weapon_length(102)|swing_damage(50,cut)|thrust_damage(0,pierce),imodbits_axe],
["dwarf_great_mattock","Dwarven Great Mattock",[("dwarf_great_mattock",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced,itc_nodachi|itcf_carry_axe_back,1050,weight(4)|difficulty(0)|spd_rtng(94)|weapon_length(96)|swing_damage(43,cut)|thrust_damage(0,pierce),imodbits_axe],
["dwarf_great_pick","Dwarven Two Handed Pickaxe",[("dwarf_great_pick",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_crush_through|itp_wooden_parry|itp_unbalanced,itc_nodachi|itcf_carry_axe_back,1650,weight(5)|difficulty(0)|spd_rtng(92)|weapon_length(94)|swing_damage(45,pierce)|thrust_damage(0,pierce),imodbits_axe],
 
["dwarf_spear","Dwarven Short Spear",[("dwarf_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1075,weight(2)|difficulty(0)|spd_rtng(95)|weapon_length(140)|swing_damage(24,blunt)|thrust_damage(27,pierce),imodbits_polearm],
["dwarf_spear_b","Dwarven Spear",[("dwarf_spear_b",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1275,weight(2.25)|difficulty(0)|spd_rtng(94)|weapon_length(150)|swing_damage(25,blunt)|thrust_damage(31,pierce),imodbits_polearm],
["dwarf_spear_c","Dwarven Pike",[("dwarf_spear_c",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry|itp_two_handed,itc_musket_melee_bayonet|itcf_carry_spear,1350,weight(2.75)|difficulty(0)|spd_rtng(93)|weapon_length(166)|swing_damage(25,blunt)|thrust_damage(30,pierce),imodbits_polearm],

# Gondor #

["good_mace","Gondorian Mace",[("good_mace",0)],itp_type_one_handed_wpn|itp_can_knock_down|itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,700,weight(2)|difficulty(0)|spd_rtng(97)|weapon_length(55)|swing_damage(24,blunt)|thrust_damage(0,pierce),imodbits_mace],
 
["gondor_inf_sword","Gondorian Arming Sword",[("gondor_inf_sword",0),("scab_gondor_inf_sword",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900,weight(1.5)|difficulty(0)|spd_rtng(100)|weapon_length(95)|swing_damage(30,cut)|thrust_damage(27,pierce),imodbits_sword],
["gondor_longsword","Gondorian Longsword",[("gondor_longsword",0),("scab_gondor_longsword",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1000,weight(1.75)|difficulty(0)|spd_rtng(97)|weapon_length(100)|swing_damage(33,cut)|thrust_damage(30,pierce),imodbits_sword],
["gondor_ranger","Gondorian Ranger Sword",[("gondor_bastard",0),("scab_gondor_ranger",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1050,weight(1.75)|difficulty(0)|spd_rtng(96)|weapon_length(104)|swing_damage(33,cut)|thrust_damage(31,pierce),imodbits_sword],
["gondor_citadel","Gondorian Elite Sword",[("gondor_citadel",0),("scab_gondor_citadel",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1100,weight(1.5)|difficulty(0)|spd_rtng(103)|weapon_length(96)|swing_damage(35,cut)|thrust_damage(32,pierce),imodbits_sword],
 
["gondor_bastard","Gondorian Bastard Sword",[("gondor_bastard",0),("scab_gondor_bastard",ixmesh_carry)],itp_type_two_handed_wpn|itp_primary,itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1100,weight(2)|difficulty(0)|spd_rtng(96)|weapon_length(104)|swing_damage(35,cut)|thrust_damage(33,pierce),imodbits_sword],
 
["da_sword_a","Dol Amroth Squire Sword",[("DA_sword_a",0),("scab_DA_sword_a",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,925,weight(1.5)|difficulty(0)|spd_rtng(104)|weapon_length(87)|swing_damage(32,cut)|thrust_damage(30,pierce),imodbits_sword],
["da_sword_b","Dol Amroth Knight Sword",[("DA_sword_b",0),("scab_DA_sword_b",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1100,weight(1.75)|difficulty(0)|spd_rtng(100)|weapon_length(102)|swing_damage(34,cut)|thrust_damage(32,pierce),imodbits_sword],
 
["da_bastard","Dol Amroth Bastard Sword",[("DA_bastard",0),("scab_DA_bastard",ixmesh_carry)],itp_type_two_handed_wpn|itp_primary,itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1225,weight(2)|difficulty(0)|spd_rtng(99)|weapon_length(109)|swing_damage(37,cut)|thrust_damage(33,pierce),imodbits_sword],
 
["linhir_eket","Linhir Shortsword",[("linhir_eket",0),("scab_linhir_eket",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,600,weight(1)|difficulty(0)|spd_rtng(110)|weapon_length(52)|swing_damage(35,cut)|thrust_damage(28,pierce),imodbits_sword],
["linhir_glend","Linhir Longsword",[("linhir_glend",0),("scab_linhir_glend",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1175,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(108)|swing_damage(35,cut)|thrust_damage(31,pierce),imodbits_sword],

["linhir_glend_2h","Linhir Sword of Glory",[("linhir_glend_2h",0),("scab_linhir_glend_2h",ixmesh_carry)],itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary,itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,1375,weight(3)|difficulty(0)|spd_rtng(97)|weapon_length(122)|swing_damage(44,cut)|thrust_damage(33,pierce),imodbits_sword_high],
 
["pelargir_eket","Pelargir Shortsword",[("pelargir_eket",0),("scab_pelargir_eket",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,550,weight(1)|difficulty(0)|spd_rtng(110)|weapon_length(48)|swing_damage(35,cut)|thrust_damage(28,pierce),imodbits_sword],
["pelargir_sword","Pelargir Marine Sword",[("pelargir_sword",0),("scab_pelargir_sword",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,850,weight(1.25)|difficulty(0)|spd_rtng(105)|weapon_length(72)|swing_damage(31,cut)|thrust_damage(34,pierce),imodbits_sword],
 
["gondor_spear","Gondorian Short Spear",[("gondor_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1275,weight(2.5)|difficulty(0)|spd_rtng(94)|weapon_length(154)|swing_damage(26,blunt)|thrust_damage(29,pierce),imodbits_polearm],
["gondor_spear_b","Gondorian Warspear",[("gondor_spear_b",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1500,weight(2.5)|difficulty(0)|spd_rtng(94)|weapon_length(171)|swing_damage(26,blunt)|thrust_damage(32,pierce),imodbits_polearm],
["gondor_tower_spear","Gondorian Fountain Guard Spear",[("gondor_tower_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_wooden_parry,itc_staff_overhead_stab|itcf_carry_spear,1750,weight(3)|difficulty(0)|spd_rtng(92)|weapon_length(183)|swing_damage(30,blunt)|thrust_damage(35,pierce),imodbits_polearm],
 
["banner_lance_gondor","Gondorian Lance with Banner",[("banner_lance_gondor",0)],itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_cutting_spear,1250,weight(3)|difficulty(0)|spd_rtng(75)|weapon_length(204)|swing_damage(20,blunt)|thrust_damage(28,pierce),imodbits_polearm],
["amroth_lance","Swan Knight Lance",[("amroth_lance",0)],itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_cutting_spear,1325,weight(3)|difficulty(0)|spd_rtng(75)|weapon_length(204)|swing_damage(23,blunt)|thrust_damage(30,pierce),imodbits_polearm],
 
["loss_axe","Lossarnach Waraxe",[("loss_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,550,weight(1.5)|difficulty(0)|spd_rtng(101)|weapon_length(46)|swing_damage(34,cut)|thrust_damage(0,pierce),imodbits_axe],
 
["loss_axe_2h","Lossarnach Great Axe",[("loss_axe_2h",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1400,weight(4.5)|difficulty(0)|spd_rtng(95)|weapon_length(99)|swing_damage(48,cut)|thrust_damage(0,pierce),imodbits_axe],

["ecthelion","Ecthelion",[("westernesse_sword_a",0),("scab_westernesse_sword_a",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,975,weight(1.5)|difficulty(0)|spd_rtng(104)|weapon_length(78)|swing_damage(37,cut)|thrust_damage(36,pierce),imodbits_sword],
["guthwine","Guthwine",[("westernesse_sword_b",0),("scab_westernesse_sword_b",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1225,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(106)|swing_damage(38,cut)|thrust_damage(33,pierce),imodbits_sword],

# Rohan #
 
["rohan_sword_a","Rohirric Straight Sword",[("rohan_sword_a",0),("scab_rohan_sword_a",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,975,weight(1.75)|difficulty(0)|spd_rtng(96)|weapon_length(115)|swing_damage(29,cut)|thrust_damage(25,pierce),imodbits_sword],
["rohan_sword_b","Rohirric Slender Sword",[("rohan_sword_b",0),("scab_rohan_sword_b",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1000,weight(1.75)|difficulty(0)|spd_rtng(96)|weapon_length(115)|swing_damage(28,cut)|thrust_damage(28,pierce),imodbits_sword],
["rohan_sword_c","Rohirric Broadsword",[("rohan_sword_c",0),("scab_rohan_sword_c",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1000,weight(1.75)|difficulty(0)|spd_rtng(95)|weapon_length(115)|swing_damage(31,cut)|thrust_damage(23,pierce),imodbits_sword],
["rohan_cavalry_sword","Rohirric Cavalry Sword",[("rohan_cavalry_sword",0)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip,1025,weight(1.25)|difficulty(0)|spd_rtng(98)|weapon_length(94)|swing_damage(34,cut)|thrust_damage(25,pierce),imodbits_sword],
["rohan_cavalry_sword_long","Rohirric Cavalry Longsword",[("rohan_cavalry_sword_long",0)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip,1125,weight(1.25)|difficulty(0)|spd_rtng(95)|weapon_length(110)|swing_damage(35,cut)|thrust_damage(22,pierce),imodbits_sword],
 
["rohan_sword_a_2h","Rohirric Sword of War",[("rohan_sword_a_2h",0),("scab_rohan_sword_a_2h",ixmesh_carry)],itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary,itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,1375,weight(3)|difficulty(0)|spd_rtng(95)|weapon_length(131)|swing_damage(42,cut)|thrust_damage(30,pierce),imodbits_sword_high],
 
["rohan_axe","Rohirric Waraxe",[("rohan_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,625,weight(1.5)|difficulty(0)|spd_rtng(100)|weapon_length(53)|swing_damage(34,cut)|thrust_damage(0,pierce),imodbits_axe],
["rohan_long_axe","Rohirric Battleaxe",[("rohan_long_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,750,weight(1.75)|difficulty(0)|spd_rtng(98)|weapon_length(61)|swing_damage(36,cut)|thrust_damage(0,pierce),imodbits_axe],
 
["rohan_axe_2h","Rohirric Great Axe",[("rohan_axe_2h",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1150,weight(4.5)|difficulty(0)|spd_rtng(92)|weapon_length(87)|swing_damage(46,cut)|thrust_damage(0,pierce),imodbits_axe],
["rohan_axe_reward","Axe of the Royal Guard",[("rohan_axe_reward",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1350,weight(5)|difficulty(0)|spd_rtng(91)|weapon_length(93)|swing_damage(51,cut)|thrust_damage(0,pierce),imodbits_axe],
 
["rohan_infantry_spear","Rohirric Warspear",[("rohan_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1450,weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(172)|swing_damage(26,blunt)|thrust_damage(30,pierce),imodbits_polearm],
["rohan_cavalry_spear","Rohirric Cavalry Spear",[("rohan_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1425,weight(2.5)|difficulty(0)|spd_rtng(93)|weapon_length(172)|swing_damage(23,blunt)|thrust_damage(32,pierce),imodbits_polearm],
 
["rohan_lance","Rohirric Lance",[("rohan_lance",0)],itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_cutting_spear,1175,weight(3)|difficulty(0)|spd_rtng(70)|weapon_length(214)|swing_damage(20,blunt)|thrust_damage(28,pierce),imodbits_polearm],
["banner_lance_rohan_a","Rohirric Lance with Banner",[("banner_lance_rohan_a",0)],itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_cutting_spear,1275,weight(3)|difficulty(0)|spd_rtng(70)|weapon_length(218)|swing_damage(21,blunt)|thrust_damage(30,pierce),imodbits_polearm],
["banner_lance_rohan_b","Rohirric Lance with Banner",[("banner_lance_rohan_b",0)],itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_cutting_spear,1275,weight(3)|difficulty(0)|spd_rtng(70)|weapon_length(218)|swing_damage(21,blunt)|thrust_damage(30,pierce),imodbits_polearm],

# Lorien #

["lorien_sword_short","Lorien Slender Blade",[("lorien_sword_short",0),("scab_lorien_sword_short",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900,weight(1)|difficulty(0)|spd_rtng(105)|weapon_length(65)|swing_damage(33,cut)|thrust_damage(29,pierce),imodbits_sword],
["lorien_sword_long","Lorien Elven Blade",[("lorien_sword_long",0),("scab_lorien_sword_long",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1175,weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(86)|swing_damage(35,cut)|thrust_damage(31,pierce),imodbits_sword],
["lorien_sword_hand_and_half","Lorien Longbladed Sword",[("lorien_sword_hand_and_half",0),("scab_lorien_sword_hand_and_half",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1275,weight(1.25)|difficulty(0)|spd_rtng(100)|weapon_length(92)|swing_damage(38,cut)|thrust_damage(32,pierce),imodbits_sword],
 
["banner_lorien_b","Lorien Pike with Banner",[("banner_lorien_b",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry|itp_two_handed,itc_musket_melee_bayonet|itcf_carry_spear,1875,weight(3.75)|difficulty(0)|spd_rtng(88)|weapon_length(285)|swing_damage(25,blunt)|thrust_damage(30,pierce),imodbits_polearm],

# Mirkwood #

["mirkwood_white_knife","Mirkwood Knife",[("mirkwood_white_knife",0),("scab_mirkwood_white_knife",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,550,weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(51)|swing_damage(31,cut)|thrust_damage(28,pierce),imodbits_sword],
["mirkwood_sword","Mirkwood Decorated Sword",[("mirkwood_sword",0),("scab_mirkwood_sword",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,975,weight(1)|difficulty(0)|spd_rtng(100)|weapon_length(99)|swing_damage(32,cut)|thrust_damage(28,pierce),imodbits_sword],
["mirkwood_longsword","Mirkwood Woodland Sword",[("mirkwood_longsword",0),("scab_mirkwood_longsword",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1000,weight(1)|difficulty(0)|spd_rtng(103)|weapon_length(92)|swing_damage(34,cut)|thrust_damage(30,pierce),imodbits_sword],
 
["mirkwood_short_spear","Mirkwood Short Spear",[("mirkwood_short_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1050,weight(1.5)|difficulty(0)|spd_rtng(105)|weapon_length(116)|swing_damage(26,blunt)|thrust_damage(28,pierce),imodbits_polearm],
["mirkwood_war_spear","Mirkwood Warspear",[("mirkwood_war_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1375,weight(1.75)|difficulty(0)|spd_rtng(97)|weapon_length(149)|swing_damage(27,blunt)|thrust_damage(32,pierce),imodbits_polearm],
["mirkwood_great_spear_large","Mirkwood Elite Spear",[("mirkwood_great_spear_large",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1525,weight(1.75)|difficulty(0)|spd_rtng(98)|weapon_length(149)|swing_damage(30,blunt)|thrust_damage(35,pierce),imodbits_polearm],
["banner_mirkwood_b","Mirkwood Pike with Banner",[("banner_mirkwood_b",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry|itp_two_handed,itc_musket_melee_bayonet|itcf_carry_spear,1875,weight(3.75)|difficulty(0)|spd_rtng(88)|weapon_length(285)|swing_damage(25,blunt)|thrust_damage(30,pierce),imodbits_polearm],

# Rivendell #

["rivendell_shortsword","Rivendell Recruit Sword",[("rivendell_shortsword",0),("scab_rivendell_shortsword",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,850,weight(1)|difficulty(0)|spd_rtng(103)|weapon_length(79)|swing_damage(29,cut)|thrust_damage(30,pierce),imodbits_sword],
["rivendell_sword","Rivendell Rider Sword",[("rivendell_sword",0),("scab_rivendell_sword",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1075,weight(1)|difficulty(0)|spd_rtng(99)|weapon_length(95)|swing_damage(31,cut)|thrust_damage(33,pierce),imodbits_sword],
["rivendell_longsword_a","Rivendell Longsword",[("rivendell_longsword_a",0),("scab_rivendell_longsword_a",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1075,weight(1.25)|difficulty(0)|spd_rtng(97)|weapon_length(104)|swing_damage(34,cut)|thrust_damage(30,pierce),imodbits_sword],
["rivendell_longsword_b","Rivendell Long Rider Sword",[("rivendell_longsword_b",0),("scab_rivendell_longsword_b",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1225,weight(1.25)|difficulty(0)|spd_rtng(96)|weapon_length(106)|swing_damage(32,cut)|thrust_damage(35,pierce),imodbits_sword],
 
["rivendell_2h_sword_a","Rivendell Greatsword",[("rivendell_2h_sword_a",0),("scab_rivendell_2h_sword_a",ixmesh_carry)],itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary,itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,1350,weight(2)|difficulty(0)|spd_rtng(96)|weapon_length(122)|swing_damage(44,cut)|thrust_damage(31,pierce),imodbits_sword_high],
["rivendell_2h_sword_b","Rivendell Greatsword",[("rivendell_2h_sword_b",0),("scab_rivendell_2h_sword_b",ixmesh_carry)],itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary,itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,1375,weight(2)|difficulty(0)|spd_rtng(94)|weapon_length(126)|swing_damage(42,cut)|thrust_damage(35,pierce),imodbits_sword_high],
 
["banner_rivendell_b","Rivendell Pike with Banner",[("banner_rivendell_b",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_cant_use_on_horseback|itp_wooden_parry|itp_two_handed,itc_musket_melee_bayonet|itcf_carry_spear,1875,weight(3.75)|difficulty(0)|spd_rtng(88)|weapon_length(285)|swing_damage(25,blunt)|thrust_damage(30,pierce),imodbits_polearm],

# Khand #

["khand_weapon_sword_pit","Variag Pit Sword",[("Khand_Weapon_Sword_Pit",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,525,weight(1.25)|difficulty(0)|spd_rtng(100)|weapon_length(66)|swing_damage(26,cut)|thrust_damage(0,pierce),imodbits_sword],
["khand_weapon_tulwar","Variag Tulwar",[("Khand_Weapon_Tulwar",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,775,weight(1.25)|difficulty(0)|spd_rtng(98)|weapon_length(93)|swing_damage(28,cut)|thrust_damage(0,pierce),imodbits_sword],
["khand_weapon_mace1","Variag Knobbed Mace",[("Khand_Weapon_Mace1",0)],itp_type_one_handed_wpn|itp_can_knock_down|itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,1075,weight(2)|difficulty(0)|spd_rtng(96)|weapon_length(77)|swing_damage(26,blunt)|thrust_damage(0,pierce),imodbits_mace],
["khand_weapon_mace2","Variag Heavy Mace",[("Khand_Weapon_Mace2",0)],itp_type_one_handed_wpn|itp_can_knock_down|itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,1125,weight(2.5)|difficulty(0)|spd_rtng(94)|weapon_length(77)|swing_damage(29,blunt)|thrust_damage(0,pierce),imodbits_mace],
["khand_weapon_axe_winged","Variag Winged Axe",[("Khand_Weapon_Axe_Winged",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,1050,weight(1.75)|difficulty(0)|spd_rtng(95)|weapon_length(95)|swing_damage(34,cut)|thrust_damage(0,pierce),imodbits_axe],
["khand_weapon_mace_spiked","Variag Spiked Mace",[("Khand_Weapon_Mace_Spiked",0)],itp_type_one_handed_wpn|itp_can_knock_down|itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,1200,weight(2.5)|difficulty(0)|spd_rtng(94)|weapon_length(81)|swing_damage(32,pierce)|thrust_damage(0,pierce),imodbits_mace],
["khand_weapon_tulwar_long","Variag Long Tulwar",[("Khand_Weapon_Tulwar_Long",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,1250,weight(1.75)|difficulty(0)|spd_rtng(97)|weapon_length(116)|swing_damage(33,cut)|thrust_damage(0,pierce),imodbits_sword],
 
["khand_weapon_axe_great","Variag Great Winged Axe",[("Khand_Weapon_Axe_Great",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_unbalanced|itp_bonus_against_shield|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1150,weight(4.5)|difficulty(0)|spd_rtng(91)|weapon_length(108)|swing_damage(43,cut)|thrust_damage(0,pierce),imodbits_axe],
["khand_weapon_mace_ram","Variag Ram Headed Mace",[("Khand_Weapon_Mace_Ram",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_unbalanced|itp_crush_through|itp_can_knock_down|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,2150,weight(7.5)|difficulty(0)|spd_rtng(88)|weapon_length(102)|swing_damage(45,blunt)|thrust_damage(0,pierce),imodbits_axe],
 
["khand_weapon_trident","Variag Trident",[("Khand_Weapon_Trident",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1500,weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(164)|swing_damage(28,blunt)|thrust_damage(32,pierce),imodbits_polearm],
["khand_weapon_voulge","Variag Bardiche",[("Khand_Weapon_Voulge",0)],itp_type_polearm|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced,itc_staff,1275,weight(4.0)|difficulty(0)|spd_rtng(90)|weapon_length(126)|swing_damage(48,cut)|thrust_damage(15,pierce),imodbits_axe],
["khand_weapon_halberd","Variag Halberd",[("Khand_Weapon_Halberd",0)],itp_type_polearm|itp_offset_lance|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_staff,1700,weight(4.0)|difficulty(0)|spd_rtng(92)|weapon_length(163)|swing_damage(44,cut)|thrust_damage(30,pierce),imodbits_axe],
 
["khand_lance","Variag Lance",[("khand_lance",0)],itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_cutting_spear,1200,weight(3)|difficulty(0)|spd_rtng(75)|weapon_length(218)|swing_damage(18,blunt)|thrust_damage(27,pierce),imodbits_polearm],

["banner_khand","Variag Banner", [("banner_khand",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry,itcf_carry_spear|itc_slashing_polearm,1125,weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20,blunt),imodbits_polearm ],

# Harad #

["harad_dagger","Haradrim Dagger",[("harad_dagger",0)],itp_type_one_handed_wpn|itp_primary|itp_no_parry,itc_dagger|itcf_carry_dagger_front_left,250,weight(0.5)|difficulty(0)|spd_rtng(114)|weapon_length(47)|swing_damage(24,cut)|thrust_damage(29,pierce),imodbits_sword_high],
["skirmisher_sword","Haradrim Skirmisher Sword",[("skirmisher_sword",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,500,weight(1)|difficulty(0)|spd_rtng(104)|weapon_length(59)|swing_damage(27,cut)|thrust_damage(0,pierce),imodbits_sword],
["harad_sabre","Haradrim Heavy Sabre",[("harad_sabre",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,575,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(69)|swing_damage(28,cut)|thrust_damage(0,pierce),imodbits_sword],
["harad_mace","Haradrim Club",[("harad_mace",0)],itp_type_one_handed_wpn|itp_can_knock_down|itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,650,weight(2)|difficulty(0)|spd_rtng(95)|weapon_length(50)|swing_damage(25,blunt)|thrust_damage(0,pierce),imodbits_mace],
["harad_heavy_sword","Haradrim Scimitar",[("harad_heavy_sword",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,675,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(76)|swing_damage(30,cut)|thrust_damage(0,pierce),imodbits_sword],
["harad_khopesh","Haradrim Khopesh",[("harad_khopesh",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,700,weight(2.25)|difficulty(0)|spd_rtng(96)|weapon_length(76)|swing_damage(32,cut)|thrust_damage(0,pierce),imodbits_sword],
["horandor_a","Haradrim Pulwar",[("horandor_a",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,825,weight(1.5)|difficulty(0)|spd_rtng(101)|weapon_length(85)|swing_damage(32,cut)|thrust_damage(0,pierce),imodbits_sword],
["black_snake_sword","Serpent Guard Sabre",[("black_snake_sword",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,750,weight(1.5)|difficulty(0)|spd_rtng(105)|weapon_length(68)|swing_damage(35,cut)|thrust_damage(0,pierce),imodbits_sword],
 
["far_harad_2h_mace","Haradrim Club",[("far_harad_2h_mace",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_unbalanced|itp_can_knock_down|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1075,weight(6)|difficulty(0)|spd_rtng(90)|weapon_length(74)|swing_damage(38,blunt)|thrust_damage(0,pierce),imodbits_axe],
 
["harad_short_spear","Haradrim Hunting Spear",[("harad_short_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1025,weight(2)|difficulty(0)|spd_rtng(97)|weapon_length(133)|swing_damage(24,blunt)|thrust_damage(26,pierce),imodbits_polearm],
["harad_long_spear","Haradrim Reinforced Spear",[("harad_long_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1550,weight(2.5)|difficulty(0)|spd_rtng(93)|weapon_length(190)|swing_damage(26,blunt)|thrust_damage(29,pierce),imodbits_polearm],
["eagle_guard_spear","Eagle Guard Halberd",[("eagle_guard_spear",0)],itp_type_polearm|itp_offset_lance|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_staff,1700,weight(4.0)|difficulty(0)|spd_rtng(91)|weapon_length(175)|swing_damage(40,cut)|thrust_damage(37,pierce),imodbits_axe],

["banner_harad","Haradrim Banner", [("banner_harad",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry,itcf_carry_spear|itc_slashing_polearm,1125,weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20,blunt),imodbits_polearm ],

# Umbar #

["corsair_cutlass","Raider's Cutlass",[("corsair_cutlass",0),("scab_corsair_cutlass",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,750,weight(1.25)|difficulty(0)|spd_rtng(100)|weapon_length(86)|swing_damage(29,cut)|thrust_damage(23,pierce),imodbits_sword],
["corsair_sword_a","Raider's Sword",[("corsair_sword_a",0),("scab_corsair_sword_a",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,775,weight(1.5)|difficulty(0)|spd_rtng(98)|weapon_length(81)|swing_damage(31,cut)|thrust_damage(28,pierce),imodbits_sword],
["corsair_sword","Raider's Straightsword",[("corsair_sword",0),("scab_corsair_sword",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,875,weight(1.5)|difficulty(0)|spd_rtng(97)|weapon_length(90)|swing_damage(32,cut)|thrust_damage(30,pierce),imodbits_sword],
["corsair_kraken_cutlass","Cutlass of Umbar",[("corsair_kraken_cutlass",0),("scab_corsair_kraken_cutlass",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1050,weight(1.5)|difficulty(0)|spd_rtng(98)|weapon_length(102)|swing_damage(35,cut)|thrust_damage(0,pierce),imodbits_sword],
 
["corsair_trident","Raider's Trident",[("corsair_trident",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1300,weight(2)|difficulty(0)|spd_rtng(96)|weapon_length(152)|swing_damage(30,cut)|thrust_damage(33,pierce),imodbits_polearm],
["corsair_pike","Raider's Pike",[("corsair_pike",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_wooden_parry|itp_cant_use_on_horseback|itp_two_handed,itc_musket_melee_bayonet|itcf_carry_spear,1375,weight(3)|difficulty(0)|spd_rtng(91)|weapon_length(192)|swing_damage(27,blunt)|thrust_damage(31,pierce),imodbits_polearm],

# Rhun #

["rhun_shortsword","Rhun Short Falchion",[("rhun_shortsword",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,575,weight(1.25)|difficulty(0)|spd_rtng(102)|weapon_length(69)|swing_damage(27,cut)|thrust_damage(0,pierce),imodbits_sword],
["rhun_infantry_scimitar","Rhun Shotel",[("rhun_infantry_scimitar",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,675,weight(1)|difficulty(0)|spd_rtng(104)|weapon_length(70)|swing_damage(25,pierce)|thrust_damage(0,pierce),imodbits_sword],
["rhun_falchion","Rhun Falchion",[("rhun_falchion",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,675,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(77)|swing_damage(29,cut)|thrust_damage(0,pierce),imodbits_sword],
["rhun_sword","Rhun Long Falchion",[("rhun_sword",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,750,weight(1.5)|difficulty(0)|spd_rtng(97)|weapon_length(89)|swing_damage(29,cut)|thrust_damage(0,pierce),imodbits_sword],
["rhun_crossbow_sword","Rhun Crossbowman Scimitar",[("rhun_crossbow_sword",0),("rhun_crossbow_sword_scab",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,675,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(76)|swing_damage(30,cut)|thrust_damage(0,pierce),imodbits_sword],
["rhun_eastclan_sword","Rhun Cleaver",[("rhun_eastclan_sword",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,725,weight(1.5)|difficulty(0)|spd_rtng(98)|weapon_length(78)|swing_damage(32,cut)|thrust_damage(0,pierce),imodbits_sword],
["rhun_infantry_sword","Rhun Scimitar",[("rhun_infantry_sword",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,775,weight(1.5)|difficulty(0)|spd_rtng(100)|weapon_length(85)|swing_damage(30,cut)|thrust_damage(0,pierce),imodbits_sword],
 
["rhun_infantry_axe","Rhun Fighting Axe",[("rhun_infantry_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,675,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(70)|swing_damage(28,cut)|thrust_damage(0,pierce),imodbits_axe],
["rhun_elite_axe","Rhun Elite Battleaxe",[("rhun_elite_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,1050,weight(2)|difficulty(0)|spd_rtng(95)|weapon_length(87)|swing_damage(37,cut)|thrust_damage(0,pierce),imodbits_axe],
 
["rhun_elite_scimitar","Rhun Elite Scimitar",[("rhun_elite_scimitar",0),("rhun_elite_scimitar_scab",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1050,weight(1.75)|difficulty(0)|spd_rtng(98)|weapon_length(107)|swing_damage(33,cut)|thrust_damage(0,pierce),imodbits_sword],
["rhun_cataphract_scimitar","Rhun Cataphract's Scimitar",[("rhun_cataphract_scimitar",0)],itp_type_one_handed_wpn|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,1050,weight(2)|difficulty(0)|spd_rtng(97)|weapon_length(111)|swing_damage(35,cut)|thrust_damage(0,pierce),imodbits_sword],
 
["rhun_elite_mace","Rhun Pointed Mace",[("rhun_elite_mace",0)],itp_type_one_handed_wpn|itp_can_knock_down|itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,1350,weight(3.25)|difficulty(0)|spd_rtng(93)|weapon_length(87)|swing_damage(31,blunt)|thrust_damage(0,pierce),imodbits_mace],
 
["rhun_greatfalchion","Rhun Two Handed Falchion",[("rhun_greatfalchion",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary,itc_nodachi|itcf_carry_sword_back,1175,weight(3)|difficulty(0)|spd_rtng(95)|weapon_length(102)|swing_damage(45,cut)|thrust_damage(0,pierce),imodbits_axe],
["rhun_greatsword","Rhun Greatsword",[("rhun_greatsword",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary,itc_nodachi|itcf_carry_sword_back,1150,weight(2)|difficulty(0)|spd_rtng(97)|weapon_length(101)|swing_damage(43,cut)|thrust_damage(0,pierce),imodbits_axe],
["rhun_battle_axe","Rhun Battleaxe",[("rhun_battle_axe",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1075,weight(3.5)|difficulty(0)|spd_rtng(95)|weapon_length(98)|swing_damage(37,cut)|thrust_damage(0,pierce),imodbits_axe],
["rhun_greataxe","Rhun Great Axe",[("rhun_greataxe",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_unbalanced|itp_bonus_against_shield|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1400,weight(5)|difficulty(0)|spd_rtng(89)|weapon_length(115)|swing_damage(50,cut)|thrust_damage(0,pierce),imodbits_axe],
 
["rhun_cataphract_spear","Rhun Elite Pike",[("rhun_cataphract_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_cant_use_on_horseback|itp_wooden_parry,itc_staff_overhead_stab|itcf_carry_spear,2025,weight(4)|difficulty(0)|spd_rtng(91)|weapon_length(286)|swing_damage(31,cut)|thrust_damage(34,pierce),imodbits_polearm],
["rhun_cataphract_spear_a","Rhun Tuffed Elite Pike",[("rhun_cataphract_spear_a",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_cant_use_on_horseback|itp_wooden_parry,itc_staff_overhead_stab|itcf_carry_spear,2025,weight(4)|difficulty(0)|spd_rtng(91)|weapon_length(286)|swing_damage(31,cut)|thrust_damage(34,pierce),imodbits_polearm],
 
["rhun_cataphract_lance","Rhun Cataphract Lance",[("rhun_cataphract_lance",0)],itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_cutting_spear,1600,weight(3)|difficulty(0)|spd_rtng(70)|weapon_length(286)|swing_damage(20,blunt)|thrust_damage(29,pierce),imodbits_polearm],
["rhun_cataphract_lance_a","Rhun Tuffed Cataphract Lance",[("rhun_cataphract_lance_a",0)],itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_cutting_spear,1600,weight(3)|difficulty(0)|spd_rtng(70)|weapon_length(286)|swing_damage(20,blunt)|thrust_damage(29,pierce),imodbits_polearm],
 
["rhun_glaive","Rhun Glaive",[("rhun_glaive",0)],itp_type_polearm|itp_cant_use_on_horseback|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_staff|itcf_carry_spear,1025,weight(3)|difficulty(0)|spd_rtng(90)|weapon_length(156)|swing_damage(45,cut)|thrust_damage(15,pierce),imodbits_axe],
["rhun_clanguard_scythe","Rhun Doubleheaded Scythe",[("rhun_clanguard_scythe",0)],itp_type_polearm|itp_two_handed|itp_primary|itp_wooden_parry,itc_slashing_polearm|itcf_carry_spear,1600,weight(2.5)|difficulty(0)|spd_rtng(91)|weapon_length(178)|swing_damage(42,cut)|thrust_damage(0,pierce),imodbits_axe],
["rhun_elite_halberd","Rhun Halberd",[("rhun_elite_halberd",0)],itp_type_polearm|itp_cant_use_on_horseback|itp_two_handed|itp_primary|itp_wooden_parry,itc_staff|itcf_carry_spear,2150,weight(5)|difficulty(0)|spd_rtng(86)|weapon_length(234)|swing_damage(49,cut)|thrust_damage(30,pierce),imodbits_axe],

["banner_rhun","Rhun Banner", [("banner_rhun",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry,itcf_carry_spear|itc_slashing_polearm,1125,weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20,blunt),imodbits_polearm ],

# Mordor #
 
["sword_of_mordor","Sword of Mordor",[("sword_of_mordor",0),("scab_sword_of_mordor",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,950,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(94)|swing_damage(32,cut)|thrust_damage(30,pierce),imodbits_sword],
["mordor_sword","Black Numenorean Greatsword",[("mordor_sword",0)],itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary,itc_greatsword|itcf_carry_sword_back,1250,weight(3)|difficulty(0)|spd_rtng(97)|weapon_length(109)|swing_damage(43,cut)|thrust_damage(35,pierce),imodbits_sword_high],
["nazgul_sword","Nazgul Sword",[("nazgul_sword",0),("nazgul_sword_scab",ixmesh_carry)],itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary,itc_greatsword|itcf_carry_sword_back,1350,weight(3)|difficulty(0)|spd_rtng(95)|weapon_length(110)|swing_damage(48,cut)|thrust_damage(37,pierce),imodbits_sword_high],
["witch_king_sword","Sword of the Witch-king",[("witch_king_sword",0)],itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield,itc_greatsword|itcf_carry_sword_back,1725,weight(3.5)|difficulty(0)|spd_rtng(93)|weapon_length(114)|swing_damage(54,cut)|thrust_damage(38,pierce),imodbits_sword_high],

# Mordor Orcs #
["orc_slasher","Crude Butchering Knife", [("orc_slasher",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_right,225,weight(0.75)|difficulty(0)|spd_rtng(108)|weapon_length(52)|swing_damage(26,cut)|thrust_damage(19,pierce),imodbits_sword ],
["orc_machete","Crude Machete", [("orc_machete",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry,itc_cleaver|itcf_carry_dagger_front_right,225,weight(1.5)|difficulty(0)|spd_rtng(103)|weapon_length(53)|swing_damage(26,cut)|thrust_damage(0,pierce),imodbits_none ],
["orc_scimitar","Crude Scimitar", [("orc_scimitar",0),], itp_type_one_handed_wpn|itp_merchandise|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,500,weight(1)|difficulty(0)|spd_rtng(103)|weapon_length(56)|swing_damage(29,cut)|thrust_damage(0,pierce),imodbits_sword ],
["orc_axe","Crude Hand Axe", [("orc_axe",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,500,weight(1.75)|difficulty(7)|spd_rtng(98)|weapon_length(56)|swing_damage(26,cut)|thrust_damage(0,pierce),imodbits_axe ],
["orc_falchion","Crude Falchion", [("orc_falchion",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,525,weight(2.25)|difficulty(8)|spd_rtng(98)|weapon_length(65)|swing_damage(28,cut)|thrust_damage(0,pierce),imodbits_sword ],
["orgaxe3","Crude War Axe", [("orgaxe3",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,525,weight(1.5)|difficulty(7)|spd_rtng(99)|weapon_length(54)|swing_damage(29,cut)|thrust_damage(0,pierce),imodbits_axe ],
["orgaxe1","Crude War Axe", [("orgaxe1",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry,itc_scimitar|itcf_carry_axe_left_hip,575,weight(1.75)|difficulty(7)|spd_rtng(98)|weapon_length(62)|swing_damage(28,cut)|thrust_damage(0,pierce),imodbits_axe ],
["orc_sabre","Crude Sabre", [("orc_sabre",0),], itp_type_one_handed_wpn|itp_merchandise|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,625,weight(1.25)|difficulty(0)|spd_rtng(102)|weapon_length(73)|swing_damage(28,cut),imodbits_sword ],
["orgsword1","Crude Sword", [("orgsword1",0),], itp_type_one_handed_wpn|itp_merchandise|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,725,weight(1.25)|difficulty(0)|spd_rtng(102)|weapon_length(78)|swing_damage(30,cut),imodbits_sword ],
["orgsword6","Crude Sabre", [("orgsword6",0),], itp_type_one_handed_wpn|itp_merchandise|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,800,weight(1.25)|difficulty(0)|spd_rtng(101)|weapon_length(90)|swing_damage(29,cut),imodbits_sword ],
["orgsword5","Crude Sword", [("orgsword5",0),], itp_type_one_handed_wpn|itp_merchandise|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,900,weight(1.5)|difficulty(0)|spd_rtng(100)|weapon_length(98)|swing_damage(31,cut),imodbits_sword ],
["orgsword7","Crude Scimitar", [("orgsword7",0),], itp_type_one_handed_wpn|itp_merchandise|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,975,weight(1.5)|difficulty(0)|spd_rtng(98)|weapon_length(108)|swing_damage(31,cut),imodbits_sword ],

["orc_club_a","Spiked Orc Club", [("orc_club_a",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,525,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(62)|swing_damage(19,pierce)|thrust_damage(0,pierce),imodbits_mace ],
["orc_club_c","Spiked Orc Club", [("orc_club_c",0)], itp_type_one_handed_wpn|itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,525,weight(3)|difficulty(0)|spd_rtng(97)|weapon_length(70)|swing_damage(21,pierce)|thrust_damage(0,pierce),imodbits_mace ],
["orc_club_d","Spiked Orc Mace", [("orc_club_d",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,800,weight(2.75)|difficulty(0)|spd_rtng(98)|weapon_length(65)|swing_damage(23,blunt)|thrust_damage(0,pierce),imodbits_mace ],
["orc_club_b","Spiked Orc Club", [("orc_club_b",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,750,weight(2.75)|difficulty(0)|spd_rtng(98)|weapon_length(65)|swing_damage(26,pierce)|thrust_damage(0,pierce),imodbits_mace ],
["orgmace3","Orc Mace", [("orgmace3",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,850,weight(3)|difficulty(0)|spd_rtng(94)|weapon_length(65)|swing_damage(26,blunt)|thrust_damage(0,pierce),imodbits_mace ],
["orgmace1","Spiked Orc Club", [("orgmace1",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,900,weight(2.85)|difficulty(0)|spd_rtng(95)|weapon_length(78)|swing_damage(27,pierce)|thrust_damage(0,pierce),imodbits_mace ],
["orgmace2","Spiked Orc Mace", [("orgmace2",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry,itc_scimitar|itcf_carry_mace_left_hip,950,weight(3)|difficulty(0)|spd_rtng(94)|weapon_length(78)|swing_damage(24,blunt)|thrust_damage(0,pierce),imodbits_mace ],

["orc_sledgehammer","Crude Sledgehammer", [("orc_sledgehammer",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced,itc_nodachi|itcf_carry_spear,1075,weight(5.75)|difficulty(10)|spd_rtng(84)|weapon_length(74)|swing_damage(33,blunt)|thrust_damage(0,pierce),imodbits_mace ],
["orc_twohanded_axe","Crude Two Handed Axe", [("orc_twohanded_axe",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced,itc_nodachi|itcf_carry_axe_back,850,weight(5)|difficulty(10)|spd_rtng(94)|weapon_length(93)|swing_damage(36,cut)|thrust_damage(0,pierce),imodbits_axe ],
["orgaxetwohanded1","Crude Battle Axe", [("orgaxetwohanded1",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced,itc_nodachi|itcf_carry_axe_back,1125,weight(5)|difficulty(10)|spd_rtng(93)|weapon_length(100)|swing_damage(39,cut)|thrust_damage(0,pierce),imodbits_axe ],

["orgspear2","Orcish Boar Spear", [("Org-Spear2",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,800,weight(2.25)|difficulty(0)|spd_rtng(97)|weapon_length(164)|swing_damage(26,cut)|thrust_damage(23,pierce),imodbits_polearm ],
["orgspear1","Orcish Spear", [("orgspear1",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,825,weight(1.75)|difficulty(0)|spd_rtng(99)|weapon_length(120)|swing_damage(24,cut)|thrust_damage(26,pierce),imodbits_polearm ],
["orgspear1b","Orcish Spear", [("Org-Spear1",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,875,weight(1.5)|difficulty(0)|spd_rtng(100)|weapon_length(125)|swing_damage(20,cut)|thrust_damage(28,pierce),imodbits_polearm ],
["orc_simple_spear","Orcish Spear", [("orc_simple_spear",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,975,weight(2.25)|difficulty(0)|spd_rtng(97)|weapon_length(150)|swing_damage(18,blunt)|thrust_damage(23,pierce),imodbits_polearm ],
["orgspear3","Crude Glaive", [("Org-Spear3",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_staff|itcf_carry_spear,1025,weight(2)|difficulty(0)|spd_rtng(97)|weapon_length(130)|swing_damage(37,cut)|thrust_damage(23,pierce),imodbits_polearm ],
["orc_skull_spear","Orcish War Spear", [("orc_skull_spear",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1075,weight(2.75)|difficulty(0)|spd_rtng(94)|weapon_length(162)|swing_damage(19,blunt)|thrust_damage(25,pierce),imodbits_polearm ],
["orc_bill","Orcish Bill Hook", [("orc_bill",0)], itp_type_polearm|itp_offset_lance|itp_bonus_against_shield|itp_primary|itp_two_handed|itp_wooden_parry,itc_staff|itcf_carry_spear,1125,weight(4)|difficulty(0)|spd_rtng(93)|weapon_length(121)|swing_damage(32,pierce)|thrust_damage(32,cut),imodbits_polearm ],
["orgspearlong","Orcish Pike", [("orgspearLONG",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry|itp_two_handed,itc_musket_melee_bayonet|itcf_carry_spear,1225,weight(3.25)|difficulty(0)|spd_rtng(90)|weapon_length(196)|swing_damage(24,cut)|thrust_damage(26,pierce),imodbits_polearm ],
["orc_halberd","Crude Halberd",[("orc_halberd",0)],itp_type_polearm|itp_offset_lance|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_staff,1275,weight(4)|difficulty(0)|spd_rtng(89)|weapon_length(143)|swing_damage(40,cut)|thrust_damage(25,pierce),imodbits_axe],

["banner_mordor","Mordor Banner", [("banner_mordor",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry,itcf_carry_spear|itc_slashing_polearm,1125,weight(2.25)|spd_rtng(87)|weapon_length(185)|swing_damage(20,blunt),imodbits_polearm ],
["banner_mordor_b","Mordor Orc Banner", [("banner_mordor_b",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry,itcf_carry_spear|itc_slashing_polearm,1325,weight(2.25)|spd_rtng(85)|weapon_length(220)|swing_damage(20,blunt),imodbits_polearm ],

# Mordor Uruks #
["uruk_falchion_b","Crude Uruk Falchion", [("uruk_falchion_b",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,575,weight(2.75)|difficulty(8)|spd_rtng(96)|weapon_length(65)|swing_damage(31,cut)|thrust_damage(0,pierce),imodbits_sword],
["uruk_falchion_a","Crude Uruk Falchion", [("uruk_falchion_a",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary,itc_scimitar|itcf_carry_sword_left_hip,600,weight(2.75)|difficulty(8)|spd_rtng(96)|weapon_length(70)|swing_damage(30,cut)|thrust_damage(0,pierce),imodbits_sword],

["uruk_voulge","Crude Voulge", [("uruk_voulge",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1125,weight(4.75)|difficulty(8)|spd_rtng(90)|weapon_length(107)|swing_damage(37,cut)|thrust_damage(0,pierce),imodbits_axe],
["uruk_heavy_axe","Crude Battle Axe", [("uruk_heavy_axe",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_nodachi|itcf_carry_axe_back,1150,weight(5.25)|difficulty(9)|spd_rtng(88)|weapon_length(97)|swing_damage(43,cut)|thrust_damage(0,pierce),imodbits_axe],

["uruk_spear","Crude Glaive", [("uruk_spear",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_staff|itcf_carry_spear,1200,weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(163)|swing_damage(34,cut)|thrust_damage(25,pierce),imodbits_polearm ],
["uruk_skull_spear","Crude War Spear", [("uruk_skull_spear",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1225,weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(178)|swing_damage(24,cut)|thrust_damage(27,pierce),imodbits_polearm ],

# Trolls #
["troll_mace_1","Troll Mace", [("troll_mace_1",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_knock_back|itp_crush_through|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_wooden_parry,itcf_overswing_twohanded|itc_scimitar,0,weight(5)|difficulty(18)|spd_rtng(85)|weapon_length(169)|swing_damage(40,pierce)|thrust_damage(0,pierce),imodbits_mace ],
["troll_mace_2","Troll Mace", [("troll_mace_2",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_knock_back|itp_crush_through|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_wooden_parry,itcf_overswing_twohanded|itc_scimitar,0,weight(5)|difficulty(18)|spd_rtng(85)|weapon_length(165)|swing_damage(40,pierce)|thrust_damage(0,pierce),imodbits_mace ],
["troll_mace_3","Troll Mace", [("troll_mace_3",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_knock_back|itp_crush_through|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_wooden_parry,itcf_overswing_twohanded|itc_scimitar,0,weight(5)|difficulty(18)|spd_rtng(85)|weapon_length(153)|swing_damage(40,pierce)|thrust_damage(0,pierce),imodbits_mace ],
["troll_mace_4","Troll Mace", [("troll_mace_4",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_knock_back|itp_crush_through|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_wooden_parry,itcf_overswing_twohanded|itc_scimitar,0,weight(5)|difficulty(18)|spd_rtng(85)|weapon_length(112)|swing_damage(38,blunt)|thrust_damage(0,pierce),imodbits_mace ],

["troll_sword_1","Troll Sword", [("troll_sword_1",0)], itp_type_one_handed_wpn|itp_bonus_against_shield|itp_knock_back|itp_crush_through|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_wooden_parry,itc_scimitar,0,weight(3)|difficulty(18)|spd_rtng(88)|weapon_length(150)|swing_damage(46,cut)|thrust_damage(0,pierce),imodbits_sword ],
["troll_sword_2","Troll Sword", [("troll_sword_2",0)], itp_type_one_handed_wpn|itp_bonus_against_shield|itp_knock_back|itp_crush_through|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_wooden_parry,itc_scimitar,0,weight(3)|difficulty(18)|spd_rtng(88)|weapon_length(185)|swing_damage(46,cut)|thrust_damage(0,pierce),imodbits_sword ],
["troll_sword_3","Troll Sword", [("troll_sword_3",0)], itp_type_one_handed_wpn|itp_bonus_against_shield|itp_knock_back|itp_crush_through|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_wooden_parry,itc_scimitar,0,weight(3)|difficulty(18)|spd_rtng(88)|weapon_length(178)|swing_damage(46,cut)|thrust_damage(0,pierce),imodbits_sword ],
["troll_sword_4","Troll Sword", [("troll_sword_4",0)], itp_type_one_handed_wpn|itp_bonus_against_shield|itp_knock_back|itp_crush_through|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_wooden_parry,itc_scimitar,0,weight(3)|difficulty(18)|spd_rtng(88)|weapon_length(215)|swing_damage(46,cut)|thrust_damage(0,pierce),imodbits_sword ],

["throwing_boulder","Boulders", [("throwing_boulder",0)], itp_type_thrown|itp_merchandise|itp_primary|itp_crush_through|itp_can_knock_down|itp_no_pick_up_from_ground,itcf_throw_stone,0,weight(8)|difficulty(5)|spd_rtng(85)|shoot_speed(20)|thrust_damage(45,blunt)|max_ammo(18)|weapon_length(64),imodbit_large_bag ],

# Balrog #
["balrog_sword","Balrog Sword", [("balrog_sword", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_crush_through|itp_bonus_against_shield|itp_knock_back|itp_no_pick_up_from_ground,itc_scimitar,0,weight(5)|abundance(100)|spd_rtng(90)|weapon_length(217)|thrust_damage(0,pierce)|swing_damage(70,cut),imodbits_sword,
[															
  (ti_on_init_item, 
  [															
      (set_position_delta, 0, 75, 0),															
      (particle_system_add_new, "psys_fireplace_fire_big"),															
	  (set_position_delta, 0, 130, 0),														
      (particle_system_add_new, "psys_fireplace_fire_big"),															
	  (set_position_delta, 0, 185, 0),														
      (particle_system_add_new, "psys_fireplace_fire_big"),															
      (set_current_color, 150, 130, 70),															
      (add_point_light, 10, 30),															
    ]),															
]],

# Oathbreakers #
["aod_sword_1a","Spectral Sword", [("aod_sword_1a", 0),("aod_sword_1a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_1b","Spectral Sword", [("aod_sword_1b", 0),("aod_sword_1b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_2a","Spectral Sword", [("aod_sword_2a", 0),("aod_sword_2a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_2b","Spectral Sword", [("aod_sword_2b", 0),("aod_sword_2b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_3a","Spectral Sword", [("aod_sword_3a", 0),("aod_sword_3a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_3b","Spectral Sword", [("aod_sword_3b", 0),("aod_sword_3b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_4a","Spectral Sword", [("aod_sword_4a", 0),("aod_sword_4a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_4b","Spectral Sword", [("aod_sword_4b", 0),("aod_sword_4b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_5a","Spectral Sword", [("aod_sword_5a", 0),("aod_sword_5a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_5b","Spectral Sword", [("aod_sword_5b", 0),("aod_sword_5b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_6a","Spectral Sword", [("aod_sword_6a", 0),("aod_sword_6a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_6b","Spectral Sword", [("aod_sword_6b", 0),("aod_sword_6b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_7a","Spectral Sword", [("aod_sword_7a", 0),("aod_sword_7a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_7b","Spectral Sword", [("aod_sword_7b", 0),("aod_sword_7b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_8a","Spectral Sword", [("aod_sword_8a", 0),("aod_sword_8a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_8b","Spectral Sword", [("aod_sword_8b", 0),("aod_sword_8b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_no_pick_up_from_ground, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(102)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(27, pierce), imodbits_sword, []],															
["aod_sword_9a","Spectral Greatsword", [("aod_sword_9a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_two_handed|itp_no_pick_up_from_ground, itc_greatsword|itcf_carry_sword_back,1075, weight(3.5)|abundance(100)|difficulty(0)|spd_rtng(95)|weapon_length(109)|swing_damage(40, cut)|thrust_damage(28, pierce), imodbits_sword, []],															
["aod_sword_9b","Spectral Greatsword", [("aod_sword_9b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_two_handed|itp_no_pick_up_from_ground, itc_greatsword|itcf_carry_sword_back,1075, weight(3.5)|abundance(100)|difficulty(0)|spd_rtng(95)|weapon_length(109)|swing_damage(40, cut)|thrust_damage(28, pierce), imodbits_sword, []],															
														
["aod_axe_1a","Spectral Axe", [("aod_axe_1a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,850, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(70)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_1b","Spectral Axe", [("aod_axe_1b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,850, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(70)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_2a","Spectral Axe", [("aod_axe_2a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,925, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(76)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_2b","Spectral Axe", [("aod_axe_2b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,925, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(76)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_3a","Spectral Axe", [("aod_axe_3a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(73)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_3b","Spectral Axe", [("aod_axe_3b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(73)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_4a","Spectral Axe", [("aod_axe_4a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(73)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_4b","Spectral Axe", [("aod_axe_4b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,900, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(73)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_5a","Spectral Axe", [("aod_axe_5a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,850, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(70)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_5b","Spectral Axe", [("aod_axe_5b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,850, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(70)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_6a","Spectral Axe", [("aod_axe_6a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,875, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(71)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_6b","Spectral Axe", [("aod_axe_6b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_scimitar|itcf_carry_axe_left_hip,875, weight(2)|abundance(100)|difficulty(0)|spd_rtng(105)|weapon_length(71)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_7a","Spectral Two-Hand Axe", [("aod_axe_7a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_axe_back,1125, weight(4)|abundance(100)|difficulty(0)|spd_rtng(92)|weapon_length(91)|swing_damage(43, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_7b","Spectral Two-Hand Axe", [("aod_axe_7b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_axe_back,1125, weight(4)|abundance(100)|difficulty(0)|spd_rtng(92)|weapon_length(91)|swing_damage(43, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_8a","Spectral Two-Hand Axe", [("aod_axe_8a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_axe_back,1325, weight(4)|abundance(100)|difficulty(0)|spd_rtng(90)|weapon_length(110)|swing_damage(43, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_8b","Spectral Two-Hand Axe", [("aod_axe_8b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_axe_back,1325, weight(4)|abundance(100)|difficulty(0)|spd_rtng(90)|weapon_length(110)|swing_damage(43, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_9a","Spectral Battleaxe", [("aod_axe_9a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_bonus_against_shield|itp_wooden_parry|itp_primary|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_axe_back,1125, weight(5)|abundance(100)|difficulty(0)|spd_rtng(86)|weapon_length(93)|swing_damage(45, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_axe_9b","Spectral Battleaxe", [("aod_axe_9b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_bonus_against_shield|itp_wooden_parry|itp_primary|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_axe_back,1125, weight(5)|abundance(100)|difficulty(0)|spd_rtng(86)|weapon_length(93)|swing_damage(45, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
														
["aod_bardiche_a","Spectral Bardiche", [("aod_bardiche_a", 0)], itp_type_polearm|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_axe_back,1325, weight(5)|abundance(100)|difficulty(0)|spd_rtng(83)|weapon_length(126)|swing_damage(47, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_bardiche_b","Spectral Bardiche", [("aod_bardiche_b", 0)], itp_type_polearm|itp_merchandise|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_axe_back,1325, weight(5)|abundance(100)|difficulty(0)|spd_rtng(83)|weapon_length(126)|swing_damage(47, cut)|thrust_damage(0, pierce), imodbits_axe, []],															
														
["aod_morningstar_a","Spectral Morningstar", [("aod_morningstar_a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_crush_through|itp_wooden_parry|itp_two_handed|itp_unbalanced|itp_no_pick_up_from_ground, itc_morningstar|itcf_carry_mace_left_hip,825, weight(2.5)|abundance(100)|difficulty(0)|spd_rtng(95)|weapon_length(62)|swing_damage(38, pierce)|thrust_damage(0, pierce), imodbits_mace, []],															
["aod_morningstar_b","Spectral Morningstar", [("aod_morningstar_b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_crush_through|itp_wooden_parry|itp_two_handed|itp_unbalanced|itp_no_pick_up_from_ground, itc_morningstar|itcf_carry_mace_left_hip,825, weight(2.5)|abundance(100)|difficulty(0)|spd_rtng(95)|weapon_length(62)|swing_damage(38, pierce)|thrust_damage(0, pierce), imodbits_mace, []],															
														
["aod_spear_a","Spectral Spear", [("aod_spear_a", 0)], itp_type_polearm|itp_merchandise|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_staff_overhead_shield|itcf_carry_spear,850, weight(2)|abundance(100)|difficulty(0)|spd_rtng(90)|weapon_length(156)|swing_damage(20, blunt)|thrust_damage(27, pierce), imodbits_polearm, []],															
["aod_spear_b","Spectral Spear", [("aod_spear_b", 0)], itp_type_polearm|itp_merchandise|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_no_pick_up_from_ground, itc_staff_overhead_shield|itcf_carry_spear,850, weight(2)|abundance(100)|difficulty(0)|spd_rtng(90)|weapon_length(156)|swing_damage(20, blunt)|thrust_damage(27, pierce), imodbits_polearm, []],															
														
["aod_pike_a","Spectral Pike", [("aod_pike_a", 0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_parry|itp_no_pick_up_from_ground, itc_musket_melee_bayonet|itcf_carry_spear,1250, weight(2.5)|abundance(100)|difficulty(0)|spd_rtng(87)|weapon_length(169)|swing_damage(0, blunt)|thrust_damage(29, pierce), imodbits_polearm, []],															
["aod_pike_b","Spectral Pike", [("aod_pike_b", 0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_parry|itp_no_pick_up_from_ground, itc_musket_melee_bayonet|itcf_carry_spear,1250, weight(2.5)|abundance(100)|difficulty(0)|spd_rtng(87)|weapon_length(169)|swing_damage(0, blunt)|thrust_damage(29, pierce), imodbits_polearm, []],															
														
["aod_maul_a","Spectral Maul", [("aod_maul_a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_crush_through|itp_can_knock_down|itp_wooden_parry|itp_wooden_attack|itp_unbalanced|itp_primary|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_spear,1100, weight(3.5)|abundance(100)|difficulty(0)|spd_rtng(83)|weapon_length(69)|swing_damage(36, blunt)|thrust_damage(0, pierce), imodbits_axe, []],															
["aod_maul_b","Spectral Maul", [("aod_maul_b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_crush_through|itp_can_knock_down|itp_wooden_parry|itp_wooden_attack|itp_unbalanced|itp_primary|itp_two_handed|itp_no_pick_up_from_ground, itc_nodachi|itcf_carry_spear,1100, weight(3.5)|abundance(100)|difficulty(0)|spd_rtng(83)|weapon_length(69)|swing_damage(36, blunt)|thrust_damage(0, pierce), imodbits_axe, []],															

# Sauron #
["sauron_mace","Sauron's Mace", [("sauron_mace",0)], itp_crush_through|itp_bonus_against_shield|itp_type_one_handed_wpn|itp_merchandise|itp_can_knock_down|itp_knock_back|itp_primary|itp_unbalanced,itcf_overswing_twohanded|itc_scimitar|itcf_carry_mace_left_hip,2850,weight(12)|difficulty(13)|spd_rtng(80)|weapon_length(90)|swing_damage(55,blunt)|thrust_damage(0, pierce),imodbits_mace],

# Isengard #
["isengard_sword","Isengard Sword", [("isengard_sword",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,650,weight(2)|difficulty(0)|spd_rtng(96)|weapon_length(70)|swing_damage(32, cut)|thrust_damage(0, pierce),imodbits_sword],
["isengard_axe", "Isengard Axe", [("isengard_axe",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,725,weight(2.5)|difficulty(9)|spd_rtng(92)|weapon_length(67)|swing_damage(34, cut)|thrust_damage(0, pierce),imodbits_axe],
["isengard_hammer","Isengard Hammer", [("isengard_hammer",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,900,weight(2)|difficulty(0)|spd_rtng(92)|weapon_length(59)|swing_damage(31, blunt)|thrust_damage(0, pierce),imodbits_mace],
["isengard_heavy_sword","Isengard Heavy Sword", [("isengard_heavy_sword",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,975,weight(2.5)|difficulty(0)|spd_rtng(94)|weapon_length(98)|swing_damage(35, cut)|thrust_damage(0, pierce),imodbits_sword],

["isengard_heavy_axe","Isengard Heavy Axe", [("isengard_heavy_axe",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,1100,weight(4.5)|difficulty(10)|spd_rtng(94)|weapon_length(104)|swing_damage(41, cut)|thrust_damage(0, pierce),imodbits_axe ],
["isengard_berserker_sword","Isengard Berserker Sword", [("isengard_berserker_sword",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,1300,weight(5.5)|difficulty(15)|spd_rtng(95)|weapon_length(115)|swing_damage(44, cut)|thrust_damage(0, pierce),imodbits_axe ],
["isengard_mallet","Isengard Mallet", [("isengard_mallet",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_spear,1325,weight(7)|difficulty(12)|spd_rtng(81)|weapon_length(79)|swing_damage(39, blunt)|thrust_damage(0, pierce),imodbits_mace],

["isengard_spear","Isengard Spear", [("isengard_spear",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_staff_overhead_shield|itcf_carry_spear,1025,weight(2.25)|difficulty(0)|spd_rtng(97)|weapon_length(150)|swing_damage(18,blunt)|thrust_damage(25,pierce),imodbits_polearm ],
["isengard_halberd","Isengard Halberd",[("isengard_halberd",0)],itp_type_polearm|itp_offset_lance|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_cutting_spear,1325,weight(4.5)|difficulty(0)|spd_rtng(87)|weapon_length(145)|swing_damage(43,cut)|thrust_damage(25,pierce),imodbits_axe],
["isengard_pike","Isengard Pike", [("isengard_pike",0)], itp_type_polearm|itp_two_handed|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_musket_melee_bayonet|itcf_carry_spear,1575,weight(3)|difficulty(0)|spd_rtng(93)|weapon_length(227)|swing_damage(18,blunt)|thrust_damage(27,pierce),imodbits_polearm ],
["isengard_heavy_pike","Isengard Heavy Pike", [("isengard_heavy_pike",0)], itp_type_polearm|itp_two_handed|itp_unbalanced|itp_offset_lance|itp_merchandise|itp_primary|itp_wooden_parry,itc_musket_melee_bayonet|itcf_carry_spear,2150,weight(5)|difficulty(0)|spd_rtng(88)|weapon_length(390)|swing_damage(18,blunt)|thrust_damage(27,pierce),imodbits_polearm ],

["banner_isengard_b","Isengard Banner", [("banner_isengard_b",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_attack|itp_wooden_parry,itcf_carry_spear|itc_slashing_polearm,1325,weight(2.25)|spd_rtng(85)|weapon_length(220)|swing_damage(20,blunt),imodbits_polearm ],

["sarustaff","Saruman's Staff", [("sarustaff",0)], itp_type_crossbow|itp_merchandise|itp_primary|itp_two_handed|itp_no_pick_up_from_ground|itp_next_item_as_melee,itcf_thrust_polearm|itcf_carry_spear,0,weight(2)|difficulty(10)|spd_rtng(60)|shoot_speed(70)|thrust_damage(80, pierce)|max_ammo(100),imodbits_crossbow,
	[
		(ti_on_weapon_attack, 
		[
			(store_trigger_param_1, ":attacker_id"),
			
			(ge, ":attacker_id", 0),
			(agent_is_active, ":attacker_id"),
			(call_script, "script_agent_play_sound_sync", ":attacker_id", "snd_lightning_missile", 0),
		])
	],
],
["sarustaff_melee","Saruman's Staff", [("sarustaff",0)], itp_type_polearm|itp_offset_lance|itp_merchandise|itp_primary,itc_staff|itcf_carry_spear,1600,weight(2)|difficulty(0)|spd_rtng(95)|weapon_length(140)|swing_damage(35, blunt)|thrust_damage(32, blunt),imodbits_polearm],

# TLD melee weapons end #
 
# TLD ranged weapons #
 
["arnor_longbow","Dunedain Longbow",[("Bow_Empire_Big_Longbow_B_01",0),("Bow_Empire_Big_Longbow_B_Carry_01",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1450,weight(2)|difficulty(4)|spd_rtng(80)|shoot_speed(60)|thrust_damage(27,pierce),imodbits_bow],
["dwarf_short_bow","Dwarfmade Shortbow",[("dwarf_short_bow",0)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow,1200,weight(0.75)|difficulty(1)|spd_rtng(90)|shoot_speed(55)|thrust_damage(19,pierce),imodbits_bow],
["dwarf_horn_bow","Dwarfmade Hornbow",[("dwarf_horn_bow",0)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow,1350,weight(1)|difficulty(3)|spd_rtng(85)|shoot_speed(56)|thrust_damage(23,pierce),imodbits_bow],
["lorien_bow","Lorien Longbow",[("lorien_bow",0),("lorien_bow_carry",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1600,weight(1)|difficulty(3)|spd_rtng(88)|shoot_speed(59)|thrust_damage(26,pierce),imodbits_bow],
["mirkwood_bow","Mirkwood Longbow",[("mirkwood_bow",0),("mirkwood_bow_carry",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1700,weight(0.75)|difficulty(3)|spd_rtng(87)|shoot_speed(61)|thrust_damage(28,pierce),imodbits_bow],
["rivendell_bow","Rivendell Decorated Bow",[("rivendell_bow",0),("rivendell_bow_carry",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1400,weight(1.5)|difficulty(3)|spd_rtng(84)|shoot_speed(57)|thrust_damage(24,pierce),imodbits_bow],
["harad_shortbow","Haradrim Shortbow",[("harad_shortbow",0)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow,1200,weight(0.75)|difficulty(1)|spd_rtng(91)|shoot_speed(55)|thrust_damage(19,pierce),imodbits_bow],
["harad_bow","Eagle Guard Longbow",[("harad_bow",0)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow,1525,weight(1)|difficulty(3)|spd_rtng(85)|shoot_speed(59)|thrust_damage(26,pierce),imodbits_bow],
["corsair_bow","Longbow of Umbar",[("corsair_bow",0),("corsair_bow_carry",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1325,weight(1.5)|difficulty(3)|spd_rtng(80)|shoot_speed(57)|thrust_damage(24,pierce),imodbits_bow],
["rhun_bow","Rhun Dragonclaw Bow",[("rhun_bow",0),("rhun_bow_carry",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1475,weight(1.5)|difficulty(3)|spd_rtng(82)|shoot_speed(57)|thrust_damage(26,pierce),imodbits_bow],
["gondor_bow","Gondorian Longbow",[("Bow_Empire_Big_Longbow_B_01",0),("Bow_Empire_Big_Longbow_B_Carry_01",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1425,weight(2)|difficulty(4)|spd_rtng(82)|shoot_speed(60)|thrust_damage(25,pierce),imodbits_bow],
["orc_bow","Crude Bow", [("orc_bow",0),("orc_bow_carry", ixmesh_carry)],itp_type_bow|itp_merchandise|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn,1150,weight(1.25)|difficulty(1)|spd_rtng(91)|shoot_speed(56)|thrust_damage(18,pierce),imodbits_bow ],
["isengard_large_bow","Isengard War Bow",[("isengard_large_bow",0),("isengard_large_bow_carry",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1375,weight(2)|difficulty(4)|spd_rtng(82)|shoot_speed(59)|thrust_damage(24,pierce),imodbits_bow],
["aod_bow_1a","Spectral Longbow", [("aod_bow_1a",0),("aod_bow_1a_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_primary|itp_two_handed|itp_no_pick_up_from_ground ,itcf_shoot_bow|itcf_carry_bow_back,1475,weight(1.75)|difficulty(0)|spd_rtng(79)|shoot_speed(56)|thrust_damage(27, pierce), imodbits_bow, []],															
["aod_bow_1b","Spectral Longbow", [("aod_bow_1b",0),("aod_bow_1b_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_primary|itp_two_handed|itp_no_pick_up_from_ground ,itcf_shoot_bow|itcf_carry_bow_back,1475,weight(1.75)|difficulty(0)|spd_rtng(79)|shoot_speed(56)|thrust_damage(27, pierce), imodbits_bow, []],															
["aod_bow_2a","Spectral Shortbow", [("aod_bow_2a",0),("aod_bow_2a_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_primary|itp_two_handed|itp_no_pick_up_from_ground ,itcf_shoot_bow|itcf_carry_bow_back,1350,weight(1)|difficulty(0)|spd_rtng(97)|shoot_speed(55)|thrust_damage(20, pierce), imodbits_bow, []],															
["aod_bow_2b","Spectral Shortbow", [("aod_bow_2b",0),("aod_bow_2b_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_primary|itp_two_handed|itp_no_pick_up_from_ground ,itcf_shoot_bow|itcf_carry_bow_back,1350,weight(1)|difficulty(0)|spd_rtng(97)|shoot_speed(55)|thrust_damage(20, pierce), imodbits_bow, []],
["legolas_galadhrim","Legolas' Bow",[("legolas_galadhrim",0),("legolas_galadhrim",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,1925,weight(1)|difficulty(4)|spd_rtng(92)|shoot_speed(62)|thrust_damage(30,pierce),imodbits_bow],
 
["dwarf_crossbow_med1","Dwarven Crossbow",[("Crossbow_Empire_Medium_A_01",0)],itp_type_crossbow|itp_primary|itp_two_handed|itp_cant_reload_on_horseback,itcf_shoot_crossbow|itcf_carry_crossbow_back,1075,weight(3)|difficulty(0)|spd_rtng(43)|shoot_speed(70)|thrust_damage(52,pierce)|max_ammo(1),imodbits_crossbow],
["dwarf_crossbow_med2","Dwarven Crossbow",[("Crossbow_Empire_Medium_B_01",0)],itp_type_crossbow|itp_primary|itp_two_handed|itp_cant_reload_on_horseback,itcf_shoot_crossbow|itcf_carry_crossbow_back,1125,weight(3)|difficulty(0)|spd_rtng(42)|shoot_speed(71)|thrust_damage(55,pierce)|max_ammo(1),imodbits_crossbow],
["dwarf_crossbow_heavy","Dwarven Heavy Crossbow",[("Crossbow_Empire_Heavy_A_01",0)],itp_type_crossbow|itp_primary|itp_two_handed|itp_cant_reload_on_horseback,itcf_shoot_crossbow|itcf_carry_crossbow_back,1250,weight(4)|difficulty(0)|spd_rtng(36)|shoot_speed(74)|thrust_damage(65,pierce)|max_ammo(1),imodbits_crossbow],
["rhun_crossbow","Rhun Dragon Crossbow",[("rhun_crossbow",0)],itp_type_crossbow|itp_primary|itp_two_handed|itp_cant_reload_on_horseback,itcf_shoot_crossbow|itcf_carry_crossbow_back,1425,weight(4)|difficulty(0)|spd_rtng(32)|shoot_speed(76)|thrust_damage(71,pierce)|max_ammo(1),imodbits_crossbow],
 
["dunland_javelins","Dunnish Javelins",[("dunland_javelin",0)],itp_type_thrown|itp_primary|itp_next_item_as_melee,itcf_throw_javelin,750,weight(3)|difficulty(0)|spd_rtng(91)|shoot_speed(25)|thrust_damage(30,pierce)|max_ammo(15)|weapon_length(88),imodbits_thrown],
["dunland_javelin_melee","Dunnish Javelin",[("dunland_javelin",0)],itp_type_polearm|itp_primary|itp_wooden_parry,itc_staff_overhead_shield,750,weight(3)|difficulty(0)|spd_rtng(95)|swing_damage(15,blunt)|thrust_damage(24,pierce)|weapon_length(88),imodbits_polearm],
["gondor_javelins","Gondorian Javelins",[("gondor_javelin",0)],itp_type_thrown|itp_primary|itp_next_item_as_melee,itcf_throw_javelin,1100,weight(4)|difficulty(0)|spd_rtng(87)|shoot_speed(26)|thrust_damage(45,pierce)|max_ammo(13)|weapon_length(82),imodbits_thrown],
["gondor_javelin_melee","Gondorian Javelin",[("gondor_javelin",0)],itp_type_polearm|itp_primary|itp_wooden_parry,itc_staff_overhead_shield,1100,weight(4)|difficulty(0)|spd_rtng(93)|swing_damage(19,blunt)|thrust_damage(28,pierce)|weapon_length(82),imodbits_polearm],
["rohan_throwing_spears","Rohirric Throwing Spears",[("rohan_throwing_spear",0)],itp_type_thrown|itp_primary|itp_next_item_as_melee,itcf_throw_javelin,1175,weight(4)|difficulty(0)|spd_rtng(85)|shoot_speed(28)|thrust_damage(50,pierce)|max_ammo(10)|weapon_length(94),imodbits_thrown],
["rohan_throwing_spear_melee","Rohirric Throwing Spear",[("rohan_throwing_spear",0)],itp_type_polearm|itp_primary|itp_two_handed|itp_wooden_parry,itc_staff_overhead_stab,1175,weight(4)|difficulty(0)|spd_rtng(92)|swing_damage(20,blunt)|thrust_damage(28,pierce)|weapon_length(94),imodbits_polearm],
["harad_javelins","Haradrim Throwing Spears",[("harad_javelin",0),("harad_javelins_quiver",ixmesh_carry)],itp_type_thrown|itp_primary|itp_next_item_as_melee,itcf_throw_javelin,900,weight(4)|difficulty(0)|spd_rtng(88)|shoot_speed(25)|thrust_damage(37,pierce)|max_ammo(14)|weapon_length(68),imodbits_thrown],
["harad_javelin_melee","Haradrim Throwing Spear",[("harad_javelin",0)],itp_type_polearm|itp_primary|itp_two_handed|itp_wooden_parry,itc_staff_overhead_stab,900,weight(4)|difficulty(0)|spd_rtng(95)|swing_damage(20,blunt)|thrust_damage(24,pierce)|weapon_length(68),imodbits_polearm],
["corsair_harpoons","Harpoons of Umbar",[("corsair_harpoon",0)],itp_type_thrown|itp_primary|itp_next_item_as_melee,itcf_throw_javelin,1125,weight(4)|difficulty(0)|spd_rtng(85)|shoot_speed(28)|thrust_damage(48,pierce)|max_ammo(12)|weapon_length(87),imodbits_thrown],
["corsair_harpoon_melee","Harpoon of Umbar",[("corsair_harpoon",0)],itp_type_polearm|itp_primary|itp_two_handed|itp_wooden_parry,itc_staff_overhead_stab,1125,weight(4)|difficulty(0)|spd_rtng(92)|swing_damage(20,blunt)|thrust_damage(28,pierce)|weapon_length(87),imodbits_polearm],
["rhun_throwing_spears","Rhun Throwing Spears",[("rhun_throwing_spear",0)],itp_type_thrown|itp_primary|itp_next_item_as_melee,itcf_throw_javelin,1225,weight(3.5)|difficulty(0)|spd_rtng(88)|shoot_speed(27)|thrust_damage(50,pierce)|max_ammo(9)|weapon_length(100),imodbits_thrown],
["rhun_throwing_spear_melee","Rhun Throwing Spear",[("rhun_throwing_spear",0)],itp_type_polearm|itp_primary|itp_two_handed|itp_wooden_parry,itc_staff_overhead_stab,1225,weight(3.5)|difficulty(0)|spd_rtng(95)|swing_damage(21,blunt)|thrust_damage(27,pierce)|weapon_length(100),imodbits_polearm],
["wooden_javelin","Wooden Javelins", [("wooden_javelin",0)],itp_type_thrown|itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_javelin,775,weight(3)|difficulty(1)|spd_rtng(82)|shoot_speed(22)|thrust_damage(28,blunt)|max_ammo(12)|weapon_length(65),imodbits_thrown ],
["wooden_javelin_melee","Wooden Javelin", [("wooden_javelin",0)],itp_type_polearm|itp_primary|itp_wooden_attack|itp_wooden_parry,itc_staff_overhead_shield,775,weight(1)|difficulty(1)|spd_rtng(88)|swing_damage(18,blunt)|thrust_damage(15,pierce)|weapon_length(75),imodbits_thrown ],
["orc_throwing_arrow","Crude Throwing Arrows", [("orc_throwing_arrow",0),("orc_throwing_arrow_bag", ixmesh_carry)],itp_type_thrown|itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn,900,weight(3)|difficulty(2)|spd_rtng(85)|shoot_speed(22)|thrust_damage(38,pierce)|max_ammo(15)|weapon_length(65),imodbits_thrown ],
["orc_throwing_arrow_melee","Crude Throwing Arrow", [("orc_throwing_arrow",0),("orc_throwing_arrow_bag", ixmesh_carry)],itp_type_polearm|itp_primary|itp_wooden_parry,itc_staff_overhead_shield,900,weight(1)|difficulty(1)|spd_rtng(91)|swing_damage(12,blunt)|thrust_damage(20,pierce)|weapon_length(75),imodbits_thrown ],
 
["dwarf_throw_axes","Dwarven Throwing Axes",[("dwarf_throw_axe",0)],itp_type_thrown|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee,itcf_throw_axe,1100,weight(2.5)|difficulty(0)|spd_rtng(98)|shoot_speed(18)|thrust_damage(44,cut)|max_ammo(18)|weapon_length(31),imodbits_thrown],
["dwarf_throw_axe_melee","Dwarven Throwing Axe",[("dwarf_throw_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield,itc_scimitar,1100,weight(2.5)|difficulty(0)|spd_rtng(103)|swing_damage(32,cut)|weapon_length(31),imodbits_thrown],
["loss_throwing_axes","Lossarnach Throwing Axes",[("loss_throwing_axe",0)],itp_type_thrown|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee,itcf_throw_axe,1175,weight(2.5)|difficulty(1)|spd_rtng(95)|shoot_speed(18)|thrust_damage(48,cut)|max_ammo(17)|weapon_length(34),imodbits_thrown],
["loss_throwing_axe_melee","Lossarnach Throwing Axe",[("loss_throwing_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield,itc_scimitar,1175,weight(2.5)|difficulty(0)|spd_rtng(103)|swing_damage(33,cut)|weapon_length(34),imodbits_thrown],
["rohan_throwing_axes","Rohirric Throwing Axes",[("rohan_throwing_axe",0)],itp_type_thrown|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee,itcf_throw_axe,1100,weight(2.5)|difficulty(0)|spd_rtng(95)|shoot_speed(18)|thrust_damage(45,cut)|max_ammo(15)|weapon_length(35),imodbits_thrown],
["rohan_throwing_axe_melee","Rohirric Throwing Axe",[("rohan_throwing_axe",0)],itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield,itc_scimitar,1100,weight(2.5)|difficulty(0)|spd_rtng(100)|swing_damage(30,cut)|weapon_length(35),imodbits_thrown],
["orc_throwing_axe", "Crude Throwing Axes", [("orc_throwing_axe",0)], itp_type_thrown|itp_bonus_against_shield|itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_axe,800,weight(4)|difficulty(2)|spd_rtng(99)|shoot_speed(18)|thrust_damage(32,cut)|max_ammo(16)|weapon_length(38),imodbits_thrown_minus_heavy ],
["orc_throwing_axe_melee", "Crude Throwing Axe", [("orc_throwing_axe",0)], itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield,itc_scimitar,800,weight(0.75)|difficulty(2)|spd_rtng(99)|weapon_length(38)|swing_damage(24,cut),imodbits_thrown_minus_heavy ],
 
["corsair_throwing_daggers","Raider's Throwing Daggers",[("corsair_throwing_dagger",0)],itp_type_thrown|itp_primary,itcf_throw_knife,400,weight(2.5)|difficulty(0)|spd_rtng(115)|shoot_speed(21)|thrust_damage(25,pierce)|max_ammo(25)|weapon_length(0),imodbits_thrown],
 
["plain_arrows","Plain Arrows",[("plain_arrow",0),("plain_arrow_flying",ixmesh_flying_ammo),("common_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,100,weight(3.5)|abundance(70)|weapon_length(101)|thrust_damage(1,pierce)|max_ammo(30),imodbits_missile],
["gondor_arrows","Gondorian Arrows",[("gondor_arrow",0),("gondor_arrow_flying",ixmesh_flying_ammo),("gondor_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,350,weight(3)|abundance(70)|weapon_length(102)|thrust_damage(2,pierce)|max_ammo(34),imodbits_missile],
["ithilien_arrows","Ithilien Arrows",[("ilithien_arrow",0),("ilithien_arrow_arrow_flying",ixmesh_flying_ammo),("ithilien_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,450,weight(3)|abundance(70)|weapon_length(102)|thrust_damage(3,pierce)|max_ammo(33),imodbits_missile],
["mirkwood_arrows","Mirkwood Leafheaded Arrows",[("mirkwood_arrow",0),("mirkwood_arrow_flying",ixmesh_flying_ammo),("mirkwood_quiver_new",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,400,weight(1.5)|abundance(70)|weapon_length(102)|thrust_damage(4,pierce)|max_ammo(27),imodbits_missile],
["lorien_arrows","Lorien Arrows",[("white_elf_arrow",0),("white_elf_arrow_flying",ixmesh_flying_ammo),("lothlorien_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,300,weight(2)|abundance(70)|weapon_length(102)|thrust_damage(3,pierce)|max_ammo(28),imodbits_missile],
["rohan_arrows1","Rohirric Arrows",[("rohan_arrow1",0),("rohan_arrow1_flying",ixmesh_flying_ammo),("rohan_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,200,weight(3)|abundance(70)|weapon_length(102)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile],
["rohan_arrows2","Rohirric Broadheaded Arrows",[("rohan_arrow2",0),("rohan_arrow2_flying",ixmesh_flying_ammo),("rohan_quiver2",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,300,weight(3)|abundance(70)|weapon_length(102)|thrust_damage(3,pierce)|max_ammo(28),imodbits_missile],
["harad_arrows","Haradrim Arrows",[("harad_arrow",0),("harad_arrow_flying",ixmesh_flying_ammo),("harad_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,200,weight(3)|abundance(70)|weapon_length(102)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile],
["corsair_arrows","Raider's Barbed Arrows",[("corsair_arrow",0),("corsair_arrow_flying",ixmesh_flying_ammo),("corsair_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,300,weight(3)|abundance(70)|weapon_length(102)|thrust_damage(3,pierce)|max_ammo(28),imodbits_missile],
["rhun_arrows","Rhun Hooked Arrows",[("rhun_arrow",0),("flying_missile",ixmesh_flying_ammo),("rhun_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back,300,weight(3)|abundance(70)|weapon_length(102)|thrust_damage(3,pierce)|max_ammo(28),imodbits_missile],
["orc_hook_arrow","Orcish Hooked Arrows", [("orc_hook_arrow",0),("orc_hook_arrow_flying",ixmesh_flying_ammo),("orc_quiver", ixmesh_carry)],itp_type_arrows|itp_merchandise,itcf_carry_quiver_back,100,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(1,pierce)|max_ammo(27),imodbits_missile],
["isengard_arrow","Isengard Arrows", [("isengard_arrow",0),("isengard_arrow_flying",ixmesh_flying_ammo),("isengard_quiver", ixmesh_carry)],itp_type_arrows|itp_merchandise,itcf_carry_quiver_back,200,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(2,pierce)|max_ammo(30),imodbits_missile],
["aod_arrow_a","Spectral Arrows", [("aod_arrow_a",0),("aod_arrow_a",ixmesh_flying_ammo),("aod_quiver_a", ixmesh_carry)], itp_type_arrows|itp_merchandise|itp_no_pick_up_from_ground, itcf_carry_quiver_back,100,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(1,pierce)|max_ammo(30),imodbits_missile],
["aod_arrow_b","Spectral Arrows", [("aod_arrow_b",0),("aod_arrow_b",ixmesh_flying_ammo),("aod_quiver_b", ixmesh_carry)], itp_type_arrows|itp_merchandise|itp_no_pick_up_from_ground, itcf_carry_quiver_back,100,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(1,pierce)|max_ammo(30),imodbits_missile],
 
["rhun_bolts","Rhun Bolts",[("rhun_bolt",0),("flying_missile",ixmesh_flying_ammo),("rhun_bolts",ixmesh_carry)],itp_type_bolts|itp_can_penetrate_shield,itcf_carry_quiver_right_vertical,400,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile],

["magic_missile","Magic Missiles",[("invalid_item",0),("magic_missile",ixmesh_flying_ammo)],itp_type_bolts|itp_can_penetrate_shield|itp_no_pick_up_from_ground,0,0,weight(0)|abundance(90)|weapon_length(63)|thrust_damage(10,pierce)|max_ammo(100),imodbits_missile],
 
# TLD ranged weapons end #
 
## TLD weapons end ##
 
## TLD shields ##
 
["arnor_cav_shield","Arnorian Small Round Shield",[("arnor_cav_shield",0)],itp_type_shield,itcf_carry_round_shield,600,weight(2.5)|hit_points(450)|body_armor(10)|spd_rtng(88)|shield_width(50),imodbits_shield],
["arnor_cav_shield_b","Arnorian Small Round Shield",[("arnor_cav_shield_b",0)],itp_type_shield,itcf_carry_round_shield,600,weight(2.5)|hit_points(450)|body_armor(10)|spd_rtng(88)|shield_width(50),imodbits_shield],
["arnor_shield_inf","Arnorian Round Shield",[("arnor_shield_inf",0)],itp_type_shield,itcf_carry_round_shield,1000,weight(3)|hit_points(480)|body_armor(12)|spd_rtng(83)|shield_width(65),imodbits_shield],
["arnor_shield_inf_b","Arnorian Round Shield",[("arnor_shield_inf_b",0)],itp_type_shield,itcf_carry_round_shield,1000,weight(3)|hit_points(480)|body_armor(12)|spd_rtng(83)|shield_width(65),imodbits_shield],
 
["dwarf_shield_n","Dwarf-made Round Shield",[("dwarf_shield_n",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,450,weight(2)|hit_points(350)|body_armor(7)|spd_rtng(88)|shield_width(70),imodbits_shield],
["dwarf_shield_m","Dwarf-made Round Shield",[("dwarf_shield_m",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,450,weight(2)|hit_points(350)|body_armor(7)|spd_rtng(88)|shield_width(70),imodbits_shield],
["dwarf_shield_e","Dwarf-made Plated Round Shield",[("dwarf_shield_e",0)],itp_type_shield,itcf_carry_round_shield,925,weight(3)|hit_points(380)|body_armor(13)|spd_rtng(85)|shield_width(70),imodbits_shield],
 
["dun_roundshield","Dunnish Wooden Shield",[("dun_roundshield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,250,weight(1.5)|hit_points(320)|body_armor(5)|spd_rtng(92)|shield_width(60),imodbits_shield],
["dun_roundshield_b","Dunnish Wooden Shield",[("dun_roundshield_b",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,250,weight(1.5)|hit_points(320)|body_armor(5)|spd_rtng(92)|shield_width(60),imodbits_shield],
 
["dwarf_shield_a","Dwarven Kite Shield",[("dwarf_shield_a",0)],itp_type_shield,itcf_carry_kite_shield,1075,weight(4)|hit_points(365)|body_armor(20)|spd_rtng(82)|shield_width(38)|shield_height(72),imodbits_shield],
["dwarf_shield_b","Dwarven Oval Shield",[("dwarf_shield_b",0)],itp_type_shield,itcf_carry_kite_shield,1050,weight(4)|hit_points(350)|body_armor(18)|spd_rtng(81)|shield_width(45)|shield_height(80),imodbits_shield],
["dwarf_shield_c","Dwarven Angled Kite Shield",[("dwarf_shield_c",0)],itp_type_shield,itcf_carry_kite_shield,1200,weight(4.5)|hit_points(320)|body_armor(20)|spd_rtng(80)|shield_width(60)|shield_height(90),imodbits_shield],
["dwarf_shield_d","Dwarven Kite Shield",[("dwarf_shield_d",0)],itp_type_shield,itcf_carry_kite_shield,1050,weight(4)|hit_points(325)|body_armor(19)|spd_rtng(82)|shield_width(48)|shield_height(80),imodbits_shield],
["dwarf_shield_f","Dwarven Rimmed Kite Shield",[("dwarf_shield_f",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,625,weight(3.5)|hit_points(325)|body_armor(12)|spd_rtng(88)|shield_width(40)|shield_height(82),imodbits_shield],
["dwarf_shield_g","Dwarven Small Kite Shield",[("dwarf_shield_g",0)],itp_type_shield,itcf_carry_kite_shield,875,weight(3)|hit_points(340)|body_armor(18)|spd_rtng(82)|shield_width(38)|shield_height(70),imodbits_shield],
["dwarf_shield_i","Dwarven Oval Shield",[("dwarf_shield_i",0)],itp_type_shield,itcf_carry_kite_shield,1050,weight(4)|hit_points(350)|body_armor(18)|spd_rtng(81)|shield_width(45)|shield_height(80),imodbits_shield],
["dwarf_shield_j","Dwarven Oval Shield",[("dwarf_shield_j",0)],itp_type_shield,itcf_carry_kite_shield,1050,weight(4)|hit_points(350)|body_armor(18)|spd_rtng(81)|shield_width(45)|shield_height(80),imodbits_shield],
["dwarf_shield_k","Dwarven Angled Kite Shield",[("dwarf_shield_k",0)],itp_type_shield,itcf_carry_kite_shield,1200,weight(4.5)|hit_points(320)|body_armor(20)|spd_rtng(80)|shield_width(60)|shield_height(90),imodbits_shield],
["dwarf_shield_l","Dwarven Kite Shield",[("dwarf_shield_l",0)],itp_type_shield,itcf_carry_kite_shield,1050,weight(4)|hit_points(325)|body_armor(19)|spd_rtng(82)|shield_width(48)|shield_height(80),imodbits_shield],
 
["gondor_square_shield","Gondorian Square Shield",[("gondor_square_shield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,575,weight(2)|hit_points(400)|body_armor(8)|spd_rtng(85)|shield_width(55)|shield_height(80),imodbits_shield],
["gondor_point_shield","Gondorian Heavy Shield",[("gondor_point_shield",0)],itp_type_shield|itp_wooden_parry|itp_cant_use_on_horseback,itcf_carry_kite_shield,850,weight(3.5)|hit_points(450)|body_armor(13)|spd_rtng(82)|shield_width(40)|shield_height(90),imodbits_shield],
["gondor_kite_shield","Gondorian Small Kite Shield",[("gondor_kite_shield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,500,weight(2)|hit_points(320)|body_armor(12)|spd_rtng(92)|shield_width(35)|shield_height(65),imodbits_shield],
["gondor_tower_shield","Gondorian Tower Shield",[("gondor_tower_shield",0)],itp_type_shield|itp_wooden_parry|itp_cant_use_on_horseback,itcf_carry_kite_shield,1475,weight(4.5)|hit_points(550)|body_armor(14)|spd_rtng(82)|shield_width(50)|shield_height(120),imodbits_shield],
["da_shield_kite_01","Dol Amroth Cavalry Shield",[("DA_shield_kite_01",0)],itp_type_shield,itcf_carry_kite_shield,800,weight(3)|hit_points(400)|body_armor(15)|spd_rtng(90)|shield_width(35)|shield_height(65),imodbits_shield],
["da_shield_kite_02","Dol Amroth Cavalry Shield",[("DA_shield_kite_02",0)],itp_type_shield,itcf_carry_kite_shield,800,weight(3)|hit_points(400)|body_armor(15)|spd_rtng(90)|shield_width(35)|shield_height(65),imodbits_shield],
["boromir_shield","Boromir's Shield",[("boromir_shield",0)],itp_type_shield,itcf_carry_round_shield,2250,weight(3)|hit_points(600)|body_armor(20)|spd_rtng(85)|shield_width(70),imodbits_shield],

["rohan_shield_plain","Rohirric Round Shield",[("rohan_shield_plain",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,300,weight(1.5)|hit_points(350)|body_armor(5)|spd_rtng(94)|shield_width(65),imodbits_shield],
["rohan_shield_green","Rohirric Round Shield",[("rohan_shield_green",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,300,weight(1.5)|hit_points(350)|body_armor(5)|spd_rtng(94)|shield_width(65),imodbits_shield],
["rohan_shield_red","Rohirric Round Shield",[("rohan_shield_red",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,300,weight(1.5)|hit_points(350)|body_armor(5)|spd_rtng(94)|shield_width(65),imodbits_shield],
["rohan_shield_plain_boss","Rohirric Heavy Round Shield",[("rohan_shield_plain_boss",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,550,weight(2.5)|hit_points(400)|body_armor(8)|spd_rtng(90)|shield_width(65),imodbits_shield],
["rohan_shield_green_boss","Rohirric Heavy Round Shield",[("rohan_shield_green_boss",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,550,weight(2.5)|hit_points(400)|body_armor(8)|spd_rtng(90)|shield_width(65),imodbits_shield],
["rohan_shield_red_boss","Rohirric Heavy Round Shield",[("rohan_shield_red_boss",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,550,weight(2.5)|hit_points(400)|body_armor(8)|spd_rtng(90)|shield_width(65),imodbits_shield],
["rohan_shield_royal","Rohirric Royal Shield",[("rohan_shield_royal",0)],itp_type_shield,itcf_carry_round_shield,1250,weight(3)|hit_points(510)|body_armor(14)|spd_rtng(88)|shield_width(65),imodbits_shield],
 
["rohan_shield1","Rohirric Painted Round Shield",[("rohan_shield1",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,700,weight(2.5)|hit_points(400)|body_armor(10)|spd_rtng(92)|shield_width(65),imodbits_shield],
["rohan_shield2","Rohirric Painted Round Shield",[("rohan_shield2",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,700,weight(2.5)|hit_points(400)|body_armor(10)|spd_rtng(92)|shield_width(65),imodbits_shield],
["rohan_shield3","Rohirric Painted Round Shield",[("rohan_shield3",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,700,weight(2.5)|hit_points(400)|body_armor(10)|spd_rtng(92)|shield_width(65),imodbits_shield],
["rohan_shield5","Rohirric Painted Round Shield",[("rohan_shield5",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,700,weight(2.5)|hit_points(400)|body_armor(10)|spd_rtng(92)|shield_width(65),imodbits_shield],
["rohan_shield4","Rohirric Gilded Round Shield",[("rohan_shield4",0)],itp_type_shield,itcf_carry_round_shield,850,weight(3)|hit_points(410)|body_armor(12)|spd_rtng(92)|shield_width(65),imodbits_shield],
["rohan_shield6","Rohirric Gilded Round Shield",[("rohan_shield6",0)],itp_type_shield,itcf_carry_round_shield,850,weight(3)|hit_points(410)|body_armor(12)|spd_rtng(92)|shield_width(65),imodbits_shield],
 
["lorien_round_shield","Lorien Round Shield",[("lorien_round_shield",0)],itp_type_shield,itcf_carry_round_shield,575,weight(1.25)|hit_points(300)|body_armor(12)|spd_rtng(96)|shield_width(60),imodbits_shield],
["lorien_kite_small","Lorien Kite Shield",[("lorien_kite_small",0)],itp_type_shield,itcf_carry_kite_shield,750,weight(1.5)|hit_points(320)|body_armor(12)|spd_rtng(95)|shield_width(60)|shield_height(85),imodbits_shield],
["lorien_kite","Lorien Infantry Shield",[("lorien_kite",0)],itp_type_shield,itcf_carry_kite_shield,1075,weight(1.5)|hit_points(330)|body_armor(14)|spd_rtng(92)|shield_width(65)|shield_height(110),imodbits_shield],
 
["mirkwood_med_shield","Mirkwood Infantry Shield",[("mirkwood_med_shield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,600,weight(1.5)|hit_points(330)|body_armor(10)|spd_rtng(92)|shield_width(55)|shield_height(80),imodbits_shield],
["mirkwood_spear_shield","Mirkwood Spearman Shield",[("mirkwood_spear_shield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,850,weight(2)|hit_points(350)|body_armor(11)|spd_rtng(92)|shield_width(55)|shield_height(110),imodbits_shield],
["mirkwood_royal_round","Mirkwood Round Shield",[("mirkwood_royal_round",0)],itp_type_shield,itcf_carry_round_shield,600,weight(1.25)|hit_points(250)|body_armor(14)|spd_rtng(96)|shield_width(65),imodbits_shield],
 
["riv_inf_shield","Rivendell Infantry Shield",[("riv_inf_shield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,725,weight(1.5)|hit_points(380)|body_armor(11)|spd_rtng(90)|shield_width(45)|shield_height(85),imodbits_shield],
["riv_cav_shield","Rivendell Rider Shield",[("riv_cav_shield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,650,weight(1)|hit_points(290)|body_armor(16)|spd_rtng(96)|shield_width(45)|shield_height(60),imodbits_shield],
 
["eastershield_b","Variag Rimmed Round Shield",[("eastershield_b",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,375,weight(1.5)|hit_points(280)|body_armor(11)|spd_rtng(92)|shield_width(45),imodbits_shield],
["eastershield_c","Variag Steel Round Shield",[("eastershield_c",0)],itp_type_shield,itcf_carry_round_shield,475,weight(1.75)|hit_points(280)|body_armor(14)|spd_rtng(90)|shield_width(45),imodbits_shield],
["eastershield_d","Variag Rimmed Round Shield",[("eastershield_d",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,500,weight(1.5)|hit_points(280)|body_armor(11)|spd_rtng(92)|shield_width(60),imodbits_shield],
["eastershield_f","Variag Steel Round Shield",[("eastershield_f",0)],itp_type_shield,itcf_carry_round_shield,675,weight(1.75)|hit_points(280)|body_armor(14)|spd_rtng(90)|shield_width(65),imodbits_shield],
["eastershield_e","Variag Round Shield",[("eastershield_e",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,300,weight(1.5)|hit_points(300)|body_armor(6)|spd_rtng(93)|shield_width(65),imodbits_shield],
["eastershield_a","Variag Steel Oval Shield",[("eastershield_a",0)],itp_type_shield,itcf_carry_kite_shield,1050,weight(3)|hit_points(350)|body_armor(15)|spd_rtng(90)|shield_width(50)|shield_height(100),imodbits_shield],
 
["harad_long_shield_a","Haradrim Tigerskin Shield",[("harad_long_shield_a",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,375,weight(2.5)|hit_points(400)|body_armor(4)|spd_rtng(88)|shield_width(50)|shield_height(125),imodbits_shield],
["harad_long_shield_b","Haradrim Snake Shield",[("harad_long_shield_b",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,525,weight(2.5)|hit_points(400)|body_armor(6)|spd_rtng(89)|shield_width(45)|shield_height(120),imodbits_shield],
["harad_long_shield_c","Haradrim Painted Oval Shield",[("harad_long_shield_c",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,625,weight(2.5)|hit_points(330)|body_armor(10)|spd_rtng(88)|shield_width(45)|shield_height(95),imodbits_shield],
["harad_long_shield_d","Haradrim Painted Oval Shield",[("harad_long_shield_d",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,550,weight(2.25)|hit_points(300)|body_armor(10)|spd_rtng(88)|shield_width(45)|shield_height(95),imodbits_shield],
["harad_long_shield_e","Haradrim Snake Shield",[("harad_long_shield_e",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,500,weight(2.5)|hit_points(430)|body_armor(5)|spd_rtng(87)|shield_width(50)|shield_height(125),imodbits_shield],
 
["harad_shield_a","Haradrim Rimmed Round Shield",[("harad_shield_a",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,400,weight(1.25)|hit_points(230)|body_armor(10)|spd_rtng(90)|shield_width(65),imodbits_shield],
["harad_shield_b","Haradrim Rimmed Round Shield",[("harad_shield_b",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,400,weight(1.25)|hit_points(230)|body_armor(10)|spd_rtng(90)|shield_width(65),imodbits_shield],
["harad_shield_c","Haradrim Rimmed Round Shield",[("harad_shield_c",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,400,weight(1.25)|hit_points(230)|body_armor(10)|spd_rtng(90)|shield_width(65),imodbits_shield],
["harad_yellow_shield","Haradrim Bossed Round Shield",[("harad_yellow_shield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,950,weight(1.5)|hit_points(300)|body_armor(16)|spd_rtng(90)|shield_width(75),imodbits_shield],
["harad_pulwar_shield","Haradrim Duel Pulwar",[("horandor_a_shield",0)],itp_type_shield,itcf_carry_wakizashi,500,weight(1.5)|hit_points(500)|body_armor(15)|spd_rtng(101)|shield_width(1),imodbits_shield],
 
["corsair_buckler_round_a","Raider's Buckler",[("corsair_buckler_round_a",0),("corsair_buckler_round_a_carry",ixmesh_carry)],itp_type_shield,itcf_carry_buckler_left,200,weight(1)|hit_points(180)|body_armor(14)|spd_rtng(98)|shield_width(30),imodbits_shield],
["corsair_buckler_round_b","Raider's Buckler",[("corsair_buckler_round_b",0),("corsair_buckler_round_b_carry",ixmesh_carry)],itp_type_shield,itcf_carry_buckler_left,225,weight(1)|hit_points(180)|body_armor(16)|spd_rtng(98)|shield_width(30),imodbits_shield],
["corsair_buckler_round_c","Raider's Buckler",[("corsair_buckler_round_c",0),("corsair_buckler_round_c_carry",ixmesh_carry)],itp_type_shield,itcf_carry_buckler_left,225,weight(1)|hit_points(180)|body_armor(16)|spd_rtng(98)|shield_width(30),imodbits_shield],
["corsair_buckler_long_a","Raider's Buckler",[("corsair_buckler_long_a",0),("corsair_buckler_long_a_carry",ixmesh_carry)],itp_type_shield,itcf_carry_buckler_left,250,weight(1)|hit_points(200)|body_armor(16)|spd_rtng(98)|shield_width(30),imodbits_shield],
["corsair_buckler_long_b","Raider's Buckler",[("corsair_buckler_long_b",0),("corsair_buckler_long_b_carry",ixmesh_carry)],itp_type_shield,itcf_carry_buckler_left,250,weight(1)|hit_points(200)|body_armor(16)|spd_rtng(98)|shield_width(30),imodbits_shield],
 
["rhun_infantry_shield_plain","Rhun Wooden Round Shield",[("rhun_infantry_shield_plain",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,175,weight(1.25)|hit_points(250)|body_armor(5)|spd_rtng(94)|shield_width(50),imodbits_shield],
["rhun_infantry_shield1","Rhun Painted Round Shield",[("rhun_infantry_shield1",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,325,weight(1.25)|hit_points(300)|body_armor(8)|spd_rtng(91)|shield_width(50),imodbits_shield],
["rhun_infantry_shield2","Rhun Painted Round Shield",[("rhun_infantry_shield2",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,325,weight(1.25)|hit_points(300)|body_armor(8)|spd_rtng(91)|shield_width(50),imodbits_shield],
["rhun_infantry_shield3","Rhun Painted Round Shield",[("rhun_infantry_shield3",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,325,weight(1.25)|hit_points(300)|body_armor(8)|spd_rtng(91)|shield_width(50),imodbits_shield],
["rhun_eastclan_shield_round","Rhun Hardwood Round Shield",[("rhun_eastclan_shield_round",0)],itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,425,weight(1.75)|hit_points(320)|body_armor(10)|spd_rtng(91)|shield_width(50),imodbits_shield],
["rhun_eastclan_shield1","Rhun Eastern Shield",[("rhun_eastclan_shield1",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,575,weight(2.5)|hit_points(300)|body_armor(12)|spd_rtng(91)|shield_width(50)|shield_height(70),imodbits_shield],
["rhun_eastclan_shield2","Rhun Eastern Shield",[("rhun_eastclan_shield2",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,575,weight(2.5)|hit_points(300)|body_armor(12)|spd_rtng(91)|shield_width(50)|shield_height(70),imodbits_shield],
["rhun_elite_shield","Rhun Elite Shield",[("rhun_elite_shield",0)],itp_type_shield,itcf_carry_kite_shield,1125,weight(2.5)|hit_points(350)|body_armor(20)|spd_rtng(91)|shield_width(50)|shield_height(70),imodbits_shield],
["rhun_elite_shield2","Rhun Elite Silver Shield",[("rhun_elite_shield2",0)],itp_type_shield,itcf_carry_kite_shield,1125,weight(2.5)|hit_points(350)|body_armor(20)|spd_rtng(91)|shield_width(50)|shield_height(70),imodbits_shield],

["angmar_shield","Angmar Shield",[("angmar_shield",0)],itp_type_shield,itcf_carry_round_shield,525,weight(1.75)|hit_points(300)|body_armor(10)|spd_rtng(90)|shield_width(65),imodbits_shield],
["mordor_man_shield_b","Mordor Painted Shield",[("mordor_man_shield_b",0)],itp_type_shield,itcf_carry_round_shield,525,weight(1.75)|hit_points(300)|body_armor(10)|spd_rtng(90)|shield_width(65),imodbits_shield],
["mordor_man_shield_a","Mordor Engraved Steel Shield",[("mordor_man_shield_a",0)],itp_type_shield,itcf_carry_round_shield,1275,weight(3)|hit_points(370)|body_armor(20)|spd_rtng(85)|shield_width(65),imodbits_shield],

["orc_shield_a", "Crude Orcish Shield", [("orc_shield_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,100,weight(2.5)|hit_points(195)|body_armor(4)|spd_rtng(93)|shield_width(50),imodbits_shield],
["mordor_orc_shield_a", "Mordor Orc Shield", [("mordor_orc_shield_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,100,weight(2.5)|hit_points(195)|body_armor(4)|spd_rtng(93)|shield_width(50),imodbits_shield],
["mordor_orc_shield_b", "Metal Mordor Orc Shield", [("mordor_orc_shield_b",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,275,weight(3)|hit_points(260)|body_armor(8)|spd_rtng(90)|shield_width(50),imodbits_shield],
["mordor_orc_shield_c", "Metal Mordor Orc Shield", [("mordor_orc_shield_c",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,275,weight(3)|hit_points(260)|body_armor(8)|spd_rtng(90)|shield_width(50),imodbits_shield],
["mordor_orc_shield_d", "Metal Gundabad Orc Shield", [("mordor_orc_shield_d",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,275,weight(3)|hit_points(260)|body_armor(8)|spd_rtng(90)|shield_width(50),imodbits_shield],
["mordor_orc_shield_e", "Metal Mordor Orc Shield", [("mordor_orc_shield_e",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,275,weight(3)|hit_points(260)|body_armor(8)|spd_rtng(90)|shield_width(50),imodbits_shield],

["moria_orc_shield_a", "Moria Orc Shield", [("moria_orc_shield_a",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,175,weight(2.5)|hit_points(195)|body_armor(7)|spd_rtng(93)|shield_width(40)|shield_height(60),imodbits_shield],
["moria_orc_shield_b", "Moria Orc Shield", [("moria_orc_shield_b",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,175,weight(2.5)|hit_points(195)|body_armor(7)|spd_rtng(93)|shield_width(40)|shield_height(60),imodbits_shield],
["moria_orc_shield_c", "Moria Orc Shield", [("moria_orc_shield_c",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,175,weight(2.5)|hit_points(195)|body_armor(7)|spd_rtng(93)|shield_width(40)|shield_height(60),imodbits_shield],

["mordor_uruk_shield_a", "Large Mordor Shield", [("mordor_uruk_shield_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,325,weight(3)|hit_points(300)|body_armor(6)|spd_rtng(90)|shield_width(50)|shield_height(90),imodbits_shield],
["mordor_uruk_shield_b", "Large Mordor Shield", [("mordor_uruk_shield_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,325,weight(3)|hit_points(300)|body_armor(6)|spd_rtng(90)|shield_width(50)|shield_height(90),imodbits_shield],
["mordor_uruk_shield_c", "Large Mordor Shield", [("mordor_uruk_shield_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry,itcf_carry_round_shield,325,weight(3)|hit_points(300)|body_armor(6)|spd_rtng(90)|shield_width(50)|shield_height(90),imodbits_shield],

["isen_orc_shield_a", "Isengard Orc Shield", [("isen_orc_shield_a",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,725,weight(2.5)|hit_points(300)|body_armor(18)|spd_rtng(90)|shield_width(50),imodbits_shield],
["isen_orc_shield_b", "Isengard Orc Shield", [("isen_orc_shield_b",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,725,weight(2.5)|hit_points(300)|body_armor(18)|spd_rtng(90)|shield_width(50),imodbits_shield],

["isen_uruk_shield_a", "Isengard Uruk-hai Shield", [("isen_uruk_shield_a",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,1450,weight(4)|hit_points(500)|body_armor(18)|spd_rtng(90)|shield_width(50)|shield_height(70),imodbits_shield],
["isen_uruk_shield_b", "Isengard Uruk-hai Shield", [("isen_uruk_shield_b",0)], itp_merchandise|itp_type_shield,itcf_carry_round_shield,1450,weight(4)|hit_points(500)|body_armor(18)|spd_rtng(90)|shield_width(50)|shield_height(70),imodbits_shield],
["orthanc_guard_shield1", "Orthanc Guard Shield", [("orthanc_guard_shield1",0)], itp_type_shield,itcf_carry_round_shield,2150,weight(4)|hit_points(600)|body_armor(20)|spd_rtng(90)|shield_width(45)|shield_height(90),imodbits_shield],
["orthanc_guard_shield2", "Orthanc Guard Shield", [("orthanc_guard_shield2",0)], itp_type_shield,itcf_carry_round_shield,2150,weight(4)|hit_points(600)|body_armor(20)|spd_rtng(90)|shield_width(45)|shield_height(90),imodbits_shield],

["aod_shield_1a", "Spectral Tower Shield", [("aod_shield_1a" ,0)], itp_type_shield|itp_no_pick_up_from_ground, itcf_carry_kite_shield,10000,weight(4)|hit_points(9000)|body_armor(30)|spd_rtng(88)|shield_width(52)|shield_height(125),imodbits_shield],
["aod_shield_1b", "Spectral Tower Shield", [("aod_shield_1b" ,0)], itp_type_shield|itp_no_pick_up_from_ground, itcf_carry_kite_shield,10000,weight(4)|hit_points(9000)|body_armor(30)|spd_rtng(88)|shield_width(52)|shield_height(125),imodbits_shield],
["aod_shield_2a", "Spectral Shield", [("aod_shield_2a" ,0)], itp_type_shield|itp_no_pick_up_from_ground, itcf_carry_kite_shield,5000,weight(2)|hit_points(3000)|body_armor(30)|spd_rtng(94)|shield_width(50)|shield_height(90),imodbits_shield],
["aod_shield_2b", "Spectral Shield", [("aod_shield_2b" ,0)], itp_type_shield|itp_no_pick_up_from_ground, itcf_carry_kite_shield,5000,weight(2)|hit_points(3000)|body_armor(30)|spd_rtng(94)|shield_width(50)|shield_height(90),imodbits_shield],
["aod_shield_3a", "Spectral Shield", [("aod_shield_3a" ,0)], itp_type_shield|itp_no_pick_up_from_ground, itcf_carry_kite_shield,6000,weight(2.5)|hit_points(3500)|body_armor(30)|spd_rtng(92)|shield_width(50)|shield_height(105),imodbits_shield],
["aod_shield_3b", "Spectral Shield", [("aod_shield_3b" ,0)], itp_type_shield|itp_no_pick_up_from_ground, itcf_carry_kite_shield,6000,weight(2.5)|hit_points(3500)|body_armor(30)|spd_rtng(92)|shield_width(50)|shield_height(105),imodbits_shield],
["aod_shield_4a", "Spectral Round Shield", [("aod_shield_4a" ,0)], itp_type_shield|itp_no_pick_up_from_ground, itcf_carry_round_shield,3000,weight(1)|hit_points(2500)|body_armor(30)|spd_rtng(96)|shield_width(64),imodbits_shield],
["aod_shield_4b", "Spectral Round Shield", [("aod_shield_4b" ,0)], itp_type_shield|itp_no_pick_up_from_ground, itcf_carry_round_shield,3000,weight(1)|hit_points(2500)|body_armor(30)|spd_rtng(96)|shield_width(64),imodbits_shield],
 
## TLD shields end ##
 
## TLD horses ##
 
["arnor_leather","Arnorian Leatherclad Destrier",[("arnor_leather",0)],itp_type_horse,0,1900,abundance(60)|hit_points(160)|body_armor(32)|difficulty(3)|horse_speed(43)|horse_maneuver(42)|horse_charge(26)|horse_scale(109),imodbits_horse_basic|imodbit_champion],
["arnor_mail","Arnorian Warhorse",[("arnor_mail",0)],itp_type_horse,0,2175,abundance(60)|hit_points(170)|body_armor(38)|difficulty(4)|horse_speed(42)|horse_maneuver(41)|horse_charge(30)|horse_scale(110),imodbits_horse_basic|imodbit_champion],
 
["gondor_horse01","Gondorian Rouncey",[("gondor_horse01",0)],itp_type_horse,0,975,abundance(60)|hit_points(120)|body_armor(12)|difficulty(1)|horse_speed(45)|horse_maneuver(47)|horse_charge(13)|horse_scale(105),imodbits_horse_basic|imodbit_champion],
["gondor_horse02","Gondorian Rouncey",[("gondor_horse02",0)],itp_type_horse,0,975,abundance(60)|hit_points(120)|body_armor(12)|difficulty(1)|horse_speed(45)|horse_maneuver(47)|horse_charge(13)|horse_scale(105),imodbits_horse_basic|imodbit_champion],
["gondor_warhorse01","Gondorian Armored Destrier",[("gondor_warhorse01",0)],itp_type_horse,0,2175,abundance(60)|hit_points(175)|body_armor(36)|difficulty(4)|horse_speed(43)|horse_maneuver(41)|horse_charge(30)|horse_scale(110),imodbits_horse_basic|imodbit_champion],
["lam_warhorse01","Lamedon Armored Warhorse",[("lam_warhorse01",0)],itp_type_horse,0,2175,abundance(60)|hit_points(175)|body_armor(36)|difficulty(4)|horse_speed(41)|horse_maneuver(43)|horse_charge(30)|horse_scale(110),imodbits_horse_basic|imodbit_champion],
["da_warhorse01","Dol Amroth Warhorse",[("da_warhorse01",0)],itp_type_horse,0,2600,abundance(60)|hit_points(190)|body_armor(47)|difficulty(4)|horse_speed(42)|horse_maneuver(43)|horse_charge(33)|horse_scale(113),imodbits_horse_basic|imodbit_champion],
["da_warhorse02","Dol Amroth Warhorse",[("da_warhorse02",0)],itp_type_horse,0,2600,abundance(60)|hit_points(190)|body_armor(47)|difficulty(4)|horse_speed(42)|horse_maneuver(43)|horse_charge(33)|horse_scale(113),imodbits_horse_basic|imodbit_champion],
 
["rohan_horse01","Rohirric Courser",[("rohan_horse01",0)],itp_type_horse,0,1150,abundance(60)|hit_points(120)|body_armor(12)|difficulty(2)|horse_speed(53)|horse_maneuver(48)|horse_charge(15)|horse_scale(102),imodbits_horse_basic|imodbit_champion],
["rohan_horse02","Rohirric Courser",[("rohan_horse02",0)],itp_type_horse,0,1375,abundance(60)|hit_points(120)|body_armor(18)|difficulty(2)|horse_speed(53)|horse_maneuver(48)|horse_charge(17)|horse_scale(104),imodbits_horse_basic|imodbit_champion],
["rohan_horse03","Rohirric Courser",[("rohan_horse03",0)],itp_type_horse,0,1375,abundance(60)|hit_points(120)|body_armor(18)|difficulty(2)|horse_speed(53)|horse_maneuver(48)|horse_charge(17)|horse_scale(104),imodbits_horse_basic|imodbit_champion],
["rohan_warhorse_leather","Rohirric Leatherclad Warhorse",[("rohan_warhorse_leather",0)],itp_type_horse,0,2075,abundance(60)|hit_points(150)|body_armor(32)|difficulty(3)|horse_speed(48)|horse_maneuver(46)|horse_charge(26)|horse_scale(105),imodbits_horse_basic|imodbit_champion],
["rohan_warhorse_scale","Rohirric Scaleclad Warhorse",[("rohan_warhorse_scale",0)],itp_type_horse,0,2525,abundance(60)|hit_points(165)|body_armor(41)|difficulty(4)|horse_speed(47)|horse_maneuver(44)|horse_charge(36)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
["rohan_warhorse_steel_scale","Rohirric Steelclad Warhorse",[("rohan_warhorse_steel_scale",0)],itp_type_horse,0,2625,abundance(60)|hit_points(170)|body_armor(45)|difficulty(4)|horse_speed(45)|horse_maneuver(43)|horse_charge(37)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
 
["loth_warhorse01","Lorien Armored Palfrey",[("loth_warhorse01",0)],itp_type_horse,0,2075,abundance(60)|hit_points(155)|body_armor(35)|difficulty(4)|horse_speed(43)|horse_maneuver(46)|horse_charge(28)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
["rivendell_warhorse01","Rivendell Armored Palfrey",[("rivendell_warhorse01",0)],itp_type_horse,0,2225,abundance(60)|hit_points(160)|body_armor(40)|difficulty(4)|horse_speed(41)|horse_maneuver(45)|horse_charge(30)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
["rivendell_warhorse02","Rivendell Armored Palfrey",[("rivendell_warhorse02",0)],itp_type_horse,0,2225,abundance(60)|hit_points(160)|body_armor(40)|difficulty(4)|horse_speed(41)|horse_maneuver(45)|horse_charge(30)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
 
["harad_horse01","Haradrim Painted Desert Horse",[("harad_horse01",0)],itp_type_horse,0,800,abundance(60)|hit_points(115)|body_armor(8)|difficulty(1)|horse_speed(43)|horse_maneuver(50)|horse_charge(11)|horse_scale(97),imodbits_horse_basic|imodbit_champion],
["harad_horse02","Haradrim Armored Desert Horse",[("harad_horse02",0)],itp_type_horse,0,1575,abundance(60)|hit_points(135)|body_armor(28)|difficulty(2)|horse_speed(43)|horse_maneuver(46)|horse_charge(20)|horse_scale(100),imodbits_horse_basic|imodbit_champion],
 
["easterling_warhorse01","Variag Scaleclad Warhorse",[("easterling_warhorse01",0)],itp_type_horse,0,2550,abundance(60)|hit_points(170)|body_armor(45)|difficulty(4)|horse_speed(42)|horse_maneuver(43)|horse_charge(36)|horse_scale(108),imodbits_horse_basic|imodbit_champion],

["rhunhorselight1","Rhun Rouncey",[("rhunhorselight1",0)],itp_type_horse,0,975,abundance(60)|hit_points(120)|body_armor(15)|difficulty(1)|horse_speed(45)|horse_maneuver(44)|horse_charge(12)|horse_scale(102),imodbits_horse_basic|imodbit_champion],
["rhunhorselight2","Rhun Rouncey",[("rhunhorselight2",0)],itp_type_horse,0,975,abundance(60)|hit_points(120)|body_armor(15)|difficulty(1)|horse_speed(44)|horse_maneuver(45)|horse_charge(12)|horse_scale(102),imodbits_horse_basic|imodbit_champion],
["rhunhorselight4","Rhun Rouncey",[("rhunhorselight4",0)],itp_type_horse,0,975,abundance(60)|hit_points(120)|body_armor(16)|difficulty(1)|horse_speed(44)|horse_maneuver(44)|horse_charge(14)|horse_scale(102),imodbits_horse_basic|imodbit_champion],
["rhunhorseheav1","Rhun Armored Draft Horse",[("rhunhorseheav1",0)],itp_type_horse,0,2200,abundance(60)|hit_points(175)|body_armor(40)|difficulty(4)|horse_speed(41)|horse_maneuver(38)|horse_charge(30)|horse_scale(115),imodbits_horse_basic|imodbit_champion],
["rhunhorseheav2","Rhun Horned Armored Draft Horse",[("rhunhorseheav2",0)],itp_type_horse,0,2200,abundance(60)|hit_points(175)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(40)|horse_charge(30)|horse_scale(115),imodbits_horse_basic|imodbit_champion],
["rhunhorseheav3","Rhun Scaleclad Draft Horse",[("rhunhorseheav3",0)],itp_type_horse,0,2425,abundance(60)|hit_points(180)|body_armor(46)|difficulty(4)|horse_speed(38)|horse_maneuver(39)|horse_charge(33)|horse_scale(115),imodbits_horse_basic|imodbit_champion],
["rhunhorseheav4","Rhun Scaleclad Draft Horse",[("rhunhorseheav4",0)],itp_type_horse,0,2425,abundance(60)|hit_points(180)|body_armor(46)|difficulty(4)|horse_speed(38)|horse_maneuver(38)|horse_charge(35)|horse_scale(115),imodbits_horse_basic|imodbit_champion],
 
["rhun_horse_heavy1","Rhun Cataphract Charger",[("rhun_horse_heavy1",0)],itp_type_horse,0,2875,abundance(60)|hit_points(170)|body_armor(56)|difficulty(4)|horse_speed(40)|horse_maneuver(41)|horse_charge(40)|horse_scale(112),imodbits_horse_basic|imodbit_champion],
["rhun_horse_heavy2","Rhun Cataphract Charger",[("rhun_horse_heavy2",0)],itp_type_horse,0,2875,abundance(60)|hit_points(170)|body_armor(54)|difficulty(4)|horse_speed(41)|horse_maneuver(42)|horse_charge(38)|horse_scale(112),imodbits_horse_basic|imodbit_champion],
 
["mordor_warhorse01","Morgulbred Warhorse",[("mordor_warhorse01",0)],itp_type_horse,0,2450,abundance(60)|hit_points(165)|body_armor(42)|difficulty(4)|horse_speed(44)|horse_maneuver(40)|horse_charge(36)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
["mordor_warhorse02","Morgulbred Charger",[("mordor_warhorse02",0)],itp_type_horse,0,3125,abundance(60)|hit_points(180)|body_armor(60)|difficulty(4)|horse_speed(40)|horse_maneuver(38)|horse_charge(45)|horse_scale(112),imodbits_horse_basic|imodbit_champion],

["warg_1d","Warg", [("warg_1D",0)], itp_merchandise|itp_type_horse, 0, 1025,abundance(90)|hit_points(130)|body_armor(14)|difficulty(10)|horse_speed(37)|horse_maneuver(46)|horse_charge(16)|horse_scale(100),imodbits_horse_basic],
["warg_1c","Warg", [("warg_1C",0)], itp_merchandise|itp_type_horse, 0, 1025,abundance(90)|hit_points(130)|body_armor(14)|difficulty(10)|horse_speed(37)|horse_maneuver(46)|horse_charge(16)|horse_scale(100),imodbits_horse_basic],
["warg_1b","Large Warg", [("warg_1B",0)], itp_merchandise|itp_type_horse, 0, 1350,abundance(90)|hit_points(140)|body_armor(20)|difficulty(10)|horse_speed(35)|horse_maneuver(45)|horse_charge(22)|horse_scale(108),imodbits_horse_basic],
["wargarmored_1b","Armoured Warg", [("wargArmored_1B",0)], itp_merchandise|itp_type_horse, 0, 1400,abundance(90)|hit_points(130)|body_armor(25)|difficulty(10)|horse_speed(37)|horse_maneuver(46)|horse_charge(20)|horse_scale(100),imodbits_horse_basic],
["wargarmored_1c","Armoured Warg", [("wargArmored_1C",0)], itp_merchandise|itp_type_horse, 0, 1400,abundance(90)|hit_points(130)|body_armor(25)|difficulty(10)|horse_speed(37)|horse_maneuver(46)|horse_charge(20)|horse_scale(100),imodbits_horse_basic],
["wargarmored_2b","Armoured Warg", [("wargArmored_2B",0)], itp_merchandise|itp_type_horse, 0, 1400,abundance(90)|hit_points(130)|body_armor(25)|difficulty(10)|horse_speed(37)|horse_maneuver(46)|horse_charge(20)|horse_scale(100),imodbits_horse_basic],
["wargarmored_2c","Armoured Warg", [("wargArmored_2C",0)], itp_merchandise|itp_type_horse, 0, 1400,abundance(90)|hit_points(130)|body_armor(25)|difficulty(10)|horse_speed(37)|horse_maneuver(46)|horse_charge(20)|horse_scale(100),imodbits_horse_basic],
["wargarmored_3a","Armoured Warg", [("wargArmored_3A",0)], itp_merchandise|itp_type_horse, 0, 1400,abundance(90)|hit_points(130)|body_armor(25)|difficulty(10)|horse_speed(37)|horse_maneuver(46)|horse_charge(20)|horse_scale(100),imodbits_horse_basic],
["wargarmored_huge","Large Armoured Warg", [("wargArmored_huge",0)], itp_merchandise|itp_type_horse, 0, 1525,abundance(90)|hit_points(150)|body_armor(25)|difficulty(10)|horse_speed(35)|horse_maneuver(43)|horse_charge(24)|horse_scale(118),imodbits_horse_basic], 

## TLD horses end ##
 
### TLD Items end ###

### Jack OSP weapons ###

["jack_anduril","Anduril",[("jack_anduril",0),("jack_anduril_scab",ixmesh_carry)],itp_type_two_handed_wpn|itp_primary,itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1325,weight(2)|difficulty(0)|spd_rtng(98)|weapon_length(112)|swing_damage(44,cut)|thrust_damage(36,pierce),imodbits_sword],
["jack_glamdring","Glamdring",[("jack_glamdring",0),("jack_glamdring_scab",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,950,weight(1.5)|difficulty(0)|spd_rtng(102)|weapon_length(99)|swing_damage(40,cut)|thrust_damage(35,pierce),imodbits_sword],
["jack_herugrim","Herugrim",[("jack_herugrim",0),("jack_herugrim_scab",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1075,weight(1.25)|difficulty(0)|spd_rtng(100)|weapon_length(88)|swing_damage(41,cut)|thrust_damage(31,pierce),imodbits_sword],
["jack_boromir","Boromir's Sword",[("jack_boromir",0),("jack_boromir_scab",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1000,weight(1.25)|difficulty(0)|spd_rtng(101)|weapon_length(85)|swing_damage(38,cut)|thrust_damage(32,pierce),imodbits_sword],
["jack_faramir","Faramir's Sword",[("jack_faramir",0),("jack_faramir_scab",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,925,weight(1.25)|difficulty(0)|spd_rtng(99)|weapon_length(87)|swing_damage(35,cut)|thrust_damage(31,pierce),imodbits_sword],
["jack_sting","Sting",[("jack_sting",0),("jack_sting_scab",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,725,weight(0.75)|difficulty(0)|spd_rtng(110)|weapon_length(55)|swing_damage(39,cut)|thrust_damage(32,pierce),imodbits_sword],

### Jack OSP weapons end ###

### Kendor Numenor items ###
 
# Helmets
 
["num_archer_helmet","Numenorean Archer Helmet",[("num_archer_helmet",0)],itp_type_head_armor,0,1300,weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["num_inf_helmet","Numenorean Footman Helmet",[("num_inf_helmet",0)],itp_type_head_armor,0,1450,weight(1.25)|abundance(100)|head_armor(29)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["num_heavy_inf_helmet","Numenorean Foot Soldier Helmet",[("num_heavy_inf_helmet",0)],itp_type_head_armor,0,1550,weight(1.5)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["num_king_helmet","Numenorean Sergeant Helmet",[("num_king_helmet",0)],itp_type_head_armor,0,1700,weight(1.5)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["num_king_guard_helmet","Numenorean King's Guard Helmet",[("num_king_guard_helmet",0)],itp_type_head_armor,0,1850,weight(1.5)|abundance(100)|head_armor(37)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["num_edain_guard_helmet","Numenorean Edain Guard Helmet",[("num_edain_guard_helmet",0)],itp_type_head_armor,0,1950,weight(1.5)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
["num_high_king_helmet","Helmet of the High King",[("num_high_king_helmet",0)],itp_type_head_armor,0,1950,weight(1.5)|abundance(100)|head_armor(39)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate],
 
# Body armor
 
["num_light_inf_armour","Numenorean Hauberk over Mail",[("num_light_inf_armour",0)],itp_type_body_armor|itp_covers_legs,0,3955,weight(12)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(13),imodbits_armor],
["num_sergeant_inf_arn","Numenorean Footman Armor",[("num_sergeant_inf_arn",0)],itp_type_body_armor|itp_covers_legs,0,5425,weight(24)|abundance(100)|head_armor(0)|body_armor(49)|leg_armor(15),imodbits_armor],
["num_sergeant_inf_gon","Numenorean Footman Armor",[("num_sergeant_inf_gon",0)],itp_type_body_armor|itp_covers_legs,0,5425,weight(24)|abundance(100)|head_armor(0)|body_armor(49)|leg_armor(15),imodbits_armor],
["num_heavy_inf_arn","Numenorean Shock Infantry Armor",[("num_heavy_inf_arn",0)],itp_type_body_armor|itp_covers_legs,0,5725,weight(26)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(15),imodbits_armor],
["num_heavy_inf_gon","Numenorean Shock Infantry Armor",[("num_heavy_inf_gon",0)],itp_type_body_armor|itp_covers_legs,0,5725,weight(26)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(15),imodbits_armor],
["num_edain_guard_armour","Numenorean Edain Guard Armor",[("num_edain_guard_armour",0)],itp_type_body_armor|itp_covers_legs,0,6295,weight(30)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(17),imodbits_armor],
["num_high_king_armour","Breastplate of the High King",[("num_high_king_armour",0)],itp_type_body_armor|itp_covers_legs,0,6630,weight(30)|abundance(100)|head_armor(0)|body_armor(60)|leg_armor(18),imodbits_armor],
 
# Boots
 
["num_light_boots","Numenorean Mail Boots",[("num_light_boots",0)],itp_type_foot_armor|itp_attach_armature,0,1085,weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(31)|difficulty(0),imodbits_armor],
["num_heavy_boots","Numenorean Plated Boots",[("num_heavy_boots",0)],itp_type_foot_armor|itp_attach_armature,0,1225,weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(35)|difficulty(0),imodbits_armor],
 
# Gauntlets
 
["num_light_gloves","Numenorean Mail Gauntlets",[("num_light_glove_L",0)],itp_type_hand_armor,0,450, weight(0.75)|abundance(100)|body_armor(3)|difficulty(0),imodbits_armor],
["num_heavy_gloves","Numenorean Plated Gauntlets",[("num_heavy_glove_L",0)],itp_type_hand_armor,0,600, weight(1.25)|abundance(100)|body_armor(4)|difficulty(0),imodbits_armor],
["num_king_gloves","Numenorean Royal Gauntlets",[("num_king_glove_L",0)],itp_type_hand_armor,0,600, weight(1.25)|abundance(100)|body_armor(4)|difficulty(0),imodbits_armor],
 
# Weapons
 
["num_range_sword","Numenorean Ranger's Sword",[("num_range_sword",0),("num_range_sword_scabbard",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,850,weight(1.5)|difficulty(0)|spd_rtng(100)|weapon_length(94)|swing_damage(28,cut)|thrust_damage(27,pierce),imodbits_sword],
["num_inf_sword","Numenorean Footman's Sword",[("num_inf_sword",0),("num_inf_sword_scabbard",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,900,weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(94)|swing_damage(30,cut)|thrust_damage(27,pierce),imodbits_sword],
["num_long_sword","Numenorean Longsword",[("num_long_sword",0),("num_long_sword_scabbard",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,975,weight(1.75)|difficulty(0)|spd_rtng(97)|weapon_length(104)|swing_damage(31,cut)|thrust_damage(27,pierce),imodbits_sword],
["num_high_sword","Numenorean Decorated Sword",[("num_high_sword",0),("num_high_sword_scabbard",ixmesh_carry)],itp_type_one_handed_wpn|itp_primary,itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,1000,weight(1.75)|difficulty(0)|spd_rtng(97)|weapon_length(94)|swing_damage(34,cut)|thrust_damage(29,pierce),imodbits_sword],
 
["num_bastard_sword","Numenorean Bastard Sword",[("num_bastard_sword",0),("num_bastard_sword_scabbard",ixmesh_carry)],itp_type_two_handed_wpn|itp_primary,itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,975,weight(3)|difficulty(0)|spd_rtng(95)|weapon_length(104)|swing_damage(35,cut)|thrust_damage(31,pierce),imodbits_sword],
 
["num_inf_great_sword","Numenorean Sword of War",[("num_inf_great_sword",0),("num_inf_great_sword_scabbard",ixmesh_carry)],itp_type_two_handed_wpn|itp_two_handed|itp_primary,itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,1275,weight(5)|difficulty(0)|spd_rtng(94)|weapon_length(128)|swing_damage(40,cut)|thrust_damage(30,pierce),imodbits_sword_high],
["num_high_great_sword","Numenorean Decorated Greatsword",[("num_high_great_sword",0),("num_high_great_sword_scabbard",ixmesh_carry)],itp_type_two_handed_wpn|itp_two_handed|itp_primary,itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,1400,weight(5)|difficulty(0)|spd_rtng(94)|weapon_length(128)|swing_damage(44,cut)|thrust_damage(32,pierce),imodbits_sword_high],
 
["num_war_spear","Numenorean Warspear",[("num_war_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_cant_use_on_horseback|itp_wooden_parry|itp_no_blur,itc_staff_overhead_stab|itcf_carry_spear,1450,weight(5)|difficulty(0)|spd_rtng(92)|weapon_length(204)|swing_damage(24,blunt)|thrust_damage(32,pierce),imodbits_polearm],
["num_high_war_spear","Numenorean Elite Warspear",[("num_high_war_spear",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_cant_use_on_horseback|itp_wooden_parry|itp_no_blur,itc_staff_overhead_stab|itcf_carry_spear,1525,weight(5)|difficulty(0)|spd_rtng(91)|weapon_length(204)|swing_damage(25,blunt)|thrust_damage(34,pierce),imodbits_polearm],
["num_banner","Numenorean Banner",[("num_banner",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_cant_use_on_horseback|itp_wooden_parry|itp_no_blur,itc_staff_overhead_stab|itcf_carry_spear,1250,weight(5)|difficulty(0)|spd_rtng(88)|weapon_length(204)|swing_damage(24,blunt)|thrust_damage(27,pierce),imodbits_polearm],
["num_banner_arn","Numenorean Banner",[("num_banner_arn",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_cant_use_on_horseback|itp_wooden_parry|itp_no_blur,itc_staff_overhead_stab|itcf_carry_spear,1250,weight(5)|difficulty(0)|spd_rtng(88)|weapon_length(204)|swing_damage(24,blunt)|thrust_damage(27,pierce),imodbits_polearm],
["num_banner_gon","Numenorean Banner",[("num_banner_gon",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_two_handed|itp_cant_use_on_horseback|itp_wooden_parry|itp_no_blur,itc_staff_overhead_stab|itcf_carry_spear,1250,weight(5)|difficulty(0)|spd_rtng(88)|weapon_length(204)|swing_damage(24,blunt)|thrust_damage(27,pierce),imodbits_polearm],
 
["num_hollow_steel_bow","Hollowsteel Bow",[("num_hollow_steel_bow",0),("num_hollow_steel_bow_carry",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed,itcf_shoot_bow,1400,weight(4)|difficulty(3)|spd_rtng(80)|shoot_speed(64)|thrust_damage(25,pierce),imodbits_bow],
 
["num_jarids","Numenorean Throwing Spears",[("num_jarid",0),("num_jarid_quiver",ixmesh_carry)],itp_type_thrown|itp_primary|itp_next_item_as_melee,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn,1125,weight(3.5)|difficulty(1)|spd_rtng(89)|shoot_speed(24)|thrust_damage(45,pierce)|max_ammo(10)|weapon_length(70),imodbits_thrown],
["num_jarids_melee","Numenorean Throwing Spear",[("num_jarid",0),("num_jarid_quiver",ixmesh_carry)],itp_type_polearm|itp_primary|itp_wooden_parry,itc_staff_overhead_shield,1125,weight(3.5)|difficulty(0)|spd_rtng(93)|swing_damage(24,cut)|thrust_damage(27,pierce)|weapon_length(70),imodbits_thrown],
 
["num_arrows","Numenorean Arrows",[("num_arrow",0),("num_arrow_flying",ixmesh_flying_ammo),("num_arrow_quiver",ixmesh_carry)],itp_type_arrows,itcf_carry_quiver_back_right,200,weight(4)|abundance(70)|weapon_length(104)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile],
 
# Shields
 
["num_heavy_shield","Numenorean Heavy Kite Shield",[("num_heavy_shield",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,1275,weight(3.5)|hit_points(440)|body_armor(12)|spd_rtng(82)|shield_width(60)|shield_height(120),imodbits_shield],
["num_heavy_shield_arn","Numenorean Heavy Kite Shield",[("num_heavy_shield_arn",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,1275,weight(3.5)|hit_points(440)|body_armor(12)|spd_rtng(82)|shield_width(60)|shield_height(120),imodbits_shield],
["num_heavy_shield_gon","Numenorean Heavy Kite Shield",[("num_heavy_shield_gon",0)],itp_type_shield|itp_wooden_parry,itcf_carry_kite_shield,1275,weight(3.5)|hit_points(440)|body_armor(12)|spd_rtng(82)|shield_width(60)|shield_height(120),imodbits_shield],

#############################
###FULL INVASION ITEMS END###
#############################

#### DEV GEAR ####															

   ### Tak ###
   ["white_savior_helm","Protector_Helmet", [("protector_holy_light", 0)], itp_type_head_armor|itp_merchandise|itp_covers_head, 0,3250, weight(2.5)|abundance(100)|head_armor(65)|difficulty(10), imodbits_armor|imodbit_cracked, []],															
   ["white_savior_armor","Protector_Armour", [("protector_holy_light_armor", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_doesnt_cover_hair, 0,7025, weight(17.5)|abundance(100)|head_armor(10)|body_armor(65)|leg_armor(15)|difficulty(8), imodbits_armor|imodbit_cracked, []],															
   ["white_savior_boot","Protector_Greaves", [("protector_holy_lightboot", 0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0,1505, weight(3.5)|abundance(100)|leg_armor(43)|difficulty(9), imodbits_armor, []],															
   ["white_savior_sword","Protector_Holy_Sword", [("protector_holy_lightj", 0),("white_savior_scabbed", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itcf_carry_sword_left_hip|itc_bastardsword|itcf_show_holster_when_drawn,1775, weight(2.25)|abundance(100)|difficulty(12)|spd_rtng(97)|weapon_length(145)|thrust_damage(37, pierce)|swing_damage(47, cut), imodbits_sword|imodbit_masterwork, []],															

   ### Ashley ###															
   ["ashley_ice_cream","Ashley's Ice Cream", [("ashley_ice_cream", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_crush_through|itp_can_knock_down|itp_wooden_attack|itp_wooden_parry, itcf_carry_sword_left_hip|itc_scimitar, 100000, weight(2)|abundance(100)|spd_rtng(105)|weapon_length(124)|thrust_damage(0, pierce)|swing_damage(40, blunt), imodbits_mace, []],															
   ["ashley_oreo","Ashley's Oreo", [("ashley_oreo", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,100000, weight(3)|abundance(100)|body_armor(50)|hit_points(750)|spd_rtng(95)|shield_width(83), imodbits_shield, []],															
   ["ashley_teddy_bears","Ashley's Teddy Bears", [("ashley_throwable_bear", 0)], itp_type_thrown|itp_merchandise|itp_primary, itcf_throw_axe,100000, weight(0.5)|abundance(100)|spd_rtng(110)|shoot_speed(45)|max_ammo(30)|thrust_damage(20, blunt), imodbits_missile|imodbit_balanced|imodbit_heavy, []],															
   ["ashley_armour","Ashley's Armour", [("ashley_misidelong", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,100000, weight(7.5)|abundance(100)|body_armor(80)|leg_armor(15), imodbits_armor, []],															
   ["tiara","Tiara", [("tiara", 0)], itp_type_head_armor|itp_merchandise|itp_covers_legs|itp_doesnt_cover_hair|itp_fit_to_head, 0,300, weight(0.5)|abundance(100)|head_armor(6)|difficulty(7), imodbits_armor|imodbit_cracked, []],															
   ["ashley_boots","Ashley's_Boots", [("rivendell_boots", 0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0,560, weight(1.25)|abundance(100)|leg_armor(16), imodbits_cloth, []],															
   
   ### Borridian ###															
   ["borridian_bladed_bow","Borridian's Bladed Bow", [("borridian_bladed_bow", 0),("invalid_item", ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary|itp_next_item_as_melee, itcf_shoot_bow|itcf_carry_bow_back,100000, weight(1.5)|abundance(100)|accuracy(200)|spd_rtng(120)|shoot_speed(100)|thrust_damage(80, pierce), imodbits_bow, []],															
   ["borridian_bladed_bow_melee","Borridian's Bladed Bow", [("borridian_bladed_bow", 0)], itp_type_two_handed_wpn|itp_primary|itp_bonus_against_shield|itp_no_blur, itc_nodachi|itcf_overswing_onehanded|itcf_slashright_onehanded|itcf_slashleft_onehanded,100000, weight(1)|abundance(100)|spd_rtng(130)|weapon_length(140)|swing_damage(40, cut), imodbits_missile|imodbit_balanced, []],															
   ["borridian_black_rivendell","Borridian's Armour", [("borridian_long_coat", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,100000, weight(5)|abundance(100)|body_armor(60)|leg_armor(10), imodbits_armor, []],															
   ["borridian_gloves","Borridian's Black Gloves", [("borridian_black_glove_L", 0)], itp_type_hand_armor|itp_merchandise, 0,100000, weight(1.5)|abundance(100)|body_armor(10), imodbits_armor, []],															
   ["borridian_black_easterling_boot","Borridian's Black Boots", [("borridian_black_boots", 0)], itp_type_foot_armor|itp_attach_armature, 0,10000, weight(3.5)|abundance(100)|leg_armor(30), imodbits_armor, []],															
   ["borridian_wabbajack", "Borridian's Wabbajack", [("wabbajack",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_cant_use_on_horseback|itp_no_pick_up_from_ground, itc_staff|itcf_carry_sword_back,100000,weight(2)|spd_rtng(97)|weapon_length(133)|swing_damage(40,blunt)|thrust_damage(30, blunt),imodbits_polearm ],															
   
   ### usnavy ###															
   ["usnavy_broadsword","usnavy's Broadsword", [("spanish_cavalry_broadsword", 0),("spanish_cavalry_broadsword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itcf_carry_sword_left_hip|itc_longsword|itcf_show_holster_when_drawn,235, weight(2.25)|abundance(100)|spd_rtng(105)|weapon_length(107)|thrust_damage(35, pierce)|swing_damage(50, cut), imodbits_sword|imodbit_masterwork, []],															
   ["usnavy_officer_uniform","usnavy's Officer Uniform", [("usnavy_officer", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,100000, weight(1)|abundance(100)|body_armor(40)|leg_armor(5), imodbits_cloth, []],															
   ["usnavy_white_boots","usnavy's White Boots", [("usnavy_white_boots", 0)], itp_type_foot_armor|itp_attach_armature, 0,10000, weight(0.5)|abundance(100)|leg_armor(10), imodbits_cloth, []],															
   
   ### Maroon ###															
   ["maroon_brisingr","Brisingr", [("maroon_brisingr", 0),("maroon_brisingr_scab", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itcf_carry_sword_back|itc_bastardsword|itcf_show_holster_when_drawn,100000, weight(5)|abundance(100)|spd_rtng(102)|weapon_length(125)|thrust_damage(50, pierce)|swing_damage(80, cut), imodbits_sword|imodbit_masterwork, []],															
   ["maroon_hand_crossbow_b","Hand_Crossbow", [("maroon_hand_crossbow_b", 0)], itp_type_crossbow|itp_merchandise|itp_primary, itcf_shoot_pistol|itcf_carry_pistol_front_left|itcf_reload_pistol,900, weight(1)|abundance(100)|accuracy(120)|spd_rtng(85)|shoot_speed(60)|max_ammo(1)|thrust_damage(34, pierce), imodbits_crossbow, []],															
   ["maroon_small_bolt","Pocket_Bolts", [("invalid_mesh", 0),("maroon_small_bolt", ixmesh_inventory),("maroon_small_bolt", ixmesh_flying_ammo),("invalid_item", ixmesh_carry)], itp_type_bolts|itp_merchandise|itp_covers_legs|itp_doesnt_cover_hair, itcf_carry_quiver_right_vertical,1200, weight(2.5)|abundance(20)|weapon_length(63)|max_ammo(29)|thrust_damage(24, pierce), imodbits_missile, []],															
   ["maroon_aegis_shield","Maroon's Aegis Shield", [("maroon_aegis_shield", 0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield,100000, weight(5)|abundance(100)|body_armor(70)|hit_points(2000)|spd_rtng(88)|shield_width(68), imodbits_shield, []],															
   ["maroon_twilight","Twilight", [("maroon_friesian_horse", 0)], itp_type_horse|itp_merchandise, 0,100000, weight(0)|abundance(60)|body_armor(50)|difficulty(7)|hit_points(1000)|horse_maneuver(45)|horse_speed(50)|horse_charge(45)|horse_scale(115), imodbits_horse_basic|imodbit_champion, []],															
   ["maroon_armour","Maroon's Armour", [("maroon_early_transitional_black", 0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0,100000, weight(15)|abundance(100)|body_armor(90)|leg_armor(20), imodbits_armor, []],															
   ["maroon_helmet","Maroon's Helmet", [("maroon_flemish_armet", 0)], itp_type_head_armor|itp_covers_head, 0,100000, weight(3)|abundance(100)|head_armor(70), imodbits_armor|imodbit_cracked, []],															
   
#DUMMY SHIELDS															
  #["tab_shield_straw_b_dummy","Plain_Wicker_Shield", [("tableau_shield_straw_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_board_shield,80, weight(1)|abundance(100)|body_armor(3)|hit_points(215)|spd_rtng(90)|shield_height(100)|shield_width(50), imodbits_shield],															
  #["tab_shield_straw_c_dummy","Wicker_Shield", [("tableau_shield_straw_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_board_shield,100, weight(1)|abundance(100)|body_armor(4)|hit_points(230)|spd_rtng(90)|shield_height(100)|shield_width(50), imodbits_shield],															
  #["tab_shield_square_a_dummy","Old_Square_Shield", [("tableau_shield_square_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,26, weight(2.5)|abundance(100)|body_armor(5)|hit_points(200)|spd_rtng(93)|shield_width(50), imodbits_shield],															
  #["tab_shield_square_b_dummy","Old_Square_Shield", [("tableau_shield_square_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,30, weight(2.5)|abundance(100)|body_armor(6)|hit_points(215)|spd_rtng(93)|shield_width(50), imodbits_shield],															
  ["tab_shield_round_a_dummy","Old_Round_Shield", [("tableau_shield_round_5", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,26, weight(2.5)|abundance(100)|body_armor(4)|hit_points(195)|spd_rtng(93)|shield_width(50), imodbits_shield],															
  ["tab_shield_round_b_dummy","Plain_Round_Shield", [("tableau_shield_round_3", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,65, weight(3)|abundance(100)|body_armor(8)|hit_points(260)|spd_rtng(90)|shield_width(50), imodbits_shield],															
  ["tab_shield_round_c_dummy","Round_Shield", [("tableau_shield_round_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,105, weight(3.5)|abundance(100)|body_armor(12)|hit_points(310)|spd_rtng(87)|shield_width(50), imodbits_shield],															
  ["tab_shield_round_d_dummy","Heavy_Round_Shield", [("tableau_shield_round_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,210, weight(4)|abundance(100)|body_armor(15)|hit_points(350)|spd_rtng(84)|shield_width(50), imodbits_shield],															
  ["tab_shield_round_e_dummy","Huscarl_Round_Shield", [("tableau_shield_round_4", 0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield,430, weight(4.5)|abundance(100)|body_armor(19)|hit_points(410)|spd_rtng(81)|shield_width(50), imodbits_shield],															
  ["tab_shield_kite_a_dummy","Old_Kite_Shield", [("tableau_shield_kite_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,33, weight(2)|abundance(100)|body_armor(5)|hit_points(165)|spd_rtng(96)|shield_height(70)|shield_width(36), imodbits_shield],															
  ["tab_shield_kite_b_dummy","Plain_Kite_Shield", [("tableau_shield_kite_3", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,70, weight(2.5)|abundance(100)|body_armor(10)|hit_points(215)|spd_rtng(93)|shield_height(70)|shield_width(36), imodbits_shield],															
  ["tab_shield_kite_c_dummy","Kite_Shield", [("tableau_shield_kite_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,156, weight(3)|abundance(100)|body_armor(13)|hit_points(265)|spd_rtng(90)|shield_height(70)|shield_width(36), imodbits_shield],															
  ["tab_shield_kite_d_dummy","Heavy_Kite_Shield", [("tableau_shield_kite_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,320, weight(3.5)|abundance(100)|body_armor(18)|hit_points(310)|spd_rtng(87)|shield_height(70)|shield_width(36), imodbits_shield],															
  ["tab_shield_kite_cav_a_dummy","Horseman_Kite_Shield", [("tableau_shield_kite_4", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,205, weight(2)|abundance(100)|body_armor(14)|hit_points(165)|spd_rtng(103)|shield_height(50)|shield_width(30), imodbits_shield],															
  ["tab_shield_kite_cav_b_dummy","Knightly_Kite_Shield", [("tableau_shield_kite_4", 0)], itp_type_shield|itp_merchandise, itcf_carry_kite_shield,360, weight(2.5)|abundance(100)|body_armor(23)|hit_points(225)|spd_rtng(100)|shield_height(50)|shield_width(30), imodbits_shield],															
  ["tab_shield_heater_a_dummy","Old_Heater_Shield", [("tableau_shield_heater_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,36, weight(2)|abundance(100)|body_armor(6)|hit_points(160)|spd_rtng(96)|shield_height(70)|shield_width(36), imodbits_shield],															
  ["tab_shield_heater_b_dummy","Plain_Heater_Shield", [("tableau_shield_heater_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,74, weight(2.5)|abundance(100)|body_armor(11)|hit_points(210)|spd_rtng(93)|shield_height(70)|shield_width(36), imodbits_shield],															
  ["tab_shield_heater_c_dummy","Heater_Shield", [("tableau_shield_heater_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,160, weight(3)|abundance(100)|body_armor(14)|hit_points(260)|spd_rtng(90)|shield_height(70)|shield_width(36), imodbits_shield],															
  ["tab_shield_heater_d_dummy","Heavy_Heater_Shield", [("tableau_shield_heater_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,332, weight(3.5)|abundance(100)|body_armor(19)|hit_points(305)|spd_rtng(87)|shield_height(70)|shield_width(36), imodbits_shield],															
  ["tab_shield_heater_cav_a_dummy","Horseman_Heater_Shield", [("tableau_shield_heater_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,229, weight(2)|abundance(100)|body_armor(16)|hit_points(160)|spd_rtng(103)|shield_height(50)|shield_width(30), imodbits_shield],															
  ["tab_shield_heater_cav_b_dummy","Knightly_Heater_Shield", [("tableau_shield_heater_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield,390, weight(2.5)|abundance(100)|body_armor(23)|hit_points(220)|spd_rtng(100)|shield_height(50)|shield_width(30), imodbits_shield],															
  ["tab_shield_pavise_a_dummy","Old_Board_Shield", [("tableau_shield_pavise_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry|itp_cant_use_on_horseback, itcf_carry_board_shield,60, weight(3.5)|abundance(100)|body_armor(4)|hit_points(280)|spd_rtng(89)|shield_height(100)|shield_width(43), imodbits_shield],															
  ["tab_shield_pavise_b_dummy","Plain_Board_Shield", [("tableau_shield_pavise_2", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry|itp_cant_use_on_horseback, itcf_carry_board_shield,114, weight(4)|abundance(100)|body_armor(8)|hit_points(360)|spd_rtng(85)|shield_height(100)|shield_width(43), imodbits_shield],															
  ["tab_shield_pavise_c_dummy","Board_Shield", [("tableau_shield_pavise_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry|itp_cant_use_on_horseback, itcf_carry_board_shield,210, weight(4.5)|abundance(100)|body_armor(10)|hit_points(430)|spd_rtng(81)|shield_height(100)|shield_width(43), imodbits_shield],															
  ["tab_shield_pavise_d_dummy","Heavy_Board_Shield", [("tableau_shield_pavise_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry|itp_cant_use_on_horseback, itcf_carry_board_shield,370, weight(5)|abundance(100)|body_armor(14)|hit_points(550)|spd_rtng(78)|shield_height(100)|shield_width(43), imodbits_shield],															
  ["tab_shield_small_round_a_dummy","Plain_Cavalry_Shield", [("tableau_shield_small_round_3", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,96, weight(2)|abundance(100)|body_armor(8)|hit_points(160)|spd_rtng(105)|shield_width(40), imodbits_shield],															
  ["tab_shield_small_round_b_dummy","Round_Cavalry_Shield", [("tableau_shield_small_round_1", 0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield,195, weight(2.5)|abundance(100)|body_armor(14)|hit_points(200)|spd_rtng(103)|shield_width(40), imodbits_shield],															
  ["tab_shield_small_round_c_dummy","Elite_Shield", [("tableau_shield_small_round_2", 0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield,370, weight(3)|abundance(100)|body_armor(22)|hit_points(250)|spd_rtng(100)|shield_width(40), imodbits_shield],															

["items_end", "Items End", [("shield_round_a",0)], 0, 0, 1, 0, 0],
]
