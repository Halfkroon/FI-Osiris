import ItemLib

BASE_MODULE_ITEMS = "module_items 1.3.py"
MODULE_ITEMS = "module_items.py"
WEAPON_CHANGELOG = "weaponchangelist.txt"
ARMOR_CHANGELOG = "armorchangelist.txt"

CHANGE_DICT = dict()

CURRENT_ITEMS_DICT = dict()

COMMON_CHANGES = [
    "weight",
]
MELEE_CHANGES = [
    # common to all melee weapons
    "spd_rtng",
    "swing_damage",
    "thrust_damage",
]
RANGED_CHANGES = [
    # common to all ranged weapons
    "spd_rtng",
    "thrust_damage",
    "shoot_speed",
    "accuracy",
    "max_ammo",
]
THROWN_CHANGES = [
    # **** use ranged changes, should be same from excel formulas ****
]
AMMO_CHANGES = [
    "thrust_damage",
    "max_ammo",
]
ARMOR_CHANGES = [
    # **** not changing armor values atm, only weight ****
    # common to all armors
    # "head_armor",
    # "body_armor",
    # "leg_armor",
]
ITEM_TYPES = [
    "MELEE",
    "RANGED",
    "THROWN",
    "AMMO",
    "ARMOR",
    "ALTRAPIER",
]
ITEM_ANIMS_CANNOT_STAB = [
    "itc_cleaver",
    "itc_scimitar",
    "itc_nodachi",
    "itc_morningstar",
    "itc_slashing_polearm",
]
ITEM_ANIMS_CANNOT_SWING = [
    "itc_pike",
    "itc_greatlance",
]

def Main():
    global CURRENT_ITEMS_DICT
    CURRENT_ITEMS_DICT = ItemLib.InitializeItemDictionary(BASE_MODULE_ITEMS)

    itemsFile = open(BASE_MODULE_ITEMS, 'r')
    writeFile = open(MODULE_ITEMS, 'w')
    changeDictionary = SetupChangeDict().copy()     # use copy to preserve original change dictionary lines for rapier alt mode parent change lines
    changesAttempted = 0

    itemLine = str("")  # stores the current item entry; can be in multiple lines
    itemID = str("")  # current item entry ID

    changeLine = str("")  # current changelist item; single line only

    for line in itemsFile:
        originalLine = line  # store original line to preserve formatting when rewriting module_items

        line = line.strip()
        hasBeginning = ItemLib.CheckLineStart(itemLine)

        if (hasBeginning is False):
            if (ItemLib.CheckLineStart(line)):

                itemLine = line
                itemID = ItemLib.GetItemID(itemLine)
                if (itemID is None):
                    print("ITEM ID ERROR")
                    return False

                changeLine = changeDictionary.get(itemID, None)
                if (changeLine == "ALTRAPIER"): changeLine = GetAltRapierChangeLine(itemID)
                if (changeLine is not None):
                    changesAttempted += 1
                    originalLine = SetItemValues(originalLine, changeLine)

        elif (hasBeginning is True):

            itemLine += line
            if (changeLine is not None): originalLine = SetItemValues(originalLine, changeLine)

        else:

            print("CASE ERROR: hasBeginning")
            return False

        if (ItemLib.CheckLineStart(itemLine) and ItemLib.CheckLineEnd(itemLine)):
            if (changeLine is not None): changeDictionary.pop(itemID)

            itemLine = str("")

        writeFile.write(originalLine)  # write to new module_items

    # end loop

    writeFile.close()

    # print change info & any errors
    print("CHANGES ATTEMPTED: " + str(changesAttempted))

    if (len(changeDictionary) > 0):
        print("CHECK MODULE_ITEMS & CHANGELOG FILE. ITEMS NOT FOUND:")
        print(changeDictionary.keys())  # print items in the changelist not found

    return True


def SetupChangeDict():
    AddChangeFile(WEAPON_CHANGELOG)
    AddChangeFile(ARMOR_CHANGELOG)

    return CHANGE_DICT


def SetItemValues(itemLine, changeLine):
    # get item type for change category
    itemType = ""
    for types in ITEM_TYPES:
        typeIndex = changeLine.rfind(types)
        if typeIndex == -1: continue
        else:
            itemType = changeLine[typeIndex : len(changeLine)]
            break

    checkSwingAndThrust = False

    if (itemType == "MELEE"):
        checkSwingAndThrust = True
        changeItemValueList = COMMON_CHANGES + MELEE_CHANGES

    elif (itemType == "ALTRAPIER"): changeItemValueList = COMMON_CHANGES + MELEE_CHANGES
    elif (itemType == "RANGED"): changeItemValueList = COMMON_CHANGES + RANGED_CHANGES
    elif (itemType == "THROWN"): changeItemValueList = COMMON_CHANGES + RANGED_CHANGES
    elif (itemType == "AMMO"): changeItemValueList = COMMON_CHANGES + AMMO_CHANGES
    elif (itemType == "ARMOR"): changeItemValueList = COMMON_CHANGES

    else:
        print("ERROR|ITEM TYPE CASE AS: " + str(itemType) + "|" + changeLine)
        return itemLine

    for itemValue in changeItemValueList:   # look for change strings in the changeline
        if (checkSwingAndThrust): itemLine = UpdateItemValue(itemLine, changeLine, itemValue, True)
        else: itemLine = UpdateItemValue(itemLine, changeLine, itemValue)  # make changes

    return itemLine


def UpdateItemValue(itemLine, changeLine, valueString, checkSwingAndThrust = False):
    DELIM_CHANGELINE = "!"  # deliminator for change list
    DELIM_ITEMLINE = ")"  # not really a deliminator; finds closing parenthesis in module_items

    # get item value from change list
    changeLineBegin = changeLine.find(valueString)
    changeLineEnd = changeLine.find(DELIM_CHANGELINE, changeLineBegin)
    if (changeLineBegin == -1 or changeLineEnd == -1): return itemLine
    changeLineValue = changeLine[changeLineBegin: changeLineEnd]    # uses full string between ! deliminator

    if (checkSwingAndThrust):
        changeLineValue = CheckMeleeCanSwingAndStab(itemLine, changeLineValue)    # check if weapon can swing or thrust & has right anims to do so

    # get indices of original item value to change
    itemLineBegin = itemLine.find(valueString)
    itemLineEnd = itemLine.find(DELIM_ITEMLINE, itemLineBegin) + 1  # add 1 to index to replace closing parenthesis
    if (itemLineBegin == -1 or itemLineEnd == -1): return itemLine

    newItemLine = Replace(itemLine, changeLineValue, itemLineBegin, itemLineEnd)
    return newItemLine


def GetAltRapierChangeLine(altMeleeID):
    # get parent item ID
    curItemsList = list(CURRENT_ITEMS_DICT.keys())
    altItemIndex = curItemsList.index(altMeleeID)
    parentItemIndex = altItemIndex - 1

    parentItemID = curItemsList[parentItemIndex]

    parentChangeLine = CHANGE_DICT.get(parentItemID, None)    # get parent melee weapon changes
    if (parentChangeLine is not None):
        swingBeginIndex = parentChangeLine.find("swing_damage(")
        swingEndIndex = parentChangeLine.find(")", swingBeginIndex)
        thrustBeginIndex = parentChangeLine.find("thrust_damage(") + 13
        thrustEndIndex = parentChangeLine.find(")", thrustBeginIndex)
        thrustValue = parentChangeLine[thrustBeginIndex : thrustEndIndex]
        parentChangeLine = Replace(parentChangeLine, thrustValue, swingBeginIndex+12, swingEndIndex)

        return parentChangeLine

    # build new change line from existing parent item stats
    parentItemStats = ItemLib.GetItemData(parentItemID, CURRENT_ITEMS_DICT, dict()).split(",")

    weight = parentItemStats[7]
    speed = parentItemStats[8]
    thrustDmg = parentItemStats[14]
    thrustType = parentItemStats[15]
    # set swing = thrust
    swingDmg = thrustDmg
    swingType = thrustType

    coreChanges = "weight(" + weight + ")!" + "spd_rtng(" + speed + ")!"

    swingChanges = ""
    if (swingDmg != "NONE" or swingType != "NONE"): swingChanges = "swing_damage(" + swingDmg + "," + swingType.casefold() + ")!"

    thrustChanges = ""
    if (thrustDmg != "NONE" or thrustType != "NONE"): thrustChanges = "thrust_damage(" + thrustDmg + "," + thrustType.casefold() + ")!"

    newChangeLine = coreChanges + swingChanges + thrustChanges + "MELEE"

    print("NO RAPIER PARENT FOUND. USING OLD RAPIER PARENT STATS: " + newChangeLine)
    return newChangeLine


def CheckMeleeCanSwingAndStab(itemLine, changeLineValue):
    isThrust = False
    valueBeginIndex = changeLineValue.find("swing_damage")  # if swing
    if (valueBeginIndex == -1):
        valueBeginIndex = changeLineValue.find("thrust_damage")    # if thrust
        if (valueBeginIndex == -1): return changeLineValue  # not a damage value changeline
        isThrust = True

    valueBeginIndex = changeLineValue.find("(") + 1 # get actual damage value index for replacement
    valueEndIndex = changeLineValue.find(",", valueBeginIndex)

    setZero = False
    if (isThrust):
        for tag in ITEM_ANIMS_CANNOT_STAB:
            if (itemLine.find(tag) > 0):
                setZero = True
                break
    else:
        for tag in ITEM_ANIMS_CANNOT_SWING:
            if (itemLine.find(tag) > 0):
                setZero = True
                break

    if not (setZero): return changeLineValue

    changeLineValue = Replace(changeLineValue, "0", valueBeginIndex, valueEndIndex)
    return changeLineValue


def AddChangeFile(fileName):
    changeFile = open(fileName, 'r')
    changeCount = 0

    for line in changeFile:

        line = line.strip()  # format line
        if (line == "" or line == "NONE"): continue  # ignore empty line

        idBegin = line.find("_")
        idEnd = line.find("!")
        if (idBegin == -1 or idEnd == -1): continue  # ignore invalid lines
        itemID = line[idBegin + 1: idEnd]  # get id; strips itm_ prefix

        itemChanges = line[idEnd + 1: len(line)]
        CHANGE_DICT.setdefault(itemID, itemChanges)
        changeCount += 1

    print("CHANGES INPUTTED: " + str(changeCount) + "| FROM: " + str(fileName))


def Replace(originalLine, newValue, beginIndex, endIndex):
    beginString = str(originalLine)[0 : beginIndex]
    endString = str(originalLine)[endIndex : len(originalLine)]

    return beginString + str(newValue) + endString


# main loop
if (Main() is False):
    print("CHECK FILE")
