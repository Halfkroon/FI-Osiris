import ItemLib
import TroopLib

# global vars

SCRIPTS_FILE = "module_scripts 1.3.py"  # pull script data

TROOPS_FILE = "module_troops 1.3.py"   # pull troop data; defender classes beginning at 2978, ending at 5272
TROOPS_IMPLICITS_FILE = "troopimplicits.txt"   # pull spreadsheet troop implicits
TROOPS_BEGIN = 3027     # defender classes begin at this line
TROOPS_END = 5399   # defender classes end at this line

ITEMS_FILE = "module_items 1.3.py"  # pull item data
ITEMS_IMPLICITS_FILES = [    # pull spreadsheet item implicits files, as list object
    "weaponimplicitlist.txt",
    "armorimplicitlist.txt",
]

WRITE_FILE_ITEMS = "troop equipment.csv"
WRITE_FILE_TROOPS = "troop info.csv"

# ****HEADER ORDER NEEDS TO MATCH ItemLib.py & ClassLib.py STRING BUILD ORDER****
DELIM_HEADERS_ITEMS = "TROOP ID,TROOP TIER,ITEM ID,NAME,TYPE,TAGS,ADJECTIVE,TIER,CLASS,SWING DMG MOD,THRUST DMG MOD,LENGTH,WEIGHT,SPEED,SWING DAMAGE,SWING TYPE,THRUST DAMAGE,THRUST TYPE,ACCURACY,SHOOT SPEED,AMMO,HEAD ARMOR,BODY ARMOR,LEG ARMOR"
DELIM_HEADERS_TROOPS = "TROOP ID,NAME,FACTION,SUBFACTION,COST,TIER,TYPE,ADJECTIVES,STR,AGI,1H,2H,POLE,BOW,XBOW,THROW,GUN,IF,ATH,RIDE,HARC,SHIELD,PS,PD,PT"

AUTO_ASSIGN_ITEM_TIER = False
CHECK_TROOP_CAN_USE = True


def Main():

    itemImplicitsDict = ItemLib.InitializeItemImplicitIdentitiesDictionary(ITEMS_IMPLICITS_FILES)
    itemDictionary = ItemLib.InitializeItemDictionary(ITEMS_FILE)
    troopImplicitsDict = TroopLib.InitializeTroopImplicitIdentitiesDictionary(TROOPS_IMPLICITS_FILE)
    troopDictionary = TroopLib.InitializeTroopDictionary(TROOPS_FILE, TROOPS_BEGIN, TROOPS_END)

    scripts = open(SCRIPTS_FILE, 'r')
    troopEquipmentData = open(WRITE_FILE_ITEMS, 'w')
    troopClassData = open(WRITE_FILE_TROOPS, 'w')

    # header lines for .csv files
    troopEquipmentData.write(DELIM_HEADERS_ITEMS + "\n")
    troopClassData.write(DELIM_HEADERS_TROOPS + "\n")

    # script find tags
    TROOP_EQUIP_TAG = "(call_script, \"script_multiplayer_set_item_available_for_troop\""
    TROOP_COST_TAG1 = "(troop_set_slot,"
    TROOP_COST_TAG2 = "slot_troop_hero_cost,"

    # objects for handling item auto assign tiers
    troopEquipmentList = list()
    itemLevelDict = dict()

    # loop to get item data & troop costs
    # only works if (call_script.../troop_set_slot...) is a single line; will not work if the script is called in a multi-line format
    for line in scripts:

        line = line.strip()
        isTroopEquipLine = CheckScriptLine(line, TROOP_EQUIP_TAG)
        isHeroLine = CheckHeroLine(line)
        isTroopCostLine = CheckScriptLine(line, TROOP_COST_TAG1) and CheckScriptLine(line, TROOP_COST_TAG2)

        if (isTroopEquipLine and isTroopCostLine):  # handle unexpected occurrence
            print(line)
            print("ERROR: LINE HAS EQUIPMENT & COST")
            continue

        if (isTroopEquipLine or isHeroLine):

            troopID = GetLineItem(line, "trp_", ",")
            if (troopID is None): print ("ERROR: " + line)
            troopIDShort = troopID[4: len(troopID)]
            troopData = TroopLib.GetTroopData(troopIDShort, troopDictionary, troopImplicitsDict, True)
            if not (isinstance(troopData, int)):
                troopData = troopData.split(",")
                troopTier = troopData[4]
            else: print("TROOP DATA NOT FOUND FOR: " + str(troopIDShort) + "| CHECK TROOP FILE BEGIN & END LINES")

            itemID = GetLineItem(line, "itm_", "\"", False, 0)
            # check for errors iterating the scripts file
            if (itemID is not None and troopID is None):
                print(line)
                print("ERROR: FOUND TROOP; NO ITEM")
                continue
            if (itemID is None and troopID is not None):
                print(line)
                print("ERROR: NO TROOP; FOUND ITEM")
                continue

            # get item data & report any errors
            itemData = ItemLib.GetItemData(itemID, itemDictionary, itemImplicitsDict)
            if (itemData == -1 or itemData == -2 or itemData == -3):
                ItemErrorHandler(itemData, itemID)
                continue
            elif (itemData == -4):  # check misc items (horses)
                if (CHECK_TROOP_CAN_USE): CheckTroopCanUseItem(troopIDShort, troopDictionary, itemID, itemDictionary)
                continue

            # check if troop can use the item/horse
            if (CHECK_TROOP_CAN_USE): CheckTroopCanUseItem(troopIDShort, troopDictionary, itemID, itemDictionary)

            # auto assign item tier implicit value based on best/worst troop equipment options
            if (AUTO_ASSIGN_ITEM_TIER):

                troopTierDict = {value : key for key, value in TroopLib.TIER_DICT.items()}
                itemLevel = int(troopTierDict[troopTier]) + 1

                existingItemLevel = itemLevelDict.get(itemID, 0)
                if (existingItemLevel == 0): itemLevelDict.setdefault(itemID, itemLevel)

                elif (existingItemLevel >= 3):
                    if (existingItemLevel > itemLevel): itemLevelDict[itemID] = itemLevel   # downgrade elite/hero items to soldier/recruit
                elif (existingItemLevel <= 2):
                    if (existingItemLevel < itemLevel <= 2): itemLevelDict[itemID] = itemLevel  # upgrade recruit items to soldier

            # write to .csv files
            troopEquipmentString = str(troopID) + "," + str(troopTier) + "," + str(itemData) + "\n"
            troopEquipmentList.append(troopEquipmentString.split(","))

        # get troop cost
        if (isTroopCostLine):

            troopID = GetLineItem(line, "trp_", ",", True)
            troopID = troopID.replace("_loadout", "")  # in case of hero

            troopCost = GetLineItem(line, TROOP_COST_TAG2, "),", True, 0)
            troopCost = troopCost.replace(" ", "")  # take out spaces

            if (troopID is None or troopCost is None):  # handle unexpected occurrence
                print(line)
                print("ERROR: COULD NOT FIND TROOP ID OR TROOP COST")
                continue

            TroopLib.AddToTroopCostDict(troopID, troopCost)

    # loop to get troop data
    for troopID, troopLine in troopDictionary.items():

        # get troop data and report any errors
        troopData = TroopLib.GetTroopData(troopID, troopDictionary, troopImplicitsDict)
        if (troopData == -1):
            print("DICTIONARY ERROR: " + str(troopID))
            continue
        troopClassData.write(str(troopID) + "," + str(troopData) + "\n")

    # get misc items not captured in module_scripts find loop
    AddMiscItemsToWrite("NONE_BOX", ItemLib.GetBoxWeapons(), itemImplicitsDict, troopEquipmentList)
    AddMiscItemsToWrite("NONE_ALTMODE", ItemLib.GetAlternateModeWeapons(), itemImplicitsDict, troopEquipmentList)
    AddMiscItemsToWrite("NONE_LEGENDARY", ItemLib.GetLegendaryWeapons(), itemImplicitsDict, troopEquipmentList)

    itemDictAsList = list(itemDictionary.keys())
    for itemAsList in troopEquipmentList:
        if (AUTO_ASSIGN_ITEM_TIER):
            miscID = itemAsList[0]
            itemID = itemAsList[1]
            itemTier = itemAsList[6]

            if (itemTier == "NONE" or itemTier == "2"): # only change tier 2 items or items w/o tier
                itemLevel = itemLevelDict.get(itemID, "NONE")
                if (itemLevel == "NONE"):
                    # cases for alt mode & box weapons
                    if (miscID == "NONE_ALTMODE"):  # check if alt mode, and for parent item ID
                        itemIDIndex = itemDictAsList.index(itemID[4 : len(itemID)]) # need to remove itm_ tag for item list compatibility
                        parentItemID = itemDictAsList[itemIDIndex - 1]
                        itemLevel = itemLevelDict.get("itm_" + parentItemID, "NONE")    # add back itm_ tag for item dict compatibility
                    elif (miscID == "NONE_BOX"):  # check if box weapon, attempt to find non-box equivalent
                        itemID = itemID.replace("_box", "")
                        itemLevel = itemLevelDict.get(itemID, "2")  # default to item level 2 if no equivalent item

                itemAsList[6] = str(itemLevel)

        # write to items file
        if (CheckItemsToIgnore(itemAsList[1])): continue
        item = ",".join(itemAsList)
        troopEquipmentData.write(item)

    # close write files - probably not needed
    troopEquipmentData.close()
    troopClassData.close()

    return True


def CheckHeroLine(line):
    isHero = CheckScriptLine(line, "slot_troop_hero_item0, \"itm") or CheckScriptLine(line, "slot_troop_hero_item1") \
             or CheckScriptLine(line, "slot_troop_hero_item2") or CheckScriptLine(line, "slot_troop_hero_item3") \
             or CheckScriptLine(line, "slot_troop_hero_body") or CheckScriptLine(line, "slot_troop_hero_head") \
             or CheckScriptLine(line, "slot_troop_hero_gloves") or CheckScriptLine(line, "slot_troop_hero_foot") \
             or CheckScriptLine(line, "slot_troop_hero_horse, \"itm")
    return isHero


def CheckScriptLine(line, scriptTag):
    if (line.find(scriptTag) == -1): return False
    return True


def ItemErrorHandler(errorCode, itemID):
    if (errorCode == -1):
        print("DICTIONARY ERROR: " + str(itemID))
        return
    if (errorCode == -2):
        # print("OBSOLETE CATEGORY: " + str(itemID))
        return
    if (errorCode == -3):
        print("CATEGORY ERROR: " + str(itemID))
        return


def AddMiscItemsToWrite(miscID, miscDict, itemImplicitsDict, writeList):

    for itemID, itemLine in miscDict.items():
        itemData = ItemLib.GetItemData(itemID, miscDict, itemImplicitsDict)
        if (itemData == -1 or itemData == -2 or itemData == -3):
            ItemErrorHandler(itemData, itemID)
            continue
        miscEquipmentString = str(miscID) + "," + "NONE" + "," + "itm_" + str(itemData) + "\n"
        writeList.append(miscEquipmentString.split(","))


def CheckItemsToIgnore(itemID):
    IGNORE_ITEM_ID = [  # unused items by box or loadout
        "itm_earendil_hammer_b",
        "itm_earendil_hammer_c",
        "itm_earendil_hammer_d",
        "itm_boss_knight_axe",
        "itm_legendary_2h_lightsabers_dummy_item",
        "itm_legendary_1h_lightsabers_dummy_item",
        "itm_legendary_polearm_lightsabers_dummy_item",
        "itm_legendary_items_end",
        "itm_legendary_blunderbuss_dummy",
        "itm_wabbajack_melee",
        "itm_wooden_staff_blackmarch_melee",
        "itm_staff_blue_wizard_melee",
        "itm_staff_necromancer_bot_a_melee",
        "itm_staff_necromancer_bot_b_melee",
        "itm_staff_necromancer_bot_c_melee",
        "itm_staff_necromancer_bot_d_melee",
        "itm_ruby_light_staff_bot_a_melee",
        "itm_ruby_light_staff_bot_b_melee",
        "itm_ruby_light_staff_bot_c_melee",
        "itm_dunland_javelin_melee",
        "itm_marauder_throwing_axes_melee",
        "itm_lui_empireestoc",
        "itm_lui_empireestoc_melee",
        "itm_lui_empireestocb",
        "itm_lui_empireestocb_melee",
        "itm_lui_newestoc",
        "itm_lui_newestoc_melee",
        "itm_no_feet",
        "itm_invis_feet",
        "itm_invis_head",
        "itm_invis_hands",
        "itm_engineer_turret_long_range",
        "itm_engineer_turret_medium_range",
        "itm_engineer_turret_short_range",
    ]
    if (IGNORE_ITEM_ID.count(itemID) > 0): return True
    return False


def CheckTroopCanUseItem(troopID, troopDictionary, itemID, itemDictionary):
    itemRequirement = ItemLib.GetItemDifficulty(itemID, itemDictionary)
    if (itemRequirement is not None):
        useStat = itemRequirement[0]
        requiredStat = itemRequirement[1]
        troopStat = TroopLib.GetTroopStatForRequirement(troopID, troopDictionary, useStat)
        if (troopStat is None): troopStat = 0
        if (int(troopStat) < int(requiredStat)):
            print(str(troopID) + " CANNOT USE " + str(itemID) + "|HAS: " + str(troopStat) + " NEEDS: " + str(
                requiredStat) + "|STAT: " + str(useStat))
        #if (useStat == "RIDE"): print(str(troopID) + " HORSE: " + str(itemID) + "|HAS: " + str(troopStat) + " NEEDS: " + str(
                #requiredStat) + "|STAT: " + str(useStat))

def GetLineItem(line, beginTag, endTag, ignoreBeginning = False, moveEndIndex = -1):
    itemBegin = line.find(beginTag)
    if (itemBegin == -1): return None

    if (ignoreBeginning): itemBegin += len(beginTag)

    itemEnd = line.find(endTag, itemBegin)
    if (itemEnd == -1): return None

    item = line[itemBegin: itemEnd + moveEndIndex]
    item = item.casefold() # set item as all lowercase, because dictionaries are case-sensitive
    return item


# main loop
Main()
