
TROOPS_DICT = dict()
TROOPS_COST_DICT = dict()
TROOPS_DICT_IMPLICITS_DICT = dict()

FACTIONS_DICT = {
    "defender_empire" : "ARCANE",
    "defender_calradia" : "CALRADIA",
    "defender_crusaders" : "CRUSADERS",
    "defender_sigmar" : "SIGMAR",
    "defender_greeks" : "GREEKS",
    "defender_lotr" : "LOTR",
    "defender_saracens" : "SARACENS",
}

SUBFACTIONS_DICT = {
    # subfaction lists must be in order; see module_scripts line 78122 or script_assign_faction_data
    "ARCANE" : ["TRIAXIAN", "NESTORIAN", "ARCANTHIAN", "BLACKMARCH"],
    "CALRADIA" : ["SWADIAN", "VAEGIR", "KHERGIT", "NORD", "RHODOK", "SARRANID", "INDEPENDENT"],
    "CRUSADERS" : ["JERUSALEM", "ANTIOCH", "TEMPLAR", "HOSPITALLER", "TEUTONIC"],
    "SIGMAR" : ["SIGMAR"],
    "GREEKS" : ["ATHENIAN", "SPARTAN", "MACEDONIAN", "HALICARN", "THRACIAN", "ROMAN", "ACHAEMENID"],
    "LOTR" : ["GONDOR", "ROHAN", "ELVEN", "DWARVEN", "DALE", "ISENGARD", "SAURONS", "GHOST"],
    "SARACENS" : ["AYYUBID", "RUM", "NIZARI"],
}

TIER_DICT = {
    "0" : "RECRUIT",
    "1" : "SOLDIER",
    "2" : "ELITE",
    "3" : "HERO",
}

TYPE_LOOKUP_LIST = [
    "tf_guarantee_armor",
    "tf_guarantee_ranged",
    "tf_mounted",
]

TYPE_RANGED = "tf_guarantee_ranged"
TYPE_CAV = "tf_mounted"


def InitializeTroopDictionary(troopsFile, lineBegin, lineEnd):
    troops = open(troopsFile, "r")

    lineCount = 0
    troopLine = ""
    troopID = ""
    TROOP_START_TAG = "[\""

    for line in troops:
        # check if the currently read line is in specified argument range
        # **** SET lineEnd TO +1 OF THE INTENDED END ENTRY **** - this loop logic will fail to grab the last entry
        if (lineCount > lineEnd): return TROOPS_DICT
        if (lineCount < lineBegin):
            lineCount += 1
            continue
        lineCount += 1

        hasTroopStart = CheckLineStartForTag(troopLine, TROOP_START_TAG)

        if (hasTroopStart is False):

            hasTroopStart = CheckLineStartForTag(line, TROOP_START_TAG)
            if (hasTroopStart is False): continue

            troopID = GetTroopID(line)
            # troopID null check
            if (troopID is None):
                return "ERROR GETTING TROOP ID"
            troopLine += line   # begin building troop dictionary value

        elif (hasTroopStart):

            if (CheckLineStartForTag(line, TROOP_START_TAG)):   # check for a new troop ID, end if true
                troopLine = TrimTroopLineEnding(troopLine)
                TROOPS_DICT.setdefault(troopID, troopLine)
                # reset local loop vars
                troopLine = line
                troopID = GetTroopID(line)

            else:

                if (CheckLineStartForTag(line, "#")): continue  # ignore line if it is commented out
                troopLine += line

    return TROOPS_DICT


def InitializeTroopImplicitIdentitiesDictionary(implicitsFile):  # implicits are spreadsheet-specific stat mods, i.e. tier/class/swing & thrust damage mods
    implicits = open(implicitsFile, "r")

    for line in implicits:
        DELIM = "!"

        troopID = ExtractTroopStat(line, "trp_", DELIM, True)    # get troop ID without trp_ prefix
        if (troopID == "NONE"): continue   # continue if line is invalid/blank

        troopID = troopID.casefold()  # convert back to lowercase; ExtractItemValue converts to uppercase

        implicitsBegin = line.find(DELIM)
        if (implicitsBegin == -1):
            print("ERROR IN ITEM IMPLICITS LIST:" + str(line))
            return None
        troopImplicits = line[implicitsBegin + 1 : len(line)]

        TROOPS_DICT_IMPLICITS_DICT.setdefault(troopID, troopImplicits)

    return TROOPS_DICT_IMPLICITS_DICT


def GetTroopData(troopID, troopDictionary, troopImplicitsDict, IgnoreCost = False):
    DELIM = ","
    troopLine = troopDictionary.get(troopID)
    if (troopLine is None): return -1

    # format troop line
    troopLine = troopLine.strip()
    troopLine = troopLine.replace(" ", "")
    troopLine = troopLine.replace("|", ",")

    # troop basic values
    name = str(ExtractTroopStat(troopLine, ",\"", "\",", False))

    faction = ExtractTroopStat(troopLine, "fac_", ",", False)
    faction = str(FACTIONS_DICT.get(faction))

    charisma = ExtractTroopStat(troopLine, "charisma(", ")", False)
    charisma = int(charisma)
    subfaction = str(SUBFACTIONS_DICT.get(faction)[charisma-1])

    tier = ExtractTroopTier(troopLine)
    tier = str(TIER_DICT.get(tier))

    troopType = str(ExtractTroopType(troopLine))

    # get troop implicits
    troopImplicits = troopImplicitsDict.get(troopID, 0)
    if (troopImplicits != 0):
        adjectives = ExtractTroopStat(troopImplicits, "adj(", ")", True)
    else:
        # set troop implicits to NONE if they don't exist
        adjectives = "NONE"

    # get troop cost
    cost = 0    # typecast back into string later on
    if (tier == "ELITE" or tier == "HERO"):
        if (troopID.find("_loadout")): troopID = troopID.replace("_loadout", "")    # if hero loadout, use non-hero loadout ID

        if not (IgnoreCost):
            cost = TROOPS_COST_DICT.get(troopID, "NONE")
            if (cost == "NONE"): print ("ERROR: NO COST FOUND: " + troopID) # handle unexpected occurrence

            tier = GetSubTier(tier, cost)

    baseStr = name + DELIM + faction + DELIM + subfaction + DELIM + str(cost) + DELIM + tier + DELIM + troopType + DELIM + adjectives + DELIM  # troop base value stats

    # troop stats
    strength = str(ExtractTroopStat(troopLine, "strength(", ")", False))
    agility = str(ExtractTroopStat(troopLine, "agility(", ")", False))

    # two types of proficiency wrappers
    proficiency = str(ExtractTroopStat(troopLine, "wpall(", ")", False))
    if (proficiency == "NONE"): proficiency = str(ExtractTroopStat(troopLine, "wpex(", ")", False)) + DELIM + "40"  # 40 base firearm prof

    statStr = strength + DELIM + agility + DELIM + proficiency + DELIM  # troop stats string

    # troop skills
    ironflesh = str(ExtractTroopStat(troopLine, "knows_ironflesh_", DELIM, False))
    athletics = str(ExtractTroopStat(troopLine, "knows_athletics_", DELIM, False))
    riding = str(ExtractTroopStat(troopLine, "knows_riding_", DELIM, False))

    horseArchery = str(ExtractTroopStat(troopLine, "knows_horse_archery_", DELIM, False))
    shield = str(ExtractTroopStat(troopLine, "knows_shield_", DELIM, False))
    powerStrike = str(ExtractTroopStat(troopLine, "knows_power_strike_", DELIM, False))
    powerDraw = str(ExtractTroopStat(troopLine, "knows_power_draw_", DELIM, False))
    powerThrow = str(ExtractTroopStat(troopLine, "knows_power_throw_", DELIM, False))

    skillStr = ironflesh + DELIM + athletics + DELIM + riding + DELIM + horseArchery + DELIM + shield + DELIM + powerStrike + DELIM + powerDraw + DELIM + powerThrow
    skillStr = skillStr.replace("NONE", "0")    # replace skills not found with 0

    troopStr = baseStr + statStr + skillStr
    return troopStr


def GetTroopStatForRequirement(troopID, troopDictionary, requirement):
    troopLine = troopDictionary.get(troopID)

    # format troop line
    troopLine = troopLine.strip()
    troopLine = troopLine.replace(" ", "")
    troopLine = troopLine.replace("|", ",")

    DELIM = ","
    if (requirement == "STR"): return str(ExtractTroopStat(troopLine, "strength(", ")", False))
    elif (requirement == "PD"): return str(ExtractTroopStat(troopLine, "knows_power_draw_", DELIM, False))
    elif (requirement == "PT"): return str(ExtractTroopStat(troopLine, "knows_power_throw_", DELIM, False))
    elif (requirement == "RIDE"): return str(ExtractTroopStat(troopLine, "knows_riding_", DELIM, False))

    return None


def ExtractTroopStat(line, beginTag, endTag, isUpperCase):
    beginIndex = line.find(beginTag)
    endIndex = line.find(endTag, beginIndex)
    if (beginIndex == -1 or endIndex == -1): return "NONE"
    troopStat = line[beginIndex + len(beginTag): endIndex]

    if (isUpperCase is False): return troopStat  # return without setting to uppercase
    troopStat = troopStat.upper()  # convert into all uppercase, for damage type cases (pierce, cut, blunt)
    return troopStat


def ExtractTroopType(troopLine):
    troopFlagsForType = _ExtractTroopFlags(troopLine)
    if (troopFlagsForType is None):
        print(troopLine)
        print("ERROR: NO TROOP TYPE FLAGS FOUND")
        return "NONE"
    if (ExtractTroopStat(troopLine, "intellect(", ")", False) == "2"): return "SUPPORT"
    if (TYPE_CAV in troopFlagsForType): return "CAVALRY"
    elif (TYPE_RANGED in troopFlagsForType): return "RANGED"
    else: return "INFANTRY"


def ExtractTroopTier(troopLine):
    # level is a string, not integer
    level = ExtractTroopStat(troopLine, "level(", ")", False)
    intLevel = int(level)
    if (intLevel >= 3): return "3"
    if (intLevel == 1):
        checkForRecruitItems = ExtractTroopStat(troopLine, "itm_", ",", False)
        if (checkForRecruitItems == "NONE"): return "1" # is soldier
        else: return "0"  # is recruit
    return str(intLevel)


def CheckLineStartForTag(line, tag):
    line = line.strip()
    line = line.replace(" ", "")  # remove all spaces
    startTag = tag
    if (line.startswith(startTag)): return True
    return False


def TrimTroopLineEnding(troopLine):
    END_TAG = "],"
    endIndex = troopLine.rfind(END_TAG)
    if (endIndex == -1): return troopLine
    return troopLine[0 : endIndex + len(END_TAG)]


def GetTroopID(line):
    line.strip()
    line.replace(" ", "")  # remove all spaces
    line = line.casefold()  # set all lowercase, because dictionaries are case-sensitive
    beginIndex = line.find("[\"")
    endIndex = line.find(",")
    if (beginIndex == -1 or endIndex == -1): return None
    return line[beginIndex + 2: endIndex - 1]


def GetSubTier(tier, cost):
    if (cost == "NONE"): return "ERROR"
    cost = int(cost)    # typecast from string to int
    if (tier == "ELITE"):
        ELITE_0 = 25000
        ELITE_1 = 35000
        ELITE_2 = 45000

        if (cost <= ELITE_0): return "ELITE_0"
        elif (cost > ELITE_0 and cost <= ELITE_1): return "ELITE_1"
        elif (cost > ELITE_1 and cost <= ELITE_2): return "ELITE_2"
        elif (cost > ELITE_2): return "ELITE_3"

    if (tier == "HERO"):
        HERO_0 = 10000
        HERO_1 = 25000
        HERO_2 = 45000
        HERO_3 = 55000

        if (cost <= HERO_0): return "HERO_0"
        elif (cost > HERO_0 and cost <= HERO_1): return "HERO_1"
        elif (cost > HERO_1 and cost <= HERO_2): return "HERO_2"
        elif (cost > HERO_2 and cost <= HERO_3): return "HERO_3"
        elif (cost > HERO_3): return "HERO_4"

    return tier


def AddToTroopCostDict(troopID, troopCost):
    troopCost = int(troopCost)
    TROOPS_COST_DICT.setdefault(troopID, troopCost)


def _ExtractTroopFlags(troopLine):
    troopFlagsForType = list()
    for flag in TYPE_LOOKUP_LIST:
        if (troopLine.find(flag) == -1): continue
        troopFlagsForType.append(flag)
    return troopFlagsForType
