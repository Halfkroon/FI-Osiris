
ARMORS = "armors.txt"
WORDS = "armor words.txt"

IGNORE = [
    # ignore any numbers
    "armor",
    "armour",
    "armours",
    "head",
    "leg",
    "foot",
    "hand",
    "and",
    "red",
    "orange",
    "yellow",
    "green",
    "blue",
    "white",
    "black",
    "brown",
]

WORD_ASSIGNMENT = {
    "leather" : "LEATHER",
    "mail" : "MAIL",
    "cloth" : "CLOTH",
    "plate": "PLATE",
    "skeleton" : "MAIL",
    "demon" : "MAIL",
    "tunic" : "CLOTH",
    "cloak" : "CLOTH",
    "brigandine" : "LEATHER",
    "lino" : "LEATHER",
    "hood" : "CLOTH",
    "cape" : "CLOTH",
    "robe" : "CLOTH",
    "padded" : "LEATHER",
    "greaves" : "PLATE",
    "scale" : "MAIL",
    "chain" : "MAIL",
    "ringed" : "MAIL",
    "valsgarde" : "PLATE",
    "cuirass" : "PLATE",
    "shirt" : "CLOTH",
    "coat" : "CLOTH",
    "breastplate" : "PLATE",
    "gauntlet" : "PLATE",
    "jacket" : "LEATHER",
    "lamellar" : "PLATE",
    "helm" : "PLATE",
    "knight" : "PLATE",
    "surcoat" : "CLOTH",
    "pelt" : "LEATHER",
    "hosen" : "CLOTH",
    "bascinet" : "PLATE",
    "salet" : "PLATE",
    "gambeson" : "LEATHER",

}

def PrintCommonNames():
    armorsFile = open(ARMORS, 'r')
    wordsFile = open(WORDS, 'w')

    wordsList = list()

    for line in armorsFile:
        line = StripItm(line)
        line = line.strip()
        wordsList.extend(line.split("_"))

    wordsList = CleanList(wordsList)
    wordFrequency = dict()

    wordsFile.write("WORD" + "," + "COUNT")

    for line in wordsList:
        if(wordFrequency.get(line, None) is None):
            wordCount = wordsList.count(line)
            wordFrequency.setdefault(line, wordCount)
            wordsFile.write(line + "," + str(wordCount))
            wordsFile.write("\n")


def CleanList(wordsList):
    cleanedList = list()
    MIN_LENGTH = 4
    for line in wordsList:
        if (line.isnumeric()): continue
        line = StripNumbers(line)

        if (IGNORE.count(line) > 0): continue
        if (len(line) < MIN_LENGTH): continue
        cleanedList.append(line)

    return cleanedList


def StripItm(line):
    return line[4:len(line)]


def StripNumbers(line):
    line = ''.join([char for char in line if not char.isdigit()])
    return line


PrintCommonNames()
