import ItemLib

READ_DICT = dict()
READ_ITEMS = "module_items 1.3.py"
WRITE_ITEMS = "module_items MOD.py"

ACCURACY_TAGS_LIST = {
    "itp_type_bow",
    "itp_type_crossbow",
    "itp_type_pistol",
    "itp_type_musket",
    "itp_type_thrown",
}


def Main():
    global READ_DICT
    READ_DICT = ItemLib.InitializeItemDictionary(READ_ITEMS)

    readItems = open(READ_ITEMS, "r")
    writeItems = open(WRITE_ITEMS, "w")

    accuracyStat = "|accuracy(100)"

    itemLine = str("")  # stores the current item entry; can be in multiple lines
    itemID = str("")  # current item entry ID

    isRangedWeapon = False
    hasAccuracyFlag = False

    alreadyHasAccuracy = 0
    addedAccuracy = 0

    for line in readItems:
        writeLine = line

        line = line.strip()
        hasBeginning = ItemLib.CheckLineStart(itemLine)

        # getting new item
        if (hasBeginning is False):
            if (ItemLib.CheckLineStart(line)):

                itemLine = line
                itemID = ItemLib.GetItemID(itemLine)
                if (itemID is None):
                    print("ITEM ID ERROR")
                    return False

                # check if ranged weapon
                for rangedTag in ACCURACY_TAGS_LIST:

                    isRangedWeapon = CheckForFlag(itemID, rangedTag, "")
                    if (isRangedWeapon):
                        # check if already has accuracy
                        hasAccuracyFlag = CheckForFlag(itemID, "accuracy", ")")
                        if (hasAccuracyFlag):
                            alreadyHasAccuracy += 1
                            break
                        else:   # try to inject accuracy stat
                            indexShootSpeed = GetFlagInjectIndex(writeLine, "shoot_speed", ")")
                            if (indexShootSpeed != -1):
                                writeLine = AddFlag(writeLine, accuracyStat, indexShootSpeed)  # modify write line
                                addedAccuracy += 1
                            break  # break from ranged tag loop

        # already has item
        elif (hasBeginning):
            itemLine += line

            if (isRangedWeapon & hasAccuracyFlag is False):
                # try to inject accuracy stat
                indexShootSpeed = GetFlagInjectIndex(writeLine, "shoot_speed", ")")
                if (indexShootSpeed != -1):
                    writeLine = AddFlag(writeLine, accuracyStat, indexShootSpeed)  # modify write line
                    addedAccuracy += 1

        # check for item entry ending
        if (ItemLib.CheckLineStart(itemLine) and ItemLib.CheckLineEnd(itemLine)):
            # reset outer loop vars
            itemLine = str("")
            isRangedWeapon = False
            hasAccuracyFlag = False

        # write to new file
        writeItems.write(writeLine)

    # end loop
    print("HAD EXISTING ACCURACY: " + str(alreadyHasAccuracy))
    print("ADDED ACCURACY: " + str(addedAccuracy))
    writeItems.close()


def GetFlagInjectIndex(line, priorStatBegin, priorStatEnd):
    priorStatBeginIndex = line.find(priorStatBegin)
    priorStatEndIndex = line.find(priorStatEnd, priorStatBeginIndex)

    return priorStatEndIndex


def AddFlag(line, flag, injectBeginIndex):
    beginHalf = line[0 : injectBeginIndex + 1]
    endHalf = line[injectBeginIndex + 1 : len(line)]
    line = beginHalf + str(flag) + endHalf

    return line


def CheckForFlag(itemID, beginTag, endTag):
    itemLineEntry = READ_DICT.get(itemID, None)
    if (itemLineEntry is None):
        print("ERROR: ITEM ID NOT FOUND")
        return False

    # format line
    itemLineEntry.casefold()
    itemLineEntry.strip()
    itemLineEntry.replace(" ", "")

    beginIndex = itemLineEntry.find(beginTag)
    endIndex = itemLineEntry.find(endTag, beginIndex)
    if (beginIndex == -1 or endIndex == -1): return False  # find stat, false if not found

    return True


# running code
Main()
