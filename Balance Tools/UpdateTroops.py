import TroopLib

BASE_MODULE_TROOPS = "module_troops 1.3.py"
MODULE_TROOPS = "module_troops.py"
CHANGELOG = "troopchangelist.txt"
CHANGELOG_DICT = dict()
CURRENT_TROOPS_DICT = dict()    # have to iterate through troops file twice to accurately replace skills while maintaining original file format :(

TROOPS_BEGIN = 3034     # defender classes begin at this line
TROOPS_END = 5403   # defender classes end at this line
TROOP_START_TAG = "[\""

STATS_DICT = {
    "strength(" : "str(",
    "agility(" : "agi(",
}
PROFICIENCY_MAP = {
    0 : "1h(",
    1 : "2h(",
    2 : "pole(",
    3 : "bow(",
    4 : "xbow(",
    5 : "throw(",
    6 : "gun(",
}
SKILLS_DICT = {
    "knows_ironflesh_" : "if(",
    "knows_athletics_" : "ath(",
    "knows_riding_" : "ride(",
    "knows_horse_archery_" : "harc(",
    "knows_shield_" : "shield(",
    "knows_power_strike_" : "ps(",
    "knows_power_draw_" : "pd(",
    "knows_power_throw_" : "pt(",
}

def Main():
    SetupChangeDictionary()
    global CURRENT_TROOPS_DICT
    CURRENT_TROOPS_DICT = TroopLib.InitializeTroopDictionary(BASE_MODULE_TROOPS, TROOPS_BEGIN, TROOPS_END)

    troopsFile = open(BASE_MODULE_TROOPS, "r")
    writeFile = open(MODULE_TROOPS, "w")

    troopID = ""
    lineCount = 0

    for line in troopsFile:
        # check if the currently read line is in specified argument range
        if (lineCount > TROOPS_END):
            writeFile.write(line)
            continue
        if (lineCount < TROOPS_BEGIN):
            lineCount += 1
            writeFile.write(line)
            continue
        lineCount += 1

        if (troopID == ""):    # check if a loop has no troop

            troopID = TroopLib.GetTroopID(line)
            if (troopID == ""):
                writeFile.write(line)
                continue   # skip line if no troop identifier

            AttemptTroopChanges(troopID, line, writeFile)
            continue

        elif(troopID != ""):
            if (TroopLib.GetTroopID(line) is not None):
                troopID = TroopLib.GetTroopID(line)  # check for a new troop ID

            AttemptTroopChanges(troopID, line, writeFile)

        else: writeFile.write(line)

def SetupChangeDictionary():
    DELIM = "!"
    changeDictionary = dict()

    changeFile = open(CHANGELOG, "r")
    changeCount = 0

    for line in changeFile:

        line.replace(" ", "")
        line.strip()    # format line

        # get troop ID
        troopIDEndIndex = line.find(DELIM)
        if (troopIDEndIndex == -1): continue
        troopID = line[0 : troopIDEndIndex]

        CHANGELOG_DICT.setdefault(troopID, line)
        changeCount += 1

    print("CHANGES INPUT: " + str(changeCount))


def AttemptTroopChanges(troopID, line, writeTo):

    changeLine = CHANGELOG_DICT.get(troopID)
    if (changeLine is None):
        writeTo.write(line)
        return

    # stats
    for stat, lookupKey in STATS_DICT.items():
        lookupValue = GetChangeValue(troopID, lookupKey)
        line = ReplaceStatValue(line, stat, ")", lookupValue)

    # proficiencies
    profList = GetOriginalProfValues(line)
    if (profList is not None):
        for index, prof in PROFICIENCY_MAP.items():
            profList[index] = GetChangeValue(troopID, prof)
        line = ReplaceProfValues(line, profList)

    # skills
    for skill, lookupKey in SKILLS_DICT.items():
        lookupValue = int(GetChangeValue(troopID, lookupKey))   # typecast to integer
        lookupValue = max(min(lookupValue,10),0)    # clamp skill target value from 0 to 10
        line = ReplaceSkillValue(line, troopID, skill, lookupValue)

    writeTo.write(line)


def ReplaceStatValue(line, beginTag, endTag, changeValue):

    beginIndex = line.find(beginTag)
    endIndex = line.find(endTag, beginIndex)

    if (beginIndex == -1): return line
    if (endIndex == -1): return line

    beginString = line[0 : beginIndex + len(beginTag)]
    endString = line[endIndex : len(line)]
    newString = beginString + str(changeValue) + endString

    return newString


def ReplaceProfValues(line, profChanges):

    beginFind = CheckClosestFind(line, "wpex(", "wpall(")
    beginIndex = beginFind[0]
    if (beginIndex == -1): return line

    if (beginFind[1] == 0): line = line.replace("wpex(", "wpall(")  # switch all wpex to wpall, because spreadsheet auto includes a min firearm proficiency
    profTag = "wpall("

    endIndex = line.find(")", beginIndex)

    beginString = line[0 : beginIndex]
    endString = line[endIndex : len(line)]

    profValues = ",".join(profChanges)  # convert proficiency list values in a string deliminated by commas
    profString = profTag + profValues   # closing parenthesis captrued by endString

    newString = beginString + profString + endString
    return newString


def ReplaceSkillValue(line, troopID, skill, skillValue):
    if (skillValue == 0):   # handle target value @ 0
        beginIndex = line.find(skill)
        if (beginIndex == -1): return line  # return if skill doesn't exist and target value @ 0
        # delete skill from troop line if skill exists
        endIndex = CheckClosestFind(line, "|", ",", beginIndex)[0]

        beginString = line[0 : beginIndex]
        endString = line[endIndex + 1 : len(line)]

        newString = beginString + endString
        return newString

    else:
        checkForExistingSkill = CURRENT_TROOPS_DICT.get(troopID).find(skill)

        if (checkForExistingSkill == -1):   # if skill doesn't exist and target value > 0, add after weapon proficiencies
            beginWPIndex = CheckClosestFind(line, "wpex(", "wpall(")[0]
            endWPIndex = CheckClosestFind(line, "),", ")|", beginWPIndex)[0] + 2    # add 2 to offset the closing brackets & comma
            if (beginWPIndex == -1): return line    # skip modifying line if cannot find weapon proficiency tags

            skillString = " " + skill + str(skillValue) + " |"
            beginString = line[0 : endWPIndex]
            endString = line[endWPIndex : len(line)]

            newString = beginString + skillString + endString
            return newString

        elif (checkForExistingSkill >= 0):   # if skill already exists & its value needs to be changed
            beginIndex = line.find(skill)
            endIndex = CheckClosestFind(line, "|", ",", beginIndex)[0]
            if (beginIndex == -1): return line  # skip modifying line if cannot find skill tag

            skillString = skill + str(skillValue)
            beginString = line[0 : beginIndex]
            endIndex = line[endIndex : len(line)]

            newString = beginString + skillString + endIndex
            return newString

        else: return line

def GetOriginalProfValues(line):

    beginFind = CheckClosestFind(line, "wpex(", "wpall(")
    beginIndex = beginFind[0]
    if (beginIndex == -1): return None

    if (beginFind[1] == 0): beginLength = len("wpex(")
    else: beginLength = len("wpall(")

    endIndex = line.find(")", beginIndex)

    profLine = line[beginIndex + beginLength : endIndex]
    profList = profLine.split(",")
    if (len(profList) < 7): profList.append(40)
    return profList


def GetChangeValue(troopID, lookupKey):
    changelistLine = CHANGELOG_DICT.get(troopID)
    lookupBeginIndex = changelistLine.find(lookupKey)
    lookupEndIndex = changelistLine.find(")", lookupBeginIndex)

    if (lookupBeginIndex == -1 or lookupEndIndex == -1): return "ERROR"
    lookupValue = changelistLine[lookupBeginIndex + len(lookupKey) : lookupEndIndex]

    return lookupValue


def CheckClosestFind(line, tag1, tag2, startIndex = 0):
    beginIndex1 = line.find(tag1, startIndex)
    beginIndex2 = line.find(tag2, startIndex)

    if (beginIndex1 == -1): beginIndex1 = 1000
    if (beginIndex2 == -1): beginIndex2 = 1000
    if (beginIndex1 == beginIndex2): return (-1, -1)

    if (beginIndex1 < beginIndex2):
        return (beginIndex1, 0)
    elif (beginIndex2 < beginIndex1):
        return (beginIndex2, 1)
    else: return (-1, -1)

Main()


