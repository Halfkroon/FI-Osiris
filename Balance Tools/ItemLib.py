# global vars

AUTO_ASSIGN_TIERS = False   # auto assigns elite & legendary weapon tiers

DEBUG_ITEMS_FILE = "debug_item_dict.txt"

ITEMS_DICT = dict()
ITEMS_INDEX_DICT = dict()
ITEMS_BOX_DICT = dict()
ITEMS_ALT_MODE_DICT = dict()
ITEMS_LEGENDARY_DICT = dict()
ITEMS_IMPLICITS_DICT = dict()

ITEM_TYPE_DICT = {
    "itp_type_one_handed_wpn": "1H",
    "itp_type_two_handed_wpn": "2H",
    "itp_type_polearm": "POLEARM",
    "itp_type_arrows": "ARROWS",
    "itp_type_bolts": "BOLTS",
    "itp_type_bow": "BOW",
    "itp_type_crossbow": "XBOW",
    "itp_type_thrown": "THROWN",
    "itp_type_head_armor": "HEAD",
    "itp_type_body_armor": "BODY",
    "itp_type_foot_armor": "LEG",
    "itp_type_hand_armor": "HAND",
    "itp_type_pistol": "PISTOL",
    "itp_type_musket": "RIFLE",
    "itp_type_bullets": "BULLETS",
    "itp_type_horse": "HORSE",
    "itp_type_shield": "SHIELD",   # change to "SHIELD" to pull shields
    "itp_type_goods": "OBS",
    "itp_type_animal": "OBS",
    "itp_type_book": "OBS",
}
ITEM_CATEGORY_DICT = {
    "1H": "WEAPON",
    "2H": "WEAPON",
    "DAGGER" : "WEAPON",
    "RAPIER" : "WEAPON",
    "BH" : "WEAPON",
    "POLEARM": "WEAPON",
    "POLEHAMMER" : "WEAPON",
    "SPEAR" : "WEAPON",
    "LANCE" : "WEAPON",
    "PIKE" : "WEAPON",
    "2H MACE" : "WEAPON",
    "ARROWS": "WEAPON",
    "BOLTS": "WEAPON",
    "BOW": "WEAPON",
    "XBOW": "WEAPON",
    "THROWN": "WEAPON",
    "HEAD": "ARMOR",
    "BODY": "ARMOR",
    "LEG": "ARMOR",
    "HAND": "ARMOR",
    "PISTOL": "WEAPON",
    "WAND" : "WEAPON",
    "RIFLE": "WEAPON",
    "STAFF" : "WEAPON",
    "BULLETS": "WEAPON",
    "THROWSTONE" : "WEAPON",
    "THROWKNIFE" : "WEAPON",
    "THROWAXE" : "WEAPON",
    "THROWJAVELIN" : "WEAPON",
    "SHIELD" : "OBS",   # change to "WEAPON" to pull shields
    "HORSE" : "HORSE",  # get horses to check against troop stat requirement
    "OBS": "OBS",
}
ITEM_TAGS_DICT = {
    "itp_can_penetrate_shield": "PS",
    "itp_bonus_against_shield": "SH",
    "itp_crush_through": "CR",
    "itp_can_knock_down": "KD",
    "itp_unbalanced": "UB",
}
ITEM_FLAGS_LIST = {
    "itp_no_parry", # rapier
    "itc_dagger",   # rapier
    "itc_rapier",
    "itp_next_item_as_melee",
    "itc_greatsword",
    "itc_longsword",
    "itc_bastardsword",
    "itc_staff_overhead_stab",  # spear
    "itc_staff_overhead_shield", # spear
    "itp_cant_use_on_horseback", # pike
    "itp_two_handed",   # pike
    "itc_musket_melee_bayonet", # pike
    "itc_greatlance",   # lance
    "itp_couchable",    # lance
    "itp_can_knock_down",   # 2h mace, polehammer
    "itp_crush_through",    # 2h mace, polehammer
    "custom_kill_info(2)",   # magic
    "itcf_throw_stone",  # throwing stones
    "itcf_throw_knife",  # throwing knives
    "itcf_throw_axe",    # throwing axes
    "itcf_throw_javelin",    # throwing javs
}


def InitializeItemDictionary(itemsFile):
    items = open(itemsFile, "r")
    hasItemStart = False
    itemLine = ""
    itemID = ""

    ITEM_BOX_STRING = "box_"

    isNextItemAltMode = False
    NEXT_ITEM_ALT_MODE_STRING = "itp_next_item_as_melee"

    ITEM_LEGENDARY_STRING = "legendary_"
    ITEM_LEGENDARY_STRING2 = "imodbits_melee_legendary"

    for line in items:

        if (hasItemStart is False):
            hasItemStart = CheckLineStart(line)
            if (hasItemStart is False): continue

            itemID = GetItemID(line)
            itemLine = itemLine + line

        else: itemLine = itemLine + line

        if (CheckLineEnd(line) is True):
            ITEMS_DICT.setdefault(itemID, itemLine)

            # check if item is a box weapon
            if (itemLine.find(ITEM_BOX_STRING) > -1): ITEMS_BOX_DICT.setdefault(itemID, itemLine)

            # check if item is queued as an alt melee mode weapon
            elif (isNextItemAltMode is True):
                ITEMS_ALT_MODE_DICT.setdefault(itemID, itemLine)
                isNextItemAltMode = False

            # check if legendary weapon
            elif (itemLine.find(ITEM_LEGENDARY_STRING) > -1 or itemLine.find(ITEM_LEGENDARY_STRING2) > -1): ITEMS_LEGENDARY_DICT.setdefault(itemID, itemLine)

            # check if next weapon is alternate melee mode
            if (itemLine.find(NEXT_ITEM_ALT_MODE_STRING) > -1): isNextItemAltMode = True  # queue next item as alternative melee mode

            # reset vars
            itemLine = ""
            itemID = ""
            hasItemStart = False

    items.close()
    return ITEMS_DICT


def GetBoxWeapons(): return ITEMS_BOX_DICT


def GetAlternateModeWeapons(): return ITEMS_ALT_MODE_DICT


def GetLegendaryWeapons(): return ITEMS_LEGENDARY_DICT


def GetItemsAsList(): return ITEMS_DICT.keys()


def InitializeItemImplicitIdentitiesDictionary(implicitsFilesList):  # implicits are spreadsheet-specific stat mods, i.e. tier/class/swing & thrust damage mods

    for implicitsFile in implicitsFilesList:
        implicits = open(implicitsFile, "r")
        for line in implicits:
            DELIM = "!"

            itemID = ExtractItemValue(line, "itm_", DELIM, True)    # get item ID without itm_ prefix
            if (itemID == "NONE"): continue   # continue if line is invalid/blank

            itemID = itemID.casefold()  # convert back to lowercase; ExtractItemValue converts to uppercase

            implicitsBegin = line.find(DELIM)
            if (implicitsBegin == -1):
                print("ERROR IN ITEM IMPLICITS LIST:" + str(line))
                return None
            itemImplicits = line[implicitsBegin + 1 : len(line)]

            ITEMS_IMPLICITS_DICT.setdefault(itemID, itemImplicits)

    return ITEMS_IMPLICITS_DICT


def GetItemData(itemID, itemDict, itemImplicitsDict):
    DELIM = ","
    originalItemID = itemID
    itemID = CheckItemPrefix(itemID)
    itemLine = itemDict.get(itemID)
    if (itemLine is None): return -1

    # format item line
    itemLine = itemLine.strip()
    itemLine = itemLine.replace(" ", "")

    itemImplicits = itemImplicitsDict.get(itemID, 0)

    name = str(ExtractItemValue(itemLine, ",\"", "\",", False))
    weight = str(ExtractItemValue(itemLine, "weight(", ")", False))
    tags = str(ExtractItemTags(itemLine))
    itemType = GetItemType(itemLine)

    category = ITEM_CATEGORY_DICT.get(itemType)
    # if (category == "WEAPON" or category == "ARMOR" or category == "SHIELD"):  # switch if() cases to pull shields
    if (category == "WEAPON" or category == "ARMOR"):
        # get item implicit values if they exist
        if (itemImplicits != 0):
            adjective = ExtractItemValue(itemImplicits, "adjectives(", ")", True)
            if (AUTO_ASSIGN_TIERS):
                if (PredictItemTier(itemLine) == "NONE"): itemTier = ExtractItemValue(itemImplicits, "itemTier(", ")", True)
                else: itemTier = PredictItemTier(itemLine)
            else: itemTier = ExtractItemValue(itemImplicits, "itemTier(", ")", True)
            itemClass = ExtractItemValue(itemImplicits, "itemClass(", ")", True)
            swingMod = ExtractItemValue(itemImplicits, "swingMod(", ")", True)
            thrustMod = ExtractItemValue(itemImplicits, "thrustMod(", ")", True)
        else:
            # set implicits to none if they do not exist
            adjective = "NONE"
            if (AUTO_ASSIGN_TIERS): itemTier = PredictItemTier(itemLine)
            else: itemTier = "NONE"
            itemClass = "NONE"
            swingMod = "0"
            thrustMod = "0"

        length = str(ExtractItemValue(itemLine, "weapon_length(", ")", False))
        speed = str(ExtractItemValue(itemLine, "spd_rtng(", ")", False))

        stringBase = originalItemID + DELIM + name + DELIM + itemType + DELIM + tags + DELIM + adjective + DELIM + itemTier + DELIM + itemClass \
                     + DELIM + swingMod + DELIM + thrustMod + DELIM + length + DELIM + weight + DELIM + speed + DELIM  # common values & implicits

        swing = str(ExtractItemValue(itemLine, "swing_damage(", ")", True))
        if (swing == "NONE"): swing = "NONE,NONE"  # correct for damage & type
        thrust = str(ExtractItemValue(itemLine, "thrust_damage(", ")", True))
        if (thrust == "NONE"): thrust = "NONE,NONE"  # correct for damage & type
        stringMelee = swing + DELIM + thrust + DELIM  # melee weapon values

        accuracy = str(ExtractItemValue(itemLine, "accuracy(", ")", False))
        shootSpeed = str(ExtractItemValue(itemLine, "shoot_speed(", ")", False))
        ammo = str(ExtractItemValue(itemLine, "max_ammo(", ")", False))
        stringRanged = accuracy + DELIM + shootSpeed + DELIM + ammo + DELIM  # ranged weapon values

        head = str(ExtractItemValue(itemLine, "head_armor(", ")", False))
        body = str(ExtractItemValue(itemLine, "body_armor(", ")", False))
        leg = str(ExtractItemValue(itemLine, "leg_armor(", ")", False))
        stringArmor = head + DELIM + body + DELIM + leg  # armor values (end delims)

        return stringBase + stringMelee + stringRanged + stringArmor

    elif (category == "OBS"): return -2
    elif (category == "HORSE"): return -4

    return -3


def GetItemDifficulty(itemID, itemDict):
    itemID = CheckItemPrefix(itemID)
    itemLine = itemDict.get(itemID)

    difficulty = ExtractItemValue(itemLine, "difficulty(", ")", False)
    if (difficulty == "NONE"): return None

    itemType = _GetGeneralItemType(itemLine)
    if (itemType == "1H" or itemType == "2H" or itemType == "POLEARM"):
        return ("STR", difficulty)
    elif (itemType == "BOW"):
        return ("PD", difficulty)
    elif (itemType == "THROWN"):
        return ("PT", difficulty)
    elif (itemType == "HORSE"):
        return ("RIDE", difficulty)

    return None


def GetItemType(itemLine):
    generalItemType = _GetGeneralItemType(itemLine)
    itemTags = _ExtractItemFlagsList(itemLine)
    magicSet = {"custom_kill_info(2)"}  # overarching magic weapon tag
    rapierSet = {"itc_rapier"} # rapier tag

    if (generalItemType == "1H"):
        # rapier

        rapierSet2 = {"itc_longsword", "itp_next_item_as_melee"}
        if (len(rapierSet.intersection(itemTags))): return "RAPIER"
        if (len(rapierSet2.intersection(itemTags)) == len(rapierSet2)): return "RAPIER"

        # dagger
        daggerSet = {"itc_dagger", "itp_no_parry"}    # tags to define dagger
        if (len(daggerSet.intersection(itemTags))): return "DAGGER"

        else: return generalItemType

    elif (generalItemType == "2H"):
        # 2h rapier
        rapierSet3 = {"itc_greatsword", "itp_next_item_as_melee"}
        if (len(rapierSet.intersection(itemTags))): return "RAPIER"
        if (len(rapierSet3.intersection(itemTags)) == len(rapierSet3)): return "RAPIER"

        # 2h mace
        mace2hSet = {"itp_can_knock_down", "itp_crush_through"}    # tags to define 2h mace
        if (len(mace2hSet.intersection(itemTags))): return "2H MACE"

        # bastard sword
        bastardSwordSet = {"itc_bastardsword"}
        if (len(bastardSwordSet.intersection(itemTags))): return "BH"

        else: return generalItemType

    elif (generalItemType == "POLEARM"):
        # polehammer
        hammerSet = {"itp_can_knock_down", "itp_crush_through"} # tags to define polehammer
        if (len(hammerSet.intersection(itemTags))): return "POLEHAMMER"

        # spear/pike/lance
        spearSet = {"itc_staff_overhead_shield", "itc_staff_overhead_stab", "itc_musket_melee_bayonet", "itc_greatlance"}  # tags to define spear/pike/lance
        pikeLanceSet = {"itc_musket_melee_bayonet", "itc_greatlance"}   # tags to define pike & lance
        pikeSet = {"itp_cant_use_on_horseback", "itp_two_handed"}   # tags to define pike
        lanceSet = {"itp_couchable"}    # tags to define lance
        if (len(spearSet.intersection(itemTags))):
            if (len(pikeLanceSet.intersection(itemTags))):
                if (len(pikeSet.intersection(itemTags))): return "PIKE"
                elif (len(lanceSet.intersection(itemTags))): return "LANCE"
                else: return "PIKE"
            return "SPEAR"

        else: return generalItemType

    elif (generalItemType == "THROWN"):
        # self explanatory
        if ("itcf_throw_stone" in itemTags): return "THROWSTONE"
        elif ("itcf_throw_knife" in itemTags): return "THROWKNIFE"
        elif ("itcf_throw_axe" in itemTags): return "THROWAXE"
        elif ("itcf_throw_javelin" in itemTags): return "THROWJAVELIN"

        else: return generalItemType

    elif (generalItemType == "PISTOL"):
        # wand
        if (len(magicSet.intersection(itemTags))): return "WAND"

        else: return generalItemType

    elif (generalItemType == "RIFLE"):
        # staff
        if (len(magicSet.intersection(itemTags))): return "STAFF"

        else: return generalItemType

    # end types
    return generalItemType


def PredictItemTier(line):
    legendaryTags = [
        "imodbits_melee_legendary",
        "imodbits_melee_legendary_blunt",
        "imodbits_shield_legendary",
        "imodbits_ranged_legendary",
        "imodbits_thrown_legendary",
        "imodbits_missile_legendary",
    ]

    for tag in legendaryTags:
        if (line.find(tag) > 0): return "4"

    return "NONE"


def GetItemID(line):
    line.strip()
    line.replace(" ", "")  # remove all spaces
    line = line.casefold()  # set all lowercase, because dictionaries are case-sensitive
    beginIndex = line.find("[\"")
    endIndex = line.find(",")
    if (beginIndex == -1 or endIndex == -1): return None
    return line[beginIndex + 2: endIndex - 1]


def CheckItemPrefix(itemID):
    # removes item tag prefix
    itmTag = "itm_"
    itmPrefix = itemID.find(itmTag)
    if (itmPrefix == -1):
        return itemID
    else:
        itemID = itemID[itmPrefix + len(itmTag): len(itemID) + 1]
        return itemID


def CheckLineStart(line):
    line = line.strip()
    line = line.replace(" ", "")  # remove all spaces
    startTag = "[\""
    if (line.startswith(startTag)): return True
    return False


def CheckLineEnd(line):
    line = line.strip()
    line = line.replace(" ", "")  # remove all spaces
    endTag = "],"
    endExcludeTag = ")],"
    if (line.endswith(endTag) and not line.endswith(endExcludeTag)): return True
    return False


def _GetGeneralItemType(line):
    typeTag = "itp_type_"
    typeBeginIndex = line.find(typeTag)

    typeEndIndex1 = line.find("|", typeBeginIndex)
    typeEndIndex2 = line.find(",", typeBeginIndex)
    typeEndIndex = min(typeEndIndex1, typeEndIndex2)  # get the first end condition; conditions can be , or |

    if (typeBeginIndex == -1 or typeEndIndex == -1): return None
    type = line[typeBeginIndex: typeEndIndex]
    return ITEM_TYPE_DICT.get(type)


def ExtractItemValue(line, beginTag, endTag, isUpperCase):
    beginIndex = line.find(beginTag)
    endIndex = line.find(endTag, beginIndex)
    if (beginIndex == -1 or endIndex == -1): return "NONE"
    itemValue = line[beginIndex + len(beginTag): endIndex]

    if (isUpperCase is False): return itemValue  # return without setting to uppercase
    itemValue = itemValue.upper()  # convert into all uppercase, for damage type cases (pierce, cut, blunt)
    return itemValue


def ExtractItemTags(line):
    TAG_DELIM = "."
    stringTags = ""
    for tag, tagID in ITEM_TAGS_DICT.items():
        if (line.find(tag) == -1): continue
        stringTags += str(tagID) + str(TAG_DELIM)
    if (stringTags.endswith(TAG_DELIM)): stringTags = stringTags[0: len(stringTags) - 1]
    return stringTags


def _ExtractItemFlagsList(line):
    itemFlagsList = list()
    for flag in ITEM_FLAGS_LIST:
        if (line.find(flag) == -1): continue
        itemFlagsList.append(flag)

    return itemFlagsList


def DebugItemsDict(moduleFile):
    InitializeItemDictionary(moduleFile)
    debugFile = open(DEBUG_ITEMS_FILE, 'w')
    for itemID, itemLine in ITEMS_DICT.items():
        debugFile.write(str(itemID) + "|" + str(itemLine))
        debugFile.write("\n")

    debugFile.write("TOTAL ITEM COUNT: " + str(len(ITEMS_DICT)))
    debugFile.close()


# DebugItemsDict("module_items MOD.py")
