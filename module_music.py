from header_music import *
####################################################################################################################
#  Each track record contains the following fields:
#  1) Track id: used for referencing tracks.
#  2) Track file: filename of the track
#  3) Track flags. See header_music.py for a list of available flags
#  4) Continue Track flags: Shows in which situations or cultures the track can continue playing. See header_music.py for a list of available flags
####################################################################################################################

# WARNING: You MUST add mtf_module_track flag to the flags of the tracks located under module directory

tracks = [
  ("mount_and_blade_title_screen", "invasion_main_theme.ogg", mtf_module_track|mtf_sit_main_title|mtf_start_immediately, 0),

  ###################
  ###FULL INVASION###
  ###################
  # General #
  ("and_the_sky_shall_unfold", "And_the_Sky_Shall_Unfold.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("a_place_among_the_stars", "A_Place_Among_the_Stars.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("a_place_among_the_stars_2", "A_Place_Among_the_Stars_2.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("elysium_1", "Elysium_1.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("elysium_2", "Elysium_2.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("epic_trailer_01", "Epic_Trailer_01.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("epic_trailer_02", "Epic_Trailer_02.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("godforsaken", "Godforsaken.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("godforsaken_3", "Godforsaken_3.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("guardians", "Guardians.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("heroic_march_1", "Heroic_March_1.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("heroic_march_3", "Heroic_March_3.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("heroic_march_04", "Heroic_March_04.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("heroic_march_05", "Heroic_March_05.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("magnificent_march_2", "Magnificent_March_2.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("magnificent_march_3", "Magnificent_March_3.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("mighty_rush_1", "Mighty_Rush_1.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("rise_of_the_phoenix_2", "Rise_of_the_Phoenix_2.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("squad_force_1", "Squad_Force_1.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("squad_force_2", "Squad_Force_2.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("squad_force_3", "Squad_Force_3.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("the_division_1", "The_Division_1.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("the_division_2", "The_Division_2.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("the_division_3", "The_Division_3.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  ("weapons_of_impact_1", "Weapons_of_Impact_1.ogg", mtf_module_track|mtf_sit_multiplayer_fight, mtf_sit_multiplayer_fight),
  
  # Minor Boss # 
  ("are_you_afraid_1", "Are_You_Afraid_1.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("army_of_angels", "Army_of_Angels.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("army_of_angels_2", "Army_of_Angels_2.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("army_of_angels_3", "Army_of_Angels_3.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("barbarians", "Barbarians.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("bring_to_an_end_2", "Bring_to_an_End_2.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("death_and_glory_2", "Death_and_Glory_2.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("end_of_days_3", "End_of_Days_3.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("epic_trailer_03", "Epic_Trailer_03.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("flying_serpent", "Flying_Serpent.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("into_the_inferno", "Into_the_Inferno.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("last_man_standing", "Last_Man_Standing.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("last_man_standing_1", "Last_Man_Standing_1.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("last_man_standing_3", "Last_Man_Standing_3.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("never_give_up", "Never_Give_Up.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("super_hero_2", "Super_Hero_2.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("super_hero_5", "Super_Hero_5.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("termination_1", "Termination_1.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("the_beast", "The_Beast.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("the_fiery_breath", "The_Fiery_Breath.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("theme_of_battle", "Theme_of_Battle.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  ("theme_of_war", "Theme_of_War.ogg", mtf_module_track|mtf_looping|mtf_sit_tavern, mtf_sit_tavern),
  
  # Major Boss #
  ("choirs_of_war", "Choirs_of_War.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("choirs_of_war_2", "Choirs_of_War_2.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("elysium_3", "Elysium_3.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("epic_trailer_04", "Epic_Trailer_04.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("goliath_1", "Goliath_1.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("goliath_2", "Goliath_2.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("goliath_3", "Goliath_3.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("heroica", "Heroica.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("packs_of_savage_dogs", "Packs_of_Savage_Dogs.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("rising", "Rising.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("skyfall_1", "Skyfall_1.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("skyfall_2", "Skyfall_2.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("skyfall_3", "Skyfall_3.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("super_hero_3", "Super_Hero_3.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("the_beast_2", "The_Beast_2.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  ("underworld", "Underworld.ogg", mtf_module_track|mtf_looping|mtf_sit_ambushed, mtf_sit_ambushed),
  
  # Dev Wave #
  ("olympus_extended", "Olympus_Extended.ogg", mtf_module_track|mtf_sit_town, mtf_sit_town|mtf_sit_multiplayer_fight),
  ("skyfall_extended", "Skyfall_Extended.ogg", mtf_module_track|mtf_looping|mtf_sit_town, mtf_sit_town),
  
  # Credits #
  ("credits", "credits.ogg", mtf_module_track|mtf_sit_feast, 0),
]
