from module_info import *
import string

dword      = 0x8000000000000000
dword_mask = 0xffffffffffffffff

density_bits      = 32
fkf_density_mask  = 0xFFFF #16K

#terain condition flags
fkf_plain             = 0x00000004
fkf_steppe            = 0x00000008
fkf_snow              = 0x00000010
fkf_desert            = 0x00000020
fkf_plain_forest      = 0x00000400
fkf_steppe_forest     = 0x00000800
fkf_snow_forest       = 0x00001000
fkf_desert_forest     = 0x00002000
fkf_terrain_mask      = 0x0000ffff

fkf_realtime_ligting  = 0x00010000 #deprecated
fkf_point_up          = 0x00020000 #uses auto-generated point-up(quad) geometry for the flora kind
fkf_align_with_ground = 0x00040000 #align the flora object with the ground normal
fkf_grass             = 0x00080000 #is grass
fkf_on_green_ground   = 0x00100000 #populate this flora on green ground
fkf_rock              = 0x00200000 #is rock 
fkf_tree              = 0x00400000 #is tree -> note that if you set this parameter, you should pass additional alternative tree definitions
fkf_snowy             = 0x00800000
fkf_guarantee         = 0x01000000

fkf_speedtree         = 0x02000000  #NOT FUNCTIONAL: we have removed speedtree support on M&B Warband

fkf_has_colony_props  = 0x04000000  # if fkf_has_colony_props -> then you can define colony_radius and colony_treshold of the flora kind


def density(g):
  if (g > fkf_density_mask):
    g = fkf_density_mask
  return ((dword | g) << density_bits)


fauna_kinds = [
  ("grass",fkf_grass|fkf_on_green_ground|fkf_guarantee|fkf_align_with_ground|fkf_point_up|fkf_plain|fkf_plain_forest|density(1500),[["grass_a","0"],["grass_b","0"],["grass_c","0"],["grass_d","0"],["grass_e","0"]]),
  ("grass_bush",fkf_grass|fkf_align_with_ground|fkf_plain|fkf_steppe|fkf_steppe_forest|density(10),[["grass_bush_a","0"],["grass_bush_b","0"]]),
  ("grass_saz",fkf_grass|fkf_on_green_ground|fkf_plain|fkf_steppe|fkf_steppe_forest|density(500),[["grass_bush_c","0"],["grass_bush_d","0"]]),
  ("grass_purple",fkf_grass|fkf_plain|fkf_steppe|fkf_steppe_forest|density(500),[["grass_bush_e","0"],["grass_bush_f","0"]]),
  ("fern",fkf_grass|fkf_plain_forest|fkf_align_with_ground|density(1000),[["fern_a","0"],["fern_b","0"]]),
  ("grass_steppe",fkf_grass|fkf_on_green_ground|fkf_guarantee|fkf_align_with_ground|fkf_point_up|fkf_steppe|fkf_steppe_forest|density(1500),[["grass_yellow_a","0"],["grass_yellow_b","0"],["grass_yellow_c","0"],["grass_yellow_d","0"],["grass_yellow_e","0"]]),

  ("grass_bush_g",fkf_grass|fkf_align_with_ground|fkf_steppe|fkf_steppe_forest|fkf_plain|fkf_plain_forest|density(400),[["grass_bush_g01","0"],["grass_bush_g02","0"],["grass_bush_g03","0"]]),
  ("grass_bush_h",fkf_grass|fkf_align_with_ground|fkf_plain|fkf_plain_forest|density(400),[["grass_bush_h01","0"],["grass_bush_h02","0"],["grass_bush_h03","0"]]),
  ("grass_bush_i",fkf_grass|fkf_align_with_ground|fkf_plain|fkf_plain_forest|density(400),[["grass_bush_i01","0"],["grass_bush_i02","0"]]),
  ("grass_bush_j",fkf_grass|fkf_align_with_ground|fkf_steppe|fkf_steppe_forest|fkf_plain|fkf_plain_forest|density(400),[["grass_bush_j01","0"],["grass_bush_j02","0"]]),
  ("grass_bush_k",fkf_grass|fkf_align_with_ground|fkf_plain|fkf_plain_forest|density(400),[["grass_bush_k01","0"],["grass_bush_k02","0"]]),
  ("grass_bush_l",fkf_align_with_ground|fkf_plain|fkf_plain_forest|density(50),[["grass_bush_l01","0"],["grass_bush_l02","0"]]),
  
  ("thorn_a",fkf_align_with_ground|fkf_plain|fkf_plain_forest|density(150),[["thorn_a","0"],["thorn_b","0"],["thorn_c","0"],["thorn_d","0"]]),

  ("basak",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["basak","0"]]),
  ("common_plant",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["common_plant","0"]]),
  ("small_plant",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(50),[["small_plant","0"],["small_plant_b","0"],["small_plant_c","0"]]),
  ("buddy_plant",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(50),[["buddy_plant","0"],["buddy_plant_b","0"]]),
  ("yellow_flower",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(50),[["yellow_flower","0"],["yellow_flower_b","0"]]),
  ("spiky_plant",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_align_with_ground|density(50),[["spiky_plant","0"]]),
  ("seedy_plant",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(50),[["seedy_plant_a","0"]]),
  ("blue_flower",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(30),[["blue_flower","0"]]),
  ("big_bush",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(30),[["big_bush","0"]]),

  ("bushes02_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(30),[["bushes02_a","bo_bushes02_a"],["bushes02_b","bo_bushes02_b"],["bushes02_c","bo_bushes02_c"]]),
  ("bushes03_a",fkf_plain|fkf_plain_forest|density(30),[["bushes03_a","0"],["bushes03_b","0"],["bushes03_c","0"]]),
  ("bushes04_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bushes04_a","0"],["bushes04_b","0"],["bushes04_c","0"]]),
  ("bushes05_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bushes05_a","0"],["bushes05_b","0"],["bushes05_c","0"]]),
  ("bushes06_a",fkf_steppe|fkf_steppe_forest|density(70),[["bushes06_a","0"],["bushes06_b","0"],["bushes06_c","0"]]),
  ("bushes07_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bushes07_a","0"],["bushes07_b","0"],["bushes07_c","0"]]),
  ("bushes08_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bushes08_a","0"],["bushes08_b","0"],["bushes08_c","0"]]),
  ("bushes09_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bushes09_a","0"],["bushes09_b","0"],["bushes09_c","0"]]),
  ("bushes10_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bushes10_a","0"],["bushes10_b","0"],["bushes10_c","0"]]),
  ("bushes11_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bushes11_a","0"],["bushes11_b","0"],["bushes11_c","0"]]),
  ("bushes12_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bushes12_a","0"],["bushes12_b","0"],["bushes12_c","0"]]),

  ("aspen",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["aspen_a","bo_aspen_a",("0","0")],["aspen_b","bo_aspen_b",("0","0")],["aspen_c","bo_aspen_c",("0","0")]]),
  ("pine_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("pine_1_a","bo_pine_1_a",("0","0")),("pine_1_b","bo_pine_1_b",("0","0"))]),
  ("pine_2",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["pine_2_a","bo_pine_2_a",("0","0")]]),
  ("pine_3",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["pine_3_a","bo_pine_3_a",("0","0")]]),
  ("pine_4",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["pine_4_a","bo_pine_4_a",("0","0")]]),
  ("snowy_pine",fkf_snow|fkf_snow_forest|fkf_tree|density(5),[["tree_snowy_a","bo_tree_snowy_a",("0","0")]]),
  ("snowy_pine_2",fkf_snow|fkf_snow_forest|fkf_tree|density(5),[["snowy_pine_2","bo_snowy_pine_2",("0","0")]]),
  ("small_rock",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_realtime_ligting|fkf_rock|density(5),[["rock_c","bo_rock_c"],["rock_d","bo_rock_d"],["rock_e","bo_rock_e"],["rock_f","bo_rock_f"],["rock_g","bo_rock_g"],["rock_h","bo_rock_h"],["rock_i","bo_rock_i"],["rock_k","bo_rock_k"]]),
  ("rock_snowy",fkf_snow|fkf_snow_forest|fkf_realtime_ligting|fkf_rock|density(5),[["rock_snowy_a","bo_rock_snowy_a"],["rock_snowy_b","bo_rock_snowy_b"],["rock_snowy_c","bo_rock_snowy_c"],]),

  ("rock",fkf_plain|fkf_align_with_ground|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_realtime_ligting|fkf_rock|density(50),[["rock1","bo_rock1"],["rock2","bo_rock2"],["rock3","bo_rock3"],["rock4","bo_rock4"],["rock5","bo_rock6"],["rock7","bo_rock7"]]),
  ("rock_snowy2",fkf_snow|fkf_snow_forest|fkf_realtime_ligting|fkf_rock|density(5),[["rock1_snowy","bo_rock1"],["rock2_snowy","bo_rock2"],["rock4_snowy","bo_rock4"],["rock6_snowy","bo_rock6"],]),


  ("tree_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_1_a","bo_tree_1_a",("0","0")),("tree_1_b","bo_tree_1_b",("0","0"))]),
  ("tree_3",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_3_a","bo_tree_3_a",("0","0")),("tree_3_b","bo_tree_3_b",("0","0"))]),
  ("tree_4",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_4_a","bo_tree_4_a",("0","0")),("tree_4_b","bo_tree_4_b",("0","0"))]),
  ("tree_5",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_5_a","bo_tree_5_a",("0","0")),("tree_5_b","bo_tree_5_b",("0","0")),("tree_5_c","bo_tree_5_c",("0","0")),("tree_5_d","bo_tree_5_d",("0","0"))]),
  ("tree_6",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_6_a","bo_tree_6_a",("0","0")),("tree_6_b","bo_tree_6_b",("0","0")),("tree_6_c","bo_tree_6_c",("0","0")),("tree_6_d","bo_tree_6_d",("0","0"))]),
  ("tree_7",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_7_a","bo_tree_7_a",("0","0")),("tree_7_b","bo_tree_7_b",("0","0")),("tree_7_c","bo_tree_7_c",("0","0"))]),
  ("tree_8",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_8_a","bo_tree_8_a",("0","0")),("tree_8_b","bo_tree_8_b",("0","0")),("tree_8_c","bo_tree_8_c",("0","0"))]),

  ("tree_9",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_9_a","bo_tree_9_a",("0","0")),("tree_9_b","bo_tree_9_a",("0","0")),("tree_9_c","bo_tree_9_a",("0","0"))]),
  ("tree_10",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_10_a","bo_tree_10_a",("0","0")),("tree_10_b","bo_tree_10_a",("0","0")),("tree_10_c","bo_tree_10_a",("0","0"))]),
  ("tree_11",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_11_a","bo_tree_11_a",("0","0")),("tree_11_b","bo_tree_11_a",("0","0")),("tree_11_c","bo_tree_11_a",("0","0"))]),
  ("tree_12",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_12_a","bo_tree_12_a",("0","0")),("tree_12_b","bo_tree_12_b",("0","0")),("tree_12_c","bo_tree_12_c",("0","0"))]),
  ("tree_14",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_14_a","bo_tree_14_a",("0","0")),("tree_14_b","bo_tree_14_b",("0","0")),("tree_14_c","bo_tree_14_c",("0","0"))]),
  ("tree_15",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_15_a","bo_tree_15_a",("mb_test1","tree_a")),("tree_15_b","bo_tree_15_b",("mb_test1","tree_a")),("tree_15_c","bo_tree_15_c",("mb_test1","tree_b"))]),
  ("tree_16",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_16_a","bo_tree_16_a",("0","0")),("tree_16_b","bo_tree_16_b",("0","0"))]),

  ("tree_17",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_17_a","bo_tree_17_a",("0","0")),("tree_17_b","bo_tree_17_b",("0","0")),("tree_17_c","bo_tree_17_c",("0","0")),("tree_17_d","bo_tree_17_d",("0","0"))]),

  ("palm",fkf_desert_forest|fkf_tree|density(4),[("palm_a","bo_palm_a",("0","0"))]),

  ("tree_new_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_a01","bo_tree_a01",("0","0")),("tree_a02","bo_tree_a01",("0","0"))]),
  ("bush_new_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bush_a01","0"],["bush_a02","0"]]),
  ("bush_new_2",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bush_new_a","0"]]),
  ("bush_new_3",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bush_new_b","0"]]),
  ("bush_new_4",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["bush_new_c","0"]]),

  ("dry_bush",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["dry_bush","0"]]),
  ("dry_leaves",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(70),[["dry_leaves","0"]]),

  ("tree_new_2",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_b01","bo_tree_b01",("0","0")),("tree_b02","bo_tree_b02",("0","0"))]),
  ("tree_new_3",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_c01","bo_tree_c01",("0","0")),("tree_c02","bo_tree_c02",("0","0"))]),

  ("tree_plane",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_18_a","bo_tree_18_a",("0","0")),("tree_18_b","bo_tree_18_b",("0","0"))]),
  ("tree_19",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tree_19_a","bo_tree_19_a",("0","0"))]),
  ("beech",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(3),[("tree_20_a","bo_tree_20_a",("0","0")),("tree_20_b","bo_tree_20_b",("0","0"))]),

  ("tall_tree",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[("tall_tree_a","bo_tall_tree_a",("0","0"))]),

  ("tree_e",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["tree_e_1","bo_tree_e_1",("0","0")],["tree_e_2","bo_tree_e_2",("0","0")],["tree_e_3","bo_tree_e_3",("0","0")]]),
  ("tree_f",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["tree_f_1","bo_tree_f_1",("0","0")],["tree_f_2","bo_tree_f_1",("0","0")],["tree_f_3","bo_tree_f_1",("0","0")]]),
  ("grape_vineyard",density(0),[("grape_vineyard","bo_grape_vineyard")]),
  ("grape_vineyard_stake",density(0),[("grape_vineyard_stake","bo_grape_vineyard_stake")]),  
  
  ("wheat",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(4),[["wheat_a","0"],["wheat_b","0"],["wheat_c","0"],["wheat_d","0"]]),
  
  ("valleyRock_rounded",fkf_rock|density(5),[["valleyRock_rounded_1","bo_valleyRock_rounded_1"],["valleyRock_rounded_2","bo_valleyRock_rounded_2"],["valleyRock_rounded_3","bo_valleyRock_rounded_3"],["valleyRock_rounded_4","bo_valleyRock_rounded_4"]]),
  ("valleyRock_flat",fkf_rock|density(5),[["valleyRock_flat_1","bo_valleyRock_flat_1"],["valleyRock_flat_2","bo_valleyRock_flat_2"],["valleyRock_flat_3","bo_valleyRock_flat_3"],["valleyRock_flat_4","bo_valleyRock_flat_4"],["valleyRock_flat_5","bo_valleyRock_flat_5"],["valleyRock_flat_6","bo_valleyRock_flat_6"]]),
  ("valleyRock_flatRounded_small",fkf_rock|density(5),[["valleyRock_flatRounded_small_1","bo_valleyRock_flatRounded_small_1"],["valleyRock_flatRounded_small_2","bo_valleyRock_flatRounded_small_2"],["valleyRock_flatRounded_small_3","bo_valleyRock_flatRounded_small_3"]]),
  ("valleyRock_flatRounded_big",fkf_rock|density(5),[["valleyRock_flatRounded_big_1","bo_valleyRock_flatRounded_big_1"],["valleyRock_flatRounded_big_2","bo_valleyRock_flatRounded_big_2"]]),
  
  ###FI OSIRIS NEW###
  ("osiris_pine_pattern",fkf_plain|fkf_plain_forest|fkf_tree|density(4),[["pine_pattern_01","bo_pine_pattern_01",("0","0")],["pine_pattern_02","bo_pine_pattern_02",("0","0")],
	["pine_pattern_03","bo_pine_pattern_03",("0","0")],["pine_pattern_04","bo_pine_pattern_04",("0","0")],["pine_pattern_05","bo_pine_pattern_05",("0","0")],["pine_pattern_06","bo_pine_pattern_06",("0","0")],
	["pine_pattern_07","bo_pine_pattern_07",("0","0")],["pine_pattern_08","bo_pine_pattern_08",("0","0")],["pine_pattern_09","bo_pine_pattern_09",("0","0")],["pine_pattern_10","bo_pine_pattern_10",("0","0")],
	["pine_pattern_11","bo_pine_pattern_11",("0","0")],["pine_pattern_12","bo_pine_pattern_12",("0","0")],["pine_pattern_13","bo_pine_pattern_13",("0","0")],["pine_pattern_14","bo_pine_pattern_14",("0","0")],
	["pine_pattern_15","bo_pine_pattern_15",("0","0")]]),
  ("osiris_snowy_pine_pattern",fkf_snow_forest|fkf_tree|density(4),[["snowy_pine_pattern_01","bo_pine_pattern_01",("0","0")],["snowy_pine_pattern_02","bo_pine_pattern_02",("0","0")],
	["snowy_pine_pattern_03","bo_pine_pattern_03",("0","0")],["snowy_pine_pattern_04","bo_pine_pattern_04",("0","0")],["snowy_pine_pattern_05","bo_pine_pattern_05",("0","0")],
	["snowy_pine_pattern_06","bo_pine_pattern_06",("0","0")],["snowy_pine_pattern_07","bo_pine_pattern_07",("0","0")],["snowy_pine_pattern_08","bo_pine_pattern_08",("0","0")],
	["snowy_pine_pattern_09","bo_pine_pattern_09",("0","0")],["snowy_pine_pattern_10","bo_pine_pattern_10",("0","0")],["snowy_pine_pattern_11","bo_pine_pattern_11",("0","0")],
	["snowy_pine_pattern_12","bo_pine_pattern_12",("0","0")],["snowy_pine_pattern_13","bo_pine_pattern_13",("0","0")],["snowy_pine_pattern_14","bo_pine_pattern_14",("0","0")],
	["snowy_pine_pattern_15","bo_pine_pattern_15",("0","0")]]),
  ("osiris_bare_oak",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["bare_oak_big","bo_oak_big",("0","0")],["bare_oak_medium","bo_oak_medium",("0","0")],["bare_oak_old","bo_oak_old",("0","0")]]),
  ("osiris_bare_maple",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["bare_maple_little","bo_maple_little",("0","0")],["bare_maple_little_2","bo_maple_little_2",("0","0")],
	["bare_maple_big","bo_maple_big",("0","0")],["bare_maple_big_2","bo_maple_big_2",("0","0")]]),
  ("osiris_snowy_oak",fkf_snow_forest|fkf_tree|density(4),[["snowy_oak_big","bo_oak_big",("0","0")],["snowy_oak_medium","bo_oak_medium",("0","0")],["snowy_oak_old","bo_oak_old",("0","0")]]),
  ("osiris_snowy_maple",fkf_snow_forest|fkf_tree|density(4),[["snowy_maple_little","bo_maple_little",("0","0")],["snowy_maple_little_2","bo_maple_little_2",("0","0")],
	["snowy_maple_big","bo_maple_big",("0","0")],["snowy_maple_big_2","bo_maple_big_2",("0","0")]]),
  ("osiris_snowy_bushes",fkf_snow_forest|density(30),[["snowy_bushes_01","0"],["snowy_bushes_02","0"],["snowy_bushes_03","0"],["snowy_bushes_04","0"],["snowy_bushes_05","0"],
    ["snowy_bushes_06","0"],["snowy_bushes_07","0"],["snowy_bushes_08","0"]]),
  ("osiris_oak",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["oak_big","bo_oak_big",("0","0")],["oak_medium","bo_oak_medium",("0","0")],["oak_old","bo_oak_old",("0","0")]]),
  ("osiris_maple",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(4),[["maple_little","bo_maple_little",("0","0")],["maple_little_2","bo_maple_little_2",("0","0")],["maple_big","bo_maple_big",("0","0")],
	["maple_big_2","bo_maple_big_2",("0","0")]]),
  ("osiris_bushes",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(30),[["bushes_01","0"],["bushes_02","0"],["bushes_03","0"],["bushes_04","0"],["bushes_05","0"],["bushes_06","0"],["bushes_07","0"],["bushes_08","0"]]),
  ("osiris_wfas_pine",fkf_plain|fkf_plain_forest|fkf_tree|density(4),[["wfas_pine_01","bo_wfas_pine_01",("0","0")],["wfas_pine_02","bo_wfas_pine_02",("0","0")],["wfas_pine_03","bo_wfas_pine_03",("0","0")],
	["wfas_pine_04","bo_wfas_pine_04",("0","0")],["wfas_pine_05","bo_wfas_pine_05",("0","0")],["wfas_pine_06","bo_wfas_pine_06",("0","0")],["wfas_pine_07","bo_wfas_pine_07",("0","0")],["wfas_pine_08","bo_wfas_pine_08",("0","0")],
	["wfas_pine_09","bo_wfas_pine_09",("0","0")],["wfas_pine_10","bo_wfas_pine_10",("0","0")],["wfas_pine_11","bo_wfas_pine_11",("0","0")],["wfas_pine_12","bo_wfas_pine_12",("0","0")],["wfas_pine_13","bo_wfas_pine_13",("0","0")],
	["wfas_pine_14","bo_wfas_pine_14",("0","0")],["wfas_pine_15","bo_wfas_pine_15",("0","0")],["wfas_pine_16","bo_wfas_pine_16",("0","0")],["wfas_pine_17","bo_wfas_pine_17",("0","0")],["wfas_pine_18","bo_wfas_pine_18",("0","0")],
	["wfas_pine_19","bo_wfas_pine_19",("0","0")],["wfas_pine_20","bo_wfas_pine_20",("0","0")],["wfas_pine_21","bo_wfas_pine_21",("0","0")],["wfas_pine_22","bo_wfas_pine_22",("0","0")],["wfas_pine_23","bo_wfas_pine_23",("0","0")],
	["wfas_pine_24","bo_wfas_pine_24",("0","0")],["wfas_pine_25","bo_wfas_pine_25",("0","0")],["wfas_pine_26","bo_wfas_pine_26",("0","0")]]),
  ("osiris_snowy_wfas_pine",fkf_snow_forest|fkf_tree|density(4),[["snowy_wfas_pine_01","bo_wfas_pine_01",("0","0")],["snowy_wfas_pine_02","bo_wfas_pine_02",("0","0")],["snowy_wfas_pine_03","bo_wfas_pine_03",("0","0")],
	["snowy_wfas_pine_04","bo_wfas_pine_04",("0","0")],["snowy_wfas_pine_05","bo_wfas_pine_05",("0","0")],["snowy_wfas_pine_06","bo_wfas_pine_06",("0","0")],["snowy_wfas_pine_07","bo_wfas_pine_07",("0","0")],
	["snowy_wfas_pine_08","bo_wfas_pine_08",("0","0")],["snowy_wfas_pine_09","bo_wfas_pine_09",("0","0")],["snowy_wfas_pine_10","bo_wfas_pine_10",("0","0")],["snowy_wfas_pine_11","bo_wfas_pine_11",("0","0")],
	["snowy_wfas_pine_12","bo_wfas_pine_12",("0","0")],["snowy_wfas_pine_13","bo_wfas_pine_13",("0","0")],["snowy_wfas_pine_14","bo_wfas_pine_14",("0","0")],["snowy_wfas_pine_15","bo_wfas_pine_15",("0","0")],
	["snowy_wfas_pine_16","bo_wfas_pine_16",("0","0")],["snowy_wfas_pine_17","bo_wfas_pine_17",("0","0")],["snowy_wfas_pine_18","bo_wfas_pine_18",("0","0")],["snowy_wfas_pine_19","bo_wfas_pine_19",("0","0")],
	["snowy_wfas_pine_20","bo_wfas_pine_20",("0","0")],["snowy_wfas_pine_21","bo_wfas_pine_21",("0","0")],["snowy_wfas_pine_22","bo_wfas_pine_22",("0","0")],["snowy_wfas_pine_23","bo_wfas_pine_23",("0","0")],
	["snowy_wfas_pine_24","bo_wfas_pine_24",("0","0")],["snowy_wfas_pine_25","bo_wfas_pine_25",("0","0")],["snowy_wfas_pine_26","bo_wfas_pine_26",("0","0")]]),
  ("osiris_bushes_oak",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(30),[["bushes_oak_01","0"],["bushes_oak_02","0"],["bushes_oak_03","0"],["bushes_oak_04","0"],["bushes_oak_05","0"],["bushes_oak_06","0"],
	["bushes_oak_07","0"],["bushes_oak_08","0"]]),
  ("osiris_snowy_bushes_oak",fkf_snow_forest|density(30),[["snowy_bushes_oak_01","0"],["snowy_bushes_oak_02","0"],["snowy_bushes_oak_03","0"],["snowy_bushes_oak_04","0"],["snowy_bushes_oak_05","0"],["snowy_bushes_oak_06","0"],
	["snowy_bushes_oak_07","0"],["snowy_bushes_oak_08","0"]]),
	
	#IMPORTS#
	('zl_fir',
  fkf_tree|density(5),
  [['PL_fur1', 'bo_pl_fur1'],
   ['PL_fur2', 'bo_pl_fur2'],
   ['PL_fur3', 'bo_pl_fur3'],]),

 ('zl_fir_tall',
  fkf_snow_forest|fkf_desert_forest|fkf_tree|density(5),
  [['PL_fur_tall1', 'bo_pl_fur_tall1'],
   ['PL_fur_tall2', 'bo_pl_fur_tall2'],
   ['PL_fur_tall3', 'bo_pl_fur_tall3']]),

 ('zl_fir_shubby',
  fkf_snow_forest|fkf_desert_forest|fkf_tree|density(1),
  [['PW_tree_2_a', 'bo_pw_tree_2_a_cyl'],
   ['PW_tree_2_b', 'bo_pw_tree_2_b_cyl'],
   ['PW_tree_e_1', 'bo_pw_tree_e_1'],
   ['PW_tree_e_2', 'bo_pw_tree_e_2'],
   ['PW_tree_e_3', 'bo_pw_tree_e_3']]),

 ('zl_birch_yellow',
  fkf_tree,
  [['PW_tree_4_a', 'bo_pw_tree_4_a_cyl'],
   ['PW_tree_4_b', 'bo_pw_tree_4_b_cyl']]),

 ('zl_birch_green',
  fkf_tree,
  [['PW_tree_5_a', 'bo_pw_tree_5_a_cyl'],
   ['PW_tree_5_b', 'bo_pw_tree_5_b_cyl'],
   ['PW_tree_5_c', 'bo_pw_tree_5_c_cyl'],
   ['PW_tree_5_d', 'bo_pw_tree_5_d_cyl']]),

 ('zl_fir_shubby_group',
  fkf_desert_forest|fkf_tree|density(2),
  [['PW_tree_6_a', 'bo_pw_tree_6_a'],
   ['PW_tree_6_b', 'bo_pw_tree_6_b'],
   ['PW_tree_6_c', 'bo_pw_tree_6_c'],
   ['PW_tree_6_d', 'bo_pw_tree_6_d']]),

 ('zl_green2_group',
  fkf_tree,
  [['PW_tree_8_a', 'bo_pw_tree_8_a'],
   ['PW_tree_8_b', 'bo_pw_tree_8_b'],
   ['PW_tree_8_c', 'bo_pw_tree_8_c']]),

 ('zl_skeleton_man',
  0,
  [['PW_tree_7_a', '0'], ['PW_tree_7_b', '0'], ['PW_tree_7_c', '0']]),

 ('zl_green_group_small',
  fkf_tree,
  [['PW_tree_10_a', 'bo_pw_tree_10_a'],
   ['PW_tree_10_b', 'bo_pw_tree_10_b'],
   ['PW_tree_10_c', 'bo_pw_tree_10_c']]),

 ('zl_green_group_large',
  fkf_tree,
  [['PW_tree_11_a', 'bo_pw_tree_11_a'],
   ['PW_tree_11_b', 'bo_pw_tree_11_b'],
   ['PW_tree_11_c', 'bo_pw_tree_11_c']]),

 ('zl_birch_tall',
  fkf_steppe_forest|fkf_tree|density(4),
  [['PW_tree_14_a', 'bo_pw_tree_14_a_cyl'],
   ['PW_tree_14_b', 'bo_pw_tree_14_b_cyl'],
   ['PW_tree_14_c', 'bo_pw_tree_14_c_cyl']]),

 ('zl_tree_16',
  fkf_plain_forest|fkf_steppe_forest|fkf_tree|density(4),
  [['PW_tree_16_a', 'bo_pw_tree_16_a_cyl'],
   ['PW_tree_16_b', 'bo_pw_tree_16_b_cyl']]),

 ('zl_birch_green_group',
  fkf_tree,
  [['PW_tree_17_a', 'bo_pw_tree_17_a'],
   ['PW_tree_17_b', 'bo_pw_tree_17_b'],
   ['PW_tree_17_c', 'bo_pw_tree_17_c'],
   ['PW_tree_17_d', 'bo_pw_tree_17_d']]),

 ('zl_birch_yellow_group',
  fkf_tree,
  [['PW_tree_18_a', 'bo_pw_tree_18_a_cyl'],
   ['PW_tree_18_b', 'bo_pw_tree_18_b_cyl']]),

 ('zl_aspen_yellow',
  fkf_tree,
  [['PW_tree_f_1', 'bo_pw_tree_f_1'],
   ['PW_tree_f_2', 'bo_pw_tree_f_2'],
   ['PW_tree_f_3', 'bo_pw_tree_f_3']]),

 ('zl_aspen_yellow_bush',
  fkf_tree,
  [['PL_aspen_yellowbush1', '0'],
   ['PL_aspen_yellowbush2', '0'],
   ['PL_aspen_yellowbush3', '0']]),

 ('zl_oak_group',
  fkf_tree,
  [['PL_oak_group1', 'bo_pl_oak_group1'],
   ['PL_oak_group2', 'bo_pl_oak_group2'],
   ['PL_oak_group3', 'bo_pl_oak_group3']]),

 ('zl_bush_white_flowers',
  0,
  [['PW_grass_bush_l01', '0'], ['PW_grass_bush_l02', '0']]),

 ('zl_bush_08',
  0,
  [['PW_bushes08_a_xx', '0'],
   ['PW_bushes08_b_xx', '0'],
   ['PW_bushes08_c_xx', '0']]),

 ('zl_fir_bush',
  fkf_desert|fkf_snow_forest|fkf_desert_forest|density(10),
  [['PW_bushes09_a', '0'], ['PW_bushes09_b', '0'], ['PW_bushes09_c', '0']]),

 ('zl_birch_green_bush',
  0,
  [['PW_bushes10_a', '0'], ['PW_bushes10_b', '0'], ['PW_bushes10_c', '0']]),

 ('zl_shalebush',
  fkf_desert_forest|density(2),
  [['PW_bushes11_a', '0'], ['PW_bushes11_b', '0'], ['PW_bushes11_c', '0']]),

 ('zl_bush_steppe_wheat',
  0,
  [['PW_bushes12_a_xx', '0'],
   ['PW_bushes12_b_xx', '0'],
   ['PW_bushes12_c_xx', '0']]),

 ('zl_pink_tree',
  fkf_tree|fkf_plain|density(10),					#InVain: added to plain (Gondor)
  [['PW_tall_tree_a', 'bo_pw_tall_tree_a_cyl'],
   ['PW_pine_4_a', 'bo_pw_pine_4_a_cyl'],
   ['PW_pine_6_a', 'bo_pw_pine_6_a_cyl']]),

 ('zl_white_flowers',
  fkf_align_with_ground|fkf_grass|density(2),
  [['PW_grass_bush_l01', '0'], ['PW_grass_bush_l02', '0']]),

 ('zl_yellow_flowers',
  fkf_align_with_ground|fkf_grass|density(2),
  [['PW_spiky_plant', '0']]),

 ('rock_snowy2', fkf_tree, [['PW_tree_snowy_b', 'bo_pw_tree_snowy_b']]),

 ('zl_reed', fkf_snow|fkf_guarantee|density(194), [['GA_reed1', '0']]),

 ('ga_grass_snow',
  fkf_snow|fkf_snow_forest|fkf_align_with_ground|fkf_grass|fkf_on_green_ground|fkf_guarantee|density(1666),
  [['PW_grass_a_xx', '0'],
   ['PW_grass_b_xx', '0'],
   ['PW_grass_b_xx', '0'],
   ['PW_grass_b_xx', '0'],
   ['PW_grass_e_xx', '0']]),

 ('ga_bushes04_a_snow',
  fkf_snow|fkf_snow_forest|fkf_align_with_ground|fkf_guarantee|density(390),
  [['PW_bushes04_a_xx', '0'],
   ['PW_bushes04_b_xx', '0'],
   ['PW_bushes04_c_xx', '0']]),

 ('ga_grass_desert',
 #fkf_desert|fkf_desert_forest|fkf_align_with_ground|fkf_grass|fkf_on_green_ground|fkf_guarantee|fkf_has_colony_props|density(2012),
  fkf_desert|fkf_desert_forest|fkf_align_with_ground|fkf_grass|fkf_on_green_ground|fkf_guarantee|density(2012),
  [['PW_grass_e_xx', '0'],
   ['PW_grass_yellow_b_xx', '0'],
   ['PW_grass_yellow_b_xx', '0'],
   ['PW_grass_e_xx', '0'],
   ['PW_grass_yellow_e', '0']]),

 ('zl_fir_shubby_single',
  fkf_snow|fkf_desert_forest|fkf_tree|density(5),
  [['PW_tree_2_a_single1', 'bo_pw_tree_2_a_single1'],
   ['PW_tree_2_a_single2', '0'],
   ['PW_tree_2_a_single3', 'bo_pw_tree_2_a_single3'],
   ['PW_tree_2_a_single2_dark', '0']]),

 ('ga_tree_3_a_brown',
  fkf_desert|fkf_desert_forest|density(22),
  [['PW_tree_3_a_brown', '0'], ['PW_tree_3_b_brown', '0']]),
]


def save_fauna_kinds():
  file = open("./flora_kinds.txt","w")
  file.write("%d\n"%len(fauna_kinds))
  for fauna_kind in fauna_kinds:
    meshes_list = fauna_kind[2]
    file.write("%s %d %d\n"%(fauna_kind[0], (dword_mask & fauna_kind[1]), len(meshes_list)))
    for m in meshes_list:
      file.write(" %s "%(m[0]))
      if (len(m) > 1):
        file.write(" %s\n"%(m[1]))
      else:
        file.write(" 0\n")
      #print(fauna_kind)
      if ( fauna_kind[1] & (fkf_tree|fkf_speedtree) ):  #if this fails make sure that you have entered the alternative tree definition (NOT FUNCTIONAL in Warband)
        #swyter-- lolwtfbbq tw??
        #speedtree_alternative = m[2]
        #file.write(" %s %s\n"%(speedtree_alternative[0], speedtree_alternative[1]))
        file.write(" %s %s\n"%(0, 0))
    #if ( fauna_kind[1] & fkf_has_colony_props ):
    #  file.write(" %s %s\n"%(fauna_kind[3], fauna_kind[4]))
  file.close()

def two_to_pow(x):
  result = 1
  for i in xrange(x):
    result = result * 2
  return result

fauna_mask = 0x80000000000000000000000000000000
low_fauna_mask =             0x8000000000000000
def save_python_header():
  file = open("./fauna_codes.py","w")
  for i_fauna_kind in xrange(len(fauna_kinds)):
    file.write("%s_1 = 0x"%(fauna_kinds[i_fauna_kind][0]))
    file.write("%x\n"%(fauna_mask | two_to_pow(i_fauna_kind)))
    file.write("%s_2 = 0x"%(fauna_kinds[i_fauna_kind][0]))
    file.write("%x\n"%(fauna_mask | ((low_fauna_mask|two_to_pow(i_fauna_kind)) << 64)))
    file.write("%s_3 = 0x"%(fauna_kinds[i_fauna_kind][0]))
    file.write("%x\n"%(fauna_mask | ((low_fauna_mask|two_to_pow(i_fauna_kind)) << 64) | two_to_pow(i_fauna_kind)))
  file.close()

print "Exporting flora data..."
save_fauna_kinds()