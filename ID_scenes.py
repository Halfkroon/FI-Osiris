scn_random_scene = 0
scn_conversation_scene = 1
scn_water = 2
scn_random_scene_steppe = 3
scn_random_scene_plain = 4
scn_random_scene_snow = 5
scn_random_scene_desert = 6
scn_random_scene_steppe_forest = 7
scn_random_scene_plain_forest = 8
scn_random_scene_snow_forest = 9
scn_random_scene_desert_forest = 10
scn_camp_scene = 11
scn_camp_scene_horse_track = 12
scn_four_ways_inn = 13
scn_test_scene = 14
scn_quick_battle_1 = 15
scn_quick_battle_2 = 16
scn_quick_battle_3 = 17
scn_quick_battle_4 = 18
scn_quick_battle_5 = 19
scn_quick_battle_6 = 20
scn_quick_battle_7 = 21
scn_salt_mine = 22
scn_novice_ground = 23
scn_zendar_arena = 24
scn_dhorak_keep = 25
scn_reserved4 = 26
scn_reserved5 = 27
scn_reserved6 = 28
scn_reserved7 = 29
scn_reserved8 = 30
scn_reserved9 = 31
scn_reserved10 = 32
scn_reserved11 = 33
scn_reserved12 = 34
scn_training_ground = 35
scn_tutorial_1 = 36
scn_tutorial_2 = 37
scn_tutorial_3 = 38
scn_tutorial_4 = 39
scn_tutorial_5 = 40
scn_training_ground_horse_track_1 = 41
scn_training_ground_horse_track_2 = 42
scn_training_ground_horse_track_3 = 43
scn_training_ground_horse_track_4 = 44
scn_training_ground_horse_track_5 = 45
scn_training_ground_ranged_melee_1 = 46
scn_training_ground_ranged_melee_2 = 47
scn_training_ground_ranged_melee_3 = 48
scn_training_ground_ranged_melee_4 = 49
scn_training_ground_ranged_melee_5 = 50
scn_zendar_center = 51
scn_the_happy_boar = 52
scn_zendar_merchant = 53
scn_town_1_center = 54
scn_town_2_center = 55
scn_town_3_center = 56
scn_town_4_center = 57
scn_town_5_center = 58
scn_town_6_center = 59
scn_town_7_center = 60
scn_town_8_center = 61
scn_town_9_center = 62
scn_town_10_center = 63
scn_town_11_center = 64
scn_town_12_center = 65
scn_town_13_center = 66
scn_town_14_center = 67
scn_town_15_center = 68
scn_town_16_center = 69
scn_town_17_center = 70
scn_town_18_center = 71
scn_town_19_center = 72
scn_town_20_center = 73
scn_town_21_center = 74
scn_town_22_center = 75
scn_town_1_castle = 76
scn_town_2_castle = 77
scn_town_3_castle = 78
scn_town_4_castle = 79
scn_town_5_castle = 80
scn_town_6_castle = 81
scn_town_7_castle = 82
scn_town_8_castle = 83
scn_town_9_castle = 84
scn_town_10_castle = 85
scn_town_11_castle = 86
scn_town_12_castle = 87
scn_town_13_castle = 88
scn_town_14_castle = 89
scn_town_15_castle = 90
scn_town_16_castle = 91
scn_town_17_castle = 92
scn_town_18_castle = 93
scn_town_19_castle = 94
scn_town_20_castle = 95
scn_town_21_castle = 96
scn_town_22_castle = 97
scn_town_1_tavern = 98
scn_town_2_tavern = 99
scn_town_3_tavern = 100
scn_town_4_tavern = 101
scn_town_5_tavern = 102
scn_town_6_tavern = 103
scn_town_7_tavern = 104
scn_town_8_tavern = 105
scn_town_9_tavern = 106
scn_town_10_tavern = 107
scn_town_11_tavern = 108
scn_town_12_tavern = 109
scn_town_13_tavern = 110
scn_town_14_tavern = 111
scn_town_15_tavern = 112
scn_town_16_tavern = 113
scn_town_17_tavern = 114
scn_town_18_tavern = 115
scn_town_19_tavern = 116
scn_town_20_tavern = 117
scn_town_21_tavern = 118
scn_town_22_tavern = 119
scn_town_1_store = 120
scn_town_2_store = 121
scn_town_3_store = 122
scn_town_4_store = 123
scn_town_5_store = 124
scn_town_6_store = 125
scn_town_7_store = 126
scn_town_8_store = 127
scn_town_9_store = 128
scn_town_10_store = 129
scn_town_11_store = 130
scn_town_12_store = 131
scn_town_13_store = 132
scn_town_14_store = 133
scn_town_15_store = 134
scn_town_16_store = 135
scn_town_17_store = 136
scn_town_18_store = 137
scn_town_19_store = 138
scn_town_20_store = 139
scn_town_21_store = 140
scn_town_22_store = 141
scn_town_1_arena = 142
scn_town_2_arena = 143
scn_town_3_arena = 144
scn_town_4_arena = 145
scn_town_5_arena = 146
scn_town_6_arena = 147
scn_town_7_arena = 148
scn_town_8_arena = 149
scn_town_9_arena = 150
scn_town_10_arena = 151
scn_town_11_arena = 152
scn_town_12_arena = 153
scn_town_13_arena = 154
scn_town_14_arena = 155
scn_town_15_arena = 156
scn_town_16_arena = 157
scn_town_17_arena = 158
scn_town_18_arena = 159
scn_town_19_arena = 160
scn_town_20_arena = 161
scn_town_21_arena = 162
scn_town_22_arena = 163
scn_town_1_prison = 164
scn_town_2_prison = 165
scn_town_3_prison = 166
scn_town_4_prison = 167
scn_town_5_prison = 168
scn_town_6_prison = 169
scn_town_7_prison = 170
scn_town_8_prison = 171
scn_town_9_prison = 172
scn_town_10_prison = 173
scn_town_11_prison = 174
scn_town_12_prison = 175
scn_town_13_prison = 176
scn_town_14_prison = 177
scn_town_15_prison = 178
scn_town_16_prison = 179
scn_town_17_prison = 180
scn_town_18_prison = 181
scn_town_19_prison = 182
scn_town_20_prison = 183
scn_town_21_prison = 184
scn_town_22_prison = 185
scn_town_1_walls = 186
scn_town_2_walls = 187
scn_town_3_walls = 188
scn_town_4_walls = 189
scn_town_5_walls = 190
scn_town_6_walls = 191
scn_town_7_walls = 192
scn_town_8_walls = 193
scn_town_9_walls = 194
scn_town_10_walls = 195
scn_town_11_walls = 196
scn_town_12_walls = 197
scn_town_13_walls = 198
scn_town_14_walls = 199
scn_town_15_walls = 200
scn_town_16_walls = 201
scn_town_17_walls = 202
scn_town_18_walls = 203
scn_town_19_walls = 204
scn_town_20_walls = 205
scn_town_21_walls = 206
scn_town_22_walls = 207
scn_town_1_alley = 208
scn_town_2_alley = 209
scn_town_3_alley = 210
scn_town_4_alley = 211
scn_town_5_alley = 212
scn_town_6_alley = 213
scn_town_7_alley = 214
scn_town_8_alley = 215
scn_town_9_alley = 216
scn_town_10_alley = 217
scn_town_11_alley = 218
scn_town_12_alley = 219
scn_town_13_alley = 220
scn_town_14_alley = 221
scn_town_15_alley = 222
scn_town_16_alley = 223
scn_town_17_alley = 224
scn_town_18_alley = 225
scn_town_19_alley = 226
scn_town_20_alley = 227
scn_town_21_alley = 228
scn_town_22_alley = 229
scn_castle_1_exterior = 230
scn_castle_1_interior = 231
scn_castle_1_prison = 232
scn_castle_2_exterior = 233
scn_castle_2_interior = 234
scn_castle_2_prison = 235
scn_castle_3_exterior = 236
scn_castle_3_interior = 237
scn_castle_3_prison = 238
scn_castle_4_exterior = 239
scn_castle_4_interior = 240
scn_castle_4_prison = 241
scn_castle_5_exterior = 242
scn_castle_5_interior = 243
scn_castle_5_prison = 244
scn_castle_6_exterior = 245
scn_castle_6_interior = 246
scn_castle_6_prison = 247
scn_castle_7_exterior = 248
scn_castle_7_interior = 249
scn_castle_7_prison = 250
scn_castle_8_exterior = 251
scn_castle_8_interior = 252
scn_castle_8_prison = 253
scn_castle_9_exterior = 254
scn_castle_9_interior = 255
scn_castle_9_prison = 256
scn_castle_10_exterior = 257
scn_castle_10_interior = 258
scn_castle_10_prison = 259
scn_castle_11_exterior = 260
scn_castle_11_interior = 261
scn_castle_11_prison = 262
scn_castle_12_exterior = 263
scn_castle_12_interior = 264
scn_castle_12_prison = 265
scn_castle_13_exterior = 266
scn_castle_13_interior = 267
scn_castle_13_prison = 268
scn_castle_14_exterior = 269
scn_castle_14_interior = 270
scn_castle_14_prison = 271
scn_castle_15_exterior = 272
scn_castle_15_interior = 273
scn_castle_15_prison = 274
scn_castle_16_exterior = 275
scn_castle_16_interior = 276
scn_castle_16_prison = 277
scn_castle_17_exterior = 278
scn_castle_17_interior = 279
scn_castle_17_prison = 280
scn_castle_18_exterior = 281
scn_castle_18_interior = 282
scn_castle_18_prison = 283
scn_castle_19_exterior = 284
scn_castle_19_interior = 285
scn_castle_19_prison = 286
scn_castle_20_exterior = 287
scn_castle_20_interior = 288
scn_castle_20_prison = 289
scn_castle_21_exterior = 290
scn_castle_21_interior = 291
scn_castle_21_prison = 292
scn_castle_22_exterior = 293
scn_castle_22_interior = 294
scn_castle_22_prison = 295
scn_castle_23_exterior = 296
scn_castle_23_interior = 297
scn_castle_23_prison = 298
scn_castle_24_exterior = 299
scn_castle_24_interior = 300
scn_castle_24_prison = 301
scn_castle_25_exterior = 302
scn_castle_25_interior = 303
scn_castle_25_prison = 304
scn_castle_26_exterior = 305
scn_castle_26_interior = 306
scn_castle_26_prison = 307
scn_castle_27_exterior = 308
scn_castle_27_interior = 309
scn_castle_27_prison = 310
scn_castle_28_exterior = 311
scn_castle_28_interior = 312
scn_castle_28_prison = 313
scn_castle_29_exterior = 314
scn_castle_29_interior = 315
scn_castle_29_prison = 316
scn_castle_30_exterior = 317
scn_castle_30_interior = 318
scn_castle_30_prison = 319
scn_castle_31_exterior = 320
scn_castle_31_interior = 321
scn_castle_31_prison = 322
scn_castle_32_exterior = 323
scn_castle_32_interior = 324
scn_castle_32_prison = 325
scn_castle_33_exterior = 326
scn_castle_33_interior = 327
scn_castle_33_prison = 328
scn_castle_34_exterior = 329
scn_castle_34_interior = 330
scn_castle_34_prison = 331
scn_castle_35_exterior = 332
scn_castle_35_interior = 333
scn_castle_35_prison = 334
scn_castle_36_exterior = 335
scn_castle_36_interior = 336
scn_castle_36_prison = 337
scn_castle_37_exterior = 338
scn_castle_37_interior = 339
scn_castle_37_prison = 340
scn_castle_38_exterior = 341
scn_castle_38_interior = 342
scn_castle_38_prison = 343
scn_castle_39_exterior = 344
scn_castle_39_interior = 345
scn_castle_39_prison = 346
scn_castle_40_exterior = 347
scn_castle_40_interior = 348
scn_castle_40_prison = 349
scn_castle_41_exterior = 350
scn_castle_41_interior = 351
scn_castle_41_prison = 352
scn_castle_42_exterior = 353
scn_castle_42_interior = 354
scn_castle_42_prison = 355
scn_castle_43_exterior = 356
scn_castle_43_interior = 357
scn_castle_43_prison = 358
scn_castle_44_exterior = 359
scn_castle_44_interior = 360
scn_castle_44_prison = 361
scn_castle_45_exterior = 362
scn_castle_45_interior = 363
scn_castle_45_prison = 364
scn_castle_46_exterior = 365
scn_castle_46_interior = 366
scn_castle_46_prison = 367
scn_castle_47_exterior = 368
scn_castle_47_interior = 369
scn_castle_47_prison = 370
scn_castle_48_exterior = 371
scn_castle_48_interior = 372
scn_castle_48_prison = 373
scn_village_1 = 374
scn_village_2 = 375
scn_village_3 = 376
scn_village_4 = 377
scn_village_5 = 378
scn_village_6 = 379
scn_village_7 = 380
scn_village_8 = 381
scn_village_9 = 382
scn_village_10 = 383
scn_village_11 = 384
scn_village_12 = 385
scn_village_13 = 386
scn_village_14 = 387
scn_village_15 = 388
scn_village_16 = 389
scn_village_17 = 390
scn_village_18 = 391
scn_village_19 = 392
scn_village_20 = 393
scn_village_21 = 394
scn_village_22 = 395
scn_village_23 = 396
scn_village_24 = 397
scn_village_25 = 398
scn_village_26 = 399
scn_village_27 = 400
scn_village_28 = 401
scn_village_29 = 402
scn_village_30 = 403
scn_village_31 = 404
scn_village_32 = 405
scn_village_33 = 406
scn_village_34 = 407
scn_village_35 = 408
scn_village_36 = 409
scn_village_37 = 410
scn_village_38 = 411
scn_village_39 = 412
scn_village_40 = 413
scn_village_41 = 414
scn_village_42 = 415
scn_village_43 = 416
scn_village_44 = 417
scn_village_45 = 418
scn_village_46 = 419
scn_village_47 = 420
scn_village_48 = 421
scn_village_49 = 422
scn_village_50 = 423
scn_village_51 = 424
scn_village_52 = 425
scn_village_53 = 426
scn_village_54 = 427
scn_village_55 = 428
scn_village_56 = 429
scn_village_57 = 430
scn_village_58 = 431
scn_village_59 = 432
scn_village_60 = 433
scn_village_61 = 434
scn_village_62 = 435
scn_village_63 = 436
scn_village_64 = 437
scn_village_65 = 438
scn_village_66 = 439
scn_village_67 = 440
scn_village_68 = 441
scn_village_69 = 442
scn_village_70 = 443
scn_village_71 = 444
scn_village_72 = 445
scn_village_73 = 446
scn_village_74 = 447
scn_village_75 = 448
scn_village_76 = 449
scn_village_77 = 450
scn_village_78 = 451
scn_village_79 = 452
scn_village_80 = 453
scn_village_81 = 454
scn_village_82 = 455
scn_village_83 = 456
scn_village_84 = 457
scn_village_85 = 458
scn_village_86 = 459
scn_village_87 = 460
scn_village_88 = 461
scn_village_89 = 462
scn_village_90 = 463
scn_village_91 = 464
scn_village_92 = 465
scn_village_93 = 466
scn_village_94 = 467
scn_village_95 = 468
scn_village_96 = 469
scn_village_97 = 470
scn_village_98 = 471
scn_village_99 = 472
scn_village_100 = 473
scn_village_101 = 474
scn_village_102 = 475
scn_village_103 = 476
scn_village_104 = 477
scn_village_105 = 478
scn_village_106 = 479
scn_village_107 = 480
scn_village_108 = 481
scn_village_109 = 482
scn_village_110 = 483
scn_field_1 = 484
scn_field_2 = 485
scn_field_3 = 486
scn_field_4 = 487
scn_field_5 = 488
scn_test2 = 489
scn_test3 = 490
scn_multi_scene_1 = 491
scn_multi_scene_2 = 492
scn_multi_scene_3 = 493
scn_multi_scene_4 = 494
scn_multi_scene_5 = 495
scn_multi_scene_6 = 496
scn_multi_scene_7 = 497
scn_multi_scene_8 = 498
scn_multi_scene_9 = 499
scn_multi_scene_10 = 500
scn_multi_scene_11 = 501
scn_multi_scene_12 = 502
scn_multi_scene_13 = 503
scn_multi_scene_14 = 504
scn_multi_scene_15 = 505
scn_multi_scene_16 = 506
scn_multi_scene_17 = 507
scn_multi_scene_18 = 508
scn_random_multi_plain_medium = 509
scn_random_multi_plain_large = 510
scn_random_multi_steppe_medium = 511
scn_random_multi_steppe_large = 512
scn_ejan_arenka_yo = 513
scn_ejan_arenka_10 = 514
scn_ejan_arenka_4q = 515
scn_ejan_arenka_5q = 516
scn_ejan_arenka_6 = 517
scn_ejan_arenka_7a = 518
scn_ejan_arenka_8 = 519
scn_ejan_arenka_9 = 520
scn_ejan_arenka_11 = 521
scn_ejan_arenka_12 = 522
scn_ejan_arenka_13 = 523
scn_ejan_arenka_14 = 524
scn_multi_scene_custom_1 = 525
scn_multi_scene_custom_2 = 526
scn_multi_scene_custom_3 = 527
scn_multi_scene_custom_4 = 528
scn_multi_scene_custom_5 = 529
scn_multi_scene_custom_6 = 530
scn_multi_scene_custom_7 = 531
scn_multi_scene_custom_8 = 532
scn_multi_scene_custom_9 = 533
scn_multi_scene_custom_10 = 534
scn_multi_scene_custom_11 = 535
scn_multi_scene_custom_12 = 536
scn_multi_scene_custom_13 = 537
scn_multi_scene_custom_14 = 538
scn_multi_scene_custom_15 = 539
scn_multi_scene_custom_16 = 540
scn_multi_scene_custom_17 = 541
scn_multi_scene_custom_18 = 542
scn_multi_scene_custom_19 = 543
scn_multi_scene_custom_20 = 544
scn_multi_scene_enl_dijon = 545
scn_multi_scene_enl_sandiboush = 546
scn_multi_scene_enl_vendetta = 547
scn_multi_scene_enl_frostybattle = 548
scn_multi_scene_enl_reveranvillage = 549
scn_multi_scene_enl_snowyhamlet = 550
scn_training_grounds = 551
scn_ailando = 552
scn_amon_hen = 553
scn_atlantis = 554
scn_blackstone_coves = 555
scn_brytenwalda = 556
scn_castle_valina = 557
scn_cathedral_of_the_grey = 558
scn_cliffs = 559
scn_dale = 560
scn_echo_dock = 561
scn_fairking_farm = 562
scn_forest_of_judgement = 563
scn_gates_of_argonath = 564
scn_jakamet_castle = 565
scn_kroghs_mill = 566
scn_ku_moku = 567
scn_minas_tirith = 568
scn_multhorne = 569
scn_paths_of_the_dead = 570
scn_pelennor_fields = 571
scn_pentacsis = 572
scn_port_defence = 573
scn_rivacheg = 574
scn_shariz = 575
scn_siege_of_hornburg = 576
scn_snowy_valley = 577
scn_stanley_fortress = 578
scn_teppic_dios = 579
scn_the_bulwark = 580
scn_tomb_of_horrors = 581
scn_triax = 582
scn_urogash_morat = 583
scn_uxkhal = 584
scn_varikev = 585
scn_grassland_small = 586
scn_grassland_medium = 587
scn_grassland_large = 588
scn_desert_small = 589
scn_desert_medium = 590
scn_desert_large = 591
scn_snow_small = 592
scn_snow_medium = 593
scn_snow_large = 594
scn_steppe_small = 595
scn_steppe_medium = 596
scn_steppe_large = 597
scn_custom_map_1 = 598
scn_custom_map_2 = 599
scn_custom_map_3 = 600
scn_custom_map_4 = 601
scn_custom_map_5 = 602
scn_custom_map_6 = 603
scn_custom_map_7 = 604
scn_custom_map_8 = 605
scn_custom_map_9 = 606
scn_custom_map_10 = 607
scn_custom_map_11 = 608
scn_custom_map_12 = 609
scn_custom_map_13 = 610
scn_custom_map_14 = 611
scn_custom_map_15 = 612
scn_custom_map_16 = 613
scn_custom_map_17 = 614
scn_custom_map_18 = 615
scn_custom_map_19 = 616
scn_custom_map_20 = 617
scn_custom_map_21 = 618
scn_custom_map_22 = 619
scn_custom_map_23 = 620
scn_custom_map_24 = 621
scn_custom_map_25 = 622
scn_custom_map_26 = 623
scn_custom_map_27 = 624
scn_custom_map_28 = 625
scn_custom_map_29 = 626
scn_custom_map_30 = 627
scn_custom_map_31 = 628
scn_custom_map_32 = 629
scn_custom_map_33 = 630
scn_custom_map_34 = 631
scn_custom_map_35 = 632
scn_custom_map_36 = 633
scn_custom_map_37 = 634
scn_custom_map_38 = 635
scn_custom_map_39 = 636
scn_custom_map_40 = 637
scn_custom_map_41 = 638
scn_custom_map_42 = 639
scn_custom_map_43 = 640
scn_custom_map_44 = 641
scn_custom_map_45 = 642
scn_custom_map_46 = 643
scn_custom_map_47 = 644
scn_custom_map_48 = 645
scn_custom_map_49 = 646
scn_custom_map_50 = 647
scn_custom_map_51 = 648
scn_custom_map_52 = 649
scn_custom_map_53 = 650
scn_custom_map_54 = 651
scn_custom_map_55 = 652
scn_custom_map_56 = 653
scn_custom_map_57 = 654
scn_custom_map_58 = 655
scn_custom_map_59 = 656
scn_custom_map_60 = 657
scn_custom_map_61 = 658
scn_custom_map_62 = 659
scn_custom_map_63 = 660
scn_custom_map_64 = 661
scn_custom_map_65 = 662
scn_custom_map_66 = 663
scn_custom_map_67 = 664
scn_custom_map_68 = 665
scn_custom_map_69 = 666
scn_custom_map_70 = 667
scn_custom_map_71 = 668
scn_custom_map_72 = 669
scn_custom_map_73 = 670
scn_custom_map_74 = 671
scn_custom_map_75 = 672
scn_custom_map_76 = 673
scn_custom_map_77 = 674
scn_custom_map_78 = 675
scn_custom_map_79 = 676
scn_custom_map_80 = 677
scn_custom_map_81 = 678
scn_custom_map_82 = 679
scn_custom_map_83 = 680
scn_custom_map_84 = 681
scn_custom_map_85 = 682
scn_custom_map_86 = 683
scn_custom_map_87 = 684
scn_custom_map_88 = 685
scn_custom_map_89 = 686
scn_custom_map_90 = 687
scn_custom_map_91 = 688
scn_custom_map_92 = 689
scn_custom_map_93 = 690
scn_custom_map_94 = 691
scn_custom_map_95 = 692
scn_custom_map_96 = 693
scn_custom_map_97 = 694
scn_custom_map_98 = 695
scn_custom_map_99 = 696
scn_custom_map_100 = 697
scn_custom_map_101 = 698
scn_custom_map_102 = 699
scn_custom_map_103 = 700
scn_custom_map_104 = 701
scn_custom_map_105 = 702
scn_custom_map_106 = 703
scn_custom_map_107 = 704
scn_custom_map_108 = 705
scn_custom_map_109 = 706
scn_custom_map_110 = 707
scn_custom_map_111 = 708
scn_custom_map_112 = 709
scn_custom_map_113 = 710
scn_custom_map_114 = 711
scn_custom_map_115 = 712
scn_custom_map_116 = 713
scn_custom_map_117 = 714
scn_custom_map_118 = 715
scn_custom_map_119 = 716
scn_custom_map_120 = 717
scn_custom_map_121 = 718
scn_custom_map_122 = 719
scn_custom_map_123 = 720
scn_custom_map_124 = 721
scn_custom_map_125 = 722
scn_custom_map_126 = 723
scn_custom_map_127 = 724
scn_custom_map_128 = 725
scn_custom_map_129 = 726
scn_custom_map_130 = 727
scn_custom_map_131 = 728
scn_custom_map_132 = 729
scn_custom_map_133 = 730
scn_custom_map_134 = 731
scn_custom_map_135 = 732
scn_custom_map_136 = 733
scn_custom_map_137 = 734
scn_custom_map_138 = 735
scn_custom_map_139 = 736
scn_custom_map_140 = 737
scn_custom_map_141 = 738
scn_custom_map_142 = 739
scn_custom_map_143 = 740
scn_custom_map_144 = 741
scn_custom_map_145 = 742
scn_custom_map_146 = 743
scn_custom_map_147 = 744
scn_custom_map_148 = 745
scn_custom_map_149 = 746
scn_custom_map_150 = 747
scn_custom_map_151 = 748
scn_custom_map_152 = 749
scn_custom_map_153 = 750
scn_custom_map_154 = 751
scn_custom_map_155 = 752
scn_custom_map_156 = 753
scn_custom_map_157 = 754
scn_custom_map_158 = 755
scn_custom_map_159 = 756
scn_custom_map_160 = 757
scn_custom_map_161 = 758
scn_custom_map_162 = 759
scn_custom_map_163 = 760
scn_custom_map_164 = 761
scn_custom_map_165 = 762
scn_custom_map_166 = 763
scn_custom_map_167 = 764
scn_custom_map_168 = 765
scn_custom_map_169 = 766
scn_custom_map_170 = 767
scn_custom_map_171 = 768
scn_custom_map_172 = 769
scn_custom_map_173 = 770
scn_custom_map_174 = 771
scn_custom_map_175 = 772
scn_custom_map_176 = 773
scn_custom_map_177 = 774
scn_custom_map_178 = 775
scn_custom_map_179 = 776
scn_custom_map_180 = 777
scn_custom_map_181 = 778
scn_custom_map_182 = 779
scn_custom_map_183 = 780
scn_custom_map_184 = 781
scn_custom_map_185 = 782
scn_custom_map_186 = 783
scn_custom_map_187 = 784
scn_custom_map_188 = 785
scn_custom_map_189 = 786
scn_custom_map_190 = 787
scn_custom_map_191 = 788
scn_custom_map_192 = 789
scn_custom_map_193 = 790
scn_custom_map_194 = 791
scn_custom_map_195 = 792
scn_custom_map_196 = 793
scn_custom_map_197 = 794
scn_custom_map_198 = 795
scn_custom_map_199 = 796
scn_custom_map_200 = 797
scn_multiplayer_maps_end = 798
scn_lair_steppe_bandits = 799
scn_lair_taiga_bandits = 800
scn_lair_desert_bandits = 801
scn_lair_forest_bandits = 802
scn_lair_mountain_bandits = 803
scn_lair_sea_raiders = 804
scn_quick_battle_scene_1 = 805
scn_quick_battle_scene_2 = 806
scn_quick_battle_scene_3 = 807
scn_quick_battle_scene_4 = 808
scn_quick_battle_scene_5 = 809
scn_quick_battle_maps_end = 810
scn_tutorial_training_ground = 811
scn_town_1_room = 812
scn_town_5_room = 813
scn_town_6_room = 814
scn_town_8_room = 815
scn_town_10_room = 816
scn_town_19_room = 817
scn_meeting_scene_steppe = 818
scn_meeting_scene_plain = 819
scn_meeting_scene_snow = 820
scn_meeting_scene_desert = 821
scn_meeting_scene_steppe_forest = 822
scn_meeting_scene_plain_forest = 823
scn_meeting_scene_snow_forest = 824
scn_meeting_scene_desert_forest = 825
scn_wedding = 826
scn_enterprise_tannery = 827
scn_enterprise_winery = 828
scn_enterprise_mill = 829
scn_enterprise_smithy = 830
scn_enterprise_dyeworks = 831
scn_enterprise_linen_weavery = 832
scn_enterprise_wool_weavery = 833
scn_enterprise_brewery = 834
scn_enterprise_oil_press = 835
